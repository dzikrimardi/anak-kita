<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();

Route::get('/&&anakita-login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/&&anakita-login', 'Auth\LoginController@login');
Route::post('/&&anakita-logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/&&anakita-register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/&&anakita-register', 'Auth\RegisterController@register');

Route::get('/&&anakita-password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('/&&anakita-password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/&&anakita-password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('/&&anakita-password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');




Route::get('/home', 'HomeController@index')->name('home');
Route::resource('/data-anak', 'DataAnakController');
Route::resource('/data-visual', 'DataVisualController');
Route::resource('/riwayat-vaksinasi', 'VaksinasiController');
Route::resource('/riwayat-kesehatan', 'KesehatanController');
Route::resource('/yayasan', 'YayasanController');
Route::resource('/user', 'UsersController');
Route::resource('/daftar-penyakit', 'DaftarPenyakitController');
// Route::resource('/log', 'AuditController');
    
Route::get('/profile/{id}','YayasanController@profile')->name('profile');
Route::post('/profile/{id}','YayasanController@updateProfile')->name('updateProfile');
Route::post('/data-anak/cari','DataAnakController@cari')->name('cariAnak');
Route::get('/log','HomeController@getAudit');


// Route for Excel and PDF Report
Route::get('excel/vaksinasi/{id}','VaksinasiController@export')->name('export.vaksinasi');
Route::get('excel/kesehatan/{id}','KesehatanController@export')->name('export.kesehatan');
Route::get('excel/data-anak/{id}','DataAnakController@export')->name('export.data-anak');
Route::get('excel/yayasan/{id}','YayasanController@export')->name('export.data-yayasan');

Route::get('pdf/vaksinasi/{id}', 'VaksinasiController@pdf')->name('pdf.vaksinasi');
Route::get('pdf/kesehatan/{id}', 'KesehatanController@pdf')->name('pdf.kesehatan');

Route::get('pdf/data-anak/{id}', 'DataAnakController@pdfDataAnak')->name('pdf.data-anak');
Route::get('pdf/data-anak/vaksinasi/{id}', 'DataAnakController@pdfVaksinasi')->name('pdf.data-anak.vaksinasi');
Route::get('pdf/data-anak/kesehatan/{id}', 'DataAnakController@pdfKesehatan')->name('pdf.data-anak.kesehatan');

Route::get('pdf/yayasan/{id}', 'YayasanController@pdfDataYayasan')->name('pdf.data-yayasan');
Route::get('pdf/yayasan/data-anak/{id}', 'YayasanController@pdfDataAnak')->name('pdf.data-yayasan.data-anak');
Route::get('pdf/yayasan/vaksinasi/{id}', 'YayasanController@pdfVaksinasi')->name('pdf.data-yayasan.vaksinasi');
Route::get('pdf/yayasan/kesehatan/{id}', 'YayasanController@pdfKesehatan')->name('pdf.data-yayasan.kesehatan');
