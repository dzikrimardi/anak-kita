<?php

namespace App\Exports;

use App\Models\Vaksinasi;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class VaksinasiExport implements FromCollection, WithMapping, WithHeadings, WithEvents
{
    use Exportable;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function collection()
    {
        return Vaksinasi::where('id',$this->id)->get();
    }
    /**
     * @var Invoice $invoice
     */
    public function map($vaksinasi): array
    {
        return [
            $vaksinasi->dataAnak->nama_anak,
            $vaksinasi->dataYayasan->nama_yayasan,
            $vaksinasi->pemberi_vaksinasi,
            $vaksinasi->tempat_vaksinasi,
            $vaksinasi->alamat_tempat_vaksinasi,
            $vaksinasi->jenis_vaksinasi,
            $vaksinasi->vaksinasi_wajib,
            $vaksinasi->tgl_vaksinasi_wajib,
            $vaksinasi->vaksinasi_tambahan,
            $vaksinasi->tgl_vaksinasi_tambahan,
            $vaksinasi->catatan,
        ];
    }
    public function headings(): array
    {
        return [
            'Nama Anak',
            'Nama Yayasan',
            'Nama Pemberi Vaksinasi',
            'Tempat Vaksinasi',
            'Alamat',
            'Jenis Vaksinasi',
            'Vaksinasi Wajib',
            'Tanggal Vaksinasi Wajib',
            'Vaksinasi Tambahan',
            'Tanggal Vaksinasi Tambahan',
            'Catatan',
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];

        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) use ($styleArray){
                $event->sheet->getStyle('A1:K1')->applyFromArray($styleArray);
            },
        ];
    }
}
