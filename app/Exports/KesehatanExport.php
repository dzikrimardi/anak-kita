<?php

namespace App\Exports;

use App\Models\Kesehatan;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class KesehatanExport implements FromCollection, WithEvents, WithMapping, WithHeadings
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function collection()
    {
        return Kesehatan::where('id', $this->id)->get();
    }
    /**
     * @var Invoice $invoice
     */
    public function map($kesehatan): array
    {
        return [
            $kesehatan->dataYayasan->nama_yayasan,
            $kesehatan->dataAnak->nama_anak,
            $kesehatan->nama_penyakit,
            $kesehatan->gejala_penyakit,
            $kesehatan->catatan_dokter,
            $kesehatan->obat_resep,
            $kesehatan->terima_obat,
        ];
    }
    public function headings(): array
    {
        return [
            'Yayasan',
            'Anak',
            'Nama Penyakit',
            'Gejala Penyakit',
            'Catatan Dokter',
            'Obat Resep',
            'Terima Obat',
        ];
    }
    
    public function registerEvents(): array
    {
        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:K1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
            },
        ];
    }
}
