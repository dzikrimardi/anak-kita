<?php

namespace App\Exports;

use App\Models\DataAnak;
use App\Models\Kesehatan;
use App\Models\Vaksinasi;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class DataAnakExport implements FromView, WithEvents
{
    use Exportable;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('export.data-anak',[
            'anak' => DataAnak::where('id',$this->id)->get(),
            'kesehatan' => Kesehatan::where('id_anak',$this->id)->get(),
            'vaksinasi' => Vaksinasi::where('id_anak',$this->id)->get(),    
            
        ]);
    }
    public function registerEvents(): array
    {
        $styleArray = [
            'font' => [
                'bold' => true,
            ]
        ];

        return [
            // Handle by a closure.
            AfterSheet::class => function (AfterSheet $event) use ($styleArray) {
                $event->sheet->getStyle('A1:K1')->applyFromArray($styleArray);
            },
        ];
    }
}
