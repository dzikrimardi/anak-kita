<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kesehatan extends Model
{
    protected $fillable = [
        'id_yayasan',
        'id_anak',
        'nama_penyakit',
        'gejala_penyakit',
        'catatan_dokter',
        'tanggal_pengobatan',
        'obat_resep',
        'terima_obat',
    ];

    public function dataAnak()
    {
        return $this->belongsTo('App\Models\DataAnak','id_anak','id');
    }

    public function dataYayasan()
    {
        return $this->belongsTo('App\Models\Yayasan','id_yayasan','id');
    }
}
