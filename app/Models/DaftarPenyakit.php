<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaftarPenyakit extends Model
{
    protected $fillable = ['nama_penyakit','deskripsi'];
}
