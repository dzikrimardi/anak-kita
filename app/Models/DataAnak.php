<?php

namespace App\Models;

use OwenIt\Auditing\Contracts\Auditable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DataAnak extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    
    protected $guarded = ['_token'];
    
    protected $fillable = [
        'id',
        'id_yayasan',
        'nama_anak',
        'jenis_kelamin',
        'tempat_lahir',
        'kota_lahir',
        'tanggal_lahir',
        'disabilitas',
        'alergi',
        'alamat',
        'nama_ortu',
        'nik_ortu',
        'alamat_ortu',
        'no_hp_ortu',
        'berat_badan',
        'tinggi_badan',
        'massa_tubuh',
        'suhu_badan',
        'tekanan_darah',
        'gol_darah',
        'rhesus',
        'nik',
        'foto_akte_lahir',
        'foto_anak',
        'foto_kartu_keluarga',
        'foto_kartu_bpjs',
        'foto_ktp',
        'foto_rontgen',
        'tgl_berat_badan',
        'tgl_tinggi_badan',
        'tgl_massa_tubuh',
        'tgl_suhu_badan',
        'tgl_tekanan_darah',
        'catatan',
    ];

    public static function kode()
    {
        // $idanak = DB::table('data_anaks')->max('id');
        // $addNol = '';
        // $idanak = (INT) $idanak + 1 ;
        // $increamentid = $idanak;

        // if (strlen($idanak) == 1) {
        //     $addNol = "000";
        // }elseif (strlen($idanak) == 2) {
        //     $addNol = "00";
        // }elseif (strlen($idanak) == 3) {
        //     $addNol = "0";
        // }

        // $idbaru = "KDA".$addNol.$increamentid;
        // return $idbaru ;
    }

    public function vaksinasi()
    {
        return $this->hasMany('App\Models\Vaksinasi','id_anak','id');
    }

    public function kesehatan()
    {
        return $this->hasMany('App\Models\Kesehatan', 'id_anak', 'id');
    }
    public function yayasan()
    {
        return $this->belongsTo('App\Models\Yayasan','id_yayasan','id');
    }

    // function for onDelete Cascade
    public static function boot()
    {
        parent::boot();

        static::deleted(function ($id) {
            $id->vaksinasi()->delete();
            $id->kesehatan()->delete();
        });
    }
}
