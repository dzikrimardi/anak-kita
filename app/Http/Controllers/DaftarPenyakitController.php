<?php

namespace App\Http\Controllers;

use App\Models\DaftarPenyakit;
use Illuminate\Http\Request;

class DaftarPenyakitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['penyakit'] = DaftarPenyakit::all();
        return view('backend.daftar-penyakit.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.daftar-penyakit.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input['nama_penyakit'] = $request->nama_penyakit;
        $input['deskripsi'] = $request->deskripsi;
        DaftarPenyakit::create($input);

        return redirect()->route('daftar-penyakit.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['penyakit'] = DaftarPenyakit::findOrFail($id);
        return view('backend.daftar-penyakit.edit', $data);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DaftarPenyakit::find($id);
        $data->nama_penyakit = $request->nama_penyakit;
        $data->deskripsi = $request->deskripsi;
        $data->update();
        return redirect()->route('daftar-penyakit.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DaftarPenyakit::destroy($id);
        return redirect()->route('daftar-penyakit.index');
    }
}
