<?php

namespace App\Http\Controllers;

use Charts;
use App\User;
use App\Models\DataAnak;
use App\Models\Yayasan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::user()->role == "superadmin") {
            $anakL = DataAnak::where('jenis_kelamin','Laki-Laki')->count();
            $anakP = DataAnak::where('jenis_kelamin','Perempuan')->count();
            $count['anaks'] = DataAnak::count();
            $count['users'] = User::where('role', 'admin')->count();
            $count['yayasans'] = Yayasan::count();
        }else{
            $anakL = DataAnak::where([
                ['id_yayasan', '=', Auth::user()->id_yayasan],
                ['jenis_kelamin', '=', 'Laki-Laki']
                ])->count();
            $anakP = DataAnak::where([
                ['id_yayasan', '=', Auth::user()->id_yayasan], 
                ['jenis_kelamin', '=', 'Perempuan']
                ])->count();
            $count['anaks'] = DataAnak::where('id_yayasan', Auth::user()->id_yayasan)->count();
            $count['users'] = User::where('id_yayasan', Auth::user()->id_yayasan)->count();
            $count['yayasans'] = Yayasan::count();
        }
        $anak = [$anakL, $anakP];


        $data['pie'] = Charts::create('pie', 'highcharts')
                            ->title('Jumlah Anak')
                            ->labels(['Laki-Laki', 'Perempuan'])
                            ->values($anak)
                            ->dimensions(500, 250)
                            ->responsive(false);

        return view('backend.dashboard',$count,$data);
    }

    public function getAudit()
    {
        $data['audits'] = DataAnak::find(1)->audits;
        return view('log',$data);
    }
}
