<?php

namespace App\Http\Controllers;

use App\Exports\KesehatanExport;
use App\Models\DaftarPenyakit;
use Illuminate\Http\Request;
use App\Models\Kesehatan;
use App\Models\Yayasan;
use App\Models\DataAnak;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class KesehatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == "superadmin") {
            $data['datas'] = Kesehatan::all();
        } else {
            $data['datas'] = Kesehatan::where('id_yayasan', Auth::user()->id_yayasan)->get();
        }
        return view('backend.riwayat-kesehatan.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role == "superadmin") {
            $data['datas'] = DataAnak::all();
            $data['yayasan'] = Yayasan::all();
            $data['penyakit'] = DaftarPenyakit::all();
        } else {
            $data['datas'] = DataAnak::where('id_yayasan', Auth::user()->id_yayasan)->get();
            $data['yayasan'] = Yayasan::where('id', Auth::user()->id_yayasan)->get();
            $data['penyakit'] = DaftarPenyakit::all();
        }

        return view('backend.riwayat-kesehatan.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $nama_penyakit = implode(', ', $request->nama_penyakit);

        $input['id_anak'] = $request->id_anak;
        $input['id_yayasan'] = $request->id_yayasan;
        $input['nama_penyakit'] = $nama_penyakit;
        $input['gejala_penyakit'] = $request->gejala_penyakit;
        $input['catatan_dokter'] = $request->catatan_dokter;
        $input['tanggal_pengobatan'] = $request->tanggal_pengobatan;
        $input['obat_resep'] = $request->obat_resep;
        $input['terima_obat'] = $request->terima_obat;

        Kesehatan::create($input);
        return redirect()->route('riwayat-kesehatan.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['kesehatan'] = Kesehatan::findOrFail($id);
        return view('backend.riwayat-kesehatan.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role == "superadmin") {
            $data['kesehatan'] = Kesehatan::findOrFail($id);
            $data['yayasan'] = Yayasan::all();
            $data['datas'] = DataAnak::all();
            $data['penyakit'] = DaftarPenyakit::all();
        } else {
            $data['kesehatan'] = Kesehatan::findOrFail($id);
            $data['penyakit'] = DaftarPenyakit::all();
            $data['datas'] = DataAnak::where('id_yayasan', Auth::user()->id_yayasan)->get();
        }
        return view('backend.riwayat-kesehatan.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Kesehatan::find($id);
        if ($request->id_anak == "") {
            $data->id_anak = $data->id_anak;
        } else {
            $data->id_anak = $request->id_anak;
        }

        if ($request->id_yayasan == "") {
            $data->id_yayasan = $data->id_yayasan;
        } else {
            $data->id_yayasan = $request->id_yayasan;
        }

        $nama_penyakit = implode(', ', $request->nama_penyakit);

        $data->nama_penyakit = $nama_penyakit;
        $data->gejala_penyakit = $request->gejala_penyakit;
        $data->catatan_dokter = $request->catatan_dokter;
        $data->tanggal_pengobatan = $request->tanggal_pengobatan;
        $data->obat_resep = $request->obat_resep;
        $data->terima_obat = $request->terima_obat;
        $data->update();

        return redirect()->route('riwayat-kesehatan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Kesehatan::findOrFail($id);
        $data->delete();

        return redirect()->route('riwayat-kesehatan.index');
    }

    // For Expor Excel where ID selected
    public function export($id)
    {
        return Excel::download(new KesehatanExport($id), 'kesehatan.csv');
    }

    public function pdf($id)
    {
        $kesehatan = Kesehatan::where('id', $id)->get();
        $pdf = PDF::loadView('pdf.kesehatan', ['kesehatan' => $kesehatan]);
        return $pdf->download('data-kesehatan.pdf');
    }   
}
