<?php

namespace App\Http\Controllers;

use PDF;
use App\Exports\VaksinasiExport;
use App\Models\Yayasan;
use App\Models\Vaksinasi;
use App\Models\DataAnak;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class VaksinasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == "superadmin") {
            $data['datas'] = Vaksinasi::all();
        } else {
            $data['datas'] = Vaksinasi::where('id_yayasan',Auth::user()->id_yayasan)->get();
        }

        return view('backend.riwayat-vaksinasi.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->role == "superadmin") {
            $data['datas'] = DataAnak::all();
            $data['yayasan'] = Yayasan::all();
        }else {
            $data['datas'] = DataAnak::where('id_yayasan',Auth::user()->id_yayasan)->get();
            $data['yayasan'] = Yayasan::where('id',Auth::user()->id_yayasan)->get();
        }

        return view('backend.riwayat-vaksinasi.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input['id_anak'] = $request->id_anak;
        $input['id_yayasan'] = $request->id_yayasan;
        $input['pemberi_vaksinasi'] = $request->pemberi_vaksinasi;
        $input['tempat_vaksinasi'] = $request->tempat_vaksinasi;
        $input['alamat_tempat_vaksinasi'] = $request->alamat_tempat_vaksinasi;
        $input['jenis_vaksinasi'] = $request->jenis_vaksinasi;
        $input['vaksinasi_wajib'] = $request->vaksinasi_wajib;
        $input['tgl_vaksinasi_wajib'] = $request->tgl_vaksinasi_wajib;
        $input['vaksinasi_tambahan'] = $request->vaksinasi_tambahan;
        $input['tgl_vaksinasi_tambahan'] = $request->tgl_vaksinasi_tambahan;
        $input['catatan'] = $request->catatan;

        Vaksinasi::create($input);
        return redirect()->route('riwayat-vaksinasi.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['vaksinasi'] = Vaksinasi::findOrFail($id);
        return view('backend.riwayat-vaksinasi.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->role == "superadmin") {
            $data['vaksinasi'] = Vaksinasi::findOrFail($id);
            $data['datas'] = DataAnak::all();
            $data['yayasan'] = Yayasan::all();
        } else {
            $data['vaksinasi'] = Vaksinasi::findOrFail($id);
            $data['datas'] = DataAnak::where('id_yayasan', Auth::user()->id_yayasan)->get();
        }
        return view('backend.riwayat-vaksinasi.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = Vaksinasi::find($id);
        if ($request->id_anak == "") {
            $data->id_anak = $data->id_anak;
        } else {
            $data->id_anak = $request->id_anak;
        }

        if ($request->id_yayasan == "") {
            $data->id_yayasan = $data->id_yayasan;
        } else {
            $data->id_yayasan = $request->id_yayasan;
        }

        if ($request->vaksinasi_wajib == "") {
            $data->vaksinasi_wajib = $data->vaksinasi_wajib;
        }else{
            $data->vaksinasi_wajib = $request->vaksinasi_wajib;
        }

        if ($request->vaksinasi_tambahan == "") {
            $data->vaksinasi_tambahan = $data->vaksinasi_tambahan;
        } else {
            $data->vaksinasi_tambahan = $request->vaksinasi_tambahan;
        }

        $data->pemberi_vaksinasi = $request->pemberi_vaksinasi;
        $data->jenis_vaksinasi = $request->jenis_vaksinasi;
        $data->tempat_vaksinasi = $request->tempat_vaksinasi;
        $data->alamat_tempat_vaksinasi = $request->alamat_tempat_vaksinasi;
        $data->tgl_vaksinasi_wajib = $request->tgl_vaksinasi_wajib;
        $data->tgl_vaksinasi_tambahan = $request->tgl_vaksinasi_tambahan;
        $data->catatan = $request->catatan;
        $data->update();

        return redirect()->route('riwayat-vaksinasi.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Vaksinasi::findOrFail($id);
        $data->delete();

        return redirect()->route('riwayat-vaksinasi.index');
    }

    // For Expor Excel where ID selected
    public function export($id)
    {
        return Excel::download(new VaksinasiExport($id), 'vaksinasi.csv');
    }

    // For Export PDF where ID selected
    public function pdf($id)
    {
        $vaksinasi = Vaksinasi::where('id',$id)->get();

        $pdf = PDF::loadView('pdf.vaksinasi', ['vaksinasi' => $vaksinasi]);
        return $pdf->download('data-vaksinasi.pdf');

    }
}
