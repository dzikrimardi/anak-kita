<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYayasansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yayasans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_yayasan')->nullable()->default(null);
            $table->string('foto_yayasan')->nullable()->default(null);
            $table->text('alamat')->nullable()->default(null);
            $table->string('no_telp')->nullable()->default(null);
            $table->string('tgl_berdiri')->nullable()->default(null);
            $table->text('visi')->nullable()->default(null);
            $table->text('misi')->nullable()->default(null);
            $table->text('tentang_yayasan')->nullable()->default(null);
            $table->string('nama_pengurus')->nullable()->default(null);
            $table->string('no_telp_pengurus')->nullable()->default(null);
            $table->text('alamat_pengurus')->nullable()->default(null);
            $table->string('jenis_kelamin_pengurus')->nullable()->default(null);
            $table->string('foto_ktp_pengurus')->nullable()->default(null);
            $table->timestamps();
        });

        DB::table('yayasans')->insert([
            'nama_yayasan' => 'Yayasan Lipotek',
            'foto_yayasan' => null,
            'alamat' => 'Jln Widyodiningrat RT 09/RW 14 no 51',
            'no_telp' => '08977148184',
            'tgl_berdiri' => date('Y-m-d'),
            'visi' => 'Mewujudkan Lembaga Pendidikan yang Berkarakter, Unggul dan Berprestasi',
            'misi' => 'Menghasilkan sumber daya manusia yang dapat menjadi aset Bangsa yang berahlak, berbudi pekerti, berwawasan dan berkemampuan untuk mengembangkan dirinya secara berkelanjutan',
            'tentang_yayasan' => 'Yayasan yang berjalan di Bidang Pendidikan, membuat anak berprestasi dan tangguh',
        ]);

        // DB::table('yayasans')->insert([
        //     'nama_yayasan' => 'Yayasan Yatim',
        //     'foto_yayasan' => null,
        //     'alamat' => 'Jln Jendral Sudirman RT 09/RW 14 no 51',
        //     'no_telp' => '089999387',
        //     'tgl_berdiri' => date('Y-m-d'),
        //     'visi' => 'Mewujudkan Lembaga Pendidikan yang Berkarakter, Unggul dan Berprestasi',
        //     'misi' => 'Menghasilkan sumber daya manusia yang dapat menjadi aset Bangsa yang berahlak, berbudi pekerti, berwawasan dan berkemampuan untuk mengembangkan dirinya secara berkelanjutan',
        //     'tentang_yayasan' => 'Yayasan yang berjalan di Bidang Pendidikan, membuat anak berprestasi dan tangguh',
        // ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yayasans');
    }
}
