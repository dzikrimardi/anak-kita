<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVaksinasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaksinasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_anak')->nullable()->default(null)->unsigned();
            $table->integer('id_yayasan')->nullable()->default(null)->unsigned();
            $table->string('pemberi_vaksinasi')->nullable()->default(null);
            $table->string('tempat_vaksinasi')->nullable()->default(null);
            $table->text('alamat_tempat_vaksinasi')->nullable()->default(null);
            $table->string('jenis_vaksinasi')->nullable()->default(null);
            $table->string('vaksinasi_wajib')->nullable()->default(null);
            $table->string('vaksinasi_tambahan')->nullable()->default(null);
            $table->string('tgl_vaksinasi_wajib')->nullable()->default(null);
            $table->string('tgl_vaksinasi_tambahan')->nullable()->default(null);
            $table->text('catatan')->nullable()->default(null);
            $table->timestamps();

            
        });


        // DB::table('vaksinasis')->insert([
        //     'id_yayasan' => '1',
        //     'id_anak' => '1',
        //     'pemberi' => 'dokter anak',
        //     'wajib_tambahan' => 'Wajib',
        //     'imunisasi_wajib' => 'Polio',
        //     'imunisasi_tambahan' => 'Varisela',
        //     'tgl_pertama' => date('m/d/Y'),
        //     'tgl_kedua' => null,
        //     'tgl_ketiga' => null,
        //     'tgl_keempat' => null,
        //     'tgl_kelima' => null,
        //     'tgl_keenam' => null,
        //     'tgl_ketujuh' => null,
        //     'tgl_kedelapan' => null,
        //     'catatan' => null,
        // ]);

        // DB::table('vaksinasis')->insert([
        //     'id_yayasan' => '2',
        //     'id_anak' => '2',
        //     'pemberi' => 'Dokter Kulit',
        //     'wajib_tambahan' => 'Wajib',
        //     'imunisasi_wajib' => 'Polio',
        //     'imunisasi_tambahan' => 'Varisela',
        //     'tgl_pertama' => date('m/d/Y'),
        //     'tgl_kedua' => null,
        //     'tgl_ketiga' => null,
        //     'tgl_keempat' => null,
        //     'tgl_kelima' => null,
        //     'tgl_keenam' => null,
        //     'tgl_ketujuh' => null,
        //     'tgl_kedelapan' => null,
        //     'catatan' => null,
        // ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaksinasis');
    }
}
