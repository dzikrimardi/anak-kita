<?php

use App\Models\Yayasan;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($x=1; $x <=4; $x++) { 
            DB::table('users')->insert([
                'id_yayasan' => Yayasan::all()->random()->id,
                'name' => $faker->name,
                'role' => 'admin',
                'email' => $faker->safeEmail,
                'password' => bcrypt('admin123'),
            ]);
        }
    }
}
