@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data Visual</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Data Visual</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-0">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>ID Anak</th>
                                        <th>Nama</th>
                                        <th>Foto KTP</th>
                                        <th>Foto Akte Lahir</th>
                                        <th>Foto Kartu Keluarga</th>
                                        <th>Foto Kartu BPJS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datas as $data)
                                    <tr>
                                        <td>{{ $data->kode_anak }}</td>
                                        <td>{{ $data->nama_anak }}</td>
                                        <td>
                                            @if($data->foto_ktp)
                                            <img src="{{asset('images/'.$data->foto_ktp)}}" alt="logo" height="50" width="50" style="display:block;margin-left:auto;margin-right:auto;">
                                            @else
                                            <p>Tidak ada gambar</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($data->foto_akte_lahir)
                                            <img src="{{asset('images/'.$data->foto_akte_lahir)}}" alt="logo" height="50" width="50" style="display:block;margin-left:auto;margin-right:auto;">
                                            @else
                                            <p>Tidak ada gambar</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($data->foto_kartu_keluarga)
                                            <img src="{{asset('images/'.$data->foto_kartu_keluarga)}}" alt="logo" height="50" width="50" style="display:block;margin-left:auto;margin-right:auto;">
                                            @else
                                            <p>Tidak ada gambar</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($data->foto_kartu_bpjs)
                                            <img src="{{asset('images/'.$data->foto_kartu_bpjs)}}" alt="logo" height="50" width="50" style="display:block;margin-left:auto;margin-right:auto;">
                                            @else
                                            <p>Tidak ada gambar</p>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection