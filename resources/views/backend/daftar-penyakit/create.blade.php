@extends('backend.layouts.app')

@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">User</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                <li class="breadcrumb-item active">User</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <!-- /# row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-title">
                        <h4>Tambah Penyakit</h4>

                    </div>
                    <div class="card-body">
                        <div class="basic-elements">
                            <form action="{{ route('daftar-penyakit.store') }}" enctype="multipart/form-data" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Nama<span style="color:red;"> *</span></label>
                                            <input type="text" name="nama_penyakit" class="form-control" value="{{ old('nama_penyakit') }}" placeholder="Nama Penyakit" required>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('nama_penyakit'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_penyakit')}}
                                                @endif
                                            </p>
                                        </div>
                                        <label>Deskripsi<span style="color:red;"> *</span></label>
                                        <div class="form-group">
                                            <textarea style="border-color:lightgrey;" name="deskripsi" cols="48" rows="5" required></textarea>
                                            <p style="color:red;font-size:13px;">
                                                @if($errors->has('deskripsi'))
                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('deskripsi')}}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-buttons">
                                    <div class="sweetalert m-t-15">
                                        <button class="btn btn-info btn sweet-success" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection