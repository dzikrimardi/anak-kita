 @extends('backend.layouts.app')

 @section('content')
 <div class="page-wrapper">
     <!-- Bread crumb -->
     <div class="row page-titles">
         <div class="col-md-5 align-self-center">
             <h3 class="text-primary">Data Anak</h3>

         </div>
         <div class="col-md-7 align-self-center">
             <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                 <li class="breadcrumb-item active">Data Anak</li>
             </ol>
         </div>
     </div>
     <div class="container-fluid">
         <div class="card pink">
             <div class="card-body p-b-0">
                 <div class="card-title">
                     <h2 class="pink-name">
                         Data Anak
                         <a href="{{ route('export.data-anak',$anak->id) }}" class="btn float-right" name="button" style="margin-left:1%;background-color:white;color:#e9748a">Excel</a>
                         <a href="{{ route('pdf.data-anak.vaksinasi',$anak->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Vaksinasi</a>
                         <a href="{{ route('pdf.data-anak.kesehatan',$anak->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Kesehatan</a>
                         <a href="{{ route('pdf.data-anak',$anak->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Data Anak</a>
                     </h2>
                 </div>
                 <!-- Nav tabs -->
                 <ul class="nav nav-tabs customtab" role="tablist">
                     <li class="nav-item"> <a class="nav-link pink-name active" data-toggle="tab" href="#data-anak" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Biodata Anak</span></a> </li>
                     <li class="nav-item"> <a class="nav-link pink-name" data-toggle="tab" href="#data-fisik" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Data Fisik</span></a> </li>
                     <li class="nav-item"> <a class="nav-link pink-name" data-toggle="tab" href="#kesehatan" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Riwayat Kesehatan</span></a> </li>
                     <li class="nav-item"> <a class="nav-link pink-name" data-toggle="tab" href="#vaksinasi" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Riwayat Vaksinasi</span></a> </li>
                     <li class="nav-item"> <a class="nav-link pink-name" data-toggle="tab" href="#data-document" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Data Document</span></a> </li>
                 </ul>
                 <!-- Tab panes -->
                 <div class="tab-content">
                     <div class="tab-pane active" id="data-anak" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;"></label><br>
                                                         @if($anak->foto_anak)
                                                         <img src="{{asset('images/'.$anak->foto_anak)}}" alt="logo" height="200" width="250">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Nama:</label><br>
                                                         <h4>{{ $anak->nama_anak }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Tempat/Tanggal Lahir:</label><br>
                                                         <h4>{{ $anak->kota_lahir }},&nbsp;{{ $anak->tanggal_lahir }}</h4>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Yayasan:</label><br>
                                                         <h4>{{ $anak->yayasan->nama_yayasan }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">ID Anak:</label><br>
                                                         <h4>{{ $anak->kode_anak }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">NIK:</label><br>
                                                         <h4>{{ $anak->nik }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Jenis Kelamin:</label><br>
                                                         <h4>{{ $anak->jenis_kelamin }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Alamat Lahir:</label><br>
                                                         <h4>{{ $anak->alamat }}, {{ $anak->tempat_lahir }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <h2>&nbsp;</h2>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr class="style">
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2 style="text-decoration:underline;">Data ortu/wali</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">NIK ortu/wali:</label><br>
                                                         <h4>{{ $anak->nik_ortu }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Nama ortu/wali:</label><br>
                                                         <h4>{{ $anak->nama_ortu }}</h4>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2>&nbsp;</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">No HP ortu/wali:</label><br>
                                                         <h4>{{ $anak->no_hp_ortu }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Alamat ortu/wali:</label><br>
                                                         <h4>{{ $anak->alamat_ortu }}</h4>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr class="style">
                                                 </div>
                                             </div>
                                             <div class="dt-buttons">
                                                 <div class="sweetalert m-t-15">
                                                     <a href="{{ route('data-anak.edit', $anak->id) }}" class="btn btn-warning btn-md m-b-10 m-l-5" name="button">Edit</a>
                                                     <a href="{{ route('data-anak.index') }}" class="btn btn-primary btn-md m-b-10 m-l-5" name="button">Back</a>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div class="tab-pane" id="data-fisik" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Golongan Darah:</label><br>
                                                         <h4 style="text-transform:uppercase;">{{ $anak->gol_darah }}{{ $anak->rhesus }} </h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Suhu Badan:</label><br>
                                                         <h4>{{ $anak->suhu_badan }}&deg;C <span style="font-size:10px;color:#ff0000;">(Terakhir di periksa: {{ $anak->tgl_suhu_badan }})</span> </h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Tekanan Darah:</label><br>
                                                         <h4>{{ $anak->tekanan_darah }} mm/Hg <span style="font-size:10px;color:#ff0000;">(Terakhir di periksa: {{ $anak->tgl_tekanan_darah }})</span></h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Berat Badan:</label><br>
                                                         <h4>{{ $anak->berat_badan }} Kg <span style="font-size:10px;color:#ff0000;">(Terakhir di periksa: {{ $anak->tgl_berat_badan }})</span></h4>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Tinggi Badan:</label><br>
                                                         <h4>{{ $anak->tinggi_badan }} Cm <span style="font-size:10px;color:#ff0000;">(Terakhir di periksa: {{ $anak->tgl_tinggi_badan }})</span></h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Indeks Massa Tubuh:</label><br>
                                                         <h4>{{ $anak->massa_tubuh }} Kg/m2 <span style="font-size:10px;color:#ff0000;">(Terakhir di periksa: {{ $anak->tgl_massa_tubuh }})</span></h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Penyandang Disabilitas:</label><br>
                                                         <h4>{{ $anak->disabilitas }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Alergi:</label><br>
                                                         <h4>{{ $anak->alergi }}</h4>
                                                     </div>

                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Catatan/Saran:</label><br>
                                                         <h4>{{ $anak->catatan }}</h4>
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div class="tab-pane" id="kesehatan" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         @if($kesehatan == null)
                                         <p>Tidak ada Riwayat Kesehatan</p>
                                         @else
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Penyakit:</label><br>
                                                         @foreach($kesehatan as $data)
                                                         <h4>{{ $data->nama_penyakit }}</h4>
                                                         @endforeach
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Tanggal Pengobatan:</label><br>
                                                         @foreach($kesehatan as $data)
                                                         <h4>{{ $data->tanggal_pengobatan }}</h4>
                                                         @endforeach
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Gelaja Penyakit:</label><br>
                                                         @foreach($kesehatan as $data)
                                                         <h4>{{ $data->gejala_penyakit }}</h4>
                                                         @endforeach
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Obat/Resep Dokter:</label><br>
                                                         @foreach($kesehatan as $data)
                                                         <h4>{{ $data->obat_resep }}</h4>
                                                         @endforeach
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Catatan Dokter:</label><br>
                                                         @foreach($kesehatan as $data)
                                                         <h4>{{ $data->catatan_dokter }}</h4>
                                                         @endforeach
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Terima Obat :</label><br>
                                                         @foreach($kesehatan as $data)
                                                         <h4>{{ $data->terima_obat }}</h4>
                                                         @endforeach
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                         @endif
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div class="tab-pane" id="vaksinasi" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         @if($vaksinasi == null)
                                         <p>Tidak ada Riwayat Vaksinasi</p>
                                         @else
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Tempat Vaksinasi:</label><br>

                                                         @foreach($vaksinasi as $data)
                                                         <h4>- {{ $data->tempat_vaksinasi }}</h4>
                                                         <hr class="style">
                                                         @endforeach
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Pemberi Imunisasi:</label><br>
                                                         @foreach($vaksinasi as $data)
                                                         <h4>- {{ $data->pemberi_vaksinasi }}</h4>
                                                         <hr class="style">
                                                         @endforeach
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Alamat Tempat Vaksinasi:</label><br>

                                                         @foreach($vaksinasi as $data)
                                                         <h4>- {{ $data->alamat_tempat_vaksinasi }}</h4>
                                                         <hr class="style">
                                                         @endforeach
                                                     </div>

                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2 style="text-decoration:underline;">Jenis Vaksinasi</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Vaksinasi Wajib:</label><br>
                                                         @foreach($vaksinasi as $data)
                                                         @if($data->vaksinasi_tambahan == "")
                                                         <h4>- {{ $data->vaksinasi_wajib }}</h4>
                                                         <hr class="style">
                                                         @endif
                                                         @endforeach
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2>&nbsp;</h2>
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Tgl Pemberian Vaksinasi Wajib:</label><br>
                                                         @foreach($vaksinasi as $data)
                                                         @if($data->tgl_vaksinasi_tambahan == "")
                                                         <h4>- {{ $data->tgl_vaksinasi_wajib }}</h4>
                                                         <hr class="style">
                                                         @endif
                                                         @endforeach
                                                     </div>

                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2>&nbsp;</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Vaksinasi Tambahan:</label><br>
                                                         @foreach($vaksinasi as $data)
                                                         @if($data->vaksinasi_wajib == "")
                                                         <h4>- {{ $data->vaksinasi_tambahan }}</h4>
                                                         <hr class="style">
                                                         @endif
                                                         @endforeach
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Catatan:</label><br>
                                                         @foreach($vaksinasi as $data)
                                                         @if($data->catatan)
                                                         <h4>- {{ $data->catatan }}</h4>
                                                         <hr class="style">
                                                         @endif
                                                         @endforeach
                                                     </div>
                                                 </div>

                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2>&nbsp;</h2>
                                                     </div>
                                                     <div class="form-group" style="margin-bottom:10%;">
                                                         <label style="font-weight:bold;">Tgl Pemberian Vaksinasi Tambahan:</label><br>
                                                         @foreach($vaksinasi as $data)
                                                         @if($data->tgl_vaksinasi_wajib == "")
                                                         <h4>- {{ $data->tgl_vaksinasi_tambahan }}</h4>
                                                         <hr class="style">
                                                         @endif
                                                         @endforeach
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                         @endif
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div class="tab-pane" id="data-document" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Akte Kelahiran:</label><br>
                                                         @if($anak->foto_akte_lahir)
                                                         <img src="{{asset('images/'.$anak->foto_akte_lahir)}}" alt="logo" height="200" width="200" style="display:block;margin-right:auto;">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Kartu Keluarga:</label><br>
                                                         @if($anak->foto_kartu_keluarga)
                                                         <img src="{{asset('images/'.$anak->foto_kartu_keluarga)}}" alt="logo" height="200" width="200" style="display:block;margin-right:auto;">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">KTP/KIA:</label><br>
                                                         @if($anak->foto_ktp)
                                                         <img src="{{asset('images/'.$anak->foto_ktp)}}" alt="logo" height="200" width="200" style="display:block;margin-right:auto;">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Kartu BPJS:</label><br>
                                                         @if($anak->foto_kartu_bpjs)
                                                         <img src="{{asset('images/'.$anak->foto_kartu_bpjs)}}" alt="logo" height="200" width="200" style="display:block;margin-right:auto;">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Foto Rontgen:</label><br>
                                                         @if($anak->foto_rontgen)
                                                         <img src="{{asset('images/'.$anak->foto_rontgen)}}" alt="logo" height="200" width="200" style="display:block;margin-right:auto;">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>

     </div>

 </div>
 @endsection