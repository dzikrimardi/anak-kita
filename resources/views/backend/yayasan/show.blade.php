 @extends('backend.layouts.app')

 @section('content')
 <div class="page-wrapper">
     <!-- Bread crumb -->
     <div class="row page-titles">
         <div class="col-md-5 align-self-center">
             <h3 class="text-primary">Data Yayasan</h3>
         </div>
         <div class="col-md-7 align-self-center">
             <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="javascript:void(0)">Data Yayasan</a></li>
                 <li class="breadcrumb-item active">Show</li>
             </ol>
         </div>
     </div>
     <!-- End Bread crumb -->
     <!-- Container fluid  -->
     <div class="container-fluid">
         <div class="card pink">
             <div class="card-body p-b-0">
                 <div class="card-title">
                     <h2 class="pink-name">
                         Data Yayasan
                         <a href="{{ route('export.data-yayasan',$yayasan->id) }}" class="btn float-right" name="button" style="margin-left:1%;background-color:white;color:#e9748a;">Excel</a>
                         <a href="{{ route('pdf.data-yayasan',$yayasan->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Yayasan</a>
                         <a href="{{ route('pdf.data-yayasan.vaksinasi',$yayasan->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Vaksinasi</a>
                         <a href="{{ route('pdf.data-yayasan.kesehatan',$yayasan->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Kesehatan</a>
                         <a href="{{ route('pdf.data-yayasan.data-anak',$yayasan->id) }}" class="btn float-right btn-xs" name="button" style="margin-left:1%;background-color:white;color:#e9748a">PDF <br> Data Anak</a>
                     </h2>
                 </div>
                 <!-- Nav tabs -->
                 <ul class="nav nav-tabs customtab" role="tablist">
                     <li class="nav-item"> <a class="nav-link pink-name active" data-toggle="tab" href="#yayasan" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Profile Yayasan</span></a> </li>
                     <li class="nav-item"> <a class="nav-link pink-name" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Profile Pengurus Yayasan</span></a> </li>
                 </ul>
                 <!-- Tab panes -->
                 <div class="tab-content">
                     <div class="tab-pane active" id="yayasan" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Nama Yayasan:</label><br>
                                                         <h4>{{ $yayasan->nama_yayasan }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Tanggal Berdiri Yayasan:</label><br>
                                                         <h4>{{ $yayasan->tgl_berdiri }}</h4>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">No Telpon:</label><br>
                                                         <h4>{{ $yayasan->no_telp }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Alamat:</label><br>
                                                         <h4>{{ $yayasan->alamat }}</h4>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr class="style">
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2 style="text-decoration:underline;">Foto dan Tentang Yayasan</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Foto Yayasan:</label><br>
                                                         @if($yayasan->foto_yayasan)
                                                         <img src="{{asset('images/'.$yayasan->foto_yayasan)}}" alt="logo" height="150" width="200">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Tentang Yayasan:</label><br>
                                                         <h4>{!! $yayasan->tentang_yayasan !!}</h4>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr class="style">
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2 style="text-decoration:underline;">Visi dan Misi</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">VISI:</label><br>
                                                         <h4>{!! $yayasan->visi !!}</h4>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2>&nbsp;</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">MISI:</label><br>
                                                         <h4>{!! $yayasan->misi !!}</h4>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="dt-buttons">
                                                 <div class="sweetalert m-t-15">
                                                     <a href="{{ route('yayasan.index') }}" class="btn btn-primary btn-md m-b-10 m-l-5" name="button">Back</a>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>

                     <div class="tab-pane" id="profile" role="tabpanel">
                         <div class="row">
                             <div class="col-lg-12">
                                 <div class="card">
                                     <div class="card-body">
                                         <div class="basic-elements">
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Nama Pengurus:</label><br>
                                                         <h4>{{ $yayasan->nama_pengurus }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Jenis Kelamin:</label><br>
                                                         <h4>{{ $yayasan->jenis_kelamin_pengurus }}</h4>
                                                     </div>
                                                 </div>
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">No Telpon:</label><br>
                                                         <h4>{{ $yayasan->no_telp_pengurus }}</h4>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Alamat:</label><br>
                                                         <h4>{{ $yayasan->alamat_pengurus }}</h4>
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr class="style">
                                                 </div>
                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-6">
                                                     <div class="form-group">
                                                         <h2 style="text-decoration:underline;">Foto</h2>
                                                     </div>
                                                     <div class="form-group">
                                                         <label style="font-weight:bold;">Foto KTP Pengurus:</label><br>
                                                         @if($yayasan->foto_ktp_pengurus)
                                                         <img src="{{asset('images/'.$yayasan->foto_ktp_pengurus)}}" alt="logo" height="150" width="200">
                                                         @else
                                                         <p>Tidak ada gambar</p>
                                                         @endif
                                                     </div>
                                                 </div>

                                             </div>
                                             <div class="row">
                                                 <div class="col-lg-12">
                                                     <hr class="style">
                                                 </div>
                                             </div>
                                             <div class="dt-buttons">
                                                 <div class="sweetalert m-t-15">
                                                     <a href="{{ route('yayasan.index') }}" class="btn btn-primary btn-md m-b-10 m-l-5" name="button">Back</a>
                                                 </div>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 @endsection