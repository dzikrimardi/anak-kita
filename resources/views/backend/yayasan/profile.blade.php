@extends('backend.layouts.app')
@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }

    textarea {
        width: 100%;
        height: 80px;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Setting Profile</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Setting Profile</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-two">
                            <div style="text-align:center;">
                                @if($yayasan->foto_yayasan == "")
                                <img src="{{ asset('/images/placeholder.png')}}" id="img_preview" alt="your image" height="200" width="200"><br>
                                @else
                                <img src="{{ asset('/images/'.$yayasan->foto_yayasan)}}" id="img_preview" height="200" width="200"><br>
                                @endif

                            </div>
                            <h3>{{ $yayasan->nama_yayasan }}</h3>
                            <div class="desc">
                                {{ strip_tags($yayasan->visi) }}
                            </div>
                            <div class="contacts">
                                <a href=""><i class="fa fa-plus"></i></a>
                                <a href=""><i class="fa fa-whatsapp"></i></a>
                                <a href=""><i class="fa fa-envelope"></i></a>
                                <div class="clear"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs profile-tab" role="tablist">
                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#settings" role="tab">Profile Yayasan</a> </li>
                        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#pengurus" role="tab">Profile Pengurus</a> </li>
                    </ul>
                    <!-- Tab panes -->
                    <form class="form-horizontal form-material" action="{{ route('yayasan.update',$yayasan->id) }}" enctype="multipart/form-data" method="post">
                        <div class="tab-content">
                            @csrf
                            {{ method_field('PUT') }}

                            <div class="tab-pane active" id="settings" role="tabpanel">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label class="col-md-12">Nama Yayasan</label>
                                        <div class="col-md-12">
                                            <input type="text" name="nama_yayasan" class="form-control form-control-line" value="{{ $yayasan->nama_yayasan }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="example-email" class="col-md-12">No telp</label>
                                        <div class="col-md-12">
                                            <input type="text" name="no_telp" class="form-control form-control-line" value="{{ $yayasan->no_telp }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Tanggal Berdiri</label>
                                        <div class="col-md-12">
                                            <input type="date" name="tgl_berdiri" class="form-control form-control-line" value="{{ $yayasan->tgl_berdiri }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Alamat</label>
                                        <div class="col-md-12">
                                            <textarea rows="10" name="alamat" cols="124" style="border-color:lightgrey;">{{ $yayasan->alamat }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Visi</label>
                                        <div class="col-md-12">
                                            <textarea id="visi" name="visi" class="form-control form-control-line">{{ $yayasan->visi }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Misi</label>
                                        <div class="col-md-12">
                                            <textarea id="misi" name="misi" class="form-control form-control-line">{{ $yayasan->misi }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Tentang Yayasan</label>
                                        <div class="col-md-12">
                                            <textarea id="tentang" name="tentang_yayasan" class="form-control form-control-line">{{ $yayasan->tentang_yayasan }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-12">Upload Foto</label>
                                        <div class="col-md-12">
                                            <input type="file" id="img_upload" name="foto_yayasan" value="{{ $yayasan->foto_yayasan }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane active" id="pengurus" role="tabpanel">
                                <div class="card-body">
                                    <div class="basic-elements">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>Nama Pengurus<span style="color:red;"> *</span></label>
                                                    <input type="text" name="nama_pengurus" class="form-control" value="{{ $yayasan->nama_pengurus }}" placeholder="Nama Pengurus" required>
                                                    <p style="color:red;font-size:13px;">
                                                        @if($errors->has('nama_pengurus'))
                                                        <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_pengurus')}}
                                                        @endif
                                                    </p>
                                                </div>

                                                <label>Foto KTP<span style="color:red;"> *</span></label>
                                                <div class="form-group dropzone">
                                                    <input class="form-control" name="foto_ktp_pengurus" type="file" value="{{ $yayasan->foto_ktp_pengurus }}" multiple />
                                                </div>
                                                <p style="color:red;font-size:13px;">
                                                    @if($errors->has('foto_ktp_pengurus'))
                                                    <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_ktp_pengurus')}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <label>No Telp<span style="color:red;"> *</span></label>
                                                    <input type="number" name="no_telp_pengurus" class="form-control" value="{{ $yayasan->no_telp_pengurus }}" placeholder="No Telp" required>
                                                    <p style="color:red;font-size:13px;">
                                                        @if($errors->has('no_telp_pengurus'))
                                                        <i class="fa fa-exclamation-circle"></i> {{$errors->first('no_telp_pengurus')}}
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin<span style="color:red;"> *</span></label>
                                                    <select class="form-control" name="jenis_kelamin_pengurus" value="{{ $yayasan->jenis_kelamin_pengurus }}">
                                                        <option selected="true" disabled="disabled" value="{{ $yayasan->jenis_kelamin_pengurus }}">{{ $yayasan->jenis_kelamin_pengurus }}</option>
                                                        <option value="Laki-Laki">Laki-Laki</option>
                                                        <option value="Perempuan">Perempuan</option>
                                                    </select>
                                                    <p style="color:red;font-size:13px;">
                                                        @if($errors->has('jenis_kelamin'))
                                                        <i class="fa fa-exclamation-circle"></i> {{$errors->first('jenis_kelamin')}}
                                                        @endif
                                                    </p>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat<span style="color:red;"> *</span></label>
                                                    <textarea required="required" id="alamat" style="border-color:lightgrey;" name="alamat_pengurus" rows="3" cols="58">{{ $yayasan->alamat_pengurus }}</textarea>
                                                    <p style="color:red;font-size:13px;">
                                                        @if($errors->has('alamat_pengurus'))
                                                        <i class="fa fa-exclamation-circle"></i> {{$errors->first('alamat_pengurus')}}
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Update Profile</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Column -->
        </div>

        <!-- End PAge Content -->
    </div>
</div>
<script type="text/javascript">
    function readUrl(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#img_preview').attr('src', e.target.result)
                ''
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#img_upload').change(function() {
        readUrl(this);
    });
</script>
@endsection