@extends('backend.layouts.app')

@section('content')
<style>
    .form-control::-webkit-input-placeholder {
        font-size: 14px;
        color: darkgrey;
    }

    .form-control::-moz-input-placeholder {
        color: darkgrey;
    }

    .form-control:-ms-input-placeholder {
        color: darkgrey;
    }

    textarea {
        width: 100%;
        height: 80px;
    }
</style>
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Yayasan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Tambah</a></li>
                <li class="breadcrumb-item active">Yayasan</li>
            </ol>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="card-body p-b-0">
                <h4 class="card-title">Data Yayasan</h4>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab" role="tablist">
                    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#yayasan" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Profile Yayasan</span></a> </li>
                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Profile Pengurus Yayasan <span style="color:red;"> *</span></span></a> </li>
                </ul>
                <!-- Tab panes -->
                <form action="{{ route('yayasan.store') }}" enctype="multipart/form-data" method="post">
                    <div class="tab-content">
                        @csrf
                        <div class="tab-pane active" id="yayasan" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="basic-elements">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Nama Yayasan<span style="color:red;"> *</span></label>
                                                            <input type="text" name="nama_yayasan" class="form-control" value="{{ old('nama_yayasan') }}" placeholder="Nama Yayasan" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('nama_yayasan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_yayasan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Tanggal Berdiri Yayasan<span style="color:red;"> *</span></label>
                                                            <input type="date" name="tgl_berdiri" class="form-control" value="{{ old('tgl_berdiri') }}" placeholder="Tanggal Berdiri Yayasan" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tgl_berdiri'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tgl_berdiri')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <label>Foto Yayasan<span style="color:red;"> *</span></label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_yayasan" type="file" value="{{ old('foto_yayasan') }}" required="required" multiple/>
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_yayasan'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_yayasan')}}
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>No Telp<span style="color:red;"> *</span></label>
                                                            <input type="number" name="no_telp" class="form-control" value="{{ old('no_telp') }}" placeholder="No Telp" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('no_telp'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('no_telp')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat<span style="color:red;"> *</span></label>
                                                            <textarea id="alamat" style="border-color:lightgrey;" name="alamat" rows="3" cols="58" required></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('alamat'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('alamat')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label>Visi</label>
                                                        <div class="form-group">
                                                            <textarea id="visi" name="visi"></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('visi'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('visi')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <label>Misi</label>
                                                        <div class="form-group">
                                                            <textarea id="misi" name="misi"></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('misi'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('misi')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <label>Tentang Yayasan</label>
                                                        <div class="form-group">
                                                            <textarea id="tentang" name="tentang_yayasan"></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('tentang_yayasan'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('tentang_yayasan')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="profile" role="tabpanel">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="basic-elements">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>Nama Pengurus<span style="color:red;"> *</span></label>
                                                            <input type="text" name="nama_pengurus" class="form-control" value="{{ old('nama_pengurus') }}" placeholder="Nama Pengurus" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('nama_pengurus'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('nama_pengurus')}}
                                                                @endif
                                                            </p>
                                                        </div>

                                                        <label>Foto KTP</label>
                                                        <div class="form-group dropzone">
                                                            <input class="form-control" name="foto_ktp_pengurus" type="file" value="{{ old('foto_ktp_pengurus') }}" multiple />
                                                        </div>
                                                        <p style="color:red;font-size:13px;">
                                                            @if($errors->has('foto_ktp_pengurus'))
                                                            <i class="fa fa-exclamation-circle"></i> {{$errors->first('foto_ktp_pengurus')}}
                                                            @endif
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label>No Telp<span style="color:red;"> *</span></label>
                                                            <input type="number" name="no_telp_pengurus" class="form-control" value="{{ old('no_telp_pengurus') }}" placeholder="No Telp" required>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('no_telp_pengurus'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('no_telp_pengurus')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Jenis Kelamin<span style="color:red;"> *</span></label>
                                                            <select class="form-control" name="jenis_kelamin_pengurus" required>
                                                                <option selected="true" disabled="disabled" value="{{ old('jenis_kelamin_pengurus') }}">Pilih</option>
                                                                <option value="Laki-Laki">Laki-Laki</option>
                                                                <option value="Perempuan">Perempuan</option>
                                                            </select>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('jenis_kelamin_pengurus'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('jenis_kelamin_pengurus')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>Alamat</label>
                                                            <textarea id="alamat" style="border-color:lightgrey;" name="alamat_pengurus" rows="3" cols="58"></textarea>
                                                            <p style="color:red;font-size:13px;">
                                                                @if($errors->has('alamat_pengurus'))
                                                                <i class="fa fa-exclamation-circle"></i> {{$errors->first('alamat_pengurus')}}
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="dt-buttons">
                        <div class="sweetalert m-t-15">
                            <button class="btn btn-info btn sweet-success" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection