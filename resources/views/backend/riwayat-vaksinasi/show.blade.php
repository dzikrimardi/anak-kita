 @extends('backend.layouts.app')

 @section('content')
 <div class="page-wrapper">
     <!-- Bread crumb -->
     <div class="row page-titles">
         <div class="col-md-5 align-self-center">
             <h3 class="text-primary">Data Vaksinasi</h3>
         </div>
         <div class="col-md-7 align-self-center">
             <ol class="breadcrumb">
                 <li class="breadcrumb-item"><a href="javascript:void(0)">Data Vaksinasi</a></li>
                 <li class="breadcrumb-item active">Show</li>
             </ol>
         </div>
     </div>
     <!-- End Bread crumb -->
     <!-- Container fluid  -->
     <div class="container-fluid">
         <!-- Start Page Content -->
         <!-- /# row -->
         <div class="row">
             <div class="col-lg-12">
                 <div class="card pink">
                     <div class="card-title">
                         <h2 style="text-decoration:underline;" class="pink-name">Info Lengkap Vaksinasi</h2>
                         <a href="{{ route('export.vaksinasi',$vaksinasi->id) }}" class="btn float-right" name="button" style="margin-left:1%;background-color:white;color:#e9748a;">Excel</a>
                         <a href="{{ route('pdf.vaksinasi',$vaksinasi->id) }}" class="btn float-right" name="button" style="background-color:white;color:#e9748a;">PDF</a>

                     </div>
                     <div class="card-body">
                         <div class="basic-elements">
                             <div class="card">
                                 <div class="row">
                                     <div class="col-lg-6">
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Nama:</label><br>
                                             <h4>{{ $vaksinasi->dataAnak->nama_anak }}</h4>
                                         </div>
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Jenis Imunisasi:</label><br>
                                             <h4>{{ $vaksinasi->jenis_vaksinasi }}</h4>
                                         </div>
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Alamat Tempat Vaksinasi:</label><br>
                                             <h4>{{ $vaksinasi->alamat_tempat_vaksinasi }}</h4>
                                         </div>
                                     </div>
                                     <div class="col-lg-6">
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Pemberi Imunisasi:</label><br>
                                             <h4>{{ $vaksinasi->pemberi_vaksinasi }}</h4>
                                         </div>
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Tempat Vaksinasi:</label><br>
                                             <h4>{{ $vaksinasi->tempat_vaksinasi }}</h4>
                                         </div>

                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-lg-12">
                                         <hr class="style">
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-lg-6">
                                         <div class="form-group">
                                             <h2 style="text-decoration:underline;">Jenis Vaksinasi</h2>
                                         </div>
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Vaksinasi Wajib:</label><br>
                                             <h4>{{ $vaksinasi->vaksinasi_wajib }}</h4>
                                         </div>
                                         <div class="form-group" style="margin-bottom:10%;">
                                             <label style="font-weight:bold;">Tgl Pemberian Vaksinasi Wajib:</label><br>
                                             <h4>{{ $vaksinasi->tgl_vaksinasi_wajib }}</h4>
                                         </div>
                                     </div>
                                     <div class="col-lg-6">
                                         <div class="form-group">
                                             <h2>&nbsp;</h2>
                                         </div>
                                         <div class="form-group">
                                             <label style="font-weight:bold;">Vaksinasi Tambahan:</label><br>
                                             <h4>{{ $vaksinasi->vaksinasi_tambahan }}</h4>
                                         </div>
                                         <div class="form-group" style="margin-bottom:10%;">
                                             <label style="font-weight:bold;">Tgl Pemberian Vaksinasi Tambahan:</label><br>
                                             <h4>{{ $vaksinasi->tgl_vaksinasi_tambahan }}</h4>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-lg-12">
                                         <hr class="style">
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-lg-6">
                                         <div class="form-group" style="margin-bottom:10%;">
                                             <label style="font-weight:bold;">Catata:</label><br>
                                             <h4>{{ $vaksinasi->catatan }}</h4>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="dt-buttons">
                                     <div class="sweetalert m-t-15">
                                         <a href="{{ route('riwayat-vaksinasi.edit', $vaksinasi->id) }}" class="btn btn-warning btn-md m-b-10 m-l-5" name="button">Edit</a>
                                         <a href="{{ route('riwayat-vaksinasi.index') }}" class="btn btn-primary btn-md m-b-10 m-l-5" name="button">Back</a>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!-- End PAge Content -->
     </div>
 </div>
 @endsection