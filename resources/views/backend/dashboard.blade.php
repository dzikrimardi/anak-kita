@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Dashboard</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        {!!$pie->html() !!}
                    </div>
                </div>
            </div>
            @if(Auth::user()->role == "superadmin")
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-users f-s-40 color-primary"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{$users}}</h2>
                                    <p class="m-b-0">User Admin</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="card p-30">
                            <div class="media">
                                <div class="media-left meida media-middle">
                                    <span><i class="fa fa-building-o f-s-40 color-warning"></i></span>
                                </div>
                                <div class="media-body media-text-right">
                                    <h2>{{$yayasans}}</h2>
                                    <p class="m-b-0">Yayasan</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    @if(Auth::user()->role == "superadmin")
                    <div class="card-body">Selamat Datang SuperAdmin ! </div>
                    @else
                    <div class="card-body">Selamat Datang Admin ! </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="row bg-white m-l-0 m-r-0 box-shadow ">

            <!-- column -->
            <!-- <div class="col-lg-8"> -->
            <div class="card">
                <div class="card-body" style="float:right;text-align:center;">
                    <b><img src="{{ asset('/images/logo.png')}}" height="150" width="300" alt="homepage" class="dark-logo" /></b>
                    <h4 class="card-title">Web Aplikasi AnaKita</h4>
                    <div>
                        <p>AnaKita adalah sebuah Web Aplikasi yang di kembangkan untuk yayasan, panti asuhan dan rumah singgah. <br>
                            Web Aplikasi ini di buat untuk proses data anak, data kesehatan anak, data vaksinasi anak yang terdapat di yayasan tersebut</p>
                    </div>
                </div>
            </div>
            <!-- </div> -->
            <!-- column -->

            <!-- column -->
            <!-- <div class="col-lg-4">
                <div class="card">
                    <div class="card-body browser">
                        <p class="f-w-600">iMacs <span class="pull-right">85%</span></p>
                        <div class="progress ">
                            <div role="progressbar" style="width: 85%; height:8px;" class="progress-bar bg-danger wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                        </div>

                        <p class="m-t-30 f-w-600">iBooks<span class="pull-right">90%</span></p>
                        <div class="progress">
                            <div role="progressbar" style="width: 90%; height:8px;" class="progress-bar bg-info wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                        </div>

                        <p class="m-t-30 f-w-600">iPhone<span class="pull-right">65%</span></p>
                        <div class="progress">
                            <div role="progressbar" style="width: 65%; height:8px;" class="progress-bar bg-success wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                        </div>

                        <p class="m-t-30 f-w-600">Samsung<span class="pull-right">65%</span></p>
                        <div class="progress">
                            <div role="progressbar" style="width: 65%; height:8px;" class="progress-bar bg-warning wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                        </div>

                        <p class="m-t-30 f-w-600">android<span class="pull-right">65%</span></p>
                        <div class="progress m-b-30">
                            <div role="progressbar" style="width: 65%; height:8px;" class="progress-bar bg-success wow animated progress-animated"> <span class="sr-only">60% Complete</span> </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!-- column -->
        </div>
        <!-- End PAge Content -->
    </div>
</div>
{!! Charts::scripts() !!}

{!! $pie->script() !!}
@endsection