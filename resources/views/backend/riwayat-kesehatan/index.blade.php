@extends('backend.layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Data Riwayat Kesehatan</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Data Riwayat Kesehatan</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive m-t-0">
                            <a href="{{ route('riwayat-kesehatan.create')}}" class="btn btn-info" name="button">Tambah Data Riwayat</a>
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Penyakit</th>
                                        <th>Catatan Dokter</th>
                                        <th>Obat/Resep</th>
                                        <th>Terima Obat</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($datas as $data)
                                    <tr>
                                        <td style="text-decoration:underline;"><a href="{{ route('riwayat-kesehatan.show',$data->id) }}">{{ $data->dataAnak->nama_anak }}</a></td>
                                        <td>{{ $data->nama_penyakit }}</td>
                                        <td>{{ $data->catatan_dokter }}</td>
                                        <td>{{ $data->obat_resep }}</td>
                                        <td>{{ $data->terima_obat }}</td>
                                        <td style="display:inline-flex;margin-left:10%;">
                                            <a href="{{ route('riwayat-kesehatan.edit', $data->id) }}" class="btn btn-warning" name="button"><i class="fa fa-pencil" aria-hidden="true"> </i></a>&nbsp;
                                            <form action="{{ route('riwayat-kesehatan.destroy',$data->id) }}" onsubmit="return confirm('Yakin ingin menghapus data ini?');" method="POST">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" class="btn btn-danger btn sweet-confirm"><i class="fa fa-trash-o" aria-hidden="true"> </i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
</div>
@endsection