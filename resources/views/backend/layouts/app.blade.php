<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/logo.png')}}">
    <title>AnaKita</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('backend/css/lib/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <!-- <link href="{{ asset('backend/css/lib/sweetalert/sweetalert.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('backend/css/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
    <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar">
    <!-- Preloader - style you can find in spinners.css -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- Main wrapper  -->
    <div id="main-wrapper">
        <!-- header header  -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <!-- Logo -->
                <div class="navbar-header">
                    <a href="{{ route('home') }}" class="navbar-brand">
                        <!-- Logo icon -->
                        <b><img src="{{ asset('/images/logo.png')}}" height="60" width="100" alt="homepage" class="dark-logo" /></b>
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <!-- <span><img src="images/logo-text.png" alt="homepage" class="dark-logo" /></span> -->
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu"></i></a> </li>

                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">

                        <!-- Search -->
                        <!-- <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search here"> <a class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </!-->



                        <!-- Profile -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{ asset('images/default-user.png') }}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right animated zoomIn">
                                <ul class="dropdown-user">
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">
                                            <i class="fa fa-power-off"></i> Keluar</a></li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- End header header -->
        <!-- Left Sidebar  -->
        <div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a class="" href="{{ url('/home') }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></span></a></li>
                        <!-- <li> <a class="" href="{{ route('daftar-penyakit.index') }}" aria-expanded="false"><i class="fa fa-eyedropper"></i><span class="hide-menu">Daftar Penyakit</span></span></a></li> -->

                        @if(Auth::user()->role == "superadmin")
                        <li> <a class="" href="{{ route('yayasan.index') }}" aria-expanded="false"><i class="fa fa-building-o"></i><span class="hide-menu">Yayasan</span></span></a></li>
                        @endif

                        <li class="nav-label">Fitur</li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-smile-o"></i><span class="hide-menu">Data Anak Asuh</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('data-anak.index') }}">Data Anak</a></li>
                            </ul>
                        </li>
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">Laporan Riwayat</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('riwayat-kesehatan.index') }}">Riwayat Kesehatan</a></li>
                                <li><a href="{{ route('riwayat-vaksinasi.index') }}">Riwayat Vaksinasi</a></li>
                            </ul>
                        </li>

                        @if(Auth::user()->role == "superadmin")
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-user-circle"></i><span class="hide-menu">User</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('user.index') }}">Admin User</a></li>
                                <li><a href="{{ route('user.create') }}">Tambah Admin</a></li>
                            </ul>
                        </li>
                        @endif

                        @if(Auth::user()->role == "admin")
                        <li> <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-cog"></i><span class="hide-menu">Pengaturan Profile</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('profile',Auth::user()->id_yayasan) }}">Settings</a></li>
                            </ul>
                        </li>
                        @endif

                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->
        <!-- Page wrapper  -->
        @yield('content')
        <footer class="footer"> Copyright © 2019 anakita.com. All rights reserved.</a></footer>
        <!-- End Page wrapper  -->
    </div>
    @include('sweetalert::alert',['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])
    <!-- End Wrapper -->
    <script src="{{ asset('backend/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <!-- All Jquery -->
    <script src="{{ asset('backend/js/lib/jquery/jquery.min.js') }}"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script> -->
    <!-- Bootstrap tether Core JavaScript -->
    <!-- <script src="{{ asset('backend/js/lib/bootstrap/js/popper.min.js') }}"></script> -->
    <script src="{{ asset('backend/js/lib/bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('backend/js/jquery.slimscroll.js') }}"></script>
    <!--Menu sidebar -->
    <script src="{{ asset('backend/js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src="{{ asset('backend/js/lib/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src="{{ asset('backend/js/custom.min.js') }}"></script>

    <!-- Sweet aler -->
    <!-- <script src="{{ asset('backend/js/lib/sweetalert/sweetalert.min.js') }}"></script> -->
    <!-- scripit init-->
    <!-- <script src="{{ asset('backend/js/lib/sweetalert/sweetalert.init.js') }}"></script> -->
    <!-- Select 2-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <!-- Datatables -->
    <script src="{{ asset('backend/js/lib/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('backend/js/lib/datatables/datatables-init.js') }}"></script>
    <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
    <!-- <script src="https://www.gstatic.com/charts/loader.js"></script> -->



    <!-- Select2 dropdown -->
    <script type="text/javascript">
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                maximumSelectionLength: 5,
            });
            $('.search-select2').select2();

            var table = $('#example23').dataTables({
                "ordering": false,
            });

            // $('#test_btn').click(function() {
            //     var data = table.$('input, select').serialize();
            //     alert('bisa cuy');
            //     return false;
            // });

        });
    </script>

    <!-- Ckeditor -->
    <script type="text/javascript">
        ClassicEditor
            .create(document.querySelector('#visi'))
            .catch(error => {
                console.log(error);
            });

        ClassicEditor
            .create(document.querySelector('#misi'))
            .catch(error => {
                console.log(error);
            });
        ClassicEditor
            .create(document.querySelector('#tentang'))
            .catch(error => {
                console.log(error);
            });
    </script>

    <!-- Disabled / Enabled form (Obat Resep) -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".yes").on('click', function() {
                if ($(this).is(':checked')) {
                    $(".resep_obat").prop('readonly', false).css("background-color", "white");
                    $(".resep_obat").prop('required', true);
                }
            });

            $(".no").on('click', function() {
                if ($(this).is(':checked')) {
                    $(".resep_obat").prop('readonly', true).css("background-color", "lightgrey").val("");
                    $(".resep_obat").prop('required', false);
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#wajib").on('click', function() {
                if ($(this).is(':checked')) {
                    $('#vaksinasi_wajib').prop('disabled', false);
                    $('#tgl_vaksinasi_wajib').prop('disabled', false);
                    $('#vaksinasi_tambahan').prop('disabled', true).val(null).trigger('change');
                    $('#tgl_vaksinasi_tambahan').prop('disabled', true).val(null);
                }
            });

            $("#tambahan").on('click', function() {
                if ($(this).is(':checked')) {
                    $('#vaksinasi_tambahan').prop('disabled', false);
                    $('#tgl_vaksinasi_tambahan').prop('disabled', false);
                    $('#vaksinasi_wajib').prop('disabled', true).val(null).trigger('change');
                    $('#tgl_vaksinasi_wajib').prop('disabled', true).val(null);
                }
            });


        });
    </script>

    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this record?`,
              text: "If you delete this, it will be gone forever.",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
    </script>

</body>

</html>