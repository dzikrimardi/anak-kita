<table>
    <thead>
        <tr>
            <th> nama yayasan </th>
            <th> foto yayasan </th>
            <th> alamat </th>
            <th> no telp </th>
            <th> tgl berdiri </th>
            <th> visi </th>
            <th> misi </th>
            <th> tentang yayasan </th>
            <th> nama pengurus </th>
            <th> no telp pengurus </th>
            <th> alamat pengurus </th>
            <th> jenis kelamin pengurus </th>
            <th> foto ktp pengurus </th>
        </tr>
    </thead>
    <tbody>
        @foreach($yayasan as $data)
        <tr>
            <td>{{$data->nama_yayasan}} </td>
            <td>{{$data->foto_yayasan}} </td>
            <td>{{$data->alamat}} </td>
            <td>{{$data->no_telp}} </td>
            <td>{{$data->tgl_berdiri}} </td>
            <td>{{ strip_tags($data->visi) }} </td>
            <td>{{ strip_tags($data->misi) }} </td>
            <td>{{ strip_tags($data->tentang_yayasan) }} </td>
            <td>{{$data->nama_pengurus}} </td>
            <td>{{$data->no_telp_pengurus}} </td>
            <td>{{$data->alamat_pengurus}} </td>
            <td>{{$data->jenis_kelamin_pengurus}} </td>
            <td>{{$data->foto_ktp_pengurus}} </td>
        </tr>
        @endforeach

    </tbody>
</table>

<table>
    <thead>
        <tr>
            <th> <strong> kode anak </strong></th>
            <th> <strong> nama yayasan </strong></th>
            <th> <strong> nama anak </strong></th>
            <th> <strong> jenis kelamin </strong></th>
            <th> <strong> tempat lahir </strong></th>
            <th> <strong> tanggal lahir </strong></th>
            <th> <strong> alamat </strong></th>
            <th> <strong> nama ayah </strong></th>
            <th> <strong> nik ayah </strong></th>
            <th> <strong> alamat ayah </strong></th>
            <th> <strong> no hp ayah </strong></th>
            <th> <strong> nama istrongu </strong></th>
            <th> <strong> nik istrongu </strong></th>
            <th> <strong> alamat istrongu </strong></th>
            <th> <strong> no hp istrongu </strong></th>
            <th> <strong> nama wali </strong></th>
            <th> <strong> nik wali </strong></th>
            <th> <strong> alamat wali </strong></th>
            <th> <strong> strongerat strongadan </strong></th>
            <th> <strong> tanggal terakhir di periksa </strong></th>
            <th> <strong> tinggi strongadan </strong></th>
            <th> <strong> tanggal terakhir di periksa </strong></th>
            <th> <strong> massa tustronguh </strong></th>
            <th> <strong> tanggal terakhir di periksa </strong></th>
            <th> <strong> suhu strongadan </strong></th>
            <th> <strong> tanggal terakhir di periksa </strong></th>
            <th> <strong> tekanan darah </strong></th>
            <th> <strong> tanggal terakhir di periksa </strong></th>
            <th> <strong> gol darah </strong></th>
            <th> <strong> umur </strong></th>
            <th> <strong> foto akte lahir </strong></th>
            <th> <strong> foto anak </strong></th>
            <th> <strong> foto kartu keluarga </strong></th>
            <th> <strong> foto kartu strongpjs </strong></th>
            <th> <strong> foto ktp </strong></th>
            <th> <strong> catatan </strong></th>
        </tr>
    </thead>
    <tbody>
        @foreach($anak as $data)
        <tr>
            <td> {{ $data->kode_anak}} </td>
            <td> {{ $data->yayasan->nama_yayasan}} </td>
            <td> {{ $data->nama_anak}} </td>
            <td> {{ $data->jenis_kelamin}} </td>
            <td> {{ $data->tempat_lahir}} </td>
            <td> {{ $data->tanggal_lahir}} </td>
            <td> {{ $data->alamat}} </td>
            <td> {{ $data->nama_ayah}} </td>
            <td> {{ $data->nik_ayah}} </td>
            <td> {{ $data->alamat_ayah}} </td>
            <td> {{ $data->no_hp_ayah}} </td>
            <td> {{ $data->nama_ibu}} </td>
            <td> {{ $data->nik_ibu}} </td>
            <td> {{ $data->alamat_ibu}} </td>
            <td> {{ $data->no_hp_ibu}} </td>
            <td> {{ $data->nama_wali}} </td>
            <td> {{ $data->nik_wali}} </td>
            <td> {{ $data->alamat_wali}} </td>
            <td> {{ $data->berat_badan}} Kg</td>
            <td> {{ $data->tgl_berat_badan}} </td>
            <td> {{ $data->tinggi_badan }} Cm </td>
            <td> {{ $data->tgl_tinggi_badan}} </td>
            <td> {{ $data->massa_tubuh}} Kg/m2</td>
            <td> {{ $data->tgl_massa_tubuh}} </td>
            <td> {{ $data->suhu_badan }}&deg;C </td>
            <td> {{ $data->tgl_suhu_badan}} </td>
            <td> {{ $data->tekanan_darah}} mm/Hg </td>
            <td> {{ $data->tgl_tekanan_darah}} </td>
            <td style="text-transform:uppercase;"> {{ $data->gol_darah}} </td>
            <td> {{ $data->umur}} Tahun </td>
            <td> {{ $data->foto_akte_lahir}} </td>
            <td> {{ $data->foto_anak}} </td>
            <td> {{ $data->foto_kartu_keluarga}} </td>
            <td> {{ $data->foto_kartu_bpjs}} </td>
            <td> {{ $data->foto_ktp}} </td>
            <td> {{ $data->catatan}} </td>
        </tr>
        @endforeach

    </tbody>
</table>
<table>
    <thead>
        <tr>
            <th> <strong>nama anak </strong></th>
            <th> <strong>nama penyakit </strong></th>
            <th> <strong>gejala penyakit </strong></th>
            <th> <strong>catatan dokter </strong></th>
            <th> <strong>obat resep </strong></th>
            <th> <strong>terima obat </strong></th>
        </tr>
    </thead>
    <tbody>
        @foreach($kesehatan as $data)
        <tr>
            <td> {{ $data->dataAnak->nama_anak }} </td>
            <td> {{ $data->nama_penyakit }} </td>
            <td> {{ $data->gejala_penyakit }} </td>
            <td> {{ $data->catatan_dokter }} </td>
            <td> {{ $data->obat_resep }} </td>
            <td> {{ $data->terima_obat }} </td>
        </tr>
        @endforeach

    </tbody>
</table>
<table>
    <thead>
        <tr>
            <th> <strong> nama anak </strong></th>
            <th> <strong> jenis vaksinasi </strong></th>
            <th> <strong> vaksinasi wajib </strong></th>
            <th> <strong> tgl vaksinasi wajib </strong></th>
            <th> <strong> vaksinasi tambahan </strong></th>
            <th> <strong> tgl vaksinasi tambahan </strong></th>
            <th> <strong> pemberi vaksinasi </strong></th>
            <th> <strong> tempat vaksinasi </strong></th>
            <th> <strong> alamat tempat vaksinasi </strong></th>
            <th> <strong> catatan </strong></th>
        </tr>
    </thead>
    <tbody>
        @foreach($vaksinasi as $data)
        <tr>
            <td> {{ $data->dataAnak->nama_anak}} </td>
            <td> {{ $data->jenis_vaksinasi}} </td>
            <td> {{ $data->vaksinasi_wajib}} </td>
            <td> {{ $data->tgl_vaksinasi_wajib}} </td>
            <td> {{ $data->vaksinasi_tambahan}} </td>
            <td> {{ $data->tgl_vaksinasi_tambahan}} </td>
            <td> {{ $data->pemberi_vaksinasi}} </td>
            <td> {{ $data->tempat_vaksinasi}} </td>
            <td> {{ $data->alamat_tempat_vaksinasi}} </td>
            <td> {{ $data->catatan}} </td>
        </tr>
        @endforeach

    </tbody>
</table>