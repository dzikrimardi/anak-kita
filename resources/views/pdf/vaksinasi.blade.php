<html lang="en">

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <center>
            <h1 class="text-center">Info Lengkap Vaksinasi</h1>
        </center>
        <div class="row">
            <div class="col-12 border border-dark">
                <!-- <h4>Order Information</h4> -->
                <div class="col-12">
                    <h4><b>Nama</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->dataAnak->nama_anak }}
                        @endforeach
                    </h4>
                    <p><b>Nama Yayasan</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->dataYayasan->nama_yayasan }}
                        @endforeach
                    </p>
                    <p><b>Pemberi Vaksinasi</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->pemberi_vaksinasi }}
                        @endforeach
                    </p>
                    <p><b>Jenis Vaksinasi</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->jenis_vaksinasi }}
                        @endforeach
                    </p>

                    <p><b>Vaksinasi Wajib</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->vaksinasi_wajib }}
                        @endforeach
                    </p>
                    <p><b>Vaksinasi Tambahan</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->vaksinasi_tambahan }}
                        @endforeach
                    </p>
                    <p><b>Tanggal Vaksinasi Wajib</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->tgl_vaksinasi_wajib }}
                        @endforeach
                    </p>
                    <p><b>Tanggal Vaksinasi Tambahan</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->tgl_vaksinasi_tambahan }}
                        @endforeach
                    </p>
                    <p><b>Rumah Sakit / Posyandu</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->tempat_vaksinasi }}
                        @endforeach
                    </p>
                    <p><b>Alamat</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->alamat_tempat_vaksinasi }}
                        @endforeach
                    </p>
                    <p><b>Catatan</b>:
                        @foreach($vaksinasi as $data)
                        {{ $data->alamat_tempat_vaksinasi }}
                        @endforeach
                    </p>

                </div>
            </div>

        </div>
        <hr />

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>
