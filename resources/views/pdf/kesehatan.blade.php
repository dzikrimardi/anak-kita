<html lang="en">

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <center>
            <h1 class="text-center">Info Lengkap Kesehatan</h1>
        </center>
        <div class="row">
            <div class="col-12 border border-dark">
                <!-- <h4>Order Information</h4> -->
                <div class="col-12">
                    <h4><b>Nama</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->dataAnak->nama_anak }}
                        @endforeach
                    </h4>
                    <p><b>Nama Yayasan</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->dataYayasan->nama_yayasan }}
                        @endforeach
                    </p>
                    <p><b>Penyakit</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->nama_penyakit }}
                        @endforeach
                    </p>

                    <p><b>Gejala Penyakit</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->gejala_penyakit }}
                        @endforeach
                    </p>
                    <p><b>Catatan Dokter</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->catatan }}
                        @endforeach
                    </p>
                    <p><b>Obat Resep</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->obat_resep }}
                        @endforeach
                    </p>
                    <p><b>Terima Obat</b>:
                        @foreach($kesehatan as $data)
                        {{ $data->terima_obat }}
                        @endforeach
                    </p>
                </div>
            </div>

        </div>
        <hr />

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>