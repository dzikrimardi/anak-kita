<html lang="en">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<head>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
    <main role="main" class="container">
        <div class="col-8">
            <h1 class="text-center">Data Yayasan</h1>
        </div>
        <div class="col-12 border border-dark">
            <h3>Profile</h3>
            <p><b>Nama</b>:
                @foreach($yayasan as $data)
                {{ $data->nama_yayasan }}
                @endforeach
            </p>
            <p><b>Tanggal Berdiri</b>:
                @foreach($yayasan as $data)
                {{ $data->tgl_berdiri }}
                @endforeach
            </p>
            <p><b>No Telp.</b>:
                @foreach($yayasan as $data)
                {{ $data->no_telp }}
                @endforeach
            </p>
            <p><b>Visi</b>:
                @foreach($yayasan as $data)
                {!! $data->visi !!}
                @endforeach
            </p>
            <p><b>Misi</b>:
                @foreach($yayasan as $data)
                {!! $data->misi !!}
                @endforeach
            </p>
            <p><b>Tentang</b>:
                @foreach($yayasan as $data)
                {!! $data->tentang_yayasan !!}
                @endforeach
            </p>
            <h3>Profile Pengurus</h3>
            <p><b>Nama Pengurus</b>:
                @foreach($yayasan as $data)
                {{ $data->nama_pengurus }}
                @endforeach
            </p>
            <p><b>Jenis Kelamin</b>:
                @foreach($yayasan as $data)
                {{ $data->jenis_kelamin_pengurus }}
                @endforeach
            </p>
            <p><b>No Telp.</b>:
                @foreach($yayasan as $data)
                {{ $data->no_telp_pengurus }}
                @endforeach
            </p>
            <p><b>Alamat.</b>:
                @foreach($yayasan as $data)
                {{ $data->alamat_pengurus }}
                @endforeach
            </p>
        </div>
        <hr />

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>

</html>