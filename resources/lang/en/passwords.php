<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata Sandi Harus lebih dari 8 karakter.',
    'reset' => 'Kata Sandi Telah di Reset!',
    'sent' => 'Reset Kata Sandi telah di Kirim email!',
    'token' => 'Token Kata Sandi Gagal.',
    'user' => "User Email Tidak Terdaftar.",
];
