<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Laporan extends CI_Controller {

	public function index(){
		//nothing
	}

	function per_surveyor(){

		$data['title'] = 'Survey - Laporan Per Surveyor';

		$sql_part_where = '';
		if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
			$sql_part_where = "WHERE id = $id ";
		}
		$sql 				= "	SELECT *, (
											SELECT GROUP_CONCAT((SELECT name FROM `groups` WHERE groups.id = users_groups.group_id  ), ', ') 
											FROM `users_groups` WHERE users.id = users_groups.user_id
										) AS list_groups 
								FROM 
									`users`
								$sql_part_where								
								";        
		$data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		if($this->uri->segment(3)){
			$sql 					= "	SELECT * FROM `survey02_jalan` WHERE user_id = $id ";        
			$data_db 				= $this->db->query($sql);
	        $data['data_db_rows'] 	= $data_db; 
		}

		$this->load->view('laporan_per_surveyor',$data);
	}
	function per_lokasi(){

		$data['title'] = 'Survey - Kategori';


		$sql 				= "SELECT * FROM `survey01_lokasi`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('laporan_per_lokasi',$data);
	}
}

