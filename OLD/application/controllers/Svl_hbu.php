<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Svl_hbu extends CI_Controller {

	public function index(){

		$sql_where 	= "";
		$sql_and 	= "";
		//if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		//}

		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$lokasi' $sql_and";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row(); 

			$sql 				= "SELECT * FROM `survey07_hbu` WHERE id_data = '$lokasi' $sql_and";
			
		}else{
			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}
		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_hbu',$data);
	}

	function db_master(){
		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}

		//$sql 					= "SELECT * FROM `groups` $sql_where_g";
        $sql 					= "SELECT `groups`.`id`,
										`survey08_pembanding`.`group_id`, 
										`survey08_pembanding`.`rangka` AS `description` 
										FROM `survey08_pembanding` 
										INNER JOIN `groups` ON `survey08_pembanding`.`group_id` = `groups`.`id`
										$sql_where_g ";
		$data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db; 

		$sql 					= "SELECT * FROM `users` $sql_where_u";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db; 

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey07_hbu` WHERE id_data_hbu = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		return $data;
	}

	function add(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Tambah HBU';
		$this->load->view('svl_hbu_add',$data);
	}

	function view(){
		$data = $this->db_master();
		$data['title'] = 'Survey - View HBU';

		$this->load->view('svl_hbu_add',$data);
	}

	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Edit HBU';

		$this->load->view('svl_hbu_add',$data);
	}

	function delete(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Hapus Asset';

		$this->load->view('svl_hbu_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Asset';

		if($this->input->post('inp_id')){
			$id_child 	= $this->input->post('inp_id');
		}else{
			$id_child 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}
		$id 			= $id_child; 

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey07_hbu` WHERE id_data_hbu = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){

				$id_data 			= $this->input->post('inp_id_data');  
				$id_data_hbu 		= $id;  
				$id_asset_internal 	= $this->input->post('inp_id_asset_internal');  
				$inp_user_id 		= $this->input->post('inp_user_id'); 
				$inp_group_id 		= $this->input->post('inp_group_id'); 
				//$inp_id_data 	 	= $this->input->post('inp_id_data'); 
				$inp_penggu_lah 	= $this->input->post('inp_penggu_lah');
				$inp_penggu_lah_d 	= $this->input->post('inp_penggu_lah_d');
				$inp_peman_lah 		= $this->input->post('inp_peman_lah');
				$inp_peman_lah_d 	= $this->input->post('inp_peman_lah_d');
				$inp_keses_lah 		= $this->input->post('inp_keses_lah');
				$inp_keses_lah_d 	= $this->input->post('inp_keses_lah_d');
				$inp_optimalisasi 	= $this->input->post('inp_optimalisasi');
				$inp_optimalisasi_d = $this->input->post('inp_optimalisasi_d'); 
				$inp_jen_pro 		= $this->input->post('inp_jen_pro'); 
				$inp_nama 			= $this->input->post('inp_nama'); 
				$inp_luas_tan 		= $this->input->post('inp_luas_tan'); 
				$inp_luas_bang 		= $this->input->post('inp_luas_bang'); 
				$inp_jarak_tap 		= $this->input->post('inp_jarak_tap'); 
				$inp_jum_kam		= $this->input->post('inp_jum_kam'); 
				$inp_kel_hot 		= $this->input->post('inp_kel_hot'); 
				$inp_grad_aprt 		= $this->input->post('inp_grad_aprt'); 
				$inp_grad_perk		= $this->input->post('inp_grad_perk'); 
				$inp_grad_ruk 	 	= $this->input->post('inp_grad_ruk'); 
				$inp_grad_mall  	= $this->input->post('inp_grad_mall'); 
				$inp_grad_per 		= $this->input->post('inp_grad_per'); 
				$inp_kaw_wis 		= $this->input->post('inp_kaw_wis'); 
				$rekomendasi 		= $this->input->post('inp_rekomendasi');
				$nilai_investasi 	= $this->input->post('inp_nilai_investasi');
				$irr 			 	= $this->input->post('inp_irr');
				$npv 			 	= $this->input->post('inp_npv');
				
				$file_url 			= 'img_files/file/hbu/';
                    //$url          		= $file_url.basename($_FILES["inp_bukti_perhitungan"]["name"]);

                    //$response     		= get_headers($url, 1);

                    //$file_exists  		= (strpos($response[0], "404") === false);

 					 $targetfolder = $file_url.basename($_FILES['inp_laporan_lengkap']['name']);
 					 //$Filetype = strtolower(pathinfo($targetfolder,PATHINFO_EXTENSION));
					  if (is_uploaded_file($_FILES['inp_laporan_lengkap']['tmp_name'])) {

							 if ($_FILES['inp_laporan_lengkap']['type'] != "application/pdf") {
							 	echo "<p>Class notes must be uploaded in PDF format.</p>";
							 } else {
							 	$result = move_uploaded_file($_FILES['inp_laporan_lengkap']['tmp_name'], $targetfolder);
							 if ($result == 1){
							 		//echo "<p>Upload done .</p>";
							 	}else{
							 		//echo "<p>Sorry, Error happened while uploading . </p>";	
							 	} 
							} #endIF
					 } #endIF



				//$bukti_perhitungan		= $file_exists;

				$laporan_lengkap 	= $_FILES['inp_laporan_lengkap']['name']; 

				$inp_ket 	    	= $this->input->post('inp_ket'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey07_hbu (
												`id_data`, 
												`id_data_hbu`,
												`id_asset_internal`,
												`user_id`, 
												`group_id`, 
												`penggunaan_lahan`,
												`penggunaan_lahan_d`,
												`pemanfaatan_lahan`,
												`pemanfaatan_lahan_d`,
												`kesesuaian_lahan`,
												`kesesuaian_lahan_d`,
												`optimalisasi_asset`,
												`optimalisasi_asset_d`,
												`jenis_properti`, 
												`nama`, 
												`luas_tanah`, 
												`luas_bangunan`, 
												`jarak_tapak`, 
												`jumlah_kamar`, 
												`kelas_hotel`, 
												`grade_apartemen`, 
												`grade_perkantoran`, 
												`grade_ruko`, 
												`grade_mall`, 
												`grade_perumahan`, 
												`kawasan_wisata`,
												`rekomendasi`,
												`nilai_investasi`,
												`irr`,
												`npv`,
												`laporan_lengkap`, 
												`keterangan`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$id_data_hbu', 
												'$id_asset_internal', 
												'$inp_user_id', 
												'$inp_group_id', 
												'$inp_penggu_lah',
												'$inp_penggu_lah_d',
												'$inp_peman_lah',
												'$inp_peman_lah_d',
												'$inp_keses_lah',
												'$inp_keses_lah_d',
												'$inp_optimalisasi',
												'$inp_optimalisasi_d',
												'$inp_jen_pro', 
												'$inp_nama', 
												'$inp_luas_tan', 
												'$inp_luas_bang', 
												'$inp_jarak_tap', 
												'$inp_jum_kam', 
												'$inp_kel_hot', 
												'$inp_grad_aprt', 
												'$inp_grad_perk', 
												'$inp_grad_ruk', 
												'$inp_grad_mall', 
												'$inp_grad_per', 
												'$inp_kaw_wis', 
												'$rekomendasi',
												'$nilai_investasi',
												'$irr',
												'$npv',
												'$laporan_lengkap',
												'$inp_ket', 
												'$approval', 
												'$respond'
									);
					";

			        if($this->db->query($sql)){
						if($id){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

		$this->load->view('svl_hbu_add',$data);

	}
}
