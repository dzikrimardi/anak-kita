<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svs_status extends CI_Controller {

	public function index(){

		$data['title'] = 'Survey - Status';

		$sql 				= "SELECT * FROM `survey01_lokasi`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svs_status',$data);
	}

	function db_master(){

		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}

		$sql 					= "SELECT * FROM `groups` $sql_where_g ";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db; 

		$sql 					= "SELECT * FROM `users` $sql_where_u ";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db; 
		return $data;
	}

	/*
	function add(){
		$data['title'] = 'Survey - Tambah Lokasi';
		$this->load->view('svs_status_add',$data);
	}
	*/
	/*
	function view(){
		$data['title'] = 'Survey - View Lokasi';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svs_status_add',$data);
	}
	*/
	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Edit Status';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svs_status_add',$data);
	}
	/*
	function delete(){
		$data['title'] = 'Survey - Hapus Lokasi';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svs_status_add',$data);
	}
	*/
	function action(){

		$data['title'] = 'Survey - Lokasi';

		if($this->input->post('inp_id_data')){
			$id 	= $this->input->post('inp_id_data');
		}else{
			$id 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey01_lokasi` WHERE id_data = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){

			$id_data 			= $id; 
			$group_id 			= $this->input->post('inp_group_id'); 
			$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
			$longitude 			= $this->input->post('inp_longitude'); 
			$latitude 			= $this->input->post('inp_latitude'); 
			$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
			$pic 				= $this->input->post('inp_pic'); 
			$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
			$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
			$nama_asset 		= $this->input->post('inp_nama_asset'); 
			$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
			$banjir				= $this->input->post('inp_banjir'); 
			$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
			$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
			$kemudahan 			= $this->input->post('inp_kemudahan'); 
			$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
			$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
			$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
			$transport_01 		= $this->input->post('inp_transport_01'); 
			$transport_02 		= $this->input->post('inp_transport_02'); 
			$alternatif1 		= $this->input->post('inp_alternatif1'); 
			$alternatif2 		= $this->input->post('inp_alternatif2'); 
			$alternatif3 		= $this->input->post('inp_alternatif3'); 
			$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
			$approval 			= $this->input->post('inp_approval'); 
			$status 			= $this->input->post('inp_status'); 
			$respond 			= $this->input->post('inp_respond');

			$sql	= "	REPLACE INTO 
							survey01_lokasi (
											`id_data`, 
											`group_id`, 
											`kode_tapak`, 
											`longitude`, 
											`latitude`, 
											`tgl_survey`, 
											`pic`, 
											`nama_bumn`, 
											`no_tgl_penetapan`, 
											`nama_asset`, 
											`nomor_asset`, 
											`banjir`, 
											`jarak_tapak_1`, 
											`jarak_tapak_2`, 
											`kemudahan`, 
											`transportasi_umum`, 
											`jarak_ke_terminal`, 
											`nama_terminal`, 
											`transport_01`, 
											`transport_02`, 
											`alternatif1`, 
											`alternatif2`, 
											`alternatif3`, 
											`sketsa_lokasi`, 
											`approval`, 
											`status`, 
											`respond`
							) VALUES (
											'$id_data', 
											'$group_id', 
											'$kode_tapak', 
											'$longitude', 
											'$latitude', 
											'$tgl_survey', 
											'$pic', 
											'$nama_bumn', 
											'$no_tgl_penetapan', 
											'$nama_asset', 
											'$nomor_asset', 
											'$banjir', 
											'$jarak_tapak_1', 
											'$jarak_tapak_2', 
											'$kemudahan', 
											'$transportasi_umum', 
											'$jarak_ke_terminal', 
											'$nama_terminal', 
											'$transport_01', 
											'$transport_02', 
											'$alternatif1', 
											'$alternatif2', 
											'$alternatif3', 
											'$sketsa_lokasi', 
											'$approval', 
											'$status', 
											'$respond'
								);
				";
		        if($this->db->query($sql)){
					if($this->input->post('inp_id_data')){
			        	$data['exec_query'] = 'update';
			        }else{
			        	$data['exec_query'] = 'ok';
			        }
		        }else{
		        	$data['exec_query'] = 'failed';
		        }

		}

		$this->load->view('svs_status_add',$data);

	}
}
