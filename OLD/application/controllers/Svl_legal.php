<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_legal extends CI_Controller {



	public function index(){



		$sql_where 	= "";

		$sql_and 	= "";

		//if(!$this->session->userdata('administrator') == '1'){

			$user_id 		= $this->session->userdata('user_id');

			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		//}



		$lokasi 		= $this->input->get('lokasi');

		if($lokasi){

			$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$lokasi' $sql_and";

	        $data_db 			= $this->db->query($sql);

	        $data['data_db_r'] 	= $data_db->row(); 


			$sql 				= "SELECT * FROM `survey04_status` WHERE id_data = '$lokasi' $sql_and";

		}else{

			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";

		}

		//echo $sql ;exit;

        $data_db 			= $this->db->query($sql);

        $data['data_db'] 	= $data_db; 


		$this->load->view('svl_legal',$data);

	}



	function db_master(){

		$sql_where_g = "";

		$sql_where_u = "";

		if(!$this->session->userdata('administrator') == '1'){

			$user_id 		= $this->session->userdata('user_id');

			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;

		}



		$sql 					= "SELECT * FROM `groups` $sql_where_g";

        $data_db 				= $this->db->query($sql);

        $data['db_ms_groups'] 	= $data_db; 


		$sql 					= "SELECT * FROM `users` $sql_where_u";

        $data_db 				= $this->db->query($sql);

        $data['db_ms_users'] 	= $data_db; 


		$sql_and = "";

		if(!$this->session->userdata('administrator') == '1'){

			$user_id 		= $this->session->userdata('user_id');

			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}



		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey04_status` WHERE id_data_status = '$id' $sql_and";

        $data_db 			= $this->db->query($sql)->row();

        $data['data_db'] 	= $data_db; 


		return $data;

	}



	function add(){

		$data = $this->db_master();

		$data['title'] = 'Survey - Tambah Legal';



		$this->load->view('svl_legal_add',$data);

	}



	function view(){

		$data = $this->db_master();

		$data['title'] = 'Survey - View Legal';



		$this->load->view('svl_legal_add',$data);

	}



	function edit(){

		$data = $this->db_master();

		$data['title'] = 'Survey - Edit Legal';





		$this->load->view('svl_legal_add',$data);

	}



	function delete(){

		$data = $this->db_master();

		$data['title'] = 'Survey - Hapus Legal';



		$this->load->view('svl_legal_add',$data);

	}



	function action(){



		$data['title'] = 'Survey - Legal';



		if($this->input->post('inp_id')){

			$id_child 	= $this->input->post('inp_id');

		}else{

			$id_child 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$id 				= $id_child; 
		

		$data['exec_query'] = '';

		if($this->input->post('delete')){

			$sql = "DELETE FROM `survey04_status` WHERE id_data_status = '$id'";

	        if($this->db->query($sql)){

	        	$data['exec_query'] = 'delete';

	        }else{

	        	$data['exec_query'] = 'failed';



	        }

		}



		if($this->input->post() and !$this->input->post('delete')){



				$id_data			= $this->input->post('inp_id_data'); 
				$id_data_stat 	 	= $id; 


				$id_asset_internal 	= $this->input->post('inp_id_asset_internal'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); //201701022

				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$status_tanah 		= $this->input->post('inp_status_tanah'); 
				$stat_tanah_ajb_d  	= $this->input->post('inp_status_tanah_ajb_d'); 
				$status_tanah_desc1 = $this->input->post('inp_status_tanah_desc1'); 
				$status_tanah_desc2 = $this->input->post('inp_status_tanah_desc2'); 
				$luas_tapak 		= $this->input->post('inp_luas_tapak'); 
				$no_sur 		 	= $this->input->post('inp_nomor_surat'); 
				$kode_sertifikat	= $this->input->post('inp_kode_sertifikat'); 
				$nama_pemegang 		= $this->input->post('inp_nama_pemegang'); 
				$tgl_terbit			= $this->input->post('inp_tgl_terbit'); 
				$tgl_berakhir 		= $this->input->post('inp_tgl_berakhir'); 
				$no_tgl_sk  		= $this->input->post('inp_no_tgl_sk'); 
				$no_peta_pendaftaran= $this->input->post('inp_no_peta_pendaftaran'); 
				$no_gambar_situasi  = $this->input->post('inp_no_gambar_situasi'); 
				$tgl_surat_ukur    	= $this->input->post('inp_tgl_surat_ukur'); 
				$keadaan_tanah 	 	= $this->input->post('inp_keadaan_tanah'); 
				$per_sertifikat 	= $this->input->post('inp_perubahan_sertifikat'); 
				$jen_perubahan 	 	= $this->input->post('inp_jenis_perubahan'); 
				$pem_hak 	 		= $this->input->post('inp_pembebasan_hak'); 
				$jen_bukti 	 		= $this->input->post('inp_jenis_bukti'); 
				$no_bukti 	 		= $this->input->post('inp_nomor_bukti'); 
				$tanggal_bukti 	 	= $this->input->post('inp_tanggal_bukti'); 
				$nilai_wajar 	 	= $this->input->post('inp_nilai_wajar'); 
				$file_url 			= 'img_files/file/aset/';
                    //$url          		= $file_url.basename($_FILES["inp_bukti_perhitungan"]["name"]);

                    //$response     		= get_headers($url, 1);

                    //$file_exists  		= (strpos($response[0], "404") === false);

 					 $targetfolder = $file_url.basename($_FILES['inp_bukti_perhitungan']['name']);
 					 //$Filetype = strtolower(pathinfo($targetfolder,PATHINFO_EXTENSION));
					  if (is_uploaded_file($_FILES['inp_bukti_perhitungan']['tmp_name'])) {

							 if ($_FILES['inp_bukti_perhitungan']['type'] != "application/pdf") {
							 	echo "<p>Class notes must be uploaded in PDF format.</p>";
							 } else {
							 	$result = move_uploaded_file($_FILES['inp_bukti_perhitungan']['tmp_name'], $targetfolder);
							 if ($result == 1){
							 		//echo "<p>Upload done .</p>";
							 	}else{
							 		//echo "<p>Sorry, Error happened while uploading . </p>";	
							 	} 
							} #endIF
					 } #endIF



				//$bukti_perhitungan		= $file_exists;

				$bukti_perhitungan 	= $_FILES['inp_bukti_perhitungan']['name']; 
				$clear 			 	= $this->input->post('inp_clear'); 
				$clean 			 	= $this->input->post('inp_clean'); 
				$rekomendasi 		= $this->input->post('inp_rekomendasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');



				$sql	= "	REPLACE INTO 

								survey04_status (

												`id_data_status`, 
												`group_id`, 
												`user_id`, 
												`id_data`, 
												`id_asset_internal`, 
												`nomor_asset`, 
												`status_tanah`, 
												`status_tanah_ajb_d`, 
												`status_tanah_desc1`, 
												`status_tanah_desc2`, 
												`luas_tapak`, 
												`nomor_surat`, 
												`kode_sertifikat`, 
												`nama_pemegang`, 
												`tgl_terbit`, 
												`tgl_berakhir`, 
												`no_tgl_sk`, 
												`no_peta_pendaftaran`, 
												`no_gambar_situasi`, 
												`tgl_surat_ukur`, 
												`keadaan_tanah`, 
												`perubahan_sertifikat`, 
												`jenis_perubahan`, 
												`pembebasan_hak`, 
												`jenis_bukti`, 
												`nomor_bukti`, 
												`tanggal_bukti`, 
												`nilai_wajar`, 
												`bukti_perhitungan`,  
												`clear`, 
												`clean`, 
												`rekomendasi`, 
												`approval`, 
												`respond`

								) VALUES (

												'$id_data_stat', 
												'$group_id', 
												'$user_id', 
												'$id_data', 
												'$id_asset_internal', 
												'$nomor_asset',
												'$status_tanah', 
												'$stat_tanah_ajb_d', 
												'$status_tanah_desc1', 
												'$status_tanah_desc2', 
												'$luas_tapak', 
												'$no_sur', 
												'$kode_sertifikat', 
												'$nama_pemegang', 
												'$tgl_terbit', 
												'$tgl_berakhir', 
												'$no_tgl_sk', 
												'$no_peta_pendaftaran', 
												'$no_gambar_situasi', 
												'$tgl_surat_ukur', 
												'$keadaan_tanah', 
												'$per_sertifikat', 
												'$jen_perubahan', 
												'$pem_hak', 
												'$jen_bukti', 
												'$no_bukti', 
												'$tanggal_bukti',
												'$nilai_wajar',
												'$bukti_perhitungan',
												'$clear',
												'$clean',
												'$rekomendasi',
												'$approval', 
												'$respond'

									);

					";



			        if($this->db->query($sql)){

						if($id){

				        	$data['exec_query'] = 'update';

				        }else{

				        	$data['exec_query'] = 'ok';

				        }

			        }else{

			        	$data['exec_query'] = 'failed';

			        }



			}



		$this->load->view('svl_legal_add',$data);



	}

}

