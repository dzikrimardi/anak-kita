<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Visual extends CI_Controller {

	public function index(){
		//nothing
	}

	function produk(){

		$data['title'] = 'Survey - Data Foto/Denah';


		$sql 				= "SELECT * FROM `survey01_lokasi`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('visual_produk',$data);
	}
	function kategori(){

		$data['title'] = 'Survey - Kategori';


		$sql 				= "SELECT * FROM `survey01_lokasi`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('visual_kategori',$data);
	}
}

