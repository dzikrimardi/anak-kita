<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_exp_sarana extends CI_Controller {

	public function index(){

		$data['title'] = 'Survey - Sarana';

		$sql_where 	= "";
		$sql_and 	= "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= 	"SELECT *,
											(	SELECT count(*) 
												FROM survey03_pasar 
												WHERE 
													survey03_pasar.id_data = survey01_lokasi.id_data
											) AS c_data_r
									 FROM `survey01_lokasi`
									 WHERE 
									 	id_data = '$lokasi' $sql_and";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row(); 

			$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$lokasi' $sql_and";
		}else{
			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}
		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 


		$this->load->view('svl_exp_sarana',$data);
	}

}
