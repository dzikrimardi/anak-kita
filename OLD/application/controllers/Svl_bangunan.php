<?php



defined('BASEPATH') OR exit('No direct script access allowed');


class Svl_bangunan extends CI_Controller {



	public function index(){

		$data['title'] = 'Riwayat Imunisasi';


		$sql_where 	= "";
		$sql_and 	= "";
		//if(!$this->session->userdata('administrator') == '1'){

			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')";
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')";
		//}



		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$lokasi' $sql_and";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row();
			$sql 				= "	SELECT *,(
											SELECT sketsa_bangunan 
											from `survey14_ft_sketsa bangunan` 
											where `survey14_ft_sketsa bangunan`.id_bangunan = survey09_bangunan.id_bangunan 
											order by 'id_sketsa_bangunan' desc limit 1
										) as sketsa_lokasi
								 	FROM 
								 		`survey09_bangunan` 
								 	WHERE id_data = '$lokasi' $sql_and";
		}else{

			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}

		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db;

		$this->load->view('svl_bangunan',$data);
	}



	function db_master(){

		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){

			$user_id 			= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')";
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}



//		$sql 					= "SELECT * FROM `groups` $sql_where_g";
		$sql 					= "SELECT `groups`.`id`,
										`survey08_pembanding`.`group_id`, 
										`survey08_pembanding`.`rangka` AS `description` 
										FROM `survey08_pembanding` 
										INNER JOIN `groups` ON `survey08_pembanding`.`group_id` = `groups`.`id`
										$sql_where_g ";

        $data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db;

		$sql 					= "SELECT * FROM `users` $sql_where_u";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db;

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){

			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')";
		}

 

		$id 				= $this->uri->segment(3);
		$sql 				= "	SELECT * ,(
											SELECT sketsa_bangunan 
											from `survey14_ft_sketsa bangunan` 
											where `survey14_ft_sketsa bangunan`.id_bangunan = survey09_bangunan.id_bangunan 
											order by 'id_sketsa_bangunan' desc limit 1
										) as sketsa_lokasi
								FROM 
									`survey09_bangunan` 
								WHERE 
									id_bangunan = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db;

		return $data;
	}

	

	function add(){

		$data = $this->db_master();
		$data['title'] = 'Survey - Tambah Bangunan';


		$this->load->view('svl_bangunan_add',$data);
	}



	function view(){

		$data = $this->db_master();
		$data['title'] = 'Survey - View Bangunan';


		$this->load->view('svl_bangunan_add',$data);
	}



	function edit(){

		$data = $this->db_master();
		$data['title'] = 'Edit Vaksinasi';


		$this->load->view('svl_bangunan_add',$data);
	}



	function delete(){

		$data = $this->db_master();


		$this->load->view('svl_bangunan_add',$data);
	}



	function action(){



		$data['title'] = 'Riwayat Imunisasi';


		if($this->input->post('inp_id')){

			$id_child 	= $this->input->post('inp_id');
		}else{

			$id_child 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9);
		}

		$id 				= $id_child;

		$data['exec_query'] = '';
		if($this->input->post('delete')){

			$sql = "DELETE FROM `survey09_bangunan` WHERE id_bangunan = '$id'";
	        if($this->db->query($sql)){

	        	$data['exec_query'] = 'delete';
	        }else{

	        	$data['exec_query'] = 'failed';


	        }

		}



		if($this->input->post() and !$this->input->post('delete')){



				$inp_id_data 			= $this->input->post('inp_id_data');
				$inp_id_data_bangunan	= $id;  

				$inp_id_ass 		= $this->input->post('inp_id_ass');

				$inp_nom_ass 		= $this->input->post('inp_nom_ass');
				$inp_user_id 	 	= $this->input->post('inp_user_id');
				$inp_group_id 	 	= $this->input->post('inp_group_id');
				$inp_lok 			= $this->input->post('inp_lok');
				$inp_jen_bang 		= $this->input->post('inp_jen_bang');
				$inp_jen_bang_d 	= $this->input->post('inp_jen_bang_d');
				$inp_tip_bang 		= $this->input->post('inp_tip_bang');
				$inp_nama_pem 		= $this->input->post('inp_nama_pem');
				$inp_nom_ass 		= $this->input->post('inp_nom_ass');
				$inp_thn_bang		= $this->input->post('inp_thn_bang');
				$inp_luas_bang_01 	= $this->input->post('inp_luas_bang_01');
				$inp_luas_bang_02 	= $this->input->post('inp_luas_bang_02');
				$inp_jml_lan		= $this->input->post('inp_jml_lan');
				$inp_jml_lan_d 		= $this->input->post('inp_jml_lan_d');
				$inp_pond  			= $this->input->post('inp_pond');
				$inp_pond_d 		= $this->input->post('inp_pond_d');
				$inp_rang 			= $this->input->post('inp_rang');
				$inp_rang_d_1 	    = $this->input->post('inp_rang_d_1');
				$inp_rang_d_2 		= $this->input->post('inp_rang_d_2');
				$inp_dinding 		= $this->input->post('inp_dinding');
				$inp_dinding_d 		= $this->input->post('inp_dinding_d');
				$inp_atap 			= $this->input->post('inp_atap');
				$inp_atap_d 		= $this->input->post('inp_atap_d');
				$inp_rangka_at 		= $this->input->post('inp_rangka_at');
				$inp_rangka_at_d 	= $this->input->post('inp_rangka_at_d');
				$inp_langit_lang 	= $this->input->post('inp_langit_lang');
				$inp_langit_lang_d 	= $this->input->post('inp_langit_lang_d');
				$inp_lantai 		= $this->input->post('inp_lantai');
				$inp_lantai_d 		= $this->input->post('inp_lantai_d');
				$inp_partisi 		= $this->input->post('inp_partisi');
				$inp_part_d 		= $this->input->post('inp_part_d');
				$inp_pintu 			= $this->input->post('inp_pintu');
				$inp_pintu_d 		= $this->input->post('inp_pintu_d');
				$inp_rang_pint 		= $this->input->post('inp_rang_pint');
				$inp_rang_pint_d 	= $this->input->post('inp_rang_pint_d');
				$inp_jen 			= $this->input->post('inp_jen');
				$inp_rang_jen 		= $this->input->post('inp_rang_jen');
				$inp_rang_jen_d 	= $this->input->post('inp_rang_jen_d');
				$inp_listrik 		= $this->input->post('inp_listrik');
				$inp_listrik_watt 	= $this->input->post('inp_listrik_watt');
				$inp_air 			= $this->input->post('inp_air');
				$inp_air_dal 		= $this->input->post('inp_air_dal');
				$inp_plum 			= $this->input->post('inp_plum');
				$inp_telp 			= $this->input->post('inp_telp');
				$inp_telp_jen 		= $this->input->post('inp_telp_jen');
				$inp_ac_unit 		= $this->input->post('inp_ac_unit');
				$inp_ac_jen 		= $this->input->post('inp_ac_jen');
				$inp_ac_jen_d 		= $this->input->post('inp_ac_jen_d');
				$inp_ac_pk 			= $this->input->post('inp_ac_pk');
				$inp_pemadam 		= $this->input->post('inp_pemadam');
				$inp_penang_petir 	= $this->input->post('inp_penang_petir');
				$inp_penang_petir_d = $this->input->post('inp_penang_petir_d');
				$inp_lift_juml 		= $this->input->post('inp_lift_juml');
				$inp_lift_jen 		= $this->input->post('inp_lift_jen');
				$inp_lift_kap_org 	= $this->input->post('inp_lift_kap_org');
				$inp_jen 			= $this->input->post('inp_jen');
				$inp_lift_kap_brg 	= $this->input->post('inp_lift_kap_brg');
				$inp_lift_merk_orang= $this->input->post('inp_lift_merk_orang');
				$inp_lift_merk_barang = $this->input->post('inp_lift_merk_barang');
				$inp_elev_unit 		= $this->input->post('inp_elev_unit');
				$inp_elev_leb 		= $this->input->post('inp_elev_leb');
				$inp_elev_pan 		= $this->input->post('inp_elev_pan');
				$inp_elev_kap_hari 	= $this->input->post('inp_elev_kap_hari');
				$inp_kond_bang 		= $this->input->post('inp_kond_bang');
				$inp_stat_peng 		= $this->input->post('inp_stat_peng');
				$inp_pengguna 		= $this->input->post('inp_pengguna');
				$inp_rang_jen 		= $this->input->post('inp_rang_jen');
				$inp_penggu_bid 	= $this->input->post('inp_penggu_bid');
				$inp_penggu_bag 	= $this->input->post('inp_penggu_bag'); 				

				$inp_stat_asset 	= $this->input->post('inp_stat_asset');
				$inp_status_d 		= $this->input->post('inp_status_d');
				$inp_luas_bangunan 	= $this->input->post('inp_luas_bang');
				$inp_panja_bang 	= $this->input->post('inp_panja_bang');
				$inp_rang_jen 		= $this->input->post('inp_rang_jen');
				$inp_leb_bang 		= $this->input->post('inp_leb_bang');
				$inp_imb_no 		= $this->input->post('inp_imb_no');
				$inp_imb_tgl 		= $this->input->post('inp_imb_tgl');
				$inp_imb_kel 		= $this->input->post('inp_imb_kel');
				$inp_stat_per 		= $this->input->post('inp_stat_per');
				$inp_rang_jen 		= $this->input->post('inp_rang_jen');
				$inp_stat_per_d 	= $this->input->post('inp_stat_per_d');
				$inp_nilai_per 		= $this->input->post('inp_nilai_per');
				$inp_status_peng 	= $this->input->post('inp_status_peng');
				$inp_cat 			= $this->input->post('inp_cat');

				$file_url 			= 'img_files/file/bangunan/';
                    //$url          		= $file_url.basename($_FILES["inp_bukti_perhitungan"]["name"]);

                    //$response     		= get_headers($url, 1);

                    //$file_exists  		= (strpos($response[0], "404") === false);

 					// $targetfolder = $file_url.basename($_FILES['inp_sketsa_bang']['name']);
 					 //$Filetype = strtolower(pathinfo($targetfolder,PATHINFO_EXTENSION));
					//  if (is_uploaded_file($_FILES['inp_sketsa_bang']['tmp_name'])) {

					//		 if ($_FILES['inp_sketsa_bang']['type'] != "application/pdf") {
					//		 	echo "<p>Class notes must be uploaded in PDF format.</p>";
					//		 } else {
					//		 	$result = move_uploaded_file($_FILES['inp_sketsa_bang']['tmp_name'], $targetfolder);
					//		 if ($result == 1){
							 		//echo "<p>Upload done .</p>";
					//		 	}else{
							 		//echo "<p>Sorry, Error happened while uploading . </p>";	
					//		 	} 
					//		} #endIF
					// } #endIF



				//$bukti_perhitungan		= $file_exists;

				//$inp_sketsa_bang 	= $_FILES['inp_sketsa_bang']['name'];

				//$inp_sketsa_bang 	= $this->input->post('inp_sketsa_bang');
				$nilai_wajar 	 	= $this->input->post('inp_nilai_wajar');

				$file_url 			= 'img_files/file/bangunan/';
                    //$url          		= $file_url.basename($_FILES["inp_bukti_perhitungan"]["name"]);

                    //$response     		= get_headers($url, 1);

                    //$file_exists  		= (strpos($response[0], "404") === false);

 					 //$targetfolder = $file_url.basename($_FILES['inp_bukti_perhitungan']['name']);
 					 //$Filetype = strtolower(pathinfo($targetfolder,PATHINFO_EXTENSION));
					//  if (is_uploaded_file($_FILES['inp_bukti_perhitungan']['tmp_name'])) {

					//		 if ($_FILES['inp_bukti_perhitungan']['type'] != "application/pdf") {
					//		 	echo "<p>Class notes must be uploaded in PDF format.</p>";
					//		 } else {
					//		 	$result = move_uploaded_file($_FILES['inp_bukti_perhitungan']['tmp_name'], $targetfolder);
					//		 if ($result == 1){
							 		//echo "<p>Upload done .</p>";
					//		 	}else{
							 		//echo "<p>Sorry, Error happened while uploading . </p>";	
					//		 	} 
					//		} #endIF
					// } #endIF



				//$bukti_perhitungan		= $file_exists;

				//$bukti_perhitungan 	= $_FILES['inp_bukti_perhitungan']['name'];

				$inp_approval 		= $this->input->post('inp_approval');
				$inp_respond 		= $this->input->post('inp_respond');
				$group_id 		= $this->session->userdata('group_id');
				$user_id 		= $this->session->userdata('user_id');
				//echo "$group_id = " . $group_id . ", $user_id = " . $user_id ;
				//exit;


				$sql	= "	REPLACE INTO 

								survey09_bangunan (

												`id_data`,
												`id_bangunan`,
												`id_asset_internal`,
												`user_id`,
												`group_id`,
												`id_lokasi`,
												`jenis_bangunan`,
												`tipe_bangunan`,
												`nama_bangunan`,
												`nomor_asset`,
												`tahun_bangun`,
												`luas_bangunan_01`,
												`luas_bangunan_02`,
												`jml_lantai`,
												`jml_lantai_d`,
												`pondasi`,
												`pondasi_d`,
												`rangka`,
												`rangka_d1`,
												`rangka_d2`,
												`dinding`,
												`dinding_d`,
												`atap`,
												`atap_d`,
												`rangka_atap`,
												`rangka_atap_d`,
												`langit_langit`,
												`langit_langit_d`,
												`lantai`,
												`lantai_d`,
												`partisi`,
												`partisi_d`,
												`pintu`,
												`pintu_d`,
												`rangka_pintu`,
												`rangka_pintu_d`,
												`jendela`,
												`rangka_jendela`,
												`rangka_jendela_d`,
												`listrik`,
												`listrik_watt`,
												`air`,
												`air_dalam`,
												`plumbing`,
												`telepon`,
												`telepon_jenis`,
												`ac_unit`,
												`ac_jenis`,
												`ac_jenis_d`,
												`ac_pk`,
												`pemadam`,
												`penangkal_petir`,
												`penangkal_petir_d`,
												`lift_jml`,
												`lift_jenis`,
												`lift_kap_org`,
												`lift_kap_brg`,
												`lift_merek_orang`,
												`lift_merek_barang`,
												`elevator_unit`,
												`elevator_lebar`,
												`elevator_panjang`,
												`elevator_kap_hari`,
												`kondisi_bangunan`,
												`status_pengelolaan`,
												`pengguna`,
												`pengguna_bidang`,
												`pengguna_bagian`,
												`status_asset`,
												`status_d`,
												`panjang_bangunan`,
												`lebar_bangunan`,
												`luas_bangunan`,
												`imb_no`,
												`imb_tgl`,
												`imb_keluar`,
												`status_perolehan`,
												`status_perolehan_d`,
												`nilai_perolehan`,
												`status_penguasaan`,
												`catatan`,
												`nilai_wajar`,
												`approval`,
												`respond`

								) VALUES (

												'$inp_id_data',
												'$inp_id_data_bangunan',
												'$inp_id_ass',
												'$inp_user_id',
												'$inp_group_id',
												'$inp_lok',
												'$inp_jen_bang',
												'$inp_tip_bang',
												'$inp_nama_pem',
												'$inp_nom_ass',
												'$inp_thn_bang',
												'$inp_luas_bang_01',
												'$inp_luas_bang_02',
												'$inp_jml_lan',
												'$inp_jml_lan_d',
												'$inp_pond',
												'$inp_pond_d',
												'$inp_rang',
												'$inp_rang_d_1',
												'$inp_rang_d_2',
												'$inp_dinding',
												'$inp_dinding_d',
												'$inp_atap',
												'$inp_atap_d',
												'$inp_rangka_at',
												'$inp_rangka_at_d',
												'$inp_langit_lang',
												'$inp_langit_lang_d',
												'$inp_lantai',
												'$inp_lantai_d',
												'$inp_partisi',
												'$inp_part_d',
												'$inp_pintu',
												'$inp_pintu_d',
												'$inp_rang_pint',
												'$inp_rang_pint_d',
												'$inp_jen',
												'$inp_rang_jen',
												'$inp_rang_jen_d',
												'$inp_listrik',
												'$inp_listrik_watt',
												'$inp_air',
												'$inp_air_dal',
												'$inp_plum',
												'$inp_telp',
												'$inp_telp_jen',
												'$inp_ac_unit',
												'$inp_ac_jen',
												'$inp_ac_jen_d',
												'$inp_ac_pk',
												'$inp_pemadam',
												'$inp_penang_petir',
												'$inp_penang_petir_d',
												'$inp_lift_juml',
												'$inp_lift_jen',
												'$inp_lift_kap_org',
												'$inp_lift_kap_brg',
												'$inp_lift_merk_orang',
												'$inp_lift_merk_barang',
												'$inp_elev_unit',
												'$inp_elev_leb',
												'$inp_elev_pan',
												'$inp_elev_kap_hari',
												'$inp_kond_bang',
												'$inp_stat_peng',
												'$inp_pengguna',
												'$inp_penggu_bid',
												'$inp_penggu_bag',
												'$inp_stat_asset',
												'$inp_status_d',
												'$inp_panja_bang',
												'$inp_leb_bang',
												'$inp_luas_bangunan',
												'$inp_imb_no',
												'$inp_imb_tgl',
												'$inp_imb_kel',
												'$inp_stat_per',
												'$inp_stat_per_d',
												'$inp_nilai_per',
												'$inp_status_peng',
												'$inp_cat',
												'$nilai_wajar',
												'$inp_approval',
												'$inp_respond'

									);
					";
					//echo $sql;
			        if($this->db->query($sql)){

						if($id){

				        	$data['exec_query'] = 'update';
				        }else{

				        	$data['exec_query'] = 'ok';
				        }


					$file_url 			= base_url().'img_files/group_'.$inp_group_id.'/'.$this->input->post('inp_sketsa_lokasi_new');
                    $url          		= $file_url;
                    $response     		= get_headers($url, 1);
                    $file_exists  		= (strpos($response[0], "404") === false);
					$sketsa_lokasi		= $file_exists?$this->input->post('inp_sketsa_lokasi_new'):$this->input->post('inp_sketsa_lokasi');
		        	$sql_batas	= "	REPLACE INTO 
									`survey14_ft_sketsa bangunan` (
											`id_sketsa_bangunan`,
											`group_id`,
											`id_bangunan`, 
											`sketsa_bangunan`
									) VALUES (
											IFNULL((SELECT tb14.id_sketsa_bangunan FROM `survey14_ft_sketsa bangunan` AS tb14 where tb14.id_bangunan = '$inp_id_data_bangunan'),null),
											'$inp_group_id', 
											'$inp_id_data_bangunan', 
											'$sketsa_lokasi'
									);
					";
					$this->db->query($sql_batas);
			        }else{

			        	$data['exec_query'] = 'failed';
			        }



			}



		$this->load->view('svl_bangunan_add',$data);


	}

}

