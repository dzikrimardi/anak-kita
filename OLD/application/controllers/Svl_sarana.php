<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Svl_sarana extends CI_Controller {

	public function index(){
		$data['title'] = 'Survey - Sarana';

		$sql_where 	= "";
		$sql_and 	= "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= 	"SELECT *,
											(	SELECT count(*) 
												FROM survey03_pasar 
												WHERE 
													survey03_pasar.id_data = survey01_lokasi.id_data
											) AS c_data_r
									 FROM `survey01_lokasi`
									 WHERE 
									 	id_data = '$lokasi' $sql_and";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row(); 

			$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$lokasi' $sql_and";
		}else{
			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}
		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_sarana',$data);
	}

	function db_master(){
		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}

		$sql 					= "SELECT * FROM `groups` $sql_where_g";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db; 

		$sql 					= "SELECT * FROM `users` $sql_where_u";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db; 

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		return $data;
	}
	
	function add(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Tambah Sarana';

		$this->load->view('svl_sarana_add',$data);
	}

	function view(){
		$data = $this->db_master();
		$data['title'] = 'Survey - View Sarana';

		$this->load->view('svl_sarana_add',$data);
	}

	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Edit Sarana';

		$this->load->view('svl_sarana_add',$data);
	}

	function delete(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Hapus Sarana';

		$this->load->view('svl_sarana_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Sarana';

		if($this->input->post('inp_id_data')){
			$id 	= $this->input->post('inp_id_data');
		}else{
			$id 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey03_pasar` WHERE id_data = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){
			

				if($this->input->post('inp_id_data')){
					$id = $this->input->post('inp_id_data');
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$nama_pas 			= $this->input->post('inp_nama_pas'); 
				$jarak  			= $this->input->post('inp_jarak'); 

				//unused
				$sarana_um  		= $this->input->post('inp_sarana_um'); 

				$sarana_um_1  		= $this->input->post('inp_sarana_um_1'); 
				$sarana_um_2  		= $this->input->post('inp_sarana_um_2'); 
				$sarana_um_3  		= $this->input->post('inp_sarana_um_3'); 
				$sarana_um_4  		= $this->input->post('inp_sarana_um_4'); 
				$sarana_um_5  		= $this->input->post('inp_sarana_um_5'); 
				$sarana_um_6  		= $this->input->post('inp_sarana_um_6'); 
				$sarana_um_7  		= $this->input->post('inp_sarana_um_7'); 

				$sarana_um_desc 	= $this->input->post('inp_sarana_um_desc'); 

				//unused
				$sarana_ter 		= $this->input->post('inp_sarana_ter'); 

				$sarana_ter_1 		= $this->input->post('inp_sarana_ter_1'); 
				$sarana_ter_2 		= $this->input->post('inp_sarana_ter_2'); 
				$sarana_ter_3 		= $this->input->post('inp_sarana_ter_3'); 
				$sarana_ter_4 		= $this->input->post('inp_sarana_ter_4'); 
				$sarana_ter_5 		= $this->input->post('inp_sarana_ter_5'); 
				$sarana_ter_6 		= $this->input->post('inp_sarana_ter_6'); 
				$sarana_ter_7 		= $this->input->post('inp_sarana_ter_7'); 

				$sarana_ter_desc 	= $this->input->post('inp_sarana_ter_desc'); 
				
				$bentuk_tap 		= $this->input->post('inp_bentuk_tap'); 
				$bentuk_tap_d 		= $this->input->post('inp_bentuk_tap_d'); 
				$top				= $this->input->post('inp_top'); 
				$top_d 				= $this->input->post('inp_top_d'); 
				$jenis_tan  		= $this->input->post('inp_jenis_tan'); 
				$jenis_tan_d 		= $this->input->post('inp_jenis_tan_d'); 
				$elevasi        	= $this->input->post('inp_elevasi'); 
				$elevasi_m    	 	= $this->input->post('inp_elevasi_m'); 
				$bat_bar 	 		= $this->input->post('inp_bat_bar'); 
				$bat_tim 	 		= $this->input->post('inp_bat_tim'); 
				$bat_ut 	 		= $this->input->post('inp_bat_ut'); 
				$bat_sel 	 		= $this->input->post('inp_bat_sel'); 
				$view_ter 	 		= $this->input->post('inp_view_ter'); 
				$view_ket 	 		= $this->input->post('inp_view_ket'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey03_pasar (
												`id_data`, 
												`group_id`,
												`user_id`, 
												`kode_tapak`, 
												`nama_pasar`, 
												`jarak`, 
												`sarana_umum_11`, 
												`sarana_umum_12`, 
												`sarana_umum_13`, 
												`sarana_umum_14`, 
												`sarana_umum_15`, 
												`sarana_umum_16`, 
												`sarana_umum_17`, 
												`sarana_umum_desc`, 
												`sarana_tersambung_11`, 
												`sarana_tersambung_12`, 
												`sarana_tersambung_13`, 
												`sarana_tersambung_14`, 
												`sarana_tersambung_15`, 
												`sarana_tersambung_16`, 
												`sarana_tersambung_17`, 
												`sarana_tersam_desc`, 
												`bentuk_tapak`, 
												`bentuk_tapak_d`, 
												`topografi`, 
												`topografi_d`, 
												`jenis_tanah`, 
												`jenis_tanah_d`, 
												`elevasi`, 
												`elevasi_meter`, 
												`batas_barat`, 
												`batas_timur`, 
												`batas_utara`, 
												`batas_selatan`, 
												`view_terbaik`, 
												`view_keterangan`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$user_id', 
												'$kode_tapak', 
												'$nama_pas', 
												'$jarak', 
												'$sarana_um_1', 
												'$sarana_um_2', 
												'$sarana_um_3', 
												'$sarana_um_4', 
												'$sarana_um_5', 
												'$sarana_um_6', 
												'$sarana_um_7', 
												'$sarana_um_desc', 
												'$sarana_ter_1', 
												'$sarana_ter_2', 
												'$sarana_ter_3', 
												'$sarana_ter_4', 
												'$sarana_ter_5', 
												'$sarana_ter_6', 
												'$sarana_ter_7', 
												'$sarana_ter_desc', 
												'$bentuk_tap', 
												'$bentuk_tap_d', 
												'$top', 
												'$top_d', 
												'$jenis_tan', 
												'$jenis_tan_d', 
												'$elevasi', 
												'number_format($elevasi_m)', 
												'$bat_bar', 
												'$bat_tim', 
												'$bat_ut', 
												'$bat_sel', 
												'$view_ter', 
												'$view_ket', 
												'$approval', 
												'$respond'
									);
					";
					//echo $sql;
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}


		$this->load->view('svl_sarana_add',$data);

	}
}
