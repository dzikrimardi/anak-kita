<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Data_survey extends CI_Controller {

	public function index(){
		//nothing
	}

	function foto(){

		$data['title'] = 'Survey - Data Foto/Denah';
		/*

		$sql 				= "	SELECT *
										, (SELECT title FROM `bm_photos_categories` WHERE gallery.tipe = bm_photos_categories.id ) AS `type_name`
								FROM 
									gallery
								";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
        */

		$sql_where = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$sql 				= "	SELECT *,
										(SELECT name FROM groups WHERE groups.id = survey01_lokasi.group_id) AS group_name,
										(select batas_utara from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_u,
										(select batas_selatan from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_s,
										(select batas_timur from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_t,
										(select batas_barat from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_b 

								 FROM `survey01_lokasi` $sql_where ";
								 //echo $sql;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

        //echo $sql;        
		$this->load->view('data_survey_foto',$data);
	}
	function kategori(){

		$data['title'] = 'Survey - Kategori';


		$sql 				= "SELECT * FROM `bm_photos_categories` ORDER BY pos ASC";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('data_survey_kategori',$data);
	}
}

