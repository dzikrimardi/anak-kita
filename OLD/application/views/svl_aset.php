<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_aset/";
$exp_url = base_url()."svl_exp_aset";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Survey - Pengelolaan Aset
            <small>&nbsp;</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Survey</a></li>

            <?if($this->input->get('lokasi')){?>

            <li>
                <a href="<?=$back_url?>">
                    Lokasi
                </a>
            </li>
            <li class="active">Jalan</li>

            <?}else{?>

            <li class="active">Lokasi</li>

            <?}?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Unit</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Aset</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tanggal Survey</b> <a class="pull-right"><?=$data_db_r->tgl_survey?></a>
                </li>
              </ul>
                <center>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right" style="margin-left: 15px">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?if($this->input->get('lokasi')){?>
                        Pengelolaan Aset
                    <?}else{?>
                        Lokasi                
                    <?}?>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="150px">
                                <center>
                                    Detail        
                                </center>
                            </th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <i class="fa fa-files-o" aria-hidden="true"></i> : 
                                    <a href="<?=$back_url?>?lokasi=<?=$row->id_data?>">
                                     <?=$row->id_data?>
                                    </a>     
                                </center>                           
                            </td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                </table>
                <?}else{?>

                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>ID Kelola</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                           <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_kelola?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_kelola?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_kelola?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                          <td><?=$row->id_kelola?></td>
                          <td><? if ($row->status_kelola == 1) { 
                              echo "Dipakai Sendiri";
                            }else if ($row->status_kelola == 2) { 
                              echo "Disewakan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_kelola_d?></td>
                          <td><? if ($row->pengguna == 1) { 
                              echo "Pusat";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><? if ($row->status_manfaat == 1) { 
                              echo "Operasional";
                            }else if ($row->status_manfaat == 2) { 
                              echo "Tidak Operasional";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_manfaat_m?></td>
                          <td><? if ($row->status_legal == 1) { 
                              echo "Clean and Clear";
                            }else if ($row->status_legal == 2) { 
                              echo "Clear & UnClean";
                            }else if ($row->status_legal == 3) { 
                              echo "UnClean and Clear";
                            }else{ 
                              echo "UnClean and UnClear";
                            }?></td>
                          <td><?=$row->catatan_unclear?></td>
                          <td><?=$row->catatan_unclean?></td>
                          <td><? if ($row->sengketa == 1) { 
                              echo "Ada";
                            }else if ($row->sengketa == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->sengketa_d?></td>
                          <td><? if ($row->mediasi == 1) { 
                              echo "Ada";
                            }else if ($row->mediasi == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->mediasi_d?></td>
                          <td><? if ($row->gugatan == 1) { 
                              echo "Ada";
                            }else if ($row->gugatan == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->gugatan_d?></td>
                          <td><? if ($row->pemblokiran == 1) { 
                              echo "Ada";
                            }else if ($row->pemblokiran == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pemblokiran_d?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                       <?
                          }
                        }
                       ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>ID Kelola</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                <?}?>
            </div>
            <!-- /.box-body -->
            <?if(!$this->session->userdata('r2d2')){?>
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Pengelolaan Aset</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_kelola?></td>
                          <td><? if ($row->status_kelola == 1) { 
                              echo "Dipakai Sendiri";
                            }else if ($row->status_kelola == 2) { 
                              echo "Disewakan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_kelola_d?></td>
                          <td><? if ($row->pengguna == 1) { 
                              echo "Pusat";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><? if ($row->status_manfaat == 1) { 
                              echo "Operasional";
                            }else if ($row->status_manfaat == 2) { 
                              echo "Tidak Operasional";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_manfaat_m?></td>
                          <td><? if ($row->status_legal == 1) { 
                              echo "Clean and Clear";
                            }else if ($row->status_legal == 2) { 
                              echo "Clear & UnClean";
                            }else if ($row->status_legal == 3) { 
                              echo "UnClean and Clear";
                            }else{ 
                              echo "UnClean and UnClear";
                            }?></td>
                          <td><?=$row->catatan_unclear?></td>
                          <td><?=$row->catatan_unclean?></td>
                          <td><? if ($row->sengketa == 1) { 
                              echo "Ada";
                            }else if ($row->sengketa == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->sengketa_d?></td>
                          <td><? if ($row->mediasi == 1) { 
                              echo "Ada";
                            }else if ($row->mediasi == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->mediasi_d?></td>
                          <td><? if ($row->gugatan == 1) { 
                              echo "Ada";
                            }else if ($row->gugatan == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->gugatan_d?></td>
                          <td><? if ($row->pemblokiran == 1) { 
                              echo "Ada";
                            }else if ($row->pemblokiran == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pemblokiran_d?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                       <?
                          }
                        }
                       ?>
                    </tbody>
                    <!--
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                    -->
                </table>       
                <?}?>         
            </div>
            <?}?>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
