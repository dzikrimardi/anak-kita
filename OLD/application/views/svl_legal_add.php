<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
//$id         = $this->input->get('lokasi');
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data     = $this->input->get('lokasi');
$back_url   = base_url()."svl_legal/";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="">Legal</li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST" enctype="multipart/form-data">
            <div class="form-horizontal box-body">

                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                            Status Legal
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">User & Group</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->email ?'selected=""selected':''?> 
                                        value="<?=$row->email?>" 
                                        ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>           



                        <div class="col-sm-6 row">
                            <select name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$data_db->group_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->name?> ( <?=$row->description?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--//edit-->



                <?if(empty($id)){?>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Id Data Status</label>

                    <div class="col-sm-10">
                        <label for="" class="control-label">
                        <font style="opacity: 0.25">
                            (auto)
                        </font>
                        </label>
                    </div>
                </div>
                <?}?>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kode Asset Internal Detail</label>

                    <div class="col-sm-6">
                        <input name="inp_id_asset_internal" type="text" class="form-control" id="" placeholder="Kode Asset Internal Detail"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->id_asset_internal)?$data_db->id_asset_internal:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nomor Asset</label>

                    <div class="col-sm-6">
                        <input name="inp_nomor_asset" type="text" class="form-control" id="" placeholder="Kode Asset PJB"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_asset)?$data_db->nomor_asset:''?>">
                    </div>
                </div>

                <div class="box-header with-border">
                    <center>
                        <h3>&nbsp;
                            
                        </h3>
                    </center>
                </div>
                </br>

                <center>
                    <h2>
                        ANALISIS LEGAL TANAH
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Status Tanah</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_status_tanah" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '1')?'checked':''?> > SHM 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '2')?'checked':''?> > HGB 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '3')?'checked':''?> > HGU
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '4')?'checked':''?> > HP 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '5')?'checked':''?> > GIRIK 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="6" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '6')?'checked':''?> > SKT 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="7" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '7')?'checked':''?> > HPL 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_status_tanah" value="8" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_tanah)?$data_db->status_tanah:'') == '8')?'checked':''?> > AJB 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Deskripsi</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_status_tanah_ajb_d" type="text" class="form-control" id="" placeholder="Status Tanah Ajb d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->status_tanah_ajb_d)?$data_db->status_tanah_ajb_d:''?>">
                        </div>

                        <div class="col-sm-8">
                            <div class="col-sm-6">
                                <input name="inp_status_tanah_desc1" type="text" class="form-control" id="" placeholder="Status Tanah desc 1"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->status_tanah_desc1)?$data_db->status_tanah_desc1:''?>">
                            </div>
                            <div class="col-sm-6 row">
                                
                                <input name="inp_status_tanah_desc2" type="text" class="form-control" id="" placeholder="Status Tanah desc 2"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->status_tanah_desc2)?$data_db->status_tanah_desc2:''?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Luas Tapak</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_luas_tapak" type="text" class="form-control" placeholder=" 0.0 Meter"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->luas_tapak)?$data_db->luas_tapak:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>M2</small> | (Sesuai Sertifikat)</label>                        
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nomor Surat</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_nomor_surat" type="text" class="form-control" id="" placeholder="Status Tanah Ajb d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_surat)?$data_db->nomor_surat:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kode Sertifikat</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_kode_sertifikat" type="text" class="form-control" placeholder=" 0.0 Meter"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_sertifikat)?$data_db->kode_sertifikat:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>(17 Digit)</small></label>                        
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Pemegang Hak</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_nama_pemegang" type="text" class="form-control" id="" placeholder="Nama Pemegang Hak"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_pemegang)?$data_db->nama_pemegang:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tanggal Terbit Sertifikat</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_tgl_terbit" type="text" class="form-control" id="" placeholder="Tanggal Terbit Sertifikat"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tgl_terbit)?$data_db->tgl_terbit:''?>">
                        </div>

                        <div class="col-sm-8">
                            <div class="col-sm-6">
                                <label for="" class="pull-right control-label  label-title">Tanggal Berakhir Sertifikat</label>
                            </div>
                            <div class="col-sm-6 row">
                                <input name="inp_tgl_berakhir" type="text" class="form-control" id="" placeholder="Tanggal Berakhir Sertifikat"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tgl_berakhir)?$data_db->tgl_berakhir:''?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nomor Tanggal SK Hak Atas Tanah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_no_tgl_sk" type="text" class="form-control" id="" placeholder="Nomor Tanggal Sk"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_tgl_sk)?$data_db->no_tgl_sk:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nomor Peta Pendaftaran Tanah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_no_peta_pendaftaran" type="text" class="form-control" id="" placeholder="Nomor Peta Pend"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_peta_pendaftaran)?$data_db->no_peta_pendaftaran:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nomor Gambar Situasi</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_no_gambar_situasi" type="text" class="form-control" id="" placeholder="Nomor Gambar Situasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_gambar_situasi)?$data_db->no_gambar_situasi:''?>">
                        </div>

                        <div class="col-sm-8">
                            <div class="col-sm-6">
                                <label for="" class="pull-right control-label  label-title">Tanggal Surat Ukur</label>
                            </div>
                            <div class="col-sm-6 row">
                                <input name="inp_tgl_surat_ukur" type="text" class="form-control" id="" placeholder="Tanggal Surat Ukur"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tgl_surat_ukur)?$data_db->tgl_surat_ukur:''?>">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Keadaan Tanah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_keadaan_tanah" type="text" class="form-control" id="" placeholder="Keadaan Tanah"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->keadaan_tanah)?$data_db->keadaan_tanah:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2  control-label label-title">Perubahan Sertifikat</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                              <label>
                            <input name="inp_perubahan_sertifikat" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perubahan_sertifikat)?$data_db->perubahan_sertifikat:'') == '1')?'checked':''?> > Ada 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perubahan_sertifikat" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perubahan_sertifikat)?$data_db->perubahan_sertifikat:'') == '2')?'checked':''?> > Tidak 
                            &nbsp;&nbsp;&nbsp;

                        </label>
                        </div>
                        <div class="col-sm-6">
                             <label for="" class="control-label label-title"><small>Jika ada pilihan</small>(Pemisahan/Penggabungan/Penggantian)</label>                   
                        </div>

                    </div>
                </div>

                 <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Pembebasan Hak Atas Tanah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_pembebasan_hak" type="text" class="form-control" id="" placeholder="Pembebasan Hak Tanah"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pembebasan_hak)?$data_db->pembebasan_hak:''?>">
                        </div>
                    </div>
                </div>

                <center>
                    <h3>
                        Status Tanah Yang Belum Bersetifikat
                    </h3>
                </center>
                <br/>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Jenis Bukti Kepemilikan</label>

                    <div class="col-sm-3">
                        <select name="inp_jenis_bukti" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                            <option value="" >- Pilih Jenis Pembuktian -</option>
                            <option value="1" <?=((!empty($data_db->jenis_bukti)?$data_db->jenis_bukti:'') == '1')?'selected':''?> >AJB dari PPAT/Notaris/Camat/Kades/Lurah</option>
                            <option value="2" <?=((!empty($data_db->jenis_bukti)?$data_db->jenis_bukti:'') == '2')?'selected':''?> >Kwitansi Pembelian</option>
                            <option value="3" <?=((!empty($data_db->jenis_bukti)?$data_db->jenis_bukti:'') == '3')?'selected':''?> >Pernyataan Aset Oleh PJB (SK Ka BPN 500)</option>
                            <option value="4" <?=((!empty($data_db->jenis_bukti)?$data_db->jenis_bukti:'') == '4')?'selected':''?> >Pernyataan Pelepasan Hak</option>
                            <option value="5" <?=((!empty($data_db->jenis_bukti)?$data_db->jenis_bukti:'') == '5')?'selected':''?> >Bukti Lainnya (PPJB/Girik/ Letter C/ dll)</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nomor Bukti</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_nomor_bukti" type="text" class="form-control" id="" placeholder="Nomor Bukti"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_bukti)?$data_db->nomor_bukti:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tanggal Bukti</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_tanggal_bukti" type="text" class="form-control" id="" placeholder="Tanggal Bukti"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tanggal_bukti)?$data_db->tanggal_bukti:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nilai Wajar</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_nilai_wajar" type="text" class="form-control" id="" placeholder="Nilai Wajar Asset Tanah"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nilai_wajar)?$data_db->nilai_wajar:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Bukti Perhitungan</label>

                    <div class="col-sm-10">
                        <input name="inp_bukti_perhitungan" type="file" id="" placeholder="Sketsa"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->bukti_perhitungan)?$data_db->bukti_perhitungan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Clear</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_clear" type="text" class="form-control" id="" placeholder="Clear"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->clear)?$data_db->clear:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Clean</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_clean" type="text" class="form-control" id="" placeholder="Clean"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->clean)?$data_db->clean:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Rekomendasi</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_rekomendasi"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_rekomendasi"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->rekomendasi)?$data_db->rekomendasi:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_rekomendasi" 
                            name="inp_rekomendasi" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->rekomendasi)?$data_db->rekomendasi:''?></textarea>
                        <?}?>

                    </div>
                </div>


                 <hr/>
                    <center>
                        <h4 style="<?=($this->session->userdata('administrator') != '1')?'opacity:0.5"':'';?>">
                            (Administrator)
                        </h4>
                    </center>
                </br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input 
                                id="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="1" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> 
                                > Ya &nbsp;&nbsp;&nbsp;
                            <input 
                                id="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="0" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> 
                                > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input 
                                type="hidden" 
                                name="inp_approval" 
                                value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>
               


                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>               

            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
