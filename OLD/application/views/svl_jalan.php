<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$back_url = base_url()."svl_jalan/";
$exp_url = base_url()."svl_exp_jalan";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Di Jalan Depan Jalan Tapak
        <small>&nbsp;</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Survey</a></li>

            <?if($this->input->get('lokasi')){?>

            <li>
                <a href="<?=$back_url?>">
                    Lokasi
                </a>
            </li>
            <li class="active">Jalan</li>

            <?}else{?>

            <li class="active">Lokasi</li>

            <?}?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Unit</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Aset</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tanggal Survey</b> <a class="pull-right"><?=$data_db_r->tgl_survey?></a>
                </li>
              </ul>
                <center>
                    <?if(empty($data_db_r->c_data_r)){?>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <?}?>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-file-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?if($this->input->get('lokasi')){?>
                        Jalan Depan Tapak
                    <?}else{?>
                        Lokasi                
                    <?}?>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>
                                <center>
                                    Detail        
                                </center>
                            </th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <i class="fa fa-files-o" aria-hidden="true"></i> : 
                                    <a href="<?=$back_url?>?lokasi=<?=$row->id_data?>">
                                     <?=$row->id_data?>
                                    </a>     
                                </center>                           
                            </td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                </table>
                <?}else{?>

                    <?//var_dump($data_db_r)?>


                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>User</th>
                            <th>Jalan Di Depan Tapak</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_data?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_data?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->user_id?></td>
                            <td><? if ($row->ada_jalan == 1) { 
                              echo "Ya";
                            }else if ($row->ada_jalan == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><?=$row->lebar_jalan?> M</td>
                            <td><? if ($row->kelas_jalan == 1) { 
                              echo "Arteri Primer";
                            }else if ($row->kelas_jalan == 2) { 
                              echo "Arteri Sekunder";
                            }else if ($row->kelas_jalan == 3) { 
                              echo "Kolektor";
                            }else if ($row->kelas_jalan == 4) { 
                              echo "Lokal / Lingkungan";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->jumlah_lajur == 1) { 
                              echo "1 (Satu Lajur)";
                            }else if ($row->jumlah_lajur == 2) { 
                              echo "2 (Dua Lajur)";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->kondisi_lalin == 1) { 
                              echo "Tinggi";
                            }else if ($row->kondisi_lalin == 2) { 
                              echo "Cukup Tinggi";
                            }else if ($row->kondisi_lalin == 3) { 
                              echo "Rendah";
                            }else if ($row->kondisi_lalin == 3) { 
                              echo "Cukup Rendah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->permukaan_jalan == 1) { 
                              echo "Aspal Penetrasi";
                            }else if ($row->permukaan_jalan == 2) { 
                              echo "Aspal Hotmix";
                            }else if ($row->permukaan_jalan == 3) { 
                              echo "Beton";
                            }else if ($row->permukaan_jalan == 4) { 
                              echo "Paving";
                            }else if ($row->permukaan_jalan == 5) { 
                              echo "Perkerasan Sirtu";
                            }else if ($row->permukaan_jalan == 6) { 
                              echo "Perkerasan Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->kondisi == 1) { 
                              echo "Baik";
                            }else if ($row->kondisi == 2) { 
                              echo "Sedang";
                            }else if ($row->kondisi == 3) { 
                              echo "Rusak";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->drainase == 1) { 
                              echo "Terbuka";
                            }else if ($row->drainase == 2) { 
                              echo "Tertutup";
                            }else if ($row->drainase == 3) { 
                              echo "Didalam Tanah";
                            }else if ($row->drainase == 4) { 
                              echo "Didalam Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->penerangan == 1) { 
                              echo "Ada";
                            }else if ($row->penerangan == 2) { 
                              echo "Tidak Ada";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>User</th>
                            <th>Jalan Di Depan Tapak</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>

                <?}?>
            </div>
            <!-- /.box-body -->
            <?if(!$this->session->userdata('r2d2')){?>
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List User</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>User</th>
                            <th>Jalan Di Depan Tapak</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                             <td><?=$row->id_data?></td>
                            <td><?=$row->user_id?></td>
                            <td><? if ($row->ada_jalan == 1) { 
                              echo "Ya";
                            }else if ($row->ada_jalan == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><?=$row->lebar_jalan?> M</td>
                            <td><? if ($row->kelas_jalan == 1) { 
                              echo "Arteri Primer";
                            }else if ($row->kelas_jalan == 2) { 
                              echo "Arteri Sekunder";
                            }else if ($row->kelas_jalan == 3) { 
                              echo "Kolektor";
                            }else if ($row->kelas_jalan == 4) { 
                              echo "Lokal / Lingkungan";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->jumlah_lajur == 1) { 
                              echo "1 (Satu Lajur)";
                            }else if ($row->jumlah_lajur == 2) { 
                              echo "2 (Dua Lajur)";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->kondisi_lalin == 1) { 
                              echo "Tinggi";
                            }else if ($row->kondisi_lalin == 2) { 
                              echo "Cukup Tinggi";
                            }else if ($row->kondisi_lalin == 3) { 
                              echo "Rendah";
                            }else if ($row->kondisi_lalin == 3) { 
                              echo "Cukup Rendah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->permukaan_jalan == 1) { 
                              echo "Aspal Penetrasi";
                            }else if ($row->permukaan_jalan == 2) { 
                              echo "Aspal Hotmix";
                            }else if ($row->permukaan_jalan == 3) { 
                              echo "Beton";
                            }else if ($row->permukaan_jalan == 4) { 
                              echo "Paving";
                            }else if ($row->permukaan_jalan == 5) { 
                              echo "Perkerasan Sirtu";
                            }else if ($row->permukaan_jalan == 6) { 
                              echo "Perkerasan Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->kondisi == 1) { 
                              echo "Baik";
                            }else if ($row->kondisi == 2) { 
                              echo "Sedang";
                            }else if ($row->kondisi == 3) { 
                              echo "Rusak";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->drainase == 1) { 
                              echo "Terbuka";
                            }else if ($row->drainase == 2) { 
                              echo "Tertutup";
                            }else if ($row->drainase == 3) { 
                              echo "Didalam Tanah";
                            }else if ($row->drainase == 4) { 
                              echo "Didalam Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->penerangan == 1) { 
                              echo "Ada";
                            }else if ($row->penerangan == 2) { 
                              echo "Tidak Ada";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                           <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>User</th>
                            <th>Jalan Di Depan Tapak</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>     
                <?}?>           
            </div>
            <?}?>           
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
