<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}

$id_data    = $this->input->get('lokasi');
$back_url   = base_url()."svl_aset/";
?>



<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Personnel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Personnel</li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST">
            <div class="form-horizontal box-body">

                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                        Aset
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">User & Group</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->email ?'selected=""selected':''?> 
                                        value="<?=$row->email?>" 
                                        ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>           



                        <div class="col-sm-6 row">
                            <select name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$data_db->group_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->name?> ( <?=$row->description?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--//edit-->



                <?if(empty($id)){?>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Id Kelola</label>

                    <div class="col-sm-10">
                        <label for="inputEmail3" class="control-label">
                        <font style="opacity: 0.25">
                            (auto)
                        </font>
                        </label>
                    </div>
                </div>
                <?}?>


                <div class="box-header with-border">
                <center>
                    <h3>
                        Data Pengelolaan Aset
                    </h3>
                </center>

                </div>
                </br>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Status Pengelolaan</label>

                    <div class="col-sm-3">
                        <label>
                            <input name="inp_stat_kel" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_kelola)?$data_db->status_kelola:'') == '1')?'checked':''?> > Dipakai Sendiri 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_stat_kel" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_kelola)?$data_db->status_kelola:'') == '2')?'checked':''?> > Disewakan 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                    <div class="col-sm-2 row">
                            <input name="inp_stat_kel_d" type="text" class="form-control" id="inputEmail3" placeholder="Status Kelola d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->status_kelola_d)?$data_db->status_kelola_d:''?>">
                        </div>&nbsp;&nbsp;&nbsp;
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Pengguna</label>

                    <div class="col-sm-1">
                        <label>
                            <input name="inp_peng" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->pengguna)?$data_db->pengguna:'') == '1')?'checked':''?> > Pusat 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                    <div class="col-sm-2 row">
                            <input name="inp_peng_bid" type="text" class="form-control" id="inputEmail3" placeholder="Pengguna Bidang.."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pengguna_bidang)?$data_db->pengguna_bidang:''?>">
                        </div>&nbsp;&nbsp;&nbsp;
                        <div class="col-sm-2 row" style="margin-left: 5px">
                            <input name="inp_peng_bag" type="text" class="form-control" id="inputEmail3" placeholder="Pengguna Bagian.."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pengguna_bagian)?$data_db->pengguna_bagian:''?>">
                        </div>
                </div>

                <div class="box-header with-border">
                <center>
                    <h3>
                        Status Pemanfaatan
                    </h3>
                </center>
            </div>
        </br>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Status Pemanfaatan</label>

                    <div class="col-sm-4">
                        <label>
                            <input name="inp_stat_m" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_manfaat)?$data_db->status_manfaat:'') == '1')?'checked':''?> > Operasional 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_stat_m" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_manfaat)?$data_db->status_manfaat:'') == '2')?'checked':''?> > Tidak Operasional 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                    <div class="col-sm-4 row">
                            <input name="inp_stat_man_m" type="text" class="form-control" id="inputEmail3" placeholder="Tidak Operasional / Sisa Sewa (Angka)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->status_manfaat_m)?$data_db->status_manfaat_m:''?>">
                        </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Status Legal</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_stat_leg" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_legal)?$data_db->status_legal:'') == '1')?'checked':''?> > Clean and Clear
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_stat_leg" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_legal)?$data_db->status_legal:'') == '2')?'checked':''?> > Clear & UnClean
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_stat_leg" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_legal)?$data_db->status_legal:'') == '3')?'checked':''?> > UnClean and Clear
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_stat_leg" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->status_legal)?$data_db->status_legal:'') == '4')?'checked':''?> > UnClean and UnClear
                            &nbsp;&nbsp;&nbsp;
                        </label>
                        <br/>
                        <small class="label-title">* Keterangan : Asset didukung legalitas / bukti kepemilikan yang sah dan secara fisik dikuasai oleh perusahaan</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Catatan un Clear</label>

                        <div class="col-sm-10">
                        <textarea name="inp_cat_un_clear" id="inp_cat_un_clear" class="form-control" <?=empty($edit)?'disabled="disabled"':'';?>><?=!empty($data_db->catatan_unclear)?$data_db->catatan_unclear:''?></textarea>
                    </div>

                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Catatan un Clean</label>

                        <div class="col-sm-10">
                        <textarea name="inp_cat_un_clean" id="inp_cat_un_clean" class="form-control" <?=empty($edit)?'disabled="disabled"':'';?>><?=!empty($data_db->catatan_unclean)?$data_db->catatan_unclean:''?></textarea>
                        <small class="label-title">* Catatan : Apabila tidak muat dicatat di baliknya</small>
                    </div>

                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Sengketa</label>

                    <div class="col-sm-2">
                            <input name="inp_seng" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sengketa)?$data_db->sengketa:'') == '1')?'checked':''?> > Ada
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_seng" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sengketa)?$data_db->sengketa:'') == '2')?'checked':''?> > Tidak
                            &nbsp;&nbsp;&nbsp;
                    </div>

                    <div class="col-sm-3 row">
                            <input name="inp_seng_d" type="text" class="form-control" placeholder="Penjelasan.."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sengketa_d)?$data_db->sengketa_d:''?>" >
                        </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Mediasi Sengketa</label>

                    <div class="col-sm-2">
                            <input name="inp_med" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->mediasi)?$data_db->mediasi:'') == '1')?'checked':''?> > Ada
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_med" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->mediasi)?$data_db->mediasi:'') == '2')?'checked':''?> > Tidak
                            &nbsp;&nbsp;&nbsp;
                    </div>

                    <div class="col-sm-3 row">
                            <input name="inp_med_d" type="text" class="form-control" placeholder="Penjelasan.."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->mediasi_d)?$data_db->mediasi_d:''?>" >
                        </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Gugatan Perkara Pengadilan</label>

                    <div class="col-sm-2">
                            <input name="inp_gug" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->gugatan)?$data_db->gugatan:'') == '1')?'checked':''?> > Ada
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_gug" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->gugatan)?$data_db->gugatan:'') == '2')?'checked':''?> > Tidak
                            &nbsp;&nbsp;&nbsp;
                    </div>

                    <div class="col-sm-3 row">
                            <input name="inp_gug_d" type="text" class="form-control" placeholder="Penjelasan.."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->gugatan_d)?$data_db->gugatan_d:''?>" >
                        </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Pemblokiran Sertifikat di BPN</label>

                    <div class="col-sm-2">
                            <input name="inp_pem" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->pemblokiran)?$data_db->pemblokiran:'') == '1')?'checked':''?> > Ada
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_pem" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->pemblokiran)?$data_db->pemblokiran:'') == '2')?'checked':''?> > Tidak
                            &nbsp;&nbsp;&nbsp;
                    </div>

                    <div class="col-sm-3 row">
                            <input name="inp_pem_d" type="text" class="form-control" placeholder="Penjelasan.."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pemblokiran_d)?$data_db->pemblokiran_d:''?>" >
                        </div>
                </div>

                <hr/>
                    <center>
                        <h4 style="<?=($this->session->userdata('administrator') != '1')?'opacity:0.5"':'';?>">
                            (Administrator)
                        </h4>
                    </center>
                </br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="1" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> 
                                > Ya &nbsp;&nbsp;&nbsp;
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="0" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> 
                                > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input 
                                type="hidden" 
                                name="inp_approval" 
                                value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>

                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>

          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>

        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
