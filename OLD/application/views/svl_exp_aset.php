<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_asset.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Kelola</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                           <td><?=$i?></td>
                          <td><?=$row->id_kelola?></td>
                          <td><? if ($row->status_kelola == 1) { 
                              echo "Dipakai Sendiri";
                            }else if ($row->status_kelola == 2) { 
                              echo "Disewakan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_kelola_d?></td>
                          <td><? if ($row->pengguna == 1) { 
                              echo "Pusat";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><? if ($row->status_manfaat == 1) { 
                              echo "Operasional";
                            }else if ($row->status_manfaat == 2) { 
                              echo "Tidak Operasional";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_manfaat_m?></td>
                          <td><? if ($row->status_legal == 1) { 
                              echo "Clean and Clear";
                            }else if ($row->status_legal == 2) { 
                              echo "Clear & UnClean";
                            }else if ($row->status_legal == 3) { 
                              echo "UnClean and Clear";
                            }else{ 
                              echo "UnClean and UnClear";
                            }?></td>
                          <td><?=$row->catatan_unclear?></td>
                          <td><?=$row->catatan_unclean?></td>
                          <td><? if ($row->sengketa == 1) { 
                              echo "Ada";
                            }else if ($row->sengketa == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->sengketa_d?></td>
                          <td><? if ($row->mediasi == 1) { 
                              echo "Ada";
                            }else if ($row->mediasi == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->mediasi_d?></td>
                          <td><? if ($row->gugatan == 1) { 
                              echo "Ada";
                            }else if ($row->gugatan == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->gugatan_d?></td>
                          <td><? if ($row->pemblokiran == 1) { 
                              echo "Ada";
                            }else if ($row->pemblokiran == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pemblokiran_d?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                       <?
                          }
                        }
                       ?>
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
