<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?//php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<!-- //20171228 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">
<style type="text/css">    
    /*iframe*/
    #upload_frame {
        border:0px;
        height:40px;
        width:400px;
        display:none;
    }

    #progress_container {
        width: 300px; 
        height: 30px; 
        border: 1px solid #CCCCCC; 
        background-color:#EBEBEB;
        display: block; 
        margin:5px 0px -15px 0px;
    }

    #progress_bar {
        position: relative; 
        height: 30px; 
        background-color: #F3631C; 
        width: 0%; 
        z-index:10; 
    }

    #progress_completed {
        font-size:16px; 
        z-index:40; 
        line-height:30px; 
        padding-left:4px; 
        color:#FFFFFF;
    }    
</style>

<?//php $this->load->view('inc/home_header_meta_title'); ?>
<?//php $this->load->view('inc/home_header_body'); ?>
<?php  
//get unique id 
$group_id   = $this->input->post('group_id')?$this->input->post('group_id'):$this->input->get('group_id');
$file_id    = $this->input->post('file_id')?$this->input->post('file_id'):$this->input->get('file_id');

$up_id      = uniqid();  
$folder     = "img_files/group_";  
$folder     = $folder.$group_id.'/';
?> 

<?
$id         = $this->uri->segment(3);
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}

$back_url   = base_url()."svl_lokasi/upload?file_id=".$file_id;
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?//php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <!--
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey Lapangan</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Lokasi</a></li>
        <li class="active">Add</li>
      </ol>
    </section>
 -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

            <?php if(!empty($_GET['file'])){?>
                <?//=$folder.$_GET['file']?>
                <center>
                <img src="<?=base_url().$folder.$_GET['file']?>" style="width:320px">
                <br/><br/>
                <a href="<?=$back_url?>">Ulangi?</a>
                </center>
            <?php }?>

                <form action="" method="post" enctype="multipart/form-data" name="form_upload" id="form_upload"> 
                    <?//php print_r($this->input->post())?>


                <input type="hidden" name="group_id" id="group_id" value="<?=$this->input->post('group_id')?$this->input->post('group_id'):$this->input->get('group_id')?>">
                <input type="hidden" name="file_id" id="file_id" value="<?=$this->input->post('file_id')?$this->input->post('file_id'):$this->input->get('file_id')?>">






            <?php 

            //process the forms and upload the files 
            if (!empty($_FILES['file'])) { 
                //specify folder for file upload 
                if(!file_exists($folder)){
                    mkdir($folder);
                }

                //specify redirect URL 
                $redirect = $back_url."upload?status=success&file="; 

                //upload the file 
                //move_uploaded_file($_FILES["file"]["tmp_name"], "$folder" . $_FILES["file"]["name"]); 
                $exp_file_name  = explode('.', $_FILES["file"]["name"]);
                $file_name      = $this->input->post('file_id');//.'.'.$exp_file_name[count($exp_file_name)-1];
                $folder         = $folder.$file_name;
                
                $redirect       = $redirect.$file_name.'&file_id='.$this->input->post('file_id').'&group_id='.$this->input->post('group_id');
                move_uploaded_file($_FILES["file"]["tmp_name"], $folder); 

                //do whatever else needs to be done (insert information into database, etc...) 
                //var_dump($folder);
                //print_r($_FILES);
                //redirect user 
                header('Location: '.$redirect); die; 
            } 
            // 

            ?> 

            <?if (empty($_GET['file'])) {?>
            <div> 
              <?php if (isset($_GET['success'])) { ?> 
              <span class="notice">Your file has been uploaded.</span> 
              <?php } ?> 
                Pilih file / Ambil dari kamera<br /> 

            <!--APC hidden field--> 
                <input type="hidden" name="APC_UPLOAD_PROGRESS" id="progress_key" value="<?php echo $up_id; ?>"/> 
            <!----> 

                <input name="file" type="file" id="file" class="btn btn-lg row" size="30"/> 

            <!--Include the iframe--> 
                <iframe id="upload_frame" name="upload_frame" frameborder="0" border="0" src="" scrolling="no" scrollbar="no" > </iframe> 
                <br /> 
            <!----> 

                <input name="Submit" type="submit" id="submit" value="Upload"  class="btn btn-small btn-block btn-info" /> 
              </div> 
            <?php }?>









         
                </form> 
            </div>
            <!-- /.col -->
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- //20171228 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<!--display bar only if file is chosen--> 
<script> 

$(document).ready(function() {  
// 

//show the progress bar only if a file field was clicked 
    var show_bar = 0; 
    $('input[type="file"]').click(function(){ 
        show_bar = 1; 
    }); 

//show iframe on form submit 
///*
    $("#form_upload").submit(function(){ 

        if (show_bar === 1) {  
            $('#upload_frame').show(); 
            function set () { 
                $('#upload_frame').attr('src','<?=$back_url?>upload_frame?up_id=<?php echo $up_id; ?>'); 
            } 
            setTimeout(set); 
        } 
    }); 
// */
//$("#view_group").val("grp");
//$("#group_id").val(  parent.document.getElementById("group_id").value );

$("#group_id").val( parent.document.getElementById("group_id").value );

<?if($this->input->get('file')){?>
parent.document.getElementById("inp_sketsa_lokasi").value = "<?=$this->input->get('file')?>";
//$("#group_id").val( parent.document.getElementById("group_id").value );
<?}?>
/*
<?if($this->input->get('file') and $this->input->get('batas') == 'u'){?>
//parent.document.getElementById("inp_sketsa_lokasi_u").value = "<?=$this->input->get('file')?>";
//$("#group_id").val( parent.document.getElementById("group_id").value );
<?}?>

<?if($this->input->get('file') and $this->input->get('batas') == 's'){?>
//parent.document.getElementById("inp_sketsa_lokasi_s").value = "<?=$this->input->get('file')?>";
//$("#group_id").val( parent.document.getElementById("group_id").value );
<?}?>

<?if($this->input->get('file') and $this->input->get('batas') == 't'){?>
//parent.document.getElementById("inp_sketsa_lokasi_t").value = "<?=$this->input->get('file')?>";
//$("#group_id").val( parent.document.getElementById("group_id").value );
<?}?>

<?if($this->input->get('file') and $this->input->get('batas') == 'b'){?>
//parent.document.getElementById("inp_sketsa_lokasi_b").value = "<?=$this->input->get('file')?>";
//$("#group_id").val( parent.document.getElementById("group_id").value );
<?}?>
*/
//$('#file').change(function() {
//    $('#form_upload').submit();
//});

document.getElementById("file").onchange = function() {
    document.getElementById("form_upload").submit();
};
}); 
</script> 
<?php $this->load->view('inc/home_footer_body'); ?>
