<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_sarana.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->nama_pasar?></td>
                          <td><?=$row->jarak?> M</td>
                          <td><? 
                            /*
                            if ($row->sarana_umum == 0) { 
                              echo "PLN";
                            }else if ($row->sarana_umum == 1) { 
                              echo "Genset";
                            }else if ($row->sarana_umum == 2) { 
                              echo "PAM";
                            }else if ($row->sarana_umum == 3) { 
                              echo "Sumur";
                            }else if ($row->sarana_umum == 4) { 
                              echo "Telepon";
                            }else if ($row->sarana_umum == 5) { 
                              echo "Gas";
                            }else{ 
                              echo "Lainnya";
                            }
                            */
                            ?>
                            <?=(!empty($row->sarana_umum_11)?'-&nbsp;PLN <br/>':'')?>
                            <?=(!empty($row->sarana_umum_12)?'-&nbsp;Genset <br/>':'')?>
                            <?=(!empty($row->sarana_umum_13)?'-&nbsp;PAM <br/>':'')?>
                            <?=(!empty($row->sarana_umum_14)?'-&nbsp;Sumur <br/>':'')?>
                            <?=(!empty($row->sarana_umum_15)?'-&nbsp;Telepon <br/>':'')?>
                            <?=(!empty($row->sarana_umum_16)?'-&nbsp;Gas <br/>':'')?>
                            <?=(!empty($row->sarana_umum_17)?'-&nbsp;Lainnya <br/>':'')?>
                            </td>
                          <td><?=$row->sarana_umum_desc?></td>
                          <td>
                            <?
                            /*
                            if ($row->sarana_tersambung == 0) { 
                              echo "PLN";
                            }else if ($row->sarana_tersambung == 1) { 
                              echo "Genset";
                            }else if ($row->sarana_tersambung == 2) { 
                              echo "PAM";
                            }else if ($row->sarana_tersambung == 3) { 
                              echo "Sumur";
                            }else if ($row->sarana_tersambung == 4) { 
                              echo "Telepon";
                            }else if ($row->sarana_tersambung == 5) { 
                              echo "Gas";
                            }else{ 
                              echo "Lainnya";
                            }
                            */
                            ?>
                            <?=(!empty($row->sarana_tersambung_11)?'-&nbsp;PLN <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_12)?'-&nbsp;Genset <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_13)?'-&nbsp;PAM <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_14)?'-&nbsp;Sumur <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_15)?'-&nbsp;Telepon <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_16)?'-&nbsp;Gas <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_17)?'-&nbsp;Lainnya&nbsp;('.$row->sarana_tersam_desc.') <br/> ':'')?>                                
                            </td>
                          <td><?=$row->sarana_tersam_desc?></td>
                          <td><? if ($row->bentuk_tapak == 1) { 
                              echo "Beraturan";
                            }else if ($row->bentuk_tapak == 2) { 
                              echo "Tidak Beraturan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->bentuk_tapak_d?></td>
                          <td><? if ($row->topografi == 1) { 
                              echo "Rata";
                            }else if ($row->topografi == 2) { 
                              echo "Landai";
                            }else if ($row->topografi == 3) { 
                              echo "Berbukit - bukit";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->topografi_d?></td>
                          <td><? if ($row->jenis_tanah == 1) { 
                              echo "Tanah Kering";
                            }else if ($row->jenis_tanah == 2) { 
                              echo "Tanah Sawah";
                            }else if ($row->jenis_tanah == 3) { 
                              echo "Rawa";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->jenis_tanah_d?></td>
                          <td><?=$row->elevasi?> M</td>
                          <td><?=$row->elevasi_meter?></td>
                          <td><?=$row->batas_barat?></td>
                          <td><?=$row->batas_timur?></td>
                          <td><?=$row->batas_utara?></td>
                          <td><?=$row->batas_selatan?></td>
                          <td><? if ($row->view_terbaik == 1) { 
                              echo "Utara";
                            }else if ($row->view_terbaik == 2) { 
                              echo "Selatan";
                            }else if ($row->view_terbaik == 3) { 
                              echo "Timur";
                            }else if ($row->view_terbaik == 4) { 
                              echo "Barat";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->view_keterangan?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?
                      }
                    }
                    ?>
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
