<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_hbu.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                         <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Asset Internal Detail</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Rekomendasi</th>
                            <th>Nilai Investasi</th>
                            <th>IRR</th>
                            <th>NPV</th>
                            <th>Laporan Lengkap</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->id_asset_internal?></td>
                          <td><? if ($row->jenis_properti == 1) { 
                              echo "Hotel";
                            }else if ($row->jenis_properti == 2) { 
                              echo "Apartemen";
                            }else if ($row->jenis_properti == 3) { 
                              echo "Gedung Perkantoran";
                            }else if ($row->jenis_properti == 4) { 
                              echo "Ruko";
                            }else if ($row->jenis_properti == 5) { 
                              echo "Mall";
                            }else if ($row->jenis_properti == 6) { 
                              echo "Perumahan";
                            }else if ($row->jenis_properti == 7) { 
                              echo "Kawasan Wisata";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->nama?></td>
                          <td><?=$row->luas_tanah?> M</td>
                          <td><?=$row->luas_bangunan?> M</td>
                          <td><?=$row->jarak_tapak?></td>
                          <td><?=$row->jumlah_kamar?></td>
                          <td><? if ($row->kelas_hotel == 1) { 
                              echo "Bintang Melati";
                            }else if ($row->kelas_hotel == 2) { 
                              echo "Bintang 2";
                            }else if ($row->kelas_hotel == 3) { 
                              echo "Bintang 3";
                            }else if ($row->kelas_hotel == 4) { 
                              echo "Bintang 4";
                            }else if ($row->kelas_hotel == 5) { 
                              echo "Bintang 5";
                            }?></td>
                          <td><? if ($row->grade_apartemen == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_apartemen == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_perkantoran == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_perkantoran == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_ruko == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_ruko == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_mall == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_mall == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_perumahan == 1) { 
                              echo "Mewah";
                            }else if ($row->grade_perumahan == 2) { 
                              echo "Menengah";
                            }else{ 
                              echo "Sederhana";
                            }?></td>
                          <td><? if ($row->kawasan_wisata == 1) { 
                              echo "Mewah";
                            }else if ($row->kawasan_wisata == 2) { 
                              echo "Menengah";
                            }else{ 
                              echo "Sederhana";
                            }?></td>
                          <td><?=$row->rekomendasi?></td>
                          <td><?=$row->nilai_investasi?></td>
                          <td><?=$row->irr?></td>
                          <td><?=$row->npv?></td>
                          <td><?=$row->laporan_lengkap?></td>  
                          <td><?=$row->keterangan?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
