<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_jalan.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>User</th>
                            <th>Jalan Di Depan Tapak</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->user_id?></td>
                            <td><? if ($row->ada_jalan == 1) { 
                              echo "Ya";
                            }else if ($row->ada_jalan == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><?=$row->lebar_jalan?> M</td>
                            <td><? if ($row->kelas_jalan == 1) { 
                              echo "Arteri Primer";
                            }else if ($row->kelas_jalan == 2) { 
                              echo "Arteri Sekunder";
                            }else if ($row->kelas_jalan == 3) { 
                              echo "Kolektor";
                            }else if ($row->kelas_jalan == 4) { 
                              echo "Lokal / Lingkungan";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->jumlah_lajur == 1) { 
                              echo "1 (Satu Lajur)";
                            }else if ($row->jumlah_lajur == 2) { 
                              echo "2 (Dua Lajur)";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->kondisi_lalin == 1) { 
                              echo "Tinggi";
                            }else if ($row->kondisi_lalin == 2) { 
                              echo "Cukup Tinggi";
                            }else if ($row->kondisi_lalin == 3) { 
                              echo "Rendah";
                            }else if ($row->kondisi_lalin == 3) { 
                              echo "Cukup Rendah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->permukaan_jalan == 1) { 
                              echo "Aspal Penetrasi";
                            }else if ($row->permukaan_jalan == 2) { 
                              echo "Aspal Hotmix";
                            }else if ($row->permukaan_jalan == 3) { 
                              echo "Beton";
                            }else if ($row->permukaan_jalan == 4) { 
                              echo "Paving";
                            }else if ($row->permukaan_jalan == 5) { 
                              echo "Perkerasan Sirtu";
                            }else if ($row->permukaan_jalan == 6) { 
                              echo "Perkerasan Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->kondisi == 1) { 
                              echo "Baik";
                            }else if ($row->kondisi == 2) { 
                              echo "Sedang";
                            }else if ($row->kondisi == 3) { 
                              echo "Rusak";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->drainase == 1) { 
                              echo "Terbuka";
                            }else if ($row->drainase == 2) { 
                              echo "Tertutup";
                            }else if ($row->drainase == 3) { 
                              echo "Didalam Tanah";
                            }else if ($row->drainase == 4) { 
                              echo "Didalam Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><? if ($row->penerangan == 1) { 
                              echo "Ada";
                            }else if ($row->penerangan == 2) { 
                              echo "Tidak Ada";
                            }else{ 
                              echo "-";
                            }?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
