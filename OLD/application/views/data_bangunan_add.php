<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(4);
$edit       = '';
$delete     = $this->uri->segment(3) == 'delete'?'true':'';
if($this->uri->segment(3) == 'add' || $this->uri->segment(3) == 'edit' ){
    $edit   = 'true';
}

$back_url   = base_url()."data/bangunan/";
?>


<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add Bangunan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Bangunan</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=$back_url?>add" method="POST">
            <div class="form-horizontal box-body">
                <!--edit-->
                <?if($id){?>
                <?//print_r($data_db);?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Bangunan</label>

                    <div class="col-sm-3">
                        <label class="col-sm-2 control-label row"><?=$id?></label>
                        <input type="hidden" name="inp_id_data_banding" type="text" class="form-control" value="<?=$id?>" >
                    </div>
                </div>
                <?}?>
                <!--//edit-->
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">User Id</label>

                    <div class="col-sm-10">
                        <input name="inp_user_id" type="text" class="form-control" placeholder="User Id"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Lokasi</label>

                    <div class="col-sm-10">
                        <input name="inp_lok" type="text" class="form-control" placeholder="Id Lokasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Bangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_jen_bang" type="text" class="form-control" placeholder="Jenis Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tipe Bangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_tip_bang" type="text" class="form-control" placeholder="Tipe Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->group_id)?$data_db->group_id:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama Pembangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_nama_pem" type="text" class="form-control" placeholder="Nama Pembangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->longitude)?$data_db->longitude:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nomor Asset</label>

                    <div class="col-sm-10">
                        <input name="inp_nom_ass" type="text" class="form-control" placeholder="Nomor Asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->latitude)?$data_db->latitude:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tahun Bangun</label>

                    <div class="col-sm-10">
                        <input name="inp_thn_bang" type="text" class="form-control" placeholder="Tahun Bangun"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tgl_survey)?$data_db->tgl_survey:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Luas Bangunan 01</label>

                    <div class="col-sm-10">
                        <input name="inp_luas_bang_01" type="text" class="form-control" id="inputEmail3" placeholder="Luas Bangunan 01"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pic)?$data_db->pic:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Luas Bangunan 02</label>

                    <div class="col-sm-10">
                        <input name="inp_luas_bang_02" type="text" class="form-control" id="inputEmail3" placeholder="Luas Bangunan 02"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_bumn)?$data_db->nama_bumn:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Lantai</label>

                    <div class="col-sm-10">
                        <input name="inp_jml_lan" type="text" class="form-control" id="inputEmail3" placeholder="Jumlah Lantai"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_tgl_penetapan)?$data_db->no_tgl_penetapan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Lantai d</label>

                    <div class="col-sm-10">
                        <input name="inp_jml_lan_d" type="text" class="form-control" id="inputEmail3" placeholder="Jumlah Lantai d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_asset)?$data_db->nama_asset:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pondasi</label>

                    <div class="col-sm-10">
                        <input name="inp_pond" type="text" class="form-control" id="inputEmail3" placeholder="Pondasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_asset)?$data_db->nomor_asset:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pondasi d</label>

                    <div class="col-sm-10">
                        <input name="inp_pond_d" type="text" class="form-control" id="inputEmail3" placeholder="Pondasi d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->banjir)?$data_db->banjir:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka</label>

                    <div class="col-sm-10">
                        <input name="inp_rang" type="text" class="form-control" id="inputEmail3" placeholder="Rangka"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak_1)?$data_db->jarak_tapak_1:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka d1</label>

                    <div class="col-sm-10">
                        <input name="inp_rang_d_1" type="text" class="form-control" id="inputEmail3" placeholder="Rangka d1"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak_2)?$data_db->jarak_tapak_2:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka d2</label>

                    <div class="col-sm-10">
                        <input name="inp_rang_d_2" type="text" class="form-control" id="inputEmail3" placeholder="Rangka d2"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kemudahan)?$data_db->kemudahan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Dinding</label>

                    <div class="col-sm-10">
                        <input name="inp_dinding" type="text" class="form-control" id="inputEmail3" placeholder="Dinding"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transportasi_umum)?$data_db->transportasi_umum:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Dinding d</label>

                    <div class="col-sm-10">
                        <input name="inp_dinding_d" type="text" class="form-control" id="inputEmail3" placeholder="Dinding d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_ke_terminal)?$data_db->jarak_ke_terminal:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Atap</label>

                    <div class="col-sm-10">
                        <input name="inp_atap" type="text" class="form-control" id="inputEmail3" placeholder="Atap"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_terminal)?$data_db->nama_terminal:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Atap d</label>

                    <div class="col-sm-10">
                        <input name="inp_atap_d" type="text" class="form-control" id="inputEmail3" placeholder="Atap d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transport_01)?$data_db->transport_01:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka Atap</label>

                    <div class="col-sm-10">
                        <input name="inp_rangka_at" type="text" class="form-control" id="inputEmail3" placeholder="Rangka Atap"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transport_02)?$data_db->transport_02:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka Atap d</label>

                    <div class="col-sm-10">
                        <input name="inp_rangka_at_d" type="text" class="form-control" id="inputEmail3" placeholder="Rangka Atap d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif1)?$data_db->alternatif1:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Langit Langit</label>

                    <div class="col-sm-10">
                        <input name="inp_langit_lang" type="text" class="form-control" id="inputEmail3" placeholder="Langit Langit"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif2)?$data_db->alternatif2:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Langit Langit d</label>

                    <div class="col-sm-10">
                        <input name="inp_langit_lang_d" type="text" class="form-control" id="inputEmail3" placeholder="Langit Langit d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif3)?$data_db->alternatif3:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lantai</label>

                    <div class="col-sm-10">
                        <input name="inp_lantai" type="text" class="form-control" id="inputEmail3" placeholder="Lantai"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lantai d</label>

                    <div class="col-sm-10">
                        <input name="inp_lantai_d" type="text" class="form-control" id="inputEmail3" placeholder="Lantai d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Partisi</label>

                    <div class="col-sm-10">
                        <input name="inp_partisi" type="text" class="form-control" id="inputEmail3" placeholder="Partisi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Partisi d</label>

                    <div class="col-sm-10">
                        <input name="inp_part_d" type="text" class="form-control" id="inputEmail3" placeholder="Partisi d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pintu</label>

                    <div class="col-sm-10">
                        <input name="inp_pintu" type="text" class="form-control" id="inputEmail3" placeholder="Pintu"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pintu d</label>

                    <div class="col-sm-10">
                        <input name="inp_pintu_d" type="text" class="form-control" id="inputEmail3" placeholder="Pintu d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka Pintu</label>

                    <div class="col-sm-10">
                        <input name="inp_rang_pint" type="text" class="form-control" id="inputEmail3" placeholder="Rangka Pintu"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka Pintu d</label>

                    <div class="col-sm-10">
                        <input name="inp_rang_pint_d" type="text" class="form-control" id="inputEmail3" placeholder="Rangka Pintu d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jendela</label>

                    <div class="col-sm-10">
                        <input name="inp_jen" type="text" class="form-control" id="inputEmail3" placeholder="Jendela"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka Jendela</label>

                    <div class="col-sm-10">
                        <input name="inp_rang_jen" type="text" class="form-control" id="inputEmail3" placeholder="Rangka Jendela"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Rangka Jendela d</label>

                    <div class="col-sm-10">
                        <input name="inp_rang_jen_d" type="text" class="form-control" id="inputEmail3" placeholder="Rangka Jendela d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Listrik</label>

                    <div class="col-sm-10">
                        <input name="inp_listrik" type="text" class="form-control" id="inputEmail3" placeholder="Listrik"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Listrik Watt</label>

                    <div class="col-sm-10">
                        <input name="inp_listrik_watt" type="text" class="form-control" id="inputEmail3" placeholder="Listrik Watt"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Air</label>

                    <div class="col-sm-10">
                        <input name="inp_air" type="text" class="form-control" id="inputEmail3" placeholder="Air"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Air Dalam</label>

                    <div class="col-sm-10">
                        <input name="inp_air_dal" type="text" class="form-control" id="inputEmail3" placeholder="Air Dalam"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Plumbing</label>

                    <div class="col-sm-10">
                        <input name="inp_plum" type="text" class="form-control" id="inputEmail3" placeholder="Plumbing"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Telepon</label>

                    <div class="col-sm-10">
                        <input name="inp_telp" type="text" class="form-control" id="inputEmail3" placeholder="Telepon"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Telepon Jenis</label>

                    <div class="col-sm-10">
                        <input name="inp_telp_jen" type="text" class="form-control" id="inputEmail3" placeholder="Telepon Jenis"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ac Unit</label>

                    <div class="col-sm-10">
                        <input name="inp_ac_unit" type="text" class="form-control" id="inputEmail3" placeholder="Ac Unit"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ac Jenis</label>

                    <div class="col-sm-10">
                        <input name="inp_ac_jen" type="text" class="form-control" id="inputEmail3" placeholder="Ac Jenis"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ac Jenis d</label>

                    <div class="col-sm-10">
                        <input name="inp_ac_jen_d" type="text" class="form-control" id="inputEmail3" placeholder="Ac Jenis d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ac Pk</label>

                    <div class="col-sm-10">
                        <input name="inp_ac_pk" type="text" class="form-control" id="inputEmail3" placeholder="Ac Pk"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pemadam</label>

                    <div class="col-sm-10">
                        <input name="inp_pemadam" type="text" class="form-control" id="inputEmail3" placeholder="Pemadam"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Penangkal Petir</label>

                    <div class="col-sm-10">
                        <input name="inp_penang_petir" type="text" class="form-control" id="inputEmail3" placeholder="Penangkal Petir"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Penangkal Petir d</label>

                    <div class="col-sm-10">
                        <input name="inp_penang_petir_d" type="text" class="form-control" id="inputEmail3" placeholder="Penangkal Petir d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lift Jumlah</label>

                    <div class="col-sm-10">
                        <input name="inp_lift_juml" type="text" class="form-control" id="inputEmail3" placeholder="Lift Jumlah"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lift Jenis</label>

                    <div class="col-sm-10">
                        <input name="inp_lift_jen" type="text" class="form-control" id="inputEmail3" placeholder="Lift Jenis"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lift Kap Org</label>

                    <div class="col-sm-10">
                        <input name="inp_lift_kap_org" type="text" class="form-control" id="inputEmail3" placeholder="Lift Kap Org"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lift Kap Brg</label>

                    <div class="col-sm-10">
                        <input name="inp_lift_kap_brg" type="text" class="form-control" id="inputEmail3" placeholder="Lift Kap Brg"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lift Merek Orang</label>

                    <div class="col-sm-10">
                        <input name="inp_lift_merk_orang" type="text" class="form-control" id="inputEmail3" placeholder="Lift Merek Orang"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lift Merek Barang</label>

                    <div class="col-sm-10">
                        <input name="inp_lift_merk_barang" type="text" class="form-control" id="inputEmail3" placeholder="Lift Merek Barang"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Elevator Unit</label>

                    <div class="col-sm-10">
                        <input name="inp_elev_unit" type="text" class="form-control" id="inputEmail3" placeholder="Elevator Unit"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Elevator Lebar</label>

                    <div class="col-sm-10">
                        <input name="inp_elev_leb" type="text" class="form-control" id="inputEmail3" placeholder="Elevator Lebar"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Elevator Panjang</label>

                    <div class="col-sm-10">
                        <input name="inp_elev_pan" type="text" class="form-control" id="inputEmail3" placeholder="Elevator Panjang"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Elevator Kap Hari</label>

                    <div class="col-sm-10">
                        <input name="inp_elev_kap_hari" type="text" class="form-control" id="inputEmail3" placeholder="Elevator Kap Hari"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kondisi Bangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_kond_bang" type="text" class="form-control" id="inputEmail3" placeholder="Kondisi Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Pengelolaan</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_peng" type="text" class="form-control" id="inputEmail3" placeholder="Status Pengelolaan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pengguna</label>

                    <div class="col-sm-10">
                        <input name="inp_pengguna" type="text" class="form-control" id="inputEmail3" placeholder="Pengguna"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pengguna Bidang</label>

                    <div class="col-sm-10">
                        <input name="inp_penggu_bid" type="text" class="form-control" id="inputEmail3" placeholder="Pengguna Bidang"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pengguna Bagian</label>

                    <div class="col-sm-10">
                        <input name="inp_penggu_bag" type="text" class="form-control" id="inputEmail3" placeholder="Pengguna Bagian"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Asset</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_asset" type="text" class="form-control" id="inputEmail3" placeholder="Status Asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status d</label>

                    <div class="col-sm-10">
                        <input name="inp_status_d" type="text" class="form-control" id="inputEmail3" placeholder="Status d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Panjang Bangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_panja_bang" type="text" class="form-control" id="inputEmail3" placeholder="Panjang Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lebar Bangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_leb_bang" type="text" class="form-control" id="inputEmail3" placeholder="Lebar Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Imb No</label>

                    <div class="col-sm-10">
                        <input name="inp_imb_no" type="text" class="form-control" id="inputEmail3" placeholder="Imb No"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Imb Tanggal</label>

                    <div class="col-sm-10">
                        <input name="inp_imb_tgl" type="text" class="form-control" id="inputEmail3" placeholder="Imb Tanggal"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Imb Keluar</label>

                    <div class="col-sm-10">
                        <input name="inp_imb_kel" type="text" class="form-control" id="inputEmail3" placeholder="Imb Keluar"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Perolehan</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_per" type="text" class="form-control" id="inputEmail3" placeholder="Status Perolehan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Perolehan d</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_per_d" type="text" class="form-control" id="inputEmail3" placeholder="Status Perolehan d"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nilai Perolehan</label>

                    <div class="col-sm-10">
                        <input name="inp_nilai_per" type="text" class="form-control" id="inputEmail3" placeholder="Nilai Perolehan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Penguasaan</label>

                    <div class="col-sm-10">
                        <input name="inp_status_peng" type="text" class="form-control" id="inputEmail3" placeholder="Status Penguasaan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Catatan</label>

                    <div class="col-sm-10">
                        <input name="inp_cat" type="text" class="form-control" id="inputEmail3" placeholder="Catatan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sketsa Bangunan</label>

                    <div class="col-sm-10">
                        <input name="inp_sketsa_bang" type="text" class="form-control" id="inputEmail3" placeholder="Sketsa Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approval" type="text" class="form-control" id="inputEmail3" placeholder="approval"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->approval)?$data_db->approval:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">respond</label>

                    <div class="col-sm-10">
                        <input name="inp_respond" type="text" class="form-control" id="inputEmail3" placeholder="respond"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->respond)?$data_db->respond:''?>">
                    </div>
                </div>

                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
