<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
//$id         = $this->uri->segment(3);
$id         = $this->input->get('lokasi');
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data    = $this->input->get('lokasi');
$back_url   = base_url()."svl_sarana/";
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey Lapangan</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Sarana</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">&nbsp;</h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST">
            <div class="form-horizontal box-body">
                <?if($id){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                        Sarana
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">User & Group</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option 
                                    <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> 
                                    <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->email ?'selected=""selected':''?> 
                                    value="<?=$row->email?>" 
                                    ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                    
                                <?}?>
                                <?}?>
                            </select>
                        </div>           

                        <div class="col-sm-6 row">
                            <select name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$data_db->group_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->name?> ( <?=$row->description?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--//edit-->

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kode Asset Internal</label>

                    <div class="col-sm-3">
                        <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Asset Internal" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Pasar/Mal Terdekat
                        <small>
                            <br/>
                            (bisa pasar tradisional atau pasar modern)
                        </small>
                    </h2>
                </center>
                <br/>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Pasar</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <input name="inp_nama_pas" type="text" class="form-control" placeholder=" Nama Pasar"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_pasar)?$data_db->nama_pasar:''?>">
                        </div>
                        <div class="col-sm-3 row">
                            <input name="inp_jarak" type="text" class="form-control" placeholder=" Jarak Pasar"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak)?$data_db->jarak:''?>">
                        </div>
                        <div class="col-sm-3">
                            <label for="" class="control-label label-title"><small>Meter</small></label>                        
                        </div>

                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Fasilitas / Sarana
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sarana Umum</label>

                    <div class="col-sm-10">
                        <label>
                            <!--
                            <div class="pull-left">
                                <input name="inp_sarana_um" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum)?$data_db->sarana_umum:'') == '1')?'checked':''?> >  PLN
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum)?$data_db->sarana_umum:'') == '2')?'checked':''?> >  Genset
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum)?$data_db->sarana_umum:'') == '3')?'checked':''?> >  PAM
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum)?$data_db->sarana_umum:'') == '4')?'checked':''?> >  Sumur
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum)?$data_db->sarana_umum:'') == '5')?'checked':''?> >  Telepon
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um" value="6" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum)?$data_db->sarana_umum:'') == '6')?'checked':''?> >  GAS
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            -->
                        </label>
                            <div class="pull-left">
                                <input name="inp_sarana_um_1" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_11)?$data_db->sarana_umum_11:'') == '1')?'checked':''?> >  PLN
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um_2" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_12)?$data_db->sarana_umum_12:'') == '1')?'checked':''?> >  Genset
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um_3" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_13)?$data_db->sarana_umum_13:'') == '1')?'checked':''?> >  PAM
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um_4" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_14)?$data_db->sarana_umum_14:'') == '1')?'checked':''?> >  Sumur
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um_5" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_15)?$data_db->sarana_umum_15:'') == '1')?'checked':''?> >  Telepon
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_um_6" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_16)?$data_db->sarana_umum_16:'') == '1')?'checked':''?> >  GAS
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="pull-left">
                                <div class="pull-left">                                    
                                    <input name="inp_sarana_um_7" value="1" type="checkbox" class=" pull-right flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_umum_17)?$data_db->sarana_umum_17:'') == '1')?'checked':''?> > 
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left">
                                        &nbsp;
                                        Lainnya
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-left">
                                        <input name="inp_sarana_um_desc" type="text" class="form-control pull-left" placeholder="Sarana Umum Lainnya" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sarana_umum_desc)?$data_db->sarana_umum_desc:''?>">
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        <label for="" class="control-label label-title">
                        <small>
                            Ketersediaan Fasilitas/ Sarana umum dilingkungan aset
                        </small>
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sarana Tersambung</label>

                    <div class="col-sm-10">
                        <label>
                            <!--
                            <div class="pull-left">
                                <input name="inp_sarana_ter" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung)?$data_db->sarana_tersambung:'') == '1')?'checked':''?> >  PLN
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung)?$data_db->sarana_tersambung:'') == '2')?'checked':''?> >  Genset
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung)?$data_db->sarana_tersambung:'') == '3')?'checked':''?> >  PAM
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung)?$data_db->sarana_tersambung:'') == '4')?'checked':''?> >  Sumur
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung)?$data_db->sarana_tersambung:'') == '5')?'checked':''?> >  Telepon
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter" value="6" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung)?$data_db->sarana_tersambung:'') == '6')?'checked':''?> >  GAS
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            -->
                        </label>
                            <div class="pull-left">
                                <input name="inp_sarana_ter_1" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_11)?$data_db->sarana_tersambung_11:'') == '1')?'checked':''?> >  PLN
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter_2" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_12)?$data_db->sarana_tersambung_12:'') == '1')?'checked':''?> >  Genset
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter_3" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_13)?$data_db->sarana_tersambung_13:'') == '1')?'checked':''?> >  PAM
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter_4" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_14)?$data_db->sarana_tersambung_14:'') == '1')?'checked':''?> >  Sumur
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter_5" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_15)?$data_db->sarana_tersambung_15:'') == '1')?'checked':''?> >  Telepon
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_sarana_ter_6" value="1" type="checkbox" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_16)?$data_db->sarana_tersambung_16:'') == '1')?'checked':''?> >  GAS
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <div class="pull-left">                                    
                                    <input name="inp_sarana_ter_7" value="1" type="checkbox" class=" pull-right flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sarana_tersambung_17)?$data_db->sarana_tersambung_17:'') == '1')?'checked':''?> > 
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left">
                                        &nbsp;
                                        Lainnya
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-left">
                                        <input name="inp_sarana_ter_desc" type="text" class="form-control pull-left" placeholder="Sarana Tersambung Lainnya" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sarana_tersam_desc)?$data_db->sarana_tersam_desc:''?>">
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        <label for="" class="control-label label-title">
                        <small>
                            Fasilitas/ Sarana yang tersambung ke lokasi aset
                        </small>
                        </label>
                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Karakteristik Tapak
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Bentuk Tapak</label>

                    <div class="col-sm-10">
                        <div class="col-sm-12 row">
                            <label>
                                <div class="pull-left">
                                    <input name="inp_bentuk_tap" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->bentuk_tapak)?$data_db->bentuk_tapak:'') == '1')?'checked':''?> > Beraturan
                                    &nbsp;&nbsp;&nbsp;
                                </div>

                                <div class="pull-left">
                                    <input name="inp_bentuk_tap" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->bentuk_tapak)?$data_db->bentuk_tapak:'') == '2')?'checked':''?> > Tidak Beraturan
                                    &nbsp;&nbsp;&nbsp;
                                </div>
                            </label>
                        </div>
                        <div class="col-sm-12 row">
                            <input name="inp_bentuk_tap_d" type="text" class="form-control pull-left" placeholder="(Segi empat/Segi Panjang/Segi Lebar/….)" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->bentuk_tapak_d)?$data_db->bentuk_tapak_d:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Topografi</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_top" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->topografi)?$data_db->topografi:'') == '1')?'checked':''?> >  Rata
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_top" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->topografi)?$data_db->topografi:'') == '2')?'checked':''?> >  Landai
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_top" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->topografi)?$data_db->topografi:'') == '3')?'checked':''?> >  Berbukit-bukit
                                &nbsp;&nbsp;&nbsp;
                            </div>


                            <div class="pull-left">
                                <div class="pull-left">                                    
                                    <input name="inp_top" value="4" type="radio" name="r3" class=" pull-right flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->topografi)?$data_db->topografi:'') == '4')?'checked':''?> > 
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left">
                                        &nbsp;
                                        Lainnya
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-left">
                                        <input name="inp_top_d" type="text" class="form-control pull-left" placeholder="Topografi Lainnya" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->topografi_d)?$data_db->topografi_d:''?>">
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        </label>
                        <label for="" class="control-label label-title">
                        <small>
                        </small>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Jenis Tanah</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_jenis_tan" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_tanah)?$data_db->jenis_tanah:'') == '1')?'checked':''?> >  Tanah kering
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jenis_tan" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_tanah)?$data_db->jenis_tanah:'') == '2')?'checked':''?> >  Tanah sawah
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jenis_tan" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_tanah)?$data_db->jenis_tanah:'') == '3')?'checked':''?> >  Rawa
                                &nbsp;&nbsp;&nbsp;
                            </div>


                            <div class="pull-left">
                                <div class="pull-left">                                    
                                    <input name="inp_jenis_tan" value="4" type="radio" name="r3" class=" pull-right flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_tanah)?$data_db->jenis_tanah:'') == '4')?'checked':''?> > 
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left">
                                        &nbsp;
                                        Lainnya
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-left">
                                        <input name="inp_jenis_tan_d" type="text" class="form-control pull-left" placeholder="Jenis Tanah Lainnya" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jenis_tanah_d)?$data_db->jenis_tanah_d:''?>">
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        </label>
                        <label for="" class="control-label label-title">
                        <small>
                        </small>
                        </label>
                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Elevasi/Posisi terhadap jalan
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Elevasi</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row" >
                            <input name="inp_elevasi" type="text" class="form-control" placeholder="Elevasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->elevasi)?$data_db->elevasi:''?>">
                        </div>
                        <div class="col-sm-3 row" style="margin-left: 5px">
                            <input name="inp_elevasi_m" type="text" class="form-control" placeholder="Elevasi Meter"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->elevasi_meter)?$data_db->elevasi_meter:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Elevasi/Posisi terhadap jalan didepannya (meter)</small></label>                        
                        </div>

                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Batas Tapak
                    </h2>
                </center>
                <br/>




                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Batas Barat</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_bar" type="text" class="form-control" id="inputEmail3" placeholder="Batas Barat" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->batas_barat)?$data_db->batas_barat:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Batas Timur</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_tim" type="text" class="form-control" id="inputEmail3" placeholder="Batas Timur" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->batas_timur)?$data_db->batas_timur:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Batas Utara</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_ut" type="text" class="form-control" id="inputEmail3" placeholder="Batas Utara" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->batas_utara)?$data_db->batas_utara:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Batas Selatan</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_sel" type="text" class="form-control" id="inputEmail3" placeholder="Batas Selatan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->batas_selatan)?$data_db->batas_selatan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">View Terbaik</label>

                    <div class="col-sm-4">
                        <select name="inp_view_ter" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                            <option value="" >- Pilih View Terbaik -</option>
                            <option <?= (!empty($data_db->view_terbaik)?$data_db->view_terbaik:'') == '1' ?'selected=""selected':''?> value="1" >Utara</option>
                            <option <?= (!empty($data_db->view_terbaik)?$data_db->view_terbaik:'') == '2' ?'selected=""selected':''?> value="2" >Selatan</option>
                            <option <?= (!empty($data_db->view_terbaik)?$data_db->view_terbaik:'') == '3' ?'selected=""selected':''?> value="3" >Timur</option>
                            <option <?= (!empty($data_db->view_terbaik)?$data_db->view_terbaik:'') == '4' ?'selected=""selected':''?> value="4" >Barat</option>
                        </select>
                    </div>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">View Keterangan</label>

                    <div class="col-sm-10">
                        <input name="inp_view_ket" type="text" class="form-control" id="" placeholder="View Terbaik" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->view_keterangan)?$data_db->view_keterangan:''?>">
                    </div>
                </div>
     

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Lain-lain
                    </h2>
                </center>
                <br/>



                 <hr/>
                    <center>
                        <h4 style="<?=($this->session->userdata('administrator') != '1')?'opacity:0.5"':'';?>">
                            (Administrator)
                        </h4>
                    </center>
                </br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input 
                                id="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="1" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> 
                                > Ya &nbsp;&nbsp;&nbsp;
                            <input 
                                id="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="0" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> 
                                > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input 
                                type="hidden" 
                                name="inp_approval" 
                                value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>
               

                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>svl_sarana" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>svl_sarana" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
