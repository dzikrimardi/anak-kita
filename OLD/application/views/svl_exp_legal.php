<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_legal.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data Status</th>
                            <th>Kode Asset Internal Detail</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Nilai Wajar</th>
                            <th>Bukti Perhitungan</th>
                            <th>Clear</th>
                            <th>Clean</th>
                            <th>Rekomendasi</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                           <td><?=$i?></td>
                          <td><?=$row->id_data_status?></td>
                          <td><?=$row->id_asset_internal?></td>
                          <td><?=$row->luas_tapak?></td>
                          <td><?=$row->status_tanah?></td>
                          <td><?=$row->status_tanah_ajb_d?></td>
                          <td><?=$row->status_tanah_desc1?></td>
                          <td><?=$row->status_tanah_desc2?></td>
                          <td><?=$row->nomor_surat?></td>
                          <td><?=$row->kode_sertifikat?></td>
                          <td><?=$row->nama_pemegang?></td>
                          <td><?=$row->tgl_terbit?></td>
                          <td><?=$row->tgl_berakhir?></td>
                          <td><?=$row->no_tgl_sk?></td>
                          <td><?=$row->no_peta_pendaftaran?></td>
                          <td><?=$row->no_gambar_situasi?></td>
                          <td><?=$row->tgl_surat_ukur?></td>
                          <td><?=$row->keadaan_tanah?></td>
                          <td><?=$row->perubahan_sertifikat?></td>
                          <td><?=$row->jenis_perubahan?></td>
                          <td><?=$row->pembebasan_hak?></td>
                          <td><?=$row->jenis_bukti?></td>
                          <td><?=$row->nomor_bukti?></td>
                          <td><?=$row->tanggal_bukti?></td>
                          <td><?=$row->nilai_wajar?></td>
                          <td><?=$row->bukti_perhitungan?></td>
                          <td><?=$row->clear?></td>
                          <td><?=$row->clean?></td>
                          <td><?=$row->rekomendasi?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
