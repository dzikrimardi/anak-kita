<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>
<?php $this->load->view('inc/home_header_css'); ?>
<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
        <!--<small>Version 2.0</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">CPU Traffic</span>
              <span class="info-box-number">90<small>%</small></span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sales</span>
              <span class="info-box-number">760</span>
            </div>
          </div>
        </div>-->
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-header with-border">
                <h3 class="box-title">Welcome, <?=$this->session->userdata('user')?></h3>
              </div>
              <div class="box-body">
                <a href="" class="btn btn-app" target="_self">
                  <span class="badge bg-purple"></span> 
                  <i class="fa fa-user"></i> 
                Account</a>
                <a href="<?=base_url()?>index.php/logout" class="btn btn-app" target="_self">
                  <span class="badge bg-purple"></span> 
                  <i class="fa fa-sign-out"></i> 
                Logout</a>
              </div>  
            </div>
          </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Users</span>
              <span class="info-box-number">2</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?if($this->input->get('group_id')){?>
                    <a href="<?=base_url()?>">
                        
                        <i class="fa fa-sign-out fa-rotate-180"></i>
                        Kembali 
                    </a>
                    |
                    <?}?>

                    Goal Completion
                </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <?if(!$this->input->get('group_id')){?>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Desc</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?if($ds_goal->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($ds_goal->result() as $row) { 
                    $i++;
                    $status_unit    = 0;
                    if(intval($row->group_done) == 0 or intval($row->group_all) == 0){
                        $percen         = 0;
                    }else{
                        $percen         = (intval($row->group_done) *100)/intval($row->group_all);
                    }
                        //$percen         = (3 *100)/intval($row->group_all);
                    ?>

                    <tr>
                        <td>
                            <a href="<?=base_url()?>?group_id=<?=$row->id?>">
                                <?=$row->name?>
                            </a>
                        </td>
                        <td>
                                <?=$row->description?>
                        </td>
                        <td>
                            <a href="<?=base_url()?>?group_id=<?=$row->id?>" class="btn btn-default">
                                Detail
                            </a>
                        </td>
                        <td>
                            <div class="progress-group">
                                <span class="progress-text">Progress</span>
                                <span class="progress-number"><b><?=$row->group_done?></b>/<?=$row->group_all?></span>

                                <div class="progress sm">
                                  <div class="progress-bar progress-bar-aqua" style="width: <?=round($percen)?>%"></div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <?}?>
                    <?}?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <?}else{?>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body table-responsive">
                    <table id="data_table" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>&nbsp;</th>
                                <th>Id Data</th>
                                <th>Status</th>
                                <th>Kode Asset Internal</th>
                                <th>Nama Unit</th>
                                <th>Nama Asset</th>
                                <th>Tgl Survey</th>
                                <th>Approval</th>
                                <th>Respond</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        $status_unit = $row->status;
                        ?>
                            <tr>
                                <td><?=$i?></td>
                                <td>
                                    <!--
                                    <center>
                                        <a href="<?//=$back_url?>edit/<?=$row->id_data?>">
                                            <i class="fa fa-edit" alt="edit"></i>
                                        </a> 
                                    </center>                           
                                    -->
                                </td>
                                <td><?=$row->id_data?></td>
                                <td>
                                    <?if($status_unit == '1'){?>
                                        <span class="label label-danger">Belum lengkap</span>
                                    <?}else if($status_unit == '2'){?>
                                        <span class="label label-warning">Lengkap, Belum diverifikasi</span>
                                    <?}else if($status_unit == '3'){?>
                                        <span class="label label-success">Lengkap, Sudah diverifikasi</span>
                                    <?}else{?>
                                        <span class="label label-info">ditugaskan ke surveyor</span>
                                    <?}?>
                                </td>
                                <td><?=$row->kode_tapak?></td>
                                <td><?=$row->nama_bumn?></td>
                                <td><?=$row->nama_asset?></td>
                                <td><?=$row->tgl_survey?></td>
                                <td><?=$row->approval==1?'yes':'-'?></td>
                                <td><?=$row->respond?></td>
                            </tr>
                        <?}?>
                        <?}?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>&nbsp;</th>
                                <th>Id Data</th>
                                <th>Status</th>
                                <th>Kode Asset Internal</th>
                                <th>Nama Unit</th>
                                <th>Nama Asset</th>
                                <th>Tgl Survey</th>
                                <th>Approval</th>
                                <th>Respond</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                  <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <?}?>

            <!--
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
        -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--
  <a href="https://www.w3schools.com/html/html5_geolocation.asp">https://www.w3schools.com/html/html5_geolocation.asp</a>
  <button id="getLoc">GetLoc1</button>
  <?
  echo "cookies : ";
  var_dump($_COOKIE);
  echo " monster";
  ?>
<form id="form">
    Message: <input id="message" name="message" type="text"/><br />
    Long: <input id="length" name="length" type="checkbox" /><br />

    <input type="submit" value="Make Toast" />
</form>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
  </footer>

  -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>js/pages/dashboard2.js"></script>
<script type="text/javascript">
    $("#getLoc").click(function() {
        //var message = document.getElementById("message").value;
        //var lengthLong = document.getElementById("length").checked;

        /* 
            Call the 'makeToast' method in the Java code. 
            'app' is specified in MainActivity.java when 
            adding the JavaScript interface. 
         */
        //window.top.app.makeToast(message, lengthLong);    
        app.get_location();    
        alert(app.get_location());
    });

</script>

    <script type="text/javascript">

        function showToast(){
            var message = document.getElementById("message").value;
            var lengthLong = document.getElementById("length").checked;

            /* 
                Call the 'makeToast' method in the Java code. 
                'app' is specified in MainActivity.java when 
                adding the JavaScript interface. 
             */
            app.makeToast(message, true);
            return false;
        }

        /* 
            Call the 'showToast' method when the form gets 
            submitted (by pressing button or return key on keyboard). 
         */
        window.onload = function(){
            var form = document.getElementById("form");
            form.onsubmit = showToast;
        }


        //app.makeToast('Welcome', true);
        app.go_in();    
        //app.makeToast('in', true);

    </script>

<?php $this->load->view('inc/home_footer_body'); ?>