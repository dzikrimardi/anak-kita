<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_sarana/";
$exp_url = base_url()."svl_exp_sarana";
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Sarana Umum
        <small>&nbsp;</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Survey</a></li>
            
            <?if($this->input->get('lokasi')){?>

            <li>
                <a href="<?=$back_url?>">
                    Lokasi
                </a>
            </li>
            <li class="active">Sarana</li>

            <?}else{?>
            
            <li class="active">Lokasi</li>
            
            <?}?>      
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Unit</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Aset</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tanggal Survey</b> <a class="pull-right"><?=$data_db_r->tgl_survey?></a>
                </li>
              </ul>
                <center>
                    <?if(empty($data_db_r->c_data_r)){?>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <?}?>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-file-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?if($this->input->get('lokasi')){?>
                        Sarana Umum
                    <?}else{?>
                        Lokasi                
                    <?}?>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="150px">
                                <center>
                                    Detail        
                                </center>
                            </th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <i class="fa fa-files-o" aria-hidden="true"></i> : 
                                    <a href="<?=$back_url?>?lokasi=<?=$row->id_data?>">
                                     <?=$row->id_data?>
                                    </a>     
                                </center>                           
                            </td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                </table>
                <?}else{?>

                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                          <td><?=$i?></td>
                          <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_data?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_data?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->nama_pasar?></td>
                          <td><?=$row->jarak?> M</td>
                          <td><? 
                            /*
                            if ($row->sarana_umum == 0) { 
                              echo "PLN";
                            }else if ($row->sarana_umum == 1) { 
                              echo "Genset";
                            }else if ($row->sarana_umum == 2) { 
                              echo "PAM";
                            }else if ($row->sarana_umum == 3) { 
                              echo "Sumur";
                            }else if ($row->sarana_umum == 4) { 
                              echo "Telepon";
                            }else if ($row->sarana_umum == 5) { 
                              echo "Gas";
                            }else{ 
                              echo "Lainnya";
                            }
                            */
                            ?>
                            <?=(!empty($row->sarana_umum_11)?'-&nbsp;PLN <br/>':'')?>
                            <?=(!empty($row->sarana_umum_12)?'-&nbsp;Genset <br/>':'')?>
                            <?=(!empty($row->sarana_umum_13)?'-&nbsp;PAM <br/>':'')?>
                            <?=(!empty($row->sarana_umum_14)?'-&nbsp;Sumur <br/>':'')?>
                            <?=(!empty($row->sarana_umum_15)?'-&nbsp;Telepon <br/>':'')?>
                            <?=(!empty($row->sarana_umum_16)?'-&nbsp;Gas <br/>':'')?>
                            <?=(!empty($row->sarana_umum_17)?'-&nbsp;Lainnya <br/>':'')?>
                            </td>
                          <td><?=$row->sarana_umum_desc?></td>
                          <td>
                            <?
                            /*
                            if ($row->sarana_tersambung == 0) { 
                              echo "PLN";
                            }else if ($row->sarana_tersambung == 1) { 
                              echo "Genset";
                            }else if ($row->sarana_tersambung == 2) { 
                              echo "PAM";
                            }else if ($row->sarana_tersambung == 3) { 
                              echo "Sumur";
                            }else if ($row->sarana_tersambung == 4) { 
                              echo "Telepon";
                            }else if ($row->sarana_tersambung == 5) { 
                              echo "Gas";
                            }else{ 
                              echo "Lainnya";
                            }
                            */
                            ?>
                            <?=(!empty($row->sarana_tersambung_11)?'-&nbsp;PLN <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_12)?'-&nbsp;Genset <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_13)?'-&nbsp;PAM <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_14)?'-&nbsp;Sumur <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_15)?'-&nbsp;Telepon <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_16)?'-&nbsp;Gas <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_17)?'-&nbsp;Lainnya&nbsp;('.$row->sarana_tersam_desc.') <br/> ':'')?>                                
                            </td>
                          <td><?=$row->sarana_tersam_desc?></td>
                          <td><? if ($row->bentuk_tapak == 1) { 
                              echo "Beraturan";
                            }else if ($row->bentuk_tapak == 2) { 
                              echo "Tidak Beraturan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->bentuk_tapak_d?></td>
                          <td><? if ($row->topografi == 1) { 
                              echo "Rata";
                            }else if ($row->topografi == 2) { 
                              echo "Landai";
                            }else if ($row->topografi == 3) { 
                              echo "Berbukit - bukit";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->topografi_d?></td>
                          <td><? if ($row->jenis_tanah == 1) { 
                              echo "Tanah Kering";
                            }else if ($row->jenis_tanah == 2) { 
                              echo "Tanah Sawah";
                            }else if ($row->jenis_tanah == 3) { 
                              echo "Rawa";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->jenis_tanah_d?></td>
                          <td><?=$row->elevasi?> M</td>
                          <td><?=$row->elevasi_meter?></td>
                          <td><?=$row->batas_barat?></td>
                          <td><?=$row->batas_timur?></td>
                          <td><?=$row->batas_utara?></td>
                          <td><?=$row->batas_selatan?></td>
                          <td><? if ($row->view_terbaik == 1) { 
                              echo "Utara";
                            }else if ($row->view_terbaik == 2) { 
                              echo "Selatan";
                            }else if ($row->view_terbaik == 3) { 
                              echo "Timur";
                            }else if ($row->view_terbaik == 4) { 
                              echo "Barat";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->view_keterangan?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                    <?
                      }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                <?}?>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Sarana Umum</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->nama_pasar?></td>
                          <td><?=$row->jarak?> M</td>
                          <td><? 
                            /*
                            if ($row->sarana_umum == 0) { 
                              echo "PLN";
                            }else if ($row->sarana_umum == 1) { 
                              echo "Genset";
                            }else if ($row->sarana_umum == 2) { 
                              echo "PAM";
                            }else if ($row->sarana_umum == 3) { 
                              echo "Sumur";
                            }else if ($row->sarana_umum == 4) { 
                              echo "Telepon";
                            }else if ($row->sarana_umum == 5) { 
                              echo "Gas";
                            }else{ 
                              echo "Lainnya";
                            }
                            */
                            ?>
                            <?=(!empty($row->sarana_umum_11)?'-&nbsp;PLN <br/>':'')?>
                            <?=(!empty($row->sarana_umum_12)?'-&nbsp;Genset <br/>':'')?>
                            <?=(!empty($row->sarana_umum_13)?'-&nbsp;PAM <br/>':'')?>
                            <?=(!empty($row->sarana_umum_14)?'-&nbsp;Sumur <br/>':'')?>
                            <?=(!empty($row->sarana_umum_15)?'-&nbsp;Telepon <br/>':'')?>
                            <?=(!empty($row->sarana_umum_16)?'-&nbsp;Gas <br/>':'')?>
                            <?=(!empty($row->sarana_umum_17)?'-&nbsp;Lainnya <br/>':'')?>
                            </td>
                          <td><?=$row->sarana_umum_desc?></td>
                          <td>
                            <?
                            /*
                            if ($row->sarana_tersambung == 0) { 
                              echo "PLN";
                            }else if ($row->sarana_tersambung == 1) { 
                              echo "Genset";
                            }else if ($row->sarana_tersambung == 2) { 
                              echo "PAM";
                            }else if ($row->sarana_tersambung == 3) { 
                              echo "Sumur";
                            }else if ($row->sarana_tersambung == 4) { 
                              echo "Telepon";
                            }else if ($row->sarana_tersambung == 5) { 
                              echo "Gas";
                            }else{ 
                              echo "Lainnya";
                            }
                            */
                            ?>
                            <?=(!empty($row->sarana_tersambung_11)?'-&nbsp;PLN <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_12)?'-&nbsp;Genset <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_13)?'-&nbsp;PAM <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_14)?'-&nbsp;Sumur <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_15)?'-&nbsp;Telepon <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_16)?'-&nbsp;Gas <br/>':'')?>
                            <?=(!empty($row->sarana_tersambung_17)?'-&nbsp;Lainnya&nbsp;('.$row->sarana_tersam_desc.') <br/> ':'')?>                                
                            </td>
                          <td><?=$row->sarana_tersam_desc?></td>
                          <td><? if ($row->bentuk_tapak == 1) { 
                              echo "Beraturan";
                            }else if ($row->bentuk_tapak == 2) { 
                              echo "Tidak Beraturan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->bentuk_tapak_d?></td>
                          <td><? if ($row->topografi == 1) { 
                              echo "Rata";
                            }else if ($row->topografi == 2) { 
                              echo "Landai";
                            }else if ($row->topografi == 3) { 
                              echo "Berbukit - bukit";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->topografi_d?></td>
                          <td><? if ($row->jenis_tanah == 1) { 
                              echo "Tanah Kering";
                            }else if ($row->jenis_tanah == 2) { 
                              echo "Tanah Sawah";
                            }else if ($row->jenis_tanah == 3) { 
                              echo "Rawa";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->jenis_tanah_d?></td>
                          <td><?=$row->elevasi?> M</td>
                          <td><?=$row->elevasi_meter?></td>
                          <td><?=$row->batas_barat?></td>
                          <td><?=$row->batas_timur?></td>
                          <td><?=$row->batas_utara?></td>
                          <td><?=$row->batas_selatan?></td>
                          <td><? if ($row->view_terbaik == 1) { 
                              echo "Utara";
                            }else if ($row->view_terbaik == 2) { 
                              echo "Selatan";
                            }else if ($row->view_terbaik == 3) { 
                              echo "Timur";
                            }else if ($row->view_terbaik == 4) { 
                              echo "Barat";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->view_keterangan?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                    <?
                      }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>          
                <?}?>            
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
