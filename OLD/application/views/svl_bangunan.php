<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_bangunan/";
$exp_url = base_url()."svl_exp_bangunan";
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$this->session->userdata('nama_yayasan')?>
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Anak Asuh</a></li>

<!--            <?if($this->input->get('lokasi')){?>

            <li>
                <a href="<?=$back_url?>">
				-->
<!--                    Lokasi -->
<!--                </a>
            </li>
            <li class="active">Bangunan</li>

            <?}else{?>

            <li class="active">Lokasi</li>

            <?}?>
	-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Anak Asuh</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                <? $this->session->set_userdata('nama_anak_yatim', $data_db_r->nama_bumn); ?>
				</li>
                <li class="list-group-item text-muted">
                  <b>Tempat/Tanggal Lahir</b> <a class="pull-right"><?=$data_db_r->latitude?> / <?=$data_db_r->tgl_survey?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Ayah Kandung</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
              </ul>
                <center>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right" style="margin-left: 15px">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?if($this->input->get('lokasi')){?>
                        Data Riwayat Vaksinasi                
                    <?}else{?>
                        Data Riwayat Vaksinasi                
                    <?}?>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="150px">
                                <center>
                                    Kode Data        
                                </center>
                            </th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Ayah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <i class="fa fa-files-o" aria-hidden="true"></i> : 
                                    <a href="<?=$back_url?>?lokasi=<?=$row->id_data?>">
                                     <?=$row->id_data?>
                                    </a>     
                                </center>                           
                            </td>
                            <td><?=$row->nama_bumn?></td>
							<td><? if ($row->banjir == 1) {
                            			echo "Laki-laki";
			                          }else{
            			                echo "Perempuan";
                        			  }?></td>
                            <td><?=$row->latitude?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td><?=$row->alamat?></td>
                            <td><?=$row->nama_asset?></td>
                         </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                 </table>
                <?}else{?>

                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Pemberi Imunisasi</th>
                            <th>Wajib/Tambahan</th>
                            <th>Jenis Imunisasi Wajib</th>
                            <th>Jenis Imunisasi Tambahan</th>
                            <th>Tgl Pemberian1</th>
                            <th>Tgl Pemberian2</th>
                            <th>Tgl Pemberian3</th>
                            <th>Tgl Pemberian4</th>
                            <th>Tgl Pemberian5</th>
                            <th>Tgl Pemberian6</th>
                            <th>Tgl Pemberian7</th>
                            <th>Tgl Pemberian8</th>
                            <th>Catatan</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_bangunan?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_bangunan?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <a href="<?=$back_url?>view/<?=$row->id_bangunan?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                           </td>
                          <td><?=$row->pondasi_d?></td>
                          <td><? if ($row->lift_jenis == 1) { 
                              echo "Wajib";
                            }else if ($row->lift_jenis == 2) { 
                              echo "Tambahan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->jenis_bangunan == 1) { 
                              echo "Hepatitis B";
                            }else if ($row->jenis_bangunan == 2) { 
                              echo "Polio";
                            }else if ($row->jenis_bangunan == 3) { 
                              echo "BCG";
                            }else if ($row->jenis_bangunan == 4) { 
                              echo "Campak (DPT-HB-HiB)";
                            }else{ 
                              echo "-";
                            }?></td>
                          <!--<td><?//=$row->jenis_bangunan_d?></td>-->
                          <td><? if ($row->tipe_bangunan == 1) { 
                              echo "Pneumokokus Conjugate Vaccine (PCV)";
                            }else if ($row->tipe_bangunan == 2) { 
                              echo "Varisela";
                            }else if ($row->tipe_bangunan == 3) { 
                              echo "Influenza";
                            }else if ($row->tipe_bangunan == 4) { 
                              echo "Hepatitis A";
                            }else if ($row->tipe_bangunan == 5) { 
                              echo "HPV (human papiloma virus)";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? echo substr($row->nama_bangunan,8,2) . "-" . substr($row->nama_bangunan,5,2) . "-" . substr($row->nama_bangunan,0,4) ; ?></td>
                          <td><? echo substr($row->nomor_asset,8,2) . "-" . substr($row->nomor_asset,5,2) . "-" . substr($row->nomor_asset,0,4) ; ?></td>
                          <td><? echo substr($row->jml_lantai_d,8,2) . "-" . substr($row->jml_lantai_d,5,2) . "-" . substr($row->jml_lantai_d,0,4) ; ?></td>
                          <td><? echo substr($row->rangka_d1,8,2) . "-" . substr($row->rangka_d1,5,2) . "-" . substr($row->rangka_d1,0,4) ; ?></td>
                          <td><? echo substr($row->rangka_d2,8,2) . "-" . substr($row->rangka_d2,5,2) . "-" . substr($row->rangka_d2,0,4) ; ?></td>
                          <td><? echo substr($row->rangka_jendela_d,8,2) . "-" . substr($row->rangka_jendela_d,5,2) . "-" . substr($row->rangka_jendela_d,0,4) ; ?></td>
                          <td><? echo substr($row->ac_jenis_d,8,2) . "-" . substr($row->ac_jenis_d,5,2) . "-" . substr($row->ac_jenis_d,0,4) ; ?></td>
                          <td><? echo substr($row->lift_merek_orang,8,2) . "-" . substr($row->lift_merek_orang,5,2) . "-" . substr($row->lift_merek_orang,0,4) ; ?></td>
 
                          <td><?=$row->catatan?></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>

                </table>
                <?}?>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
			     <h1>
        			<?=$this->session->userdata('nama_yayasan')?>
        				<small>&nbsp;</small></h1>

                    <h1>Riwayat Vaksinasi</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                      <tr>
                            <th>No</th>
                            <th>Pemberi Imunisasi</th>
                            <th>Wajib/Tambahan</th>
                            <th>Jenis Imunisasi Wajib</th>
                            <th>Jenis Imunisasi Tambahan</th>
                            <th>Tgl Pemberian1</th>
                            <th>Tgl Pemberian2</th>
                            <th>Tgl Pemberian3</th>
                            <th>Tgl Pemberian4</th>
                            <th>Tgl Pemberian5</th>
                            <th>Tgl Pemberian6</th>
                            <th>Tgl Pemberian7</th>
                            <th>Tgl Pemberian8</th>
                            <th>Catatan</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->pondasi_d?></td>
                          <td><? if ($row->lift_jenis == 1) { 
                              echo "Wajib";
                            }else if ($row->lift_jenis == 2) { 
                              echo "Tambahan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->jenis_bangunan == 1) { 
                              echo "Hepatitis B";
                            }else if ($row->jenis_bangunan == 2) { 
                              echo "Polio";
                            }else if ($row->jenis_bangunan == 3) { 
                              echo "BCG";
                            }else if ($row->jenis_bangunan == 4) { 
                              echo "Campak (DPT-HB-HiB)";
                            }else{ 
                              echo "-";
                            }?></td>
                          <!--<td><?//=$row->jenis_bangunan_d?></td>-->
                          <td><? if ($row->tipe_bangunan == 1) { 
                              echo "Pneumokokus Conjugate Vaccine (PCV)";
                            }else if ($row->tipe_bangunan == 2) { 
                              echo "Varisela";
                            }else if ($row->tipe_bangunan == 3) { 
                              echo "Influenza";
                            }else if ($row->tipe_bangunan == 4) { 
                              echo "Hepatitis A";
                            }else if ($row->tipe_bangunan == 5) { 
                              echo "HPV (human papiloma virus)";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? echo substr($row->nama_bangunan,8,2) . "-" . substr($row->nama_bangunan,5,2) . "-" . substr($row->nama_bangunan,0,4) ; ?></td>
                          <td><? echo substr($row->nomor_asset,8,2) . "-" . substr($row->nomor_asset,5,2) . "-" . substr($row->nomor_asset,0,4) ; ?></td>
                          <td><? echo substr($row->jml_lantai_d,8,2) . "-" . substr($row->jml_lantai_d,5,2) . "-" . substr($row->jml_lantai_d,0,4) ; ?></td>
                          <td><? echo substr($row->rangka_d1,8,2) . "-" . substr($row->rangka_d1,5,2) . "-" . substr($row->rangka_d1,0,4) ; ?></td>
                          <td><? echo substr($row->rangka_d2,8,2) . "-" . substr($row->rangka_d2,5,2) . "-" . substr($row->rangka_d2,0,4) ; ?></td>
                          <td><? echo substr($row->rangka_jendela_d,8,2) . "-" . substr($row->rangka_jendela_d,5,2) . "-" . substr($row->rangka_jendela_d,0,4) ; ?></td>
                          <td><? echo substr($row->ac_jenis_d,8,2) . "-" . substr($row->ac_jenis_d,5,2) . "-" . substr($row->ac_jenis_d,0,4) ; ?></td>
                          <td><? echo substr($row->lift_merek_orang,8,2) . "-" . substr($row->lift_merek_orang,5,2) . "-" . substr($row->lift_merek_orang,0,4) ; ?></td>
                          <td><?=$row->catatan?></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                </table>       
                <?}?>          
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
