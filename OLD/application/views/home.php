<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>
<?php $this->load->view('inc/home_header_css'); ?>
<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Home
        <!--<small>Version 2.0</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li class="active">Dashboard</li>-->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!--<div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">CPU Traffic</span>
              <span class="info-box-number">90<small>%</small></span>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-google-plus"></i></span>
            <div class="info-box-content">
              <span class="info-box-text">Likes</span>
              <span class="info-box-number">41,410</span>
            </div>
          </div>
        </div>
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Sales</span>
              <span class="info-box-number">760</span>
            </div>
          </div>
        </div>-->
        <div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <div class="box-header with-border">
                <h3 class="box-title">Welcome, <?=$this->session->userdata('user')?></h3>
              </div>
              <div class="box-body">
                <a href="" class="btn btn-app" target="_self">
                  <span class="badge bg-purple"></span> 
                  <i class="fa fa-user"></i> 
                Account</a>
                <a href="<?=base_url()?>index.php/logout" class="btn btn-app" target="_self">
                  <span class="badge bg-purple"></span> 
                  <i class="fa fa-sign-out"></i> 
                Logout</a>
              </div>  
            </div>
          </div>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Users</span>
              <span class="info-box-number">2</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
      <!-- /.row -->

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?if($this->input->get('group_id')){?>
                    <a href="<?=base_url()?>">
                        
                        <i class="fa fa-sign-out fa-rotate-180"></i>
                        Kembali 
                    </a>
                    |
                    <?}?>

                    AnaKita Data Centre
                </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <?if(!$this->input->get('group_id')){?>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <?}
?>

            <!--
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
        -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!--
  <a href="https://www.w3schools.com/html/html5_geolocation.asp">https://www.w3schools.com/html/html5_geolocation.asp</a>
  <button id="getLoc">GetLoc1</button>
  <?
  echo "cookies : ";
  var_dump($_COOKIE);
  echo " monster";
  ?>
<form id="form">
    Message: <input id="message" name="message" type="text"/><br />
    Long: <input id="length" name="length" type="checkbox" /><br />

    <input type="submit" value="Make Toast" />
</form>

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      
    </div>
  </footer>

  -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>js/pages/dashboard2.js"></script>
<script type="text/javascript">
    $("#getLoc").click(function() {
        //var message = document.getElementById("message").value;
        //var lengthLong = document.getElementById("length").checked;

        /* 
            Call the 'makeToast' method in the Java code. 
            'app' is specified in MainActivity.java when 
            adding the JavaScript interface. 
         */
        //window.top.app.makeToast(message, lengthLong);    
        app.get_location();    
        alert(app.get_location());
    });

</script>

    <script type="text/javascript">

        function showToast(){
            var message = document.getElementById("message").value;
            var lengthLong = document.getElementById("length").checked;

            /* 
                Call the 'makeToast' method in the Java code. 
                'app' is specified in MainActivity.java when 
                adding the JavaScript interface. 
             */
            app.makeToast(message, true);
            return false;
        }

        /* 
            Call the 'showToast' method when the form gets 
            submitted (by pressing button or return key on keyboard). 
         */
        window.onload = function(){
            var form = document.getElementById("form");
            form.onsubmit = showToast;
        }


        //app.makeToast('Welcome', true);
        app.go_in();    
        //app.makeToast('in', true);

    </script>

<?php $this->load->view('inc/home_footer_body'); ?>