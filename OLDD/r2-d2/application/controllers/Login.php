<?php defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {
	public function index(){
        if($this->input->get('r2d2')){
            $this->session->set_userdata('r2d2', 'true');
        }
		$this->load->view('login');
	}
}
