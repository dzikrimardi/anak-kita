<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function index(){
		$data['title'] = 'Admin - Users';

		$sql 				= "	SELECT *
								FROM 
									`admin_users`
								";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 		
        $this->load->view('admin',$data);
	}
	
	function add(){
		$data['title'] = 'Admin - Create User';
		$this->load->view('admin_add',$data);
	}

	function groups(){
		$data['title'] = 'Admin - Create Groups';
		$this->load->view('admin_groups',$data);
	}
}
