<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_legal extends CI_Controller {

	public function index(){

		$data['title'] = 'Survey - Legal';

		$sql 				= "SELECT * FROM `survey04_status`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_legal',$data);
	}

	function add(){
		$data['title'] = 'Survey - Tambah Legal';
		$this->load->view('svl_legal_add',$data);
	}

	function view(){
		$data['title'] = 'Survey - View Legal';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey04_status` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_legal_add',$data);
	}

	function edit(){
		$data['title'] = 'Survey - Edit Legal';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey04_status` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_legal_add',$data);
	}

	function delete(){
		$data['title'] = 'Survey - Hapus Legal';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey04_status` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_legal_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Legal';

		if($this->input->post('inp_id_data_stat')){
			$id 	= $this->input->post('inp_id_data_stat');
		}else{
			$id 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey04_status` WHERE id_data = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data_stat')){
					$id = $this->input->post('inp_id_data_stat');
				}
				$id_data_stat		= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$id_data 	 		= $this->input->post('id_data'); 
				$status_tanah 		= $this->input->post('status_tanah'); 
				$stat_tanah_ajb_d  	= $this->input->post('status_tanah_ajb_d'); 
				$status_tanah_desc1 = $this->input->post('status_tanah_desc1'); 
				$status_tanah_desc2 = $this->input->post('status_tanah_desc2'); 
				$luas_tapak 		= $this->input->post('luas_tapak'); 
				$no_sur 		 	= $this->input->post('nomor_surat'); 
				$kode_sertifikat	= $this->input->post('kode_sertifikat'); 
				$nama_pemegang 		= $this->input->post('nama_pemegang'); 
				$tgl_terbit			= $this->input->post('tgl_terbit'); 
				$tgl_berakhir 		= $this->input->post('tgl_berakhir'); 
				$no_tgl_sk  		= $this->input->post('no_tgl_sk'); 
				$no_peta_pendaftaran= $this->input->post('no_peta_pendaftaran'); 
				$no_gambar_situasi  = $this->input->post('no_gambar_situasi'); 
				$tgl_surat_ukur    	= $this->input->post('tgl_surat_ukur'); 
				$keadaan_tanah 	 	= $this->input->post('keadaan_tanah'); 
				$per_sertifikat 	= $this->input->post('perubahan_sertifikat'); 
				$jen_perubahan 	 	= $this->input->post('jenis_perubahan'); 
				$pem_hak 	 		= $this->input->post('pembebasan_hak'); 
				$jen_bukti 	 		= $this->input->post('jenis_bukti'); 
				$no_bukti 	 		= $this->input->post('nomor_bukti'); 
				$tanggal_bukti 	 	= $this->input->post('tanggal_bukti'); 
				$approval 			= $this->input->post('inp_approv'); 
				$respond 			= $this->input->post('inp_resp');

				$sql	= "	REPLACE INTO 
								survey04_status (
												`id_data_status`, 
												`group_id`,
												`user_id`, 
												`id_data`, 
												`status_tanah`, 
												`status_tanah_ajb_d`, 
												`status_tanah_desc1`, 
												`status_tanah_desc2`, 
												`luas_tapak`, 
												`nomor_surat`, 
												`kode_sertifikat`, 
												`nama_pemegang`, 
												`tgl_terbit`, 
												`tgl_berakhir`, 
												`no_tgl_sk`, 
												`no_peta_pendaftaran`, 
												`no_gambar_situasi`, 
												`tgl_surat_ukur`, 
												`keadaan_tanah`, 
												`perubahan_sertifikat`, 
												`jenis_perubahan`, 
												`pembebasan_hak`, 
												`jenis_bukti`, 
												`nomor_bukti`, 
												`tanggal_bukti`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data_stat', 
												'$group_id', 
												'$user_id', 
												'$id_data', 
												'$status_tanah', 
												'$stat_tanah_ajb_d', 
												'$status_tanah_desc1', 
												'$status_tanah_desc2', 
												'$luas_tapak', 
												'$no_sur', 
												'$kode_sertifikat', 
												'$nama_pemegang', 
												'$tgl_terbit', 
												'$tgl_berakhir', 
												'$no_tgl_sk', 
												'$no_peta_pendaftaran', 
												'$no_gambar_situasi', 
												'$tgl_surat_ukur', 
												'$keadaan_tanah', 
												'$per_sertifikat', 
												'$jen_perubahan', 
												'$pem_hak', 
												'$jen_bukti', 
												'$no_bukti', 
												`$tanggal_bukti`,
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data_stat')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

		$this->load->view('svl_legal_add',$data);

	}
}
