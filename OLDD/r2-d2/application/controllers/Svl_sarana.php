<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_sarana extends CI_Controller {

	public function index(){

		$data['title'] = 'Survey - Sarana';

		$sql 				= "SELECT * FROM `survey03_pasar`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_sarana',$data);
	}

	function add(){
		$data['title'] = 'Survey - Tambah Sarana';
		$this->load->view('svl_sarana_add',$data);
	}

	function view(){
		$data['title'] = 'Survey - View Sarana';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_sarana_add',$data);
	}

	function edit(){
		$data['title'] = 'Survey - Edit Sarana';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_sarana_add',$data);
	}

	function delete(){
		$data['title'] = 'Survey - Hapus Sarana';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_sarana_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Sarana';

		if($this->input->post('inp_id_data')){
			$id 	= $this->input->post('inp_id_data');
		}else{
			$id 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey03_pasar` WHERE id_data = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data')){
					$id = $this->input->post('inp_id_data');
				}
				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$nama_pas 			= $this->input->post('inp_nama_pas'); 
				$jarak  			= $this->input->post('inp_jarak'); 
				$sarana_um  		= $this->input->post('inp_sarana_um'); 
				$sarana_um_desc 	= $this->input->post('inp_sarana_um_desc'); 
				$sarana_ter 		= $this->input->post('inp_sarana_ter'); 
				$sarana_ter_desc 	= $this->input->post('inp_sarana_ter_desc'); 
				$bentuk_tap 		= $this->input->post('inp_bentuk_tap'); 
				$bentuk_tap_d 		= $this->input->post('inp_bentuk_tap_d'); 
				$top				= $this->input->post('inp_top'); 
				$top_d 				= $this->input->post('inp_top_d'); 
				$jenis_tan  		= $this->input->post('inp_jenis_tan'); 
				$jenis_tan_d 		= $this->input->post('inp_jenis_tan_d'); 
				$elevasi        	= $this->input->post('inp_elevasi'); 
				$elevasi_m    	 	= $this->input->post('inp_elevasi_m'); 
				$bat_bar 	 		= $this->input->post('inp_bat_bar'); 
				$bat_tim 	 		= $this->input->post('inp_bat_tim'); 
				$bat_ut 	 		= $this->input->post('inp_bat_ut'); 
				$bat_sel 	 		= $this->input->post('inp_bat_sel'); 
				$view_ter 	 		= $this->input->post('inp_view_ter'); 
				$view_ket 	 		= $this->input->post('inp_view_ket'); 
				$approval 			= $this->input->post('inp_approv'); 
				$respond 			= $this->input->post('inp_resp');

				$sql	= "	REPLACE INTO 
								survey03_pasar (
												`id_data`, 
												`group_id`,
												`user_id`, 
												`kode_tapak`, 
												`nama_pasar`, 
												`jarak`, 
												`sarana_umum`, 
												`sarana_umum_desc`, 
												`sarana_tersambung`, 
												`sarana_tersam_desc`, 
												`bentuk_tapak`, 
												`bentuk_tapak_d`, 
												`topografi`, 
												`topografi_d`, 
												`jenis_tanah`, 
												`jenis_tanah_d`, 
												`elevasi`, 
												`elevasi_meter`, 
												`batas_barat`, 
												`batas_timur`, 
												`batas_utara`, 
												`batas_selatan`, 
												`view_terbaik`, 
												`view_keterangan`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$user_id', 
												'$kode_tapak', 
												'$nama_pas', 
												'$jarak', 
												'$sarana_um', 
												'$sarana_um_desc', 
												'$sarana_ter', 
												'$sarana_ter_desc', 
												'$bentuk_tap', 
												'$bentuk_tap_d', 
												'$top', 
												'$top_d', 
												'$jenis_tan', 
												'$jenis_tan_d', 
												'$elevasi', 
												'$elevasi_m', 
												'$bat_bar', 
												'$bat_tim', 
												'$bat_ut', 
												'$bat_sel', 
												'$view_ter', 
												'$view_ket', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}


		$this->load->view('svl_sarana_add',$data);

	}
}
