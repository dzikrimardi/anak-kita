<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	public function index(){
		$data['title'] = 'Survey - Users';

		$sql 				= "	SELECT *, (
											SELECT GROUP_CONCAT((SELECT name FROM `groups` WHERE groups.id = users_groups.group_id )) 
											FROM `users_groups` WHERE users.id = users_groups.user_id
										) AS list_groups 
								FROM 
									`users`
								";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('users',$data);
	}
	function action(){
		$data['exec_query'] = '';
		if($this->input->post()){
			$id 	= $this->input->post('inp_id')?$this->input->post('inp_id'):'NULL';
			$user 	= $this->input->post('inp_user');
			$pass 	= $this->input->post('inp_pass');
			$email 	= $this->input->post('inp_email');
			$first 	= $this->input->post('inp_first');
			$last 	= $this->input->post('inp_last');


			$sql	= "	REPLACE INTO 
							`users` (
										`id`, 
										`username`, 
										`password`, 
										`email`, 
										`first_name`, 
										`last_name`
						) VALUES (
										$id, 
										'$user', 
										'$pass', 
										'$email',
										'$first', 
										'$last'
						);";
			if($pass != $this->input->post('inp_pass_c')){
				$data['exec_query'] = 'error_pass_c';
			}else if(!$pass){
				$data['exec_query'] = 'error_pass_e';
			}else if(!$user || !$email || !$first|| !$last){
				$data['exec_query'] = 'error_e';
			}else{
		        if($this->db->query($sql)){
		        	if($this->input->post('inp_id')){
		        		$id_user = $id; 
		        		$sql	= "DELETE FROM users_groups WHERE user_id = '$id_user'";
		        		$this->db->query($sql);
		        	}else{
			        	$sql	 = "SELECT id FROM users ORDER BY id DESC LIMIT 1";
			        	$db_user = $this->db->query($sql)->row();
			        	$id_user = $db_user->id;
		        	}
					foreach ($this->input->post('inp_groups') as $groups_key => $groups_value) {
						//print_r($groups);
			        	$sql	= "	INSERT INTO `users_groups` (
			        										id,
			        										user_id, 
			        										group_id
			        				) VALUES (
			        										NULL, 
			        										'$id_user', 
			        										'$groups_value'
			        				);";
						$this->db->query($sql);
					}
		        	$data['exec_query'] = 'ok';
		        }else{
		        	$data['exec_query'] = 'failed';
		        }
			}

		}
		$data['title'] = 'Survey - User';
		$this->load->view('users_add',$data);
	}	
	function add(){
		$data['title'] = 'Survey - Add User';
		$this->load->view('users_add',$data);
	}

	function add_group(){
		$data['exec_query'] = '';
		if($this->input->post()){
			$name 	= $this->input->post('name');
			$desc 	= $this->input->post('desc');


			$sql	= "	INSERT INTO 
							`groups` (
										`id`, 
										`name`, 
										`description`
						) VALUES (
										NULL, 
										'$name', 
										'$desc'
						);";
		        if($this->db->query($sql)){
		        	/*$sql	= "SELECT id FROM groups ORDER BY id DESC LIMIT 1";
		        	$db_user = $this->db->query($sql)->row();
		        	$id_user = $db_user->id;
					foreach ($this->input->post('inp_groups') as $groups_key => $groups_value) {
						//print_r($groups);
			        	$sql	= "	INSERT INTO `groups` (
			        										id,
			        										user_id, 
			        										group_id
			        				) VALUES (
			        										NULL, 
			        										'$id_user', 
			        										'$groups_value'
			        				);";
						$this->db->query($sql);
					}*/
		        	$data['exec_query'] = 'ok';
		        }else{
		        	$data['exec_query'] = 'failed';
		        }

		}
		$data['title'] = 'Survey - Add User';
		$this->load->view('user_group_add',$data);
	}

	function edit(){
		$data['title'] = 'Survey - Edit User';

		$id = $this->uri->segment(3);

		$sql 				= "	SELECT *, (
											SELECT GROUP_CONCAT(group_id) 
											FROM 
												`users_groups` 
											WHERE 
												users.id = users_groups.user_id
										) AS id_list_groups , (
											SELECT GROUP_CONCAT((SELECT name FROM `groups` WHERE groups.id = users_groups.group_id  )) 
											FROM `users_groups` WHERE users.id = users_groups.user_id
										) AS list_groups 
								FROM 
									`users`
								WHERE id = $id
								";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 
        $data['editable']	= 'true';
		$this->load->view('users_edit',$data);
	}

	function view(){
		$data['title'] = 'Survey - Edit User';

		$id = $this->uri->segment(3);

		$sql 				= "	SELECT *, (
											SELECT GROUP_CONCAT((SELECT name FROM `groups` WHERE groups.id = users_groups.group_id  ), ', ') 
											FROM `users_groups` WHERE users.id = users_groups.user_id
										) AS list_groups 
								FROM 
									`users`
								WHERE id = $id
								";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 
		$this->load->view('users_edit',$data);
	}

	function groups(){
		$data['title'] = 'Survey - Groups';

		$sql 				= "SELECT * FROM `groups`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('users_groups',$data);
	}
}
