<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Data extends CI_Controller {

	/*
	public function index(){

		$data['title'] = 'Survey - Lokasi';

		$sql 				= "SELECT * FROM `survey01_lokasi`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_lokasi',$data);
	}
	function add_lokasi(){

		$data['title'] = 'Survey - Add User';
		$this->load->view('data_lokasi_add',$data);
	}
	*/

	function lokasi(){
		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Lokasi';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Lokasi';
				}else{
					$data['title'] 		= 'Survey - Edit Lokasi';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data');
				$sql = "DELETE FROM `survey01_lokasi` WHERE id_data = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data')){
					$id = $this->input->post('inp_id_data');
				}
				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_lokasi_add',$data);
		}else{
			$data['title'] = 'Survey - Lokasi';

			$sql 				= "SELECT * FROM `survey01_lokasi`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_lokasi',$data);
		}
	}

	function jalan(){

		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Jalan';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Jalan';
				}else{
					$data['title'] 		= 'Survey - Edit Jalan';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey02_jalan` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

				$data['exec_query'] = '';
				if($this->input->post('delete')){
					$id = $this->input->post('inp_id_data');
					$sql = "DELETE FROM `survey02_jalan` WHERE id_data = '$id'";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'delete';
			        }else{
			        	$data['exec_query'] = 'failed';

			        }
				}
				if($this->input->post() and !$this->input->post('delete')){

					if($this->input->post('inp_id_data')){
						$id = $this->input->post('inp_id_data');
					}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$ada_jalan 			= $this->input->post('inp_ada_jalan'); 
				$kode_tapak			= $this->input->post('inp_kode_tapak'); 
				$peruntukan 		= $this->input->post('inp_peruntukan'); 
				$peruntukan_lainnya = $this->input->post('inp_peruntukan_dll'); 
				$kdb 				= $this->input->post('inp_kdb'); 
				$klb 				= $this->input->post('inp_klb'); 
				$gsb 			 	= $this->input->post('inp_gsb'); 
				$tinggi_maks 		= $this->input->post('inp_tinggi_maks'); 
				$sesuai_per 		= $this->input->post('inp_sesuai_per'); 
				$sesuai_eks 		= $this->input->post('inp_sesuai_eks'); 
				$leb_jal 			= $this->input->post('inp_leb_jal'); 
				$kel_jal 			= $this->input->post('inp_kel_jal'); 
				$jum_laj 			= $this->input->post('inp_jum_laj'); 
				$kon_lal 		 	= $this->input->post('inp_kon_lal'); 
				$perm_jal 		 	= $this->input->post('inp_perm_jal'); 
				$kond 		 		= $this->input->post('inp_kond'); 
				$drain 		 		= $this->input->post('inp_drain'); 
				$pener 		 		= $this->input->post('inp_pener'); 
				$petun_1 	 		= $this->input->post('inp_petun_1'); 
				$petun_2 	 		= $this->input->post('inp_petun_2'); 
				$petun_3 	 		= $this->input->post('inp_petun_3'); 
				$petun_4 	 		= $this->input->post('inp_petun_4'); 
				$petun_5 			= $this->input->post('inp_petun_5'); 
				$petun_6 			= $this->input->post('inp_petun_6');
				$approval 			= $this->input->post('inp_approv');
				$respond 			= $this->input->post('inp_resp');


				$sql	= "	REPLACE INTO 
								survey02_jalan (
												`id_data`, 
												`group_id`, 
												`user_id`, 
												`ada_jalan`, 
												`kode_tapak`, 
												`peruntukan`, 
												`peruntukan_lainnya`, 
												`kdb`, 
												`klb`, 
												`gsb`, 
												`tinggi_maks`, 
												`sesuai_peraturan`, 
												`sesuai_eksisting`, 
												`lebar_jalan`, 
												`kelas_jalan`, 
												`jumlah_lajur`, 
												`kondisi_lalin`, 
												`permukaan_jalan`, 
												`kondisi`, 
												`drainase`, 
												`penerangan`, 
												`petunjuk1`, 
												`petunjuk2`, 
												`petunjuk3`, 
												`petunjuk4`,
												`petunjuk5`,
												`petunjuk6`,
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$user_id', 
												'$ada_jalan', 
												'$kode_tapak', 
												'$peruntukan', 
												'$peruntukan_lainnya', 
												'$kdb', 
												'$klb', 
												'$gsb', 
												'$tinggi_maks', 
												'$sesuai_per', 
												'$sesuai_eks', 
												'$leb_jal', 
												'$kel_jal', 
												'$jum_laj', 
												'$kon_lal', 
												'$perm_jal', 
												'$kond', 
												'$drain', 
												'$pener', 
												'$petun_1', 
												'$petun_2', 
												'$petun_3', 
												'$petun_4', 
												'$petun_5', 
												'$petun_6', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_jalan_add',$data);
		}else{

			$data['title'] = 'Survey - Jalan';

			$sql 				= "SELECT * FROM `survey02_jalan`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_jalan',$data);
		}
	}

	function sarana(){
		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Sarana';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Sarana';
				}else{
					$data['title'] 		= 'Survey - Edit Sarana';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey03_pasar` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data');
				$sql = "DELETE FROM `survey03_pasar` WHERE id_data = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data')){
					$id = $this->input->post('inp_id_data');
				}
				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$nama_pas 			= $this->input->post('inp_nama_pas'); 
				$jarak  			= $this->input->post('inp_jarak'); 
				$sarana_um  		= $this->input->post('inp_sarana_um'); 
				$sarana_um_desc 	= $this->input->post('inp_sarana_um_desc'); 
				$sarana_ter 		= $this->input->post('inp_sarana_ter'); 
				$sarana_ter_desc 	= $this->input->post('inp_sarana_ter_desc'); 
				$bentuk_tap 		= $this->input->post('inp_bentuk_tap'); 
				$bentuk_tap_d 		= $this->input->post('inp_bentuk_tap_d'); 
				$top				= $this->input->post('inp_top'); 
				$top_d 				= $this->input->post('inp_top_d'); 
				$jenis_tan  		= $this->input->post('inp_jenis_tan'); 
				$jenis_tan_d 		= $this->input->post('inp_jenis_tan_d'); 
				$elevasi        	= $this->input->post('inp_elevasi'); 
				$elevasi_m    	 	= $this->input->post('inp_elevasi_m'); 
				$bat_bar 	 		= $this->input->post('inp_bat_bar'); 
				$bat_tim 	 		= $this->input->post('inp_bat_tim'); 
				$bat_ut 	 		= $this->input->post('inp_bat_ut'); 
				$bat_sel 	 		= $this->input->post('inp_bat_sel'); 
				$view_ter 	 		= $this->input->post('inp_view_ter'); 
				$view_ket 	 		= $this->input->post('inp_view_ket'); 
				$approval 			= $this->input->post('inp_approv'); 
				$respond 			= $this->input->post('inp_resp');

				$sql	= "	REPLACE INTO 
								survey03_pasar (
												`id_data`, 
												`group_id`,
												`user_id`, 
												`kode_tapak`, 
												`nama_pasar`, 
												`jarak`, 
												`sarana_umum`, 
												`sarana_umum_desc`, 
												`sarana_tersambung`, 
												`sarana_tersam_desc`, 
												`bentuk_tapak`, 
												`bentuk_tapak_d`, 
												`topografi`, 
												`topografi_d`, 
												`jenis_tanah`, 
												`jenis_tanah_d`, 
												`elevasi`, 
												`elevasi_meter`, 
												`batas_barat`, 
												`batas_timur`, 
												`batas_utara`, 
												`batas_selatan`, 
												`view_terbaik`, 
												`view_keterangan`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$user_id', 
												'$kode_tapak', 
												'$nama_pas', 
												'$jarak', 
												'$sarana_um', 
												'$sarana_um_desc', 
												'$sarana_ter', 
												'$sarana_ter_desc', 
												'$bentuk_tap', 
												'$bentuk_tap_d', 
												'$top', 
												'$top_d', 
												'$jenis_tan', 
												'$jenis_tan_d', 
												'$elevasi', 
												'$elevasi_m', 
												'$bat_bar', 
												'$bat_tim', 
												'$bat_ut', 
												'$bat_sel', 
												'$view_ter', 
												'$view_ket', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_sarana_add',$data);
		}else{
			$data['title'] = 'Survey - Sarana';

			$sql 				= "SELECT * FROM `survey03_pasar`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_sarana',$data);
		}
		
	}

	function legal(){
		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Legal';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Legal';
				}else{
					$data['title'] 		= 'Survey - Edit Legal';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey04_status` WHERE id_data_status = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data_stat');
				$sql = "DELETE FROM `survey04_status` WHERE id_data_status = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data_stat')){
					$id = $this->input->post('inp_id_data_stat');
				}
				$id_data_stat		= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$id_data 	 		= $this->input->post('id_data'); 
				$status_tanah 		= $this->input->post('status_tanah'); 
				$stat_tanah_ajb_d  	= $this->input->post('status_tanah_ajb_d'); 
				$status_tanah_desc1 = $this->input->post('status_tanah_desc1'); 
				$status_tanah_desc2 = $this->input->post('status_tanah_desc2'); 
				$luas_tapak 		= $this->input->post('luas_tapak'); 
				$no_sur 		 	= $this->input->post('nomor_surat'); 
				$kode_sertifikat	= $this->input->post('kode_sertifikat'); 
				$nama_pemegang 		= $this->input->post('nama_pemegang'); 
				$tgl_terbit			= $this->input->post('tgl_terbit'); 
				$tgl_berakhir 		= $this->input->post('tgl_berakhir'); 
				$no_tgl_sk  		= $this->input->post('no_tgl_sk'); 
				$no_peta_pendaftaran= $this->input->post('no_peta_pendaftaran'); 
				$no_gambar_situasi  = $this->input->post('no_gambar_situasi'); 
				$tgl_surat_ukur    	= $this->input->post('tgl_surat_ukur'); 
				$keadaan_tanah 	 	= $this->input->post('keadaan_tanah'); 
				$per_sertifikat 	= $this->input->post('perubahan_sertifikat'); 
				$jen_perubahan 	 	= $this->input->post('jenis_perubahan'); 
				$pem_hak 	 		= $this->input->post('pembebasan_hak'); 
				$jen_bukti 	 		= $this->input->post('jenis_bukti'); 
				$no_bukti 	 		= $this->input->post('nomor_bukti'); 
				$tanggal_bukti 	 	= $this->input->post('tanggal_bukti'); 
				$approval 			= $this->input->post('inp_approv'); 
				$respond 			= $this->input->post('inp_resp');

				$sql	= "	REPLACE INTO 
								survey04_status (
												`id_data_status`, 
												`group_id`,
												`user_id`, 
												`id_data`, 
												`status_tanah`, 
												`status_tanah_ajb_d`, 
												`status_tanah_desc1`, 
												`status_tanah_desc2`, 
												`luas_tapak`, 
												`nomor_surat`, 
												`kode_sertifikat`, 
												`nama_pemegang`, 
												`tgl_terbit`, 
												`tgl_berakhir`, 
												`no_tgl_sk`, 
												`no_peta_pendaftaran`, 
												`no_gambar_situasi`, 
												`tgl_surat_ukur`, 
												`keadaan_tanah`, 
												`perubahan_sertifikat`, 
												`jenis_perubahan`, 
												`pembebasan_hak`, 
												`jenis_bukti`, 
												`nomor_bukti`, 
												`tanggal_bukti`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data_stat', 
												'$group_id', 
												'$user_id', 
												'$id_data', 
												'$status_tanah', 
												'$stat_tanah_ajb_d', 
												'$status_tanah_desc1', 
												'$status_tanah_desc2', 
												'$luas_tapak', 
												'$no_sur', 
												'$kode_sertifikat', 
												'$nama_pemegang', 
												'$tgl_terbit', 
												'$tgl_berakhir', 
												'$no_tgl_sk', 
												'$no_peta_pendaftaran', 
												'$no_gambar_situasi', 
												'$tgl_surat_ukur', 
												'$keadaan_tanah', 
												'$per_sertifikat', 
												'$jen_perubahan', 
												'$pem_hak', 
												'$jen_bukti', 
												'$no_bukti', 
												`$tanggal_bukti`,
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_legal_add',$data);
		}else{
			$data['title'] = 'Survey - Legal';

			$sql 				= "SELECT * FROM `survey04_status`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_legal',$data);
		}
	}

	function aset(){

		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Asset';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Asset';
				}else{
					$data['title'] 		= 'Survey - Edit Asset';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey06_kelola` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data');
				$sql = "DELETE FROM `survey06_kelola` WHERE id_data = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data')){
					$id = $this->input->post('inp_id_data');
				}
				$id_data 			= $id;  
				$user_id 			= $this->input->post('inp_user_id'); 
				$stat_kel 	 		= $this->input->post('inp_stat_kel'); 
				$stat_kel_d 		= $this->input->post('inp_stat_kel_d'); 
				$peng 			  	= $this->input->post('inp_peng'); 
				$peng_bid 			= $this->input->post('inp_peng_bid'); 
				$peng_bag 			= $this->input->post('inp_peng_bag'); 
				$stat_m 			= $this->input->post('inp_stat_m'); 
				$stat_m_man_m		= $this->input->post('inp_stat_man_m'); 
				$stat_leg 			= $this->input->post('inp_stat_leg'); 
				$inp_cat_un_clear 	= $this->input->post('inp_cat_un_clear'); 
				$inp_cat_un_clean	= $this->input->post('inp_cat_un_clean'); 
				$inp_seng 	 		= $this->input->post('inp_seng'); 
				$inp_seng_d  		= $this->input->post('inp_seng_d'); 
				$inp_med 			= $this->input->post('inp_med'); 
				$inp_med_d 			= $this->input->post('inp_med_d'); 
				$inp_gug 	    	= $this->input->post('inp_gug'); 
				$inp_gug_d 	 	 	= $this->input->post('inp_gug_d'); 
				$inp_pem 		 	= $this->input->post('inp_pem'); 
				$inp_pem_d 	 	 	= $this->input->post('inp_pem_d'); 
				$approval 			= $this->input->post('inp_approv'); 
				$respond 			= $this->input->post('inp_resp');

				$sql	= "	REPLACE INTO 
								survey06_kelola (
												`id_data`, 
												`user_id`, 
												`status_kelola`, 
												`status_kelola_d`, 
												`pengguna`, 
												`pengguna_bidang`, 
												`pengguna_bagian`, 
												`status_manfaat`, 
												`status_manfaat_m`, 
												`status_legal`, 
												`catatan_unclear`, 
												`catatan_unclean`, 
												`sengketa`, 
												`sengketa_d`, 
												`mediasi`, 
												`mediasi_d`, 
												`gugatan`, 
												`gugatan_d`, 
												`pemblokiran`, 
												`pemblokiran_d`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$user_id', 
												'$stat_kel', 
												'$stat_kel_d', 
												'$peng', 
												'$peng_bid', 
												'$peng_bag', 
												'$stat_m', 
												'$stat_m_man_m', 
												'$stat_leg', 
												'$inp_cat_un_clear', 
												'$inp_cat_un_clean', 
												'$inp_seng', 
												'$inp_seng_d', 
												'$inp_med', 
												'$inp_med_d', 
												'$inp_gug', 
												'$inp_gug_d', 
												'$inp_pem', 
												'$inp_pem_d', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_aset_add',$data);
		}else{
			$data['title'] = 'Survey - Legal';

			$sql 				= "SELECT * FROM `survey06_kelola`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_aset',$data);
		}
	}



	function hbu(){
		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah HBU';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus HBU';
				}else{
					$data['title'] 		= 'Survey - Edit HBU';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey07_hbu` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data_hbu');
				$sql = "DELETE FROM `survey07_hbu` WHERE id_data = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data_hbu')){
					$id = $this->input->post('inp_id_data_hbu');
				}
				$id_data_hbu 		= $id;  
				$inp_user_id 		= $this->input->post('inp_user_id'); 
				$inp_id_data 	 	= $this->input->post('inp_id_data'); 
				$inp_jen_pro 		= $this->input->post('inp_jen_pro'); 
				$inp_nama 			= $this->input->post('inp_nama'); 
				$inp_luas_tan 		= $this->input->post('inp_luas_tan'); 
				$inp_luas_bang 		= $this->input->post('inp_luas_bang'); 
				$inp_jarak_tap 		= $this->input->post('inp_jarak_tap'); 
				$inp_jum_kam		= $this->input->post('inp_jum_kam'); 
				$inp_kel_hot 		= $this->input->post('inp_kel_hot'); 
				$inp_grad_aprt 		= $this->input->post('inp_grad_aprt'); 
				$inp_grad_perk		= $this->input->post('inp_grad_perk'); 
				$inp_grad_ruk 	 	= $this->input->post('inp_grad_ruk'); 
				$inp_grad_mall  	= $this->input->post('inp_grad_mall'); 
				$inp_grad_per 		= $this->input->post('inp_grad_per'); 
				$inp_kaw_wis 		= $this->input->post('inp_kaw_wis'); 
				$inp_ket 	    	= $this->input->post('inp_ket'); 
				$approval 			= $this->input->post('inp_approv'); 
				$respond 			= $this->input->post('inp_resp');

				$sql	= "	REPLACE INTO 
								survey06_kelola (
												`id_data_hbu`, 
												`user_id`, 
												`id_data`, 
												`jenis_properti`, 
												`nama`, 
												`luas_tanah`, 
												`luas_bangunan`, 
												`jarak_tapak`, 
												`jumlah_kamar`, 
												`kelas_hotel`, 
												`grade_apartemen`, 
												`grade_perkantoran`, 
												`grade_ruko`, 
												`grade_mall`, 
												`grade_perumahan`, 
												`kawasan_wisata`, 
												`keterangan`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data_hbu', 
												'$inp_user_id', 
												'$inp_id_data', 
												'$inp_jen_pro', 
												'$inp_nama', 
												'$inp_luas_tan', 
												'$inp_luas_bang', 
												'$inp_jarak_tap', 
												'$inp_jum_kam', 
												'$inp_kel_hot', 
												'$inp_grad_aprt', 
												'$inp_grad_perk', 
												'$inp_grad_ruk', 
												'$inp_grad_mall', 
												'$inp_grad_per', 
												'$inp_kaw_wis', 
												'$inp_ket', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data_hbu')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_hbu_add',$data);
		}else{
			$data['title'] = 'Survey - Legal';

			$sql 				= "SELECT * FROM `survey07_hbu`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_hbu',$data);
		}
	}



	function pembanding(){
		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Pembanding';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Pembanding';
				}else{
					$data['title'] 		= 'Edit Yayasan';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey08_pembanding` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data_banding');
				$sql = "DELETE FROM `survey08_pembanding` WHERE id_data = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data_banding')){
					$id = $this->input->post('inp_id_data_banding');
				}
				$id_data_banding 	= $id;  
				$inp_id_data 		= $this->input->post('inp_id_data'); 
				$inp_user_id 	 	= $this->input->post('inp_user_id'); 
				$inp_latitude 		= $this->input->post('inp_latitude'); 
				$inp_longitude 		= $this->input->post('inp_longitude'); 
				$inp_foto_data 		= $this->input->post('inp_foto_data'); 
				$inp_harga_pen 		= $this->input->post('inp_harga_pen'); 
				$inp_harga_trans 	= $this->input->post('inp_harga_trans'); 
				$inp_tgl			= $this->input->post('inp_tgl'); 
				$inp_sum_data 		= $this->input->post('inp_sum_data'); 
				$inp_no_telp 		= $this->input->post('inp_no_telp'); 
				$inp_perunt			= $this->input->post('inp_perunt'); 
				$inp_lebar_jln_01 	= $this->input->post('inp_lebar_jln_01'); 
				$inp_perker  		= $this->input->post('inp_perker'); 
				$inp_kond_jal 		= $this->input->post('inp_kond_jal'); 
				$inp_ang_um 		= $this->input->post('inp_ang_um'); 
				$inp_luas_tan 	    = $this->input->post('inp_luas_tan'); 
				$inp_sert 			= $this->input->post('inp_sert'); 
				$inp_bent 			= $this->input->post('inp_bent');
				$inp_elv 			= $this->input->post('inp_elv'); 
				$inp_lebar_jln_02 	= $this->input->post('inp_lebar_jln_02');
				$inp_transport_01 	= $this->input->post('inp_transport_01'); 
				$inp_sudut 			= $this->input->post('inp_sudut');
				$inp_harg_m2 		= $this->input->post('inp_harg_m2'); 
				$inp_luas_m2 		= $this->input->post('inp_luas_m2');
				$inp_dbangun_thn 	= $this->input->post('inp_dbangun_thn'); 
				$inp_penggu 		= $this->input->post('inp_penggu');
				$inp_rangka 		= $this->input->post('inp_rangka'); 
				$inp_juml_lantai 	= $this->input->post('inp_juml_lantai');
				$inp_lantai 		= $this->input->post('inp_lantai'); 
				$inp_dind 			= $this->input->post('inp_dind');
				$inp_atap 			= $this->input->post('inp_atap'); 
				$inp_langit_langit 	= $this->input->post('inp_langit_langit');
				$inp_kondisi 		= $this->input->post('inp_kondisi'); 
				$inp_harga_m2_bang 	= $this->input->post('inp_harga_m2_bang');
				$inp_sketsa 		= $this->input->post('inp_sketsa'); 
				$inp_approval 		= $this->input->post('inp_approval');
				$inp_respond 		= $this->input->post('inp_respond'); 


				$sql	= "	REPLACE INTO 
								survey08_pembanding (
												`id_data_banding`, 
												`id_data`, 
												`user_id`, 
												`latitude`, 
												`longitude`, 
												`foto_data`, 
												`harga_penawaran`, 
												`harga_transaksi`, 
												`tanggal`, 
												`sumber_data`, 
												`no_telepon`, 
												`peruntukan`, 
												`lebar_jalan_01`, 
												`perkerasan`, 
												`kondisi_jalan`, 
												`angkutan_umum`, 
												`luas_tanah`, 
												`sertifikat`, 
												`bentuk`, 
												`elevasi`, 
												`lebar_jalan_02`, 
												`sudut`, 
												`tusuk_sate`, 
												`harga_m2`, 
												`luas_m2`, 
												`dibangun_tahun`, 
												`penggunaan`, 
												`rangka`, 
												`jumlah_lantai`, 
												`lantai`, 
												`dinding`, 
												`atap`, 
												`langit_langit`, 
												`kondisi`, 
												`harga_m2_bangunan`, 
												`sketsa`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data_banding', 
												'$inp_id_data', 
												'$inp_user_id', 
												'$inp_latitude', 
												'$inp_longitude', 
												'$inp_foto_data', 
												'$inp_harga_pen', 
												'$inp_harga_trans', 
												'$inp_tgl', 
												'$inp_sum_data', 
												'$inp_no_telp', 
												'$inp_perunt', 
												'$inp_lebar_jln_01', 
												'$inp_perker', 
												'$inp_kond_jal', 
												'$inp_ang_um', 
												'$inp_luas_tan', 
												'$inp_sert', 
												'$inp_bent',
												`$inp_elv`,
												`$inp_lebar_jln_02`,
												`$inp_transport_01`,
												`$inp_sudut`,
												`$inp_harg_m2`,
												`$inp_luas_m2`,
												`$inp_dbangun_thn`,
												`$inp_penggu`,
												`$inp_rangka`,
												`$inp_juml_lantai`,
												`$inp_lantai`,
												`$inp_dind`,
												`$inp_atap`,
												`$inp_langit_langit`,
												`$inp_kondisi`,
												`$inp_harga_m2_bang`,
												`$inp_sketsa`,
												`$inp_approval`,
												`$inp_respond`

									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data_banding')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_pembanding_add',$data);
		}else{
			$data['title'] = 'Survey - Legal';

			$sql 				= "SELECT * FROM `survey08_pembanding`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_pembanding',$data);
		}
		}



	function bangunan(){
		if($this->uri->segment(3)){

			if($this->input->post()){
				//$name 	= $this->input->post('name');
				//$desc 	= $this->input->post('desc');

				if($this->uri->segment(3) == 'add'){
					$data['title'] 		= 'Tambah Vaksinasi';

					$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
				}else{
					$data['title'] 		= 'Edit Vaksinasi';

					$id = $this->uri->segment(4);

					$sql 				= "SELECT * FROM `survey09_bangunan` WHERE data_id = '$id'";
			        $data_db 			= $this->db->query($sql);
			        $data['data_db'] 	= $data_db; 
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'ok';
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_bangunan_add',$data);
		}else{
		$data['title'] = 'Riwayat Imunisasi';

		$sql 				= "SELECT * FROM `survey09_bangunan`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_bang',$data);
		}
	}

}
