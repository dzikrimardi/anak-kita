<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Data_survey extends CI_Controller {

	public function index(){
		//nothing
	}

	function foto(){

		$data['title'] = 'Survey - Data Foto/Denah';


		$sql 				= "	SELECT *
										, (SELECT title FROM `bm_photos_categories` WHERE gallery.tipe = bm_photos_categories.id ) AS `type_name`
								FROM 
									gallery
								";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('data_survey_foto',$data);
	}
	function kategori(){

		$data['title'] = 'Survey - Kategori';


		$sql 				= "SELECT * FROM `bm_photos_categories` ORDER BY pos ASC";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		$this->load->view('data_survey_kategori',$data);
	}
}

