<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Data extends CI_Controller {

	/*
	public function index(){

		$data['title'] = 'Survey - Lokasi';

		$sql 				= "SELECT * FROM `survey01_lokasi`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_lokasi',$data);
	}
	function add_lokasi(){

		$data['title'] = 'Survey - Add User';
		$this->load->view('data_lokasi_add',$data);
	}
	*/

	function add_sarana(){
		$data['exec_query'] = '';
		if($this->input->post()){
			$name 	= $this->input->post('name');
			$desc 	= $this->input->post('desc');


			$sql	= "	INSERT INTO 
							`groups` (
										`id`, 
										`name`, 
										`description`
						) VALUES (
										NULL, 
										'$name', 
										'$desc'
						);";
		        if($this->db->query($sql)){
		        	/*$sql	= "SELECT id FROM groups ORDER BY id DESC LIMIT 1";
		        	$db_user = $this->db->query($sql)->row();
		        	$id_user = $db_user->id;
					foreach ($this->input->post('inp_groups') as $groups_key => $groups_value) {
						//print_r($groups);
			        	$sql	= "	INSERT INTO `groups` (
			        										id,
			        										user_id, 
			        										group_id
			        				) VALUES (
			        										NULL, 
			        										'$id_user', 
			        										'$groups_value'
			        				);";
						$this->db->query($sql);
					}*/
		        	$data['exec_query'] = 'ok';
		        }else{
		        	$data['exec_query'] = 'failed';
		        }

		}
		$data['title'] = 'Survey - Add User';
		$this->load->view('data_sarana_add',$data);
	}

	function lokasi(){
		if($this->uri->segment(3)){
			if($this->uri->segment(3) == 'add'){
				$data['title'] 		= 'Survey - Tambah Lokasi';

				$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
			}else{
				if($this->uri->segment(3) == 'delete'){
					$data['title'] 		= 'Survey - Hapus Lokasi';
				}else{
					$data['title'] 		= 'Survey - Edit Lokasi';
				}

				$id 				= $this->uri->segment(4); 

				$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
		        $data_db 			= $this->db->query($sql)->row();
		        $data['data_db'] 	= $data_db; 
		        //print_r($data_db);
			}

			$data['exec_query'] = '';
			if($this->input->post('delete')){
				$id = $this->input->post('inp_id_data');
				$sql = "DELETE FROM `survey01_lokasi` WHERE id_data = '$id'";
		        if($this->db->query($sql)){
		        	$data['exec_query'] = 'delete';
		        }else{
		        	$data['exec_query'] = 'failed';

		        }
			}
			if($this->input->post() and !$this->input->post('delete')){

				if($this->input->post('inp_id_data')){
					$id = $this->input->post('inp_id_data');
				}
				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
						if($this->input->post('inp_id_data')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_lokasi_add',$data);
		}else{
			$data['title'] = 'Survey - Lokasi';

			$sql 				= "SELECT * FROM `survey01_lokasi`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_lokasi',$data);
		}
	}

	function jalan(){

		if($this->uri->segment(3)){

			$data['exec_query'] = '';
			if($this->input->post()){
				//$name 	= $this->input->post('name');
				//$desc 	= $this->input->post('desc');

				if($this->uri->segment(3) == 'add'){
					$data['title'] 		= 'Survey - Tambah Jalan';

					$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
				}else{
					$data['title'] 		= 'Survey - Edit Jalan';

					$id = $this->uri->segment(4);

					$sql 				= "SELECT * FROM `survey02_jalan` WHERE data_id = '$id'";
			        $data_db 			= $this->db->query($sql);
			        $data['data_db'] 	= $data_db; 
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'ok';
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_jalan_add',$data);
		}else{

			$data['title'] = 'Survey - Jalan';

			$sql 				= "SELECT * FROM `survey02_jalan`";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db'] 	= $data_db; 

			$this->load->view('data_jalan',$data);
		}
	}

	function sarana(){
		if($this->uri->segment(3)){

			$data['exec_query'] = '';
			if($this->input->post()){
				//$name 	= $this->input->post('name');
				//$desc 	= $this->input->post('desc');

				if($this->uri->segment(3) == 'add'){
					$data['title'] 		= 'Survey - Tambah Jalan';

					$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
				}else{
					$data['title'] 		= 'Survey - Edit Jalan';

					$id = $this->uri->segment(4);

					$sql 				= "SELECT * FROM `survey03_pasar` WHERE data_id = '$id'";
			        $data_db 			= $this->db->query($sql);
			        $data['data_db'] 	= $data_db; 
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'ok';
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_sarana_add',$data);
		}else{
		$data['title'] = 'Survey - Sarana';

		$sql 				= "SELECT * FROM `survey03_pasar`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_sarana',$data);
		}
	}

	function legal(){
		if($this->uri->segment(3)){

			$data['exec_query'] = '';
			if($this->input->post()){
				//$name 	= $this->input->post('name');
				//$desc 	= $this->input->post('desc');

				if($this->uri->segment(3) == 'add'){
					$data['title'] 		= 'Survey - Tambah Legal';

					$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
				}else{
					$data['title'] 		= 'Survey - Edit Legal';

					$id = $this->uri->segment(4);

					$sql 				= "SELECT * FROM `survey04_status` WHERE data_id = '$id'";
			        $data_db 			= $this->db->query($sql);
			        $data['data_db'] 	= $data_db; 
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'ok';
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_legal_add',$data);
		}else{
		$data['title'] = 'Survey - Legal Tanah';

		$sql 				= "SELECT * FROM `survey04_status`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_legal',$data);
		}
	}

	function aset(){

		if($this->uri->segment(3)){

			$data['exec_query'] = '';
			if($this->input->post()){
				//$name 	= $this->input->post('name');
				//$desc 	= $this->input->post('desc');

				if($this->uri->segment(3) == 'add'){
					$data['title'] 		= 'Survey - Tambah Aset';

					$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
				}else{
					$data['title'] 		= 'Survey - Edit Aset';

					$id = $this->uri->segment(4);

					$sql 				= "SELECT * FROM `survey06_kelola` WHERE data_id = '$id'";
			        $data_db 			= $this->db->query($sql);
			        $data['data_db'] 	= $data_db; 
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'ok';
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_aset_add',$data);
		}else{
		$data['title'] = 'Survey - Aset';

		$sql 				= "SELECT * FROM `survey06_kelola`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_aset',$data);
		}
	}



	function hbu(){
		if($this->uri->segment(3)){

			$data['exec_query'] = '';
			if($this->input->post()){
				//$name 	= $this->input->post('name');
				//$desc 	= $this->input->post('desc');

				if($this->uri->segment(3) == 'add'){
					$data['title'] 		= 'Survey - Tambah Hbu';

					$id 				= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
				}else{
					$data['title'] 		= 'Survey - Edit Hbu';

					$id = $this->uri->segment(4);

					$sql 				= "SELECT * FROM `survey07_hbu` WHERE data_id = '$id'";
			        $data_db 			= $this->db->query($sql);
			        $data['data_db'] 	= $data_db; 
				}

				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
				$longitude 			= $this->input->post('inp_longitude'); 
				$latitude 			= $this->input->post('inp_latitude'); 
				$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
				$pic 				= $this->input->post('inp_pic'); 
				$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
				$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
				$nama_asset 		= $this->input->post('inp_nama_asset'); 
				$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
				$banjir				= $this->input->post('inp_banjir'); 
				$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
				$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
				$kemudahan 			= $this->input->post('inp_kemudahan'); 
				$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
				$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
				$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
				$transport_01 		= $this->input->post('inp_transport_01'); 
				$transport_02 		= $this->input->post('inp_transport_02'); 
				$alternatif1 		= $this->input->post('inp_alternatif1'); 
				$alternatif2 		= $this->input->post('inp_alternatif2'); 
				$alternatif3 		= $this->input->post('inp_alternatif3'); 
				$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond'
									);
					";
			        if($this->db->query($sql)){
			        	$data['exec_query'] = 'ok';
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

			$this->load->view('data_hbu_add',$data);
		}else{
		$data['title'] = 'Survey - hbu';

		$sql 				= "SELECT * FROM `survey07_hbu`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_hbu',$data);
		}
	}



	function pembanding(){

		$data['title'] = 'Survey - Pembanding';

		$sql 				= "SELECT * FROM `survey08_pembanding`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_pem',$data);
	}



	function bangunan(){

		$data['title'] = 'Riwayat Imunisasi';

		$sql 				= "SELECT * FROM `survey09_bangunan`";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('data_bang',$data);
	}

}

