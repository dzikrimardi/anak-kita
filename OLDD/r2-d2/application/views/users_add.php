<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create
        <small>User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">User Info</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=base_url()?>users/action" method="POST">
            <div class="form-horizontal box-body">

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>

                    <div class="col-sm-3">
                        <input name="inp_user" type="text" class="form-control" id="inputEmail3" placeholder="Username">
                    </div>
                </div>

                <br/>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-3">
                        <input name="inp_pass" type="password" class="form-control" placeholder="Password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Retype&nbsp;Password</label>

                    <div class="col-sm-3">
                        <input name="inp_pass_c" type="password" class="form-control" placeholder="Retype Password">
                    </div>
                </div>

                <hr/>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>

                    <div class="col-sm-10">
                        <input name="inp_first" type="text" class="form-control" placeholder="First Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>

                    <div class="col-sm-10">
                        <input name="inp_last" type="text" class="form-control" placeholder="Last Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">E-mail</label>

                    <div class="col-sm-10">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input name="inp_email" type="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                </div>

                <hr/>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Groups</label>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="inp_groups[]" value="1"> Surveyor Kelompok 1
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="inp_groups[]" value="2"> Surveyor Kelompok 2
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="inp_groups[]" value="3"> Surveyor Kelompok 3
                            </label>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="<?=base_url()?>users" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>users" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>users" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>users/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>users" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>users/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
