<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/login_header_head'); ?>
<?    $this->load->view('inc/login_header_css'); ?>
<?    $this->load->view('inc/login_header_meta_title'); ?>
<?    $this->load->view('inc/login_header_body'); ?>


<div class="login-box">
  <div class="login-logo">
    <a href="<?=base_url();?>"><b>Sur</b>vey</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Admin Survey TB & HBU</p>

    <form action="<?=base_url();?>" method="post">
      <div class="form-group has-feedback">
        <input name="user" type="email" class="form-control" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="pass" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <br/><br/>
          <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
