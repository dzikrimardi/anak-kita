<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Jalan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Jalan Depan Tapak</h3>
              <a href="<?=base_url()?>data/jalan/add" class="btn btn-small btn-info pull-right">
                    Input &nbsp; 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <!--
                                <a href="<?=base_url()?>users/edit/<?=$row->id?>" class="pull-right">
                                    <i class="fa fa-edit" alt="edit"></i>
                                </a> 
                                <span class="pull-right">&nbsp;&nbsp;&nbsp;</span> 
                                <a href="<?=base_url()?>users/view/<?=$row->id?>" class="pull-right">
                                    <i class="fa fa-eye" alt="view"></i>
                                </a>
                                -->
                            </td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->lebar_jalan?></td>
                            <td><?=$row->kelas_jalan?></td>
                            <td><?=$row->jumlah_lajur?></td>
                            <td><?=$row->kondisi_lalin?></td>
                            <td><?=$row->permukaan_jalan?></td>
                            <td><?=$row->kondisi?></td>
                            <td><?=$row->drainase?></td>
                            <td><?=$row->penerangan?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List User</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->lebar_jalan?></td>
                            <td><?=$row->kelas_jalan?></td>
                            <td><?=$row->jumlah_lajur?></td>
                            <td><?=$row->kondisi_lalin?></td>
                            <td><?=$row->permukaan_jalan?></td>
                            <td><?=$row->kondisi?></td>
                            <td><?=$row->drainase?></td>
                            <td><?=$row->penerangan?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
