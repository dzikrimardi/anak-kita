<header class="main-header">

    <!-- Logo -->
    <a href="<?=base_url()?>../" target="r2d2-page" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S/b>VY</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Mobile Survey</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                  <span class="hidden-xs">Mobile</span>
                </a>
                <ul class="dropdown-menu">
                    <!--<li class="user-body">
                    <div class="row">
                      <div class="col-xs-4 text-center">
                        <a href="#">Followers</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Sales</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Friends</a>
                      </div>
                    </div>
                    </li>-->
                    <!-- Menu Footer-->
                    <!--
                    <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a href="<?=base_url()?>logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                    </li>
                  -->
                </ul>
            </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
        </div>

    </nav>
</header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="height: 65px">
        <div class="pull-left info" style="left: 5px">
          <p>Mobile Survey</p>
          <a href="<?=base_url()?>panel/account"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?=$this->uri->segment(1)=='home'?'active':''?>">
          <a href="<?=base_url()?>../" target="r2d2-page">
            <i class="fa fa-home"></i> <span>Home</span>
            <span class="pull-right-container">
              <!--<i class="fa fa-angle-left pull-right"></i>-->
            </span>
          </a>
          <!--<ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>-->
        </li>
          <?if(false){?>
         <li class="<?=$this->uri->segment(1)=='users'?'active':''?> treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='users' && $this->uri->segment(2)==''?'active':''?>">
                <a href="<?=base_url()?>../users" target="r2d2-page"><i class="fa fa-circle-o"></i> List</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='add'?'active':''?>" >
                <a href="<?=base_url()?>../users/add" target="r2d2-page"><i class="fa fa-circle-o"></i> Create</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='groups'?'active':''?>" >
                <a href="<?=base_url()?>../users/groups" target="r2d2-page"><i class="fa fa-circle-o"></i> User Groups</a>
            </li>
          </ul>
        </li>
          <?}?>
        <li>
          <a href="#penugasan">
            <i class="ion ion-image"></i> <span>Pemberian Penugasan</span>
            <!--<span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>-->
          </a>
        </li>
        <li class="<?=$this->uri->segment(1)=='data' || substr($this->uri->segment(1),0,3)=='svl'?'':''?> active treeview">
          <a href="#">
            <i class="ion ion-edit"></i>
            <span>Survey-Lapangan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
            <ul class="treeview-menu">
                <li class="<?=$this->uri->segment(1)=='svl_lokasi'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_lokasi" target="r2d2-page"><i class="fa fa-circle-o"></i> Data Lokasi</a>
                  <!--
                  <ul>
                    <li>
                      <a href="<?=base_url()?>../svl_lokasi/lat?r2d2=true" target="r2d2-page" id="get_lat"><i class="fa fa-circle-o"></i> Latitude</a>
                    </li>
                  </ul>
                -->
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_jalan'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_jalan" target="r2d2-page"><i class="fa fa-circle-o"></i> Jalan di Depan Tapak</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_sarana'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_sarana" target="r2d2-page"><i class="fa fa-circle-o"></i> Sarana Umum</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_legal'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_legal" target="r2d2-page"><i class="fa fa-circle-o"></i> Data Legal Tanah</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_aset'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_aset" target="r2d2-page"><i class="fa fa-circle-o"></i> Pengelolaan Asset</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_hbu'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_hbu" target="r2d2-page"><i class="fa fa-circle-o"></i> HBU</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_pembanding'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_pembanding" target="r2d2-page"><i class="fa fa-circle-o"></i> Form Pembanding</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_bangunan'?'active':''?>" >
                    <a href="<?=base_url()?>../svl_bangunan" target="r2d2-page"><i class="fa fa-circle-o"></i> Form Bangunan</a>
                </li>
            </ul>
        </li>
        <?if(false){?>
        <li class="treeview <?=$this->uri->segment(1)=='survey_admin'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i>
            <span>Survey-Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='survey_admin' && $this->uri->segment(2)=='download'?'active':''?>" >
                <a href="<?=base_url()?>survey_admin/download"><i class="fa fa-circle-o"></i> Download Hasil Survey</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='upload'?'active':''?>" >
                <a href="<?=base_url()?>survey_admin/upload"><i class="fa fa-circle-o"></i> Upload Hasil Pengolahan</a>
            </li>
          </ul>
        </li>
        <li class="treeview <?=$this->uri->segment(1)=='data_survey'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i> <span>Data Foto/Denah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='data_survey' && $this->uri->segment(2)=='foto'?'active':''?>" >
                <a href="<?=base_url()?>data_survey/foto"><i class="fa fa-circle-o"></i> Daftar Foto/Denah</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='kategori'?'active':''?>" >
                <a href="<?=base_url()?>data_survey/kategori"><i class="fa fa-circle-o"></i> Kategori</a>
            </li>
          </ul>
        </li>
        <li class="treeview <?=$this->uri->segment(1)=='laporan'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='laporan' && $this->uri->segment(2)=='per_surveyor'?'active':''?>" >
                <a href="<?=base_url()?>laporan/per_surveyor"><i class="ion ion-edit"></i>per Surveyor</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='per_lokasi'?'active':''?>" >
                <a href="<?=base_url()?>laporan/per_lokasi"><i class="ion ion-edit"></i>per Lokasi</a>
            </li>
          </ul>
        </li>
        <li class="treeview <?=$this->uri->segment(1)=='visual'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i> <span>Visualisasi Titik</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(2)=='produk'?'active':''?>" >
                <a href="<?=base_url()?>visual/produk"><i class="ion ion-edit"></i>Daftar Produk</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='kategori'?'active':''?>" >
                <a href="<?=base_url()?>visual/kategori"><i class="ion ion-edit"></i>Kategori</a>
            </li>
          </ul>
        </li>
        <li class="<?=$this->uri->segment(1)=='admin'?'active':''?> treeview">
          <a href="#">
            <i class="fa fa-cog"></i> <span>Admin Panel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='admin' && $this->uri->segment(2)==''?'active':''?>" >
                <a href="<?=base_url()?>admin"><i class="ion ion-edit"></i>Admin Users</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='add'?'active':''?>" >
                <a href="<?=base_url()?>admin/add"><i class="ion ion-edit"></i>Create Admin User</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='groups'?'active':''?>" >
                <a href="<?=base_url()?>admin/groups"><i class="ion ion-edit"></i>Create Admin Groups</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Utilities</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(2)==''?'active':''?>" >
                <a href="<?=base_url()?>utilities/database"><i class="ion ion-edit"></i>Database Versions</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
            <a href="<?=base_url()?>logout">
                <i class="fa fa-sign-out"></i> <span>Sign Out</span>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-circle-o"></i> Level One
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Level Two
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
        <?}?>
        <li class="header">Latitude</li>
        <li>
            <a href="#">
                <i class="fa fa-circle-o text-aqua"></i> 
                <span id="view_lat">
                    <?=!empty($_COOKIE['lat'])?$_COOKIE['lat']:''?>,
                    <?=!empty($_COOKIE['long'])?$_COOKIE['long']:''?>
                </span>
            </a>
        </li>
          
        <!--<li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
        <!--
        <li class="header">USEFUL LINKS</li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Frontend Website</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>API Site</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-green"></i> <span>Github Repo</span></a></li>
        -->
      </ul>
                <font color="white">
            <?
            //echo "cookies : ";
            //var_dump($_COOKIE);
            //echo " monster";
            ?>
                </font>

    </section>
    <!-- /.sidebar -->
  </aside>
  