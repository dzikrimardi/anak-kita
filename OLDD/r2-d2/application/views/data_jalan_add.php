<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add Jalan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Jalan</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=base_url()?>data/add_jalan" method="POST">
            <div class="form-horizontal box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data</label>

                    <div class="col-sm-10">
                        <input name="inp_id_data" type="text" class="form-control" placeholder="Id Data">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">User Id</label>

                    <div class="col-sm-10">
                        <input name="inp_user_id" type="text" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ada Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_ada_jalan" type="text" class="form-control" placeholder="Ada Jalan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kode Tapak</label>

                    <div class="col-sm-10">
                        <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Tapak">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Peruntukan</label>

                    <div class="col-sm-10">
                        <input name="inp_peruntukan" type="text" class="form-control" placeholder="Peruntukan">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Peruntukan Lainnya</label>

                    <div class="col-sm-10">
                        <input name="inp_peruntukan_dll" type="text" class="form-control" placeholder="Peruntukan Lainnya">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kdb</label>

                    <div class="col-sm-10">
                        <input name="inp_kdb" type="text" class="form-control" placeholder="Kdb">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Klb</label>

                    <div class="col-sm-10">
                        <input name="inp_klb" type="text" class="form-control" id="inputEmail3" placeholder="Klb">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Gsb</label>

                    <div class="col-sm-10">
                        <input name="inp_gsb" type="text" class="form-control" id="inputEmail3" placeholder="Gsb">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tinggi Maks</label>

                    <div class="col-sm-10">
                        <input name="inp_tinggi_maks" type="text" class="form-control" id="inputEmail3" placeholder="Tinggi Maks">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sesuai Peraturan</label>

                    <div class="col-sm-10">
                        <input name="inp_sesuai_per" type="text" class="form-control" id="inputEmail3" placeholder="Sesuai Peraturan">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sesuai Eksisting</label>

                    <div class="col-sm-10">
                        <input name="inp_sesuai_eks" type="text" class="form-control" id="inputEmail3" placeholder="Sesuai Eksisting">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lebar Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_leb_jal" type="text" class="form-control" id="inputEmail3" placeholder="Lebar Jalan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kelas Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_kel_jal" type="text" class="form-control" id="inputEmail3" placeholder="Kelas Jalan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Lajur</label>

                    <div class="col-sm-10">
                        <input name="inp_jum_laj" type="text" class="form-control" id="inputEmail3" placeholder="Jumlah Lajur">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kondisi Lalin</label>

                    <div class="col-sm-10">
                        <input name="inp_kon_lal" type="text" class="form-control" id="inputEmail3" placeholder="Kondisi Lalin">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Permukaan Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_perm_jal" type="text" class="form-control" id="inputEmail3" placeholder="Permukaan Jalan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kondisi</label>

                    <div class="col-sm-10">
                        <input name="inp_kond" type="text" class="form-control" id="inputEmail3" placeholder="Kondisi">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Drainase</label>

                    <div class="col-sm-10">
                        <input name="inp_drain" type="text" class="form-control" id="inputEmail3" placeholder="Drainase">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Penerangan</label>

                    <div class="col-sm-10">
                        <input name="inp_pener" type="text" class="form-control" id="inputEmail3" placeholder="Penerangan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 1</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_1" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 1">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 2</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_2" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 2">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 3</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_3" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 3">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 4</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_4" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 4">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 5</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_5" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 5">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 6</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_6" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 6">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approv" type="text" class="form-control" id="inputEmail3" placeholder="Approval">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Respond</label>

                    <div class="col-sm-10">
                        <input name="inp_resp" type="text" class="form-control" id="inputEmail3" placeholder="Respond">
                    </div>
                </div>
               
                <div class="box-footer">
                    <a href="<?=base_url()?>data/jalan" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/jalan" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/jalan" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/jalan/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/jalan" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/jalan/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
