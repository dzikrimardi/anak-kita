<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Aset</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Pengelolaan Aset</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->status_kelola?></td>
                          <td><?=$row->status_kelola_d?></td>
                          <td><?=$row->pengguna?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><?=$row->status_manfaat?></td>
                          <td><?=$row->status_manfaat_m?></td>
                          <td><?=$row->status_legal?></td>
                          <td><?=$row->catatan_unclear?></td>
                          <td><?=$row->catatan_unclean?></td>
                          <td><?=$row->sengketa?></td>
                          <td><?=$row->sengketa_d?></td>
                          <td><?=$row->mediasi?></td>
                          <td><?=$row->mediasi_d?></td>
                          <td><?=$row->gugatan?></td>
                          <td><?=$row->gugatan_d?></td>
                          <td><?=$row->pemblokiran?></td>
                          <td><?=$row->pemblokiran_d?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                       <?
                          }
                        }
                       ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Pengelolaan Aset</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->status_kelola?></td>
                          <td><?=$row->status_kelola_d?></td>
                          <td><?=$row->pengguna?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><?=$row->status_manfaat?></td>
                          <td><?=$row->status_manfaat_m?></td>
                          <td><?=$row->status_legal?></td>
                          <td><?=$row->catatan_unclear?></td>
                          <td><?=$row->catatan_unclean?></td>
                          <td><?=$row->sengketa?></td>
                          <td><?=$row->sengketa_d?></td>
                          <td><?=$row->mediasi?></td>
                          <td><?=$row->mediasi_d?></td>
                          <td><?=$row->gugatan?></td>
                          <td><?=$row->gugatan_d?></td>
                          <td><?=$row->pemblokiran?></td>
                          <td><?=$row->pemblokiran_d?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                        </tr>
                       <?
                          }
                        }
                       ?>
                    </tbody>
                    <!--
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Status Kelola</th>
                            <th>Status Kelola d</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Manfaat</th>
                            <th>Status Manfaat m</th>
                            <th>Status Legal</th>
                            <th>Catatan unClear</th>
                            <th>Catatan unClean</th>
                            <th>Sengketa</th>
                            <th>Sengketa d</th>
                            <th>Mediasi</th>
                            <th>Mediasi d</th>
                            <th>Gugatan</th>
                            <th>Gugatan d</th>
                            <th>Pemblokiran</th>
                            <th>Pemblokiran d</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                    -->
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
