<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">
<?//php $data_title['title'] = $title;?>
<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Users</a></li>
        <!--
        <li class="active">Laporan</li>
        -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <?if(!$this->uri->segment(3)){?>
            <div class="box-header">
              <h3 class="box-title">Per Surveyor</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Username</th>
                            <th>First&nbsp;Name</th>
                            <th>Last&nbsp;Name</th>
                            <th>Email</th>
                            <th>Groups</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td>
                                <?=$i?>
                            </td>
                            <td>
                                <a href="<?=base_url()?>laporan/per_surveyor/<?=$row->id?>">
                                    <i class="fa fa-eye" alt="view"></i>&nbsp;view
                                </a>
                            </td>
                            <td>
                                <?=$row->username?>                            
                            </td>
                          <td><?=$row->first_name?></td>
                          <td><?=$row->last_name?></td>
                          <td><?=$row->email?></td>
                          <td><?=substr($row->list_groups, 0, strlen($row->list_groups)-2)?></td>
                          <td><?=$row->active==1?'Active':'Non&nbsp;Active'?></td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>First&nbsp;Name</th>
                            <th>Last&nbsp;Name</th>
                            <th>Email</th>
                            <th>Groups</th>
                            <th>Status</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List User</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>First&nbsp;Name</th>
                            <th>Last&nbsp;Name</th>
                            <th>Email</th>
                            <th>Groups</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->username?></td>
                          <td><?=$row->first_name?></td>
                          <td><?=$row->last_name?></td>
                          <td><?=$row->email?></td>
                          <td><?=substr($row->list_groups, 0, strlen($row->list_groups)-2)?></td>
                          <td><?=$row->active==1?'Active':'Non&nbsp;Active'?></td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>First&nbsp;Name</th>
                            <th>Last&nbsp;Name</th>
                            <th>Email</th>
                            <th>Groups</th>
                            <th>Status</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
            <?}else{?>

                <?
                $db_obj = $data_db->row();
                //print_r($db_obj);
                ?>
            <div class="box-header">
                <center>
                    <h1>
                        <?=$db_obj->first_name?> <?=$db_obj->last_name?>
                        <br/>
                        <small>
                            <?=substr($db_obj->list_groups, 0, strlen($db_obj->list_groups)-2)?>
                            <br/>
                            <b>
                                <?=$db_obj->active==1?'Active':'Non&nbsp;Active'?>
                            </b>
                        </small>
                    </h1>
                </center>            
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db_rows->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db_rows->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->lebar_jalan?></td>
                            <td><?=$row->kelas_jalan?></td>
                            <td><?=$row->jumlah_lajur?></td>
                            <td><?=$row->kondisi_lalin?></td>
                            <td><?=$row->permukaan_jalan?></td>
                            <td><?=$row->kondisi?></td>
                            <td><?=$row->drainase?></td>
                            <td><?=$row->penerangan?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <?
                $db_obj = $data_db->row();
                print_r($db_obj);
                ?>
                <center>
                    <h1>
                        <?=$db_obj->first_name?> <?=$db_obj->last_name?>
                        <br/>
                        <small>
                            <?=substr($db_obj->list_groups, 0, strlen($db_obj->list_groups)-2)?>
                            <br/>
                            <b>
                                <?=$db_obj->active==1?'Active':'Non&nbsp;Active'?>
                            </b>
                        </small>
                    </h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db_rows->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db_rows->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->lebar_jalan?></td>
                            <td><?=$row->kelas_jalan?></td>
                            <td><?=$row->jumlah_lajur?></td>
                            <td><?=$row->kondisi_lalin?></td>
                            <td><?=$row->permukaan_jalan?></td>
                            <td><?=$row->kondisi?></td>
                            <td><?=$row->drainase?></td>
                            <td><?=$row->penerangan?></td>
                            <td><?=$row->petunjuk1?></td>
                            <td><?=$row->petunjuk2?></td>
                            <td><?=$row->petunjuk3?></td>
                            <td><?=$row->petunjuk4?></td>
                            <td><?=$row->petunjuk5?></td>
                            <td><?=$row->petunjuk6?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Lebar Jalan</th>
                            <th>Kelas Jalan</th>
                            <th>Jumlah Lajur</th>
                            <th>Kondisi Lalin</th>
                            <th>Permukaan Jalan</th>
                            <th>Kondisi</th>
                            <th>Drainase</th>
                            <th>Penerangan</th>
                            <th>Petunjuk&nbsp;1</th>
                            <th>Petunjuk&nbsp;2</th>
                            <th>Petunjuk&nbsp;3</th>
                            <th>Petunjuk&nbsp;4</th>
                            <th>Petunjuk&nbsp;5</th>
                            <th>Petunjuk&nbsp;6</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->          
            <?}?>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
