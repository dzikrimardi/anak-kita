<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}

$back_url   = base_url()."svl_jalan/";
?>


<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey Lapangan</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Jalan</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Jalan</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=$back_url?>action" method="POST">
            <div class="form-horizontal box-body">
                <!--edit-->
                <?if($id){?>
                <?//print_r($data_db);?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data</label>

                    <div class="col-sm-3">
                        <label class="col-sm-2 control-label row"><?=$id?></label>
                        <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id?>" >
                    </div>
                </div>
                <?}?>
                <!--//edit-->


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Group Id</label>

                    <div class="col-sm-10">
                        <input name="inp_group_id" type="text" class="form-control" placeholder="Group Id" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->group_id)?$data_db->group_id:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">User Id</label>

                    <div class="col-sm-10">
                        <input name="inp_user_id" type="text" class="form-control" placeholder="User Id" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->user_id)?$data_db->user_id:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Ada Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_ada_jalan" type="text" class="form-control" placeholder="Ada Jalan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->ada_jalan)?$data_db->ada_jalan:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kode Tapak</label>

                    <div class="col-sm-10">
                        <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Tapak" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Peruntukan</label>

                    <div class="col-sm-10">
                        <input name="inp_peruntukan" type="text" class="form-control" placeholder="Peruntukan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->peruntukan)?$data_db->peruntukan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Peruntukan Lainnya</label>

                    <div class="col-sm-10">
                        <input name="inp_peruntukan_dll" type="text" class="form-control" placeholder="Peruntukan Lainnya" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->peruntukan_lainnya)?$data_db->peruntukan_lainnya:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kdb</label>

                    <div class="col-sm-10">
                        <input name="inp_kdb" type="text" class="form-control" placeholder="Kdb" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kdb)?$data_db->kdb:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Klb</label>

                    <div class="col-sm-10">
                        <input name="inp_klb" type="text" class="form-control" id="inputEmail3" placeholder="Klb" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->klb)?$data_db->klb:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Gsb</label>

                    <div class="col-sm-10">
                        <input name="inp_gsb" type="text" class="form-control" id="inputEmail3" placeholder="Gsb" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->gsb)?$data_db->gsb:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tinggi Maks</label>

                    <div class="col-sm-10">
                        <input name="inp_tinggi_maks" type="text" class="form-control" id="inputEmail3" placeholder="Tinggi Maks" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tinggi_maks)?$data_db->tinggi_maks:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sesuai Peraturan</label>

                    <div class="col-sm-10">
                        <input name="inp_sesuai_per" type="text" class="form-control" id="inputEmail3" placeholder="Sesuai Peraturan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sesuai_peraturan)?$data_db->sesuai_peraturan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sesuai Eksisting</label>

                    <div class="col-sm-10">
                        <input name="inp_sesuai_eks" type="text" class="form-control" id="inputEmail3" placeholder="Sesuai Eksisting" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sesuai_eksisting)?$data_db->sesuai_eksisting:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Lebar Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_leb_jal" type="text" class="form-control" id="inputEmail3" placeholder="Lebar Jalan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lebar_jalan)?$data_db->lebar_jalan:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kelas Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_kel_jal" type="text" class="form-control" id="inputEmail3" placeholder="Kelas Jalan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kelas_jalan)?$data_db->kelas_jalan:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jumlah Lajur</label>

                    <div class="col-sm-10">
                        <input name="inp_jum_laj" type="text" class="form-control" id="inputEmail3" placeholder="Jumlah Lajur" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jumlah_lajur)?$data_db->jumlah_lajur:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kondisi Lalin</label>

                    <div class="col-sm-10">
                        <input name="inp_kon_lal" type="text" class="form-control" id="inputEmail3" placeholder="Kondisi Lalin" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kondisi_lalin)?$data_db->kondisi_lalin:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Permukaan Jalan</label>

                    <div class="col-sm-10">
                        <input name="inp_perm_jal" type="text" class="form-control" id="inputEmail3" placeholder="Permukaan Jalan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kondisi</label>

                    <div class="col-sm-10">
                        <input name="inp_kond" type="text" class="form-control" id="inputEmail3" placeholder="Kondisi" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kondisi)?$data_db->kondisi:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Drainase</label>

                    <div class="col-sm-10">
                        <input name="inp_drain" type="text" class="form-control" id="inputEmail3" placeholder="Drainase" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->drainase)?$data_db->drainase:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Penerangan</label>

                    <div class="col-sm-10">
                        <input name="inp_pener" type="text" class="form-control" id="inputEmail3" placeholder="Penerangan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->penerangan)?$data_db->penerangan:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 1</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_1" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 1" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk1)?$data_db->petunjuk1:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 2</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_2" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 2" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk2)?$data_db->petunjuk2:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 3</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_3" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 3" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk3)?$data_db->petunjuk3:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 4</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_4" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 4" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk4)?$data_db->petunjuk4:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 5</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_5" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 5" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk5)?$data_db->petunjuk5:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Petunjuk 6</label>

                    <div class="col-sm-10">
                        <input name="inp_petun_6" type="text" class="form-control" id="inputEmail3" placeholder="Petunjuk 6" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk6)?$data_db->petunjuk6:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approv" type="text" class="form-control" id="inputEmail3" placeholder="Approval" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->approval)?$data_db->approval:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Respond</label>

                    <div class="col-sm-10">
                        <input name="inp_resp" type="text" class="form-control" id="inputEmail3" placeholder="Respond" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->respond)?$data_db->respond:''?>">
                    </div>
                </div>
               


                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
