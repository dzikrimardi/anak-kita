<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper_">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Legal</li>
      </ol>
    </section>

    <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Data Legal</h3>
              <a href="<?=base_url()?>data/legal/add" class="btn btn-small btn-info pull-right">
                    Input &nbsp; 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->luas_tapak?></td>
                          <td><?=$row->status_tanah?></td>
                          <td><?=$row->status_tanah_ajb_d?></td>
                          <td><?=$row->status_tanah_desc1?></td>
                          <td><?=$row->status_tanah_desc2?></td>
                          <td><?=$row->nomor_surat?></td>
                          <td><?=$row->kode_sertifikat?></td>
                          <td><?=$row->nama_pemegang?></td>
                          <td><?=$row->tgl_terbit?></td>
                          <td><?=$row->tgl_berakhir?></td>
                          <td><?=$row->no_tgl_sk?></td>
                          <td><?=$row->no_peta_pendaftaran?></td>
                          <td><?=$row->no_gambar_situasi?></td>
                          <td><?=$row->tgl_surat_ukur?></td>
                          <td><?=$row->keadaan_tanah?></td>
                          <td><?=$row->perubahan_sertifikat?></td>
                          <td><?=$row->jenis_perubahan?></td>
                          <td><?=$row->pembebasan_hak?></td>
                          <td><?=$row->jenis_bukti?></td>
                          <td><?=$row->nomor_bukti?></td>
                          <td><?=$row->tanggal_bukti?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Data Legal</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                         <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->luas_tapak?></td>
                          <td><?=$row->status_tanah?></td>
                          <td><?=$row->status_tanah_ajb_d?></td>
                          <td><?=$row->status_tanah_desc1?></td>
                          <td><?=$row->status_tanah_desc2?></td>
                          <td><?=$row->nomor_surat?></td>
                          <td><?=$row->kode_sertifikat?></td>
                          <td><?=$row->nama_pemegang?></td>
                          <td><?=$row->tgl_terbit?></td>
                          <td><?=$row->tgl_berakhir?></td>
                          <td><?=$row->no_tgl_sk?></td>
                          <td><?=$row->no_peta_pendaftaran?></td>
                          <td><?=$row->no_gambar_situasi?></td>
                          <td><?=$row->tgl_surat_ukur?></td>
                          <td><?=$row->keadaan_tanah?></td>
                          <td><?=$row->perubahan_sertifikat?></td>
                          <td><?=$row->jenis_perubahan?></td>
                          <td><?=$row->pembebasan_hak?></td>
                          <td><?=$row->jenis_bukti?></td>
                          <td><?=$row->nomor_bukti?></td>
                          <td><?=$row->tanggal_bukti?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
