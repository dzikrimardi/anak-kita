<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper_">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">HBU</li>
      </ol>
    </section>

    <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List HBU</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$jenis_properti?></td>
                          <td><?=$row->nama?></td>
                          <td><?=$row->luas_tanah?></td>
                          <td><?=$row->luas_bangunan?></td>
                          <td><?=$row->jarak_tapak?></td>
                          <td><?=$row->jumlah_kamar?></td>
                          <td><?=$row->kelas_hotel?></td>
                          <td><?=$row->grade_apartemen?></td>
                          <td><?=$row->grade_perkantoran?></td>
                          <td><?=$row->grade_ruko?></td>
                          <td><?=$row->grade_mall?></td>
                          <td><?=$row->grade_perumahan?></td>
                          <td><?=$row->kawasan_wisata?></td>
                          <td><?=$row->keterangan?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List HBU</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$jenis_properti?></td>
                          <td><?=$row->nama?></td>
                          <td><?=$row->luas_tanah?></td>
                          <td><?=$row->luas_bangunan?></td>
                          <td><?=$row->jarak_tapak?></td>
                          <td><?=$row->jumlah_kamar?></td>
                          <td><?=$row->kelas_hotel?></td>
                          <td><?=$row->grade_apartemen?></td>
                          <td><?=$row->grade_perkantoran?></td>
                          <td><?=$row->grade_ruko?></td>
                          <td><?=$row->grade_mall?></td>
                          <td><?=$row->grade_perumahan?></td>
                          <td><?=$row->kawasan_wisata?></td>
                          <td><?=$row->keterangan?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
