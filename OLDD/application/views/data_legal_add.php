<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(4);
$edit       = '';
$delete     = $this->uri->segment(3) == 'delete'?'true':'';
if($this->uri->segment(3) == 'add' || $this->uri->segment(3) == 'edit' ){
    $edit   = 'true';
}

//$back_url   = base_url()."data/jalan/";
?>


<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add Sarana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Sarana</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=base_url()?>data/sarana/add" method="POST">
            <div class="form-horizontal box-body">
                <?if($id){?>
                <?//print_r($data_db);?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data</label>

                    <div class="col-sm-3">
                        <label class="col-sm-2 control-label row"><?=$id?></label>
                        <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id?>" >
                    </div>
                </div>
                <?}?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data Status</label>

                    <div class="col-sm-10">
                        <input name="inp_id_data_status" type="text" class="form-control" placeholder="Id Data Status">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Group Id</label>

                    <div class="col-sm-10">
                        <input name="inp_group_id" type="text" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">User Id</label>

                    <div class="col-sm-10">
                        <input name="inp_user_id" type="text" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Tanah</label>

                    <div class="col-sm-10">
                        <input name="inp_status_tanah" type="text" class="form-control" placeholder="Status Tanah">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Tanah ajb d</label>

                    <div class="col-sm-10">
                        <input name="inp_status_tanah_ajb_d" type="text" class="form-control" placeholder="Status Tanah ajb d">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Tanah desc 1</label>

                    <div class="col-sm-10">
                        <input name="inp_status_tanah_desc1" type="text" class="form-control" placeholder="Status Tanah desc 1">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Tanah desc 2</label>

                    <div class="col-sm-10">
                        <input name="inp_status_tanah_desc2" type="text" class="form-control" placeholder="Status Tanah desc 2">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Luas Tapak</label>

                    <div class="col-sm-10">
                        <input name="inp_luas_tapak" type="text" class="form-control" id="inputEmail3" placeholder="Luas Tapak">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nomor Surat</label>

                    <div class="col-sm-10">
                        <input name="inp_nomor_surat" type="text" class="form-control" id="inputEmail3" placeholder="Nomor Surat">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kode Sertifikat</label>

                    <div class="col-sm-10">
                        <input name="inp_kode_sertifikat" type="text" class="form-control" id="inputEmail3" placeholder="Kode Sertifikat">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama Pemegang</label>

                    <div class="col-sm-10">
                        <input name="inp_nama_pemegangm" type="text" class="form-control" id="inputEmail3" placeholder="Nama Pemegang">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Terbit</label>

                    <div class="col-sm-10">
                        <input name="inp_tgl_terbit" type="text" class="form-control" id="inputEmail3" placeholder="Tanggal Terbit">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Berakhir</label>

                    <div class="col-sm-10">
                        <input name="inp_tgl_berakhir" type="text" class="form-control" id="inputEmail3" placeholder="Tanggal Berakhir">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">No Tanggal sk</label>

                    <div class="col-sm-10">
                        <input name="inp_no_tgl_sk" type="text" class="form-control" id="inputEmail3" placeholder="No Tanggal sk">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">No Peta Pendaftaran</label>

                    <div class="col-sm-10">
                        <input name="inp_no_peta_pendaftaran" type="text" class="form-control" id="inputEmail3" placeholder="No Peta Pendaftaran">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">No Gambar Situasi</label>

                    <div class="col-sm-10">
                        <input name="inp_no_gambar_situasi" type="text" class="form-control" id="inputEmail3" placeholder="No Gambar Situasi">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Surat Ukur</label>

                    <div class="col-sm-10">
                        <input name="inp_tgl_surat_ukur" type="text" class="form-control" id="inputEmail3" placeholder="Tanggal Surat Ukur">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Keadaan Tanah</label>

                    <div class="col-sm-10">
                        <input name="inp_keadaan_tanah" type="text" class="form-control" id="inputEmail3" placeholder="Keadaan Tanah">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Perubahan Sertifikat</label>

                    <div class="col-sm-10">
                        <input name="inp_perubahan_sertifikat" type="text" class="form-control" id="inputEmail3" placeholder="Perubahan Sertifikat">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Perubahan</label>

                    <div class="col-sm-10">
                        <input name="inp_jenis_perubahan" type="text" class="form-control" id="inputEmail3" placeholder="Jenis Perubahan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pembebasan Hak</label>

                    <div class="col-sm-10">
                        <input name="inp_pembebasan_hak" type="text" class="form-control" id="inputEmail3" placeholder="Pembebasan Hak">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Bukti</label>

                    <div class="col-sm-10">
                        <input name="inp_jenis_bukti" type="text" class="form-control" id="inputEmail3" placeholder="Jenis Bukti">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nomor Bukti</label>

                    <div class="col-sm-10">
                        <input name="inp_nomor_bukti" type="text" class="form-control" id="inputEmail3" placeholder="Nomor Bukti">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Bukti</label>

                    <div class="col-sm-10">
                        <input name="inp_tanggal_bukti" type="text" class="form-control" id="inputEmail3" placeholder="Tanggal Bukti">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approv" type="text" class="form-control" id="inputEmail3" placeholder="Approval">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Respond</label>

                    <div class="col-sm-10">
                        <input name="inp_respond" type="text" class="form-control" id="inputEmail3" placeholder="Respond">
                    </div>
                </div>
               
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/sarana/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/sarana/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
