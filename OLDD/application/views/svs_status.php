<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$back_url = base_url()."svs_status/";
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Lokasi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">
                Status Data Lokasi
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Id Data</th>
                            <th>Status</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    $status_unit = $row->status;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                </center>                           
                            </td>
                            <td><?=$row->id_data?></td>
                            <td>
                                <?if($status_unit == '1'){?>
                                    <span class="label label-danger">Belum lengkap</span>
                                <?}else if($status_unit == '2'){?>
                                    <span class="label label-warning">Lengkap, Belum diverifikasi</span>
                                <?}else if($status_unit == '3'){?>
                                    <span class="label label-success">Lengkap, Sudah diverifikasi</span>
                                <?}else{?>
                                    <span class="label label-info">ditugaskan ke surveyor</span>
                                <?}?>
                            </td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->longitude?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td><?=$row->approval==1?'yes':'-'?></td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Id Data</th>
                            <th>Status</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>Data Lokasi</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Asset Internal</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport 01</th>
                            <th>Transport 02</th>
                            <th>Alternatif 2</th>
                            <th>Alternatif 3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Actions</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Asset Internal</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport 01</th>
                            <th>Transport 02</th>
                            <th>Alternatif 2</th>
                            <th>Alternatif 3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Actions</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
