<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<!-- //20171228 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">
<style type="text/css">    
    /*iframe*/
    #upload_frame {
        border:0px;
        height:40px;
        width:400px;
        display:none;
    }

    #progress_container {
        width: 300px; 
        height: 30px; 
        border: 1px solid #CCCCCC; 
        background-color:#EBEBEB;
        display: block; 
        margin:5px 0px -15px 0px;
    }

    #progress_bar {
        position: relative; 
        height: 30px; 
        background-color: #F3631C; 
        width: 0%; 
        z-index:10; 
    }

    #progress_completed {
        font-size:16px; 
        z-index:40; 
        line-height:30px; 
        padding-left:4px; 
        color:#FFFFFF;
    }    
</style>


<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}

$back_url   = base_url()."svl_lokasi/";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <!--
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey Lapangan</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Lokasi</a></li>
        <li class="active">Add</li>
      </ol>
    </section>
    -->

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">







<?php 

$url = basename($_SERVER['SCRIPT_FILENAME']); 

//Get file upload progress information. 
if(isset($_GET['progress_key'])) { 
    $status = apc_fetch('upload_'.$_GET['progress_key']); 
    echo $status['current']/$status['total']*100; 
    die; 
} 
// 

?> 


                <body style="margin:0px"> 
                <!--Progress bar divs--> 
                <div id="progress_container"> 
                    <div id="progress_bar"> 
                           <div id="progress_completed"></div> 
                    </div> 
                </div> 
                <!----> 








         
            </div>
            <!-- /.col -->
        </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- //20171228 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<script> 
$(document).ready(function() {  
// 

    setInterval(function()  
        { 
    $.get("<?php echo $url; ?>?progress_key=<?php echo $_GET['up_id']; ?>&randval="+ Math.random(), {  
        //get request to the current URL (upload_frame.php) which calls the code at the top of the page.  It checks the file's progress based on the file id "progress_key=" and returns the value with the function below:
    }, 
        function(data)    //return information back from jQuery's get request 
            { 
                $('#progress_container').fadeIn(100);    //fade in progress bar     
                $('#progress_bar').width(data +"%");    //set width of progress bar based on the $status value (set at the top of this page) 
                $('#progress_completed').html(parseInt(data) +"%");    //display the % completed within the progress bar 
            } 
        )},500);    //Interval is set at 500 milliseconds (the progress bar will refresh every .5 seconds) 

}); 


</script> 
<?php $this->load->view('inc/home_footer_body'); ?>
