<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
//$id         = $this->input->get('lokasi');
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data    = $this->input->get('lokasi');
$back_url   = base_url()."svl_pembanding/";

$file_id    = time();

$img_file   = "";
$img_file_d = "";
$img_file_j = "";

if(!empty($data_db->sketsa_lokasi)){
    $img_file           = base_url().'img_files/group_'.$data_db->group_id.'/'.$data_db->sketsa_lokasi;
    if(!(file_exists('img_files/group_'.$data_db->group_id.'/'.$data_db->sketsa_lokasi))){
        $img_file       = base_url().'img_files/group_/'.$data_db->sketsa_lokasi;
    }
}

if(!empty($data_db->sketsa_lokasi_d)){
    $img_file_d         = base_url().'img_files/group_'.$data_db->group_id.'/'.$data_db->sketsa_lokasi_d;
    if(!(file_exists('img_files/group_'.$data_db->group_id.'/'.$data_db->sketsa_lokasi_d))){
        $img_file_d     = base_url().'img_files/group_/'.$data_db->sketsa_lokasi_d;
    }
}

if(!empty($data_db->sketsa_lokasi_j)){
    $img_file_j         = base_url().'img_files/group_'.$data_db->group_id.'/'.$data_db->sketsa_lokasi_j;
    if(!(file_exists('img_files/group_'.$data_db->group_id.'/'.$data_db->sketsa_lokasi_j))){
        $img_file_j     = base_url().'img_files/group_/'.$data_db->sketsa_lokasi_j;
    }
}
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey</a></li>
        <li class="">Pembanding</li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST">
            <div class="form-horizontal box-body">

                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                            Pembanding
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">User & Group</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->email ?'selected=""selected':''?> 
                                        value="<?=$row->email?>" 
                                        ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>           



                        <div class="col-sm-6 row">
                            <select id="group_id" name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$data_db->group_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->name?> ( <?=$row->description?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--//edit-->

                <?if(empty($id)){?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Id Data Banding</label>

                    <div class="col-sm-10">
                        <label for="inputEmail3" class="control-label">
                        <font style="opacity: 0.25">
                            (auto)
                        </font>
                        </label>
                    </div>
                </div>
                <?}?>

                <div class="box-header with-border">
                    <center>
                        <h3>
                            Lokasi
                        </h3>
                    </center>
                </div>
                </br>
                <!--

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Foto Data</label>

                    <div class="col-sm-10">
                        <input name="inp_sketsa_lokasi" type="file" id="" placeholder="sketsa_lokasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>

                -->
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Lokasi (Latitude)</label>

                    <div class="col-sm-10 row">
                        <div class="col-sm-5"">
                            <input name="inp_latitude" id="latitude" type="text" class="form-control" placeholder="Latitude"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->latitude)?$data_db->latitude:''?>">
                        </div>
                        <div class="col-sm-5"">
                            <input name="inp_longitude" id="longitude" type="text" class="form-control" placeholder="Longitude"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->longitude)?$data_db->longitude:''?>">
                        </div>
                        <div class="col-sm-2"">
                            <font style="opacity: 7">
                                <a href="#get_lat" id="get_lat">
                                    <i class="fa fa-2x fa-map-marker"></i> 
                                </a>
                                <div id="view_lat"></div>
                            </font>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Harga Penawaran</label>

                    <div class="col-sm-3">
                        <input name="inp_harga_pen" type="text" class="form-control" placeholder="Harga Penawaran"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->harga_penawaran)?$data_db->harga_penawaran:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Harga Transaksi</label>

                    <div class="col-sm-3">
                        <input name="inp_harga_trans" type="text" class="form-control" placeholder="Harga Transaksi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->harga_transaksi)?$data_db->harga_transaksi:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tanggal</label>

                    <div class="col-sm-10">
                        <label for="" class="control-label"><?=date('Y-m-d')?></label>
                        
                        <input name="inp_tgl" type="hidden" class="form-control" placeholder="Tanggal"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tanggal)?$data_db->tanggal:date('Y-m-d')?>">
                    </div>
                </div>

                <div class="form-group label-title">
                    <label for="" class="col-sm-2 control-label">Sumber Data</label>

                    <div class="col-sm-3">
                        <input name="inp_sum_data" type="text" class="form-control" id="" placeholder="Sumber Data"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sumber_data)?$data_db->sumber_data:''?>">
                    </div>
                </div>

                <div class="form-group label-title">
                    <label for="" class="col-sm-2 control-label">No Telepon</label>

                    <div class="col-sm-3">
                        <input name="inp_no_telp" type="text" class="form-control" id="" placeholder="No Telepon"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_telepon)?$data_db->no_telepon:''?>">
                    </div>
                </div>

                <div class="box-header with-border">
                    <center>
                        <h3>
                            Lingkungan
                        </h3>
                    </center>
                </div>
                </br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Peruntukan</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_perunt" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '1')?'checked':''?> > Perumahan
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_perunt" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '2')?'checked':''?> > Komersial
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_perunt" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '3')?'checked':''?> > Industri
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_perunt" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '4')?'checked':''?> > Pertanian
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_perunt" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '5')?'checked':''?> > Campuran
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <div class="pull-left">                                    
                                    <input name="inp_perunt" value="6" type="radio" name="r3" class=" pull-right flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '6')?'checked':''?> > 
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left">
                                        &nbsp;
                                        Lainnya
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Lebar Jalan</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_lebar_jln_01" type="text" class="form-control" placeholder=" 0.0 Meter"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lebar_jalan_01)?$data_db->lebar_jalan_01:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Meter</small> | Lebar jalan di depan aset</label>                        
                        </div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Permukaan Jalan</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_perker" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perkerasan)?$data_db->perkerasan:'') == '1')?'checked':''?> > Aspal penetrasi
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perker" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perkerasan)?$data_db->perkerasan:'') == '2')?'checked':''?> > Aspal hotmix
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perker" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perkerasan)?$data_db->perkerasan:'') == '3')?'checked':''?> > Beton
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perker" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perkerasan)?$data_db->perkerasan:'') == '4')?'checked':''?> > Paving
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perker" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perkerasan)?$data_db->perkerasan:'') == '5')?'checked':''?> > Perkerasan sirtu
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perker" value="6" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->perkerasan)?$data_db->perkerasan:'') == '6')?'checked':''?> > Perkerasan tanah
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kondisi</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_kond_jal" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_jalan)?$data_db->kondisi_jalan:'') == '1')?'checked':''?> > Baik
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kond_jal" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_jalan)?$data_db->kondisi_jalan:'') == '2')?'checked':''?> > Sedang
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kond_jal" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_jalan)?$data_db->kondisi_jalan:'') == '3')?'checked':''?> > Rusak
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Angkutan Umum</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_ang_um" type="radio" name="r3" class="flat-red" <?=(!empty($data_db->banjir)?$data_db->angkutan_umum:'' == '1')?'checked':''?> > Ada 
                            &nbsp;&nbsp;&nbsp;
                            <input name="inp_ang_um" type="radio" name="r3" class="flat-red" <?=empty($data_db->angkutan_umum)?'checked':''?> > Tidak 
                        </label>
                    </div>
                </div>

                <div class="box-header with-border">
                    <center>
                        <h3>
                            Tanah
                        </h3>
                    </center>
                </div>
                </br>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Luas Tanah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_luas_tan" type="text" class="form-control" id="" placeholder="Luas (m2)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->luas_tanah)?$data_db->luas_tanah:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>M<sup>2</sup></small></label>                        
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sertifikat</label>

                    <div class="col-sm-6">
                        <input name="inp_sert" type="text" class="form-control" id="" placeholder="Sertifikat"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sertifikat)?$data_db->sertifikat:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Bentuk</label>

                    <div class="col-sm-6">
                        <input name="inp_bent" type="text" class="form-control" id="" placeholder="Bentuk"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->bentuk)?$data_db->bentuk:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Elevasi</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                        <input name="inp_elv" type="text" class="form-control" id="" placeholder="Elevasi (Meter)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->elevasi)?$data_db->elevasi:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Meter</small></label>                        
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Lebar Jalan</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_lebar_jln_02" type="text" class="form-control" id="" placeholder="Lebar Jalan 02"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lebar_jalan_02)?$data_db->lebar_jalan_02:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Meter</small></label>                        
                        </div>

                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sudut</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_sudut" type="radio" name="r3" class="flat-red" <?=(!empty($data_db->sudut)?$data_db->sudut:'' == '1')?'checked':''?> > Ya 
                            &nbsp;&nbsp;&nbsp;
                            <input name="inp_sudut" type="radio" name="r3" class="flat-red" <?=empty($data_db->sudut)?'checked':''?> > Tidak 
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tusuk Sate</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_tusuk_sate" type="radio" name="r3" class="flat-red" <?=(!empty($data_db->tusuk_sate)?$data_db->tusuk_sate:'' == '1')?'checked':''?> > Ya 
                            &nbsp;&nbsp;&nbsp;
                            <input name="inp_tusuk_sate" type="radio" name="r3" class="flat-red" <?=empty($data_db->tusuk_sate)?'checked':''?> > Tidak 
                        </label>
                    </div>
                </div>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Harga <small>M<sup>2</sup></small></label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_harg_m2" type="text" class="form-control" id="" placeholder="Harga M2"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->harga_m2)?$data_db->harga_m2:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Harga</small></label>                        
                        </div>

                    </div>
                </div>


                <div class="box-header with-border">
                    <center>
                        <h3>
                            Bangunan
                        </h3>
                    </center>
                </div>
                </br>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Luas M<sup>2</sup></label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_luas_m2" type="text" class="form-control" id="" placeholder="Luas M2"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->luas_m2)?$data_db->luas_m2:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>M<sup>2</sup></small></label>                        
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Di Bangun Tahun</label>

                    <div class="col-sm-3">
                        <input name="inp_dbangun_thn" type="text" class="form-control" id="" placeholder="Di Bangun Tahun"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->dibangun_tahun)?$data_db->dibangun_tahun:''?>">
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Penggunaan</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_penggu" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->penggunaan)?$data_db->penggunaan:'') == '1')?'checked':''?> > Kantor
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_penggu" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->penggunaan)?$data_db->penggunaan:'') == '2')?'checked':''?> > Ruko 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_penggu" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->penggunaan)?$data_db->penggunaan:'') == '3')?'checked':''?> > Rumah Tinggal
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_penggu" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->penggunaan)?$data_db->penggunaan:'') == '4')?'checked':''?> > Gudang 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Rangka</label>

                    <div class="col-sm-6">
                        <input name="inp_rangka" type="text" class="form-control" id="inputEmail3" placeholder="Rangka"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->rangka)?$data_db->rangka:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Jumlah Lantai</label>

                    <div class="col-sm-6">
                        <input name="inp_juml_lantai" type="text" class="form-control" id="inputEmail3" placeholder="Jumlah Lantai"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jumlah_lantai)?$data_db->jumlah_lantai:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Lantai</label>

                    <div class="col-sm-6">
                        <input name="inp_lantai" type="text" class="form-control" id="inputEmail3" placeholder="Lantai"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lantai)?$data_db->lantai:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Dinding</label>

                    <div class="col-sm-6">
                        <input name="inp_dind" type="text" class="form-control" id="inputEmail3" placeholder="Dinding"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->dinding)?$data_db->dinding:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Atap</label>

                    <div class="col-sm-6">
                        <input name="inp_atap" type="text" class="form-control" id="inputEmail3" placeholder="Atap"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->atap)?$data_db->atap:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Langit Langit</label>

                    <div class="col-sm-6">
                        <input name="inp_langit_langit" type="text" class="form-control" id="inputEmail3" placeholder="Langit Langit"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->langit_langit)?$data_db->langit_langit:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Kondisi</label>

                    <div class="col-sm-6">
                        <input name="inp_kondisi" type="text" class="form-control" id="inputEmail3" placeholder="Kondisi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kondisi)?$data_db->kondisi:''?>">
                    </div>
                </div>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Harga Bangunan</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_harga_m2_bang" type="text" class="form-control" id="" placeholder="Harga/M2 Bangunan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->harga_m2_bangunan)?$data_db->harga_m2_bangunan:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Harga</small>/<small>M<sup>2</sup></small></label>                        
                        </div>

                    </div>
                </div>

                <!--

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sketsa</label>

                    <div class="col-sm-10">
                        <input name="inp_sketsa" type="file" id="" placeholder="Sketsa"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa)?$data_db->sketsa:''?>">
                    </div>
                </div>

                <center>
                    <h2>
                        Sketsa Lokasi
                    </h2>
                </center>
                <br/>
                -->
            
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sketsa Lokasi</label>

                    <div class="col-sm-10">
                        <span class="pull-left ">
                            <?php
                                $file_url =base_url().'img_files/group_'.(!empty($data_db->group_id)?$data_db->group_id:'').'/'.(!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:'pbd_'.$file_id.'_sketsa.jpg');
                                $file_url = $img_file;
                                //echo "file_url:".$file_url;
                                //if($edit){
                                    $url          = $file_url;
                                    $response     = get_headers($url, 1);
                                    $file_exists  = (strpos($response[0], "404") === false);
                                    if($file_exists){
                            ?>
                                <img src="<?=$file_url?>" style="width:320px">
                            <?php 
                                    }
                            
                                //}
                            ?>
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi" name="inp_sketsa_lokasi" type="hidden" placeholder="sketsa_lokasi Utara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:'pbd_'.$file_id.'_sketsa.jpg'?>">
                            <input id="inp_sketsa_lokasi_new" name="inp_sketsa_lokasi_new" type="hidden" value="<?='pbd_'.$file_id.'_sketsa.jpg'?>">
                            <iframe id="in_f_upload" name="in_f_upload" src="<?=base_url()?>svl_lokasi/upload?file_id=<?='pbd_'.$file_id.'_sketsa.jpg'?>" frameborder="0" border="1" src="" scrolling="yes" scrollbar="yes" style="height: 320px;width: 360px" > </iframe> 
                        </span>
                    
                    </div>
                </div>

                <center>
                    <h2>
                        Foto Lokasi
                    </h2>
                </center>
                <br/>
                
            
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tampak Depan</label>

                    <div class="col-sm-10">
                        <span class="pull-left ">
                            <?php 
                                $file_url =base_url().'img_files/group_'.(!empty($data_db->group_id)?$data_db->group_id:'').'/'.(!empty($data_db->sketsa_lokasi_d)?$data_db->sketsa_lokasi_d:'pbd_'.$file_id.'_dpn.jpg');
                                $file_url = $img_file_d;
                                //echo "file_url:".$file_url;
                                //if($edit){
                                    $url          = $file_url;
                                    $response     = get_headers($url, 1);
                                    $file_exists  = (strpos($response[0], "404") === false);
                                    if($file_exists){
                            ?>
                                <img src="<?=$file_url?>" style="width:320px">
                            <?php 
                                    }
                            
                                //}
                            ?>
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi_dpn" name="inp_sketsa_lokasi_dpn" type="hidden" placeholder="sketsa_lokasi Utara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi_d)?$data_db->sketsa_lokasi_d:'pbd_'.$file_id.'_dpn.jpg'?>">
                            <input id="inp_sketsa_lokasi_dpn_new" name="inp_sketsa_lokasi_dpn_new" type="hidden" value="<?='pbd_'.$file_id.'_dpn.jpg'?>">
                            <iframe id="in_f_upload" name="in_f_upload" src="<?=base_url()?>svl_lokasi/upload?file_id=<?='pbd_'.$file_id.'_dpn.jpg'?>" frameborder="0" border="1" src="" scrolling="yes" scrollbar="yes" style="height: 320px;width: 360px" > </iframe> 
                        </span>
                    
                    </div>
                </div>


                
            
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tampak Jalan Depan</label>

                    <div class="col-sm-10">
                        <span class="pull-left ">
                            <?php 
                                $file_url =base_url().'img_files/group_'.(!empty($data_db->group_id)?$data_db->group_id:'').'/'.(!empty($data_db->sketsa_lokasi_j)?$data_db->sketsa_lokasi_j:'pbd_'.$file_id.'_jln_dpn.jpg');
                                $file_url = $img_file_j;
                                //echo "file_url:".$file_url;
                                //if($edit){
                                    $url          = $file_url;
                                    $response     = get_headers($url, 1);
                                    $file_exists  = (strpos($response[0], "404") === false);
                                    if($file_exists){
                            ?>
                                <img src="<?=$file_url?>" style="width:320px">
                            <?php 
                                    }
                            
                                //}
                            ?>
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi_jln_dpn" name="inp_sketsa_lokasi_jln_dpn" type="hidden" placeholder="sketsa_lokasi Utara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi_j)?$data_db->sketsa_lokasi_j:'pbd_'.$file_id.'_jln_dpn.jpg'?>">
                            <input id="inp_sketsa_lokasi_jln_dpn_new" name="inp_sketsa_lokasi_jln_dpn_new" type="hidden" value="<?='pbd_'.$file_id.'_jln_dpn.jpg'?>">
                            <iframe id="in_f_upload" name="in_f_upload" src="<?=base_url()?>svl_lokasi/upload?file_id=<?='pbd_'.$file_id.'_jln_dpn.jpg'?>" frameborder="0" border="1" src="" scrolling="yes" scrollbar="yes" style="height: 320px;width: 360px" > </iframe> 
                        </span>
                    
                    </div>
                </div>




                



                <hr/>
                    <center>
                        <h4 style="<?=($this->session->userdata('administrator') != '1')?'opacity:0.5"':'';?>">
                            (Administrator)
                        </h4>
                    </center>
                </br>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="1" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> 
                                > Ya &nbsp;&nbsp;&nbsp;
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="0" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> 
                                > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input 
                                type="hidden" 
                                name="inp_approval" 
                                value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>


                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>

          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    $("#group_id").change(function() {
        //alert("test");
        in_f_upload.document.getElementById("group_id").value = $("#group_id").val();
    });    
</script>

<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
