<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper_">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Pembanding</li>
      </ol>
    </section>

    <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Pembanding</h3>
              <a href="<?=base_url()?>data/pembanding/add" class="btn btn-small btn-info pull-right">
                    Input &nbsp; 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->latitude?></td>
                          <td><?=$row->longitude?></td>
                          <td><?=$row->foto_data?></td>
                          <td><?=$row->harga_penawaran?></td>
                          <td><?=$row->harga_transaksi?></td>
                          <td><?=$row->tanggal?></td>
                          <td><?=$row->sumber_data?></td>
                          <td><?=$row->no_telepon?></td>
                          <td><?=$row->peruntukan?></td>
                          <td><?=$row->lebar_jalan_01?></td>
                          <td><?=$row->perkerasan?></td>
                          <td><?=$row->kondisi_jalan?></td>
                          <td><?=$row->angkutan_umum?></td>
                          <td><?=$row->luas_tanah?></td>
                          <td><?=$row->sertifikat?></td>
                          <td><?=$row->bentuk?></td>
                          <td><?=$row->elevasi?></td>
                          <td><?=$row->lebar_jalan_02?></td>
                          <td><?=$row->sudut?></td>
                          <td><?=$row->tusuk_sate?></td>
                          <td><?=$row->harga_m2?></td>
                          <td><?=$row->luas_m2?></td>
                          <td><?=$row->dibangun_tahun?></td>
                          <td><?=$row->penggunaan?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->jumlah_lantai?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->kondisi?></td>
                          <td><?=$row->harga_m2_bangunan?></td>
                          <td><?=$row->sketsa?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Pembanding</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->latitude?></td>
                          <td><?=$row->longitude?></td>
                          <td><?=$row->foto_data?></td>
                          <td><?=$row->harga_penawaran?></td>
                          <td><?=$row->harga_transaksi?></td>
                          <td><?=$row->tanggal?></td>
                          <td><?=$row->sumber_data?></td>
                          <td><?=$row->no_telepon?></td>
                          <td><?=$row->peruntukan?></td>
                          <td><?=$row->lebar_jalan_01?></td>
                          <td><?=$row->perkerasan?></td>
                          <td><?=$row->kondisi_jalan?></td>
                          <td><?=$row->angkutan_umum?></td>
                          <td><?=$row->luas_tanah?></td>
                          <td><?=$row->sertifikat?></td>
                          <td><?=$row->bentuk?></td>
                          <td><?=$row->elevasi?></td>
                          <td><?=$row->lebar_jalan_02?></td>
                          <td><?=$row->sudut?></td>
                          <td><?=$row->tusuk_sate?></td>
                          <td><?=$row->harga_m2?></td>
                          <td><?=$row->luas_m2?></td>
                          <td><?=$row->dibangun_tahun?></td>
                          <td><?=$row->penggunaan?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->jumlah_lantai?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->kondisi?></td>
                          <td><?=$row->harga_m2_bangunan?></td>
                          <td><?=$row->sketsa?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
