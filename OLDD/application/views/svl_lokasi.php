<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$back_url = base_url()."svl_lokasi/";
$exp_url = base_url()."svl_exp_lokasi";
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


<!-- Content Wrapper. Contains page content -->
<div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?=$this->session->userdata('nama_yayasan')?>
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil-square-o"></i>Data</a></li>
        <li class="active">Pribadi</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    Data - Anak Asuh
                </h3> 
                <a href="<?=$back_url?>add" class="btn btn-small btn-info pull-right" style="margin-left: 15px">
                    Input &nbsp; 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
                <a href="<?=$exp_url?>" class="btn btn-small btn-success pull-right">
                    Export to Excel &nbsp; 
                </a>
                
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Anak</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Ayah</th>
                            <th>NIK Ayah</th>
                            <th>Alamat Ayah</th>
                            <th>No HP Ayah</th>
                            <th>Nama Ibu</th>
                            <th>NIK Ibu</th>
                            <th>Alamat Ibu</th>
                            <th>No HP Ibu</th>
                            <th>Nama Wali</th>
                            <th>NIK Wali</th>
                            <th>Alamat Wali</th>
                            <th>No HP Wali</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_data?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_data?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                            <td><?=$row->id_data?></td> 
                            <td><?=$row->nama_bumn?></td> 
                            <td><?=$row->banjir==1?'Laki-laki':'Perempuan'?></td> 
                            <td><?=$row->latitude?></td> 
                          <td><? echo substr($row->tgl_survey,8,2) . "-" . substr($row->tgl_survey,5,2) . "-" . substr($row->tgl_survey,0,4) ; ?></td>
                            <td><?=$row->alamat?></td> 
                            <td><?=$row->nama_asset?></td> 
                            <td><?=$row->longitude?></td> 
                            <td><?=$row->kode_tapak?></td> 
                            <td><?=$row->jarak_tapak_1?></td>
                            <td><?=$row->jarak_tapak_2?></td>
                            <td><?=$row->pic?></td>
                            <td><?=$row->transport_01?></td>
                            <td><?=$row->transportasi_umum?></td>
                            <td><?=$row->jarak_ke_terminal?></td>
                            <td><?=$row->no_tgl_penetapan?></td>
                            <td><?=$row->nama_terminal?></td>
                            <td><?=$row->transport_02?></td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
<!--                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Personnel</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Ayah</th>
                            <th>Nomor Asset</th>
                            <th>Alamat Ayah</th>
                            <th>No HP Ayah</th>
                            <th>Nama Ibu</th>
                            <th>Alamat Ibu</th>
                            <th>No HP Ibu</th>
                            <th>Nama Wali</th>
                            <th>Alamat Wali</th>
                            <th>No HP Wali</th>
                        </tr>
                    </tfoot> -->
                </table>
            </div>
            <!-- /.box-body -->
            <?if(!$this->session->userdata('r2d2')){?>
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
				<h2><?=$this->session->userdata('nama_yayasan')?></h2>
                </center>
                <center>
                    <h2>Data Anak Asuh</h2>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Anak</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Ayah</th>
                            <th>NIK Ayah</th>
                            <th>Alamat Ayah</th>
                            <th>No HP Ayah</th>
                            <th>Nama Ibu</th>
                            <th>NIK Ibu</th>
                            <th>Alamat Ibu</th>
                            <th>No HP Ibu</th>
                            <th>Nama Wali</th>
                            <th>NIK Wali</th>
                            <th>Alamat Wali</th>
                            <th>No HP Wali</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                           <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->banjir==1?'Laki-laki':'Perempuan'?></td>
                            <td><?=$row->latitude?></td>
                          <td><? echo substr($row->tgl_survey,8,2) . "-" . substr($row->tgl_survey,5,2) . "-" . substr($row->tgl_survey,0,4) ; ?></td>
                            <td><?=$row->alamat?></td>
                            <td><?=$row->nama_asset?></td>
							<td><?=$row->longitude?></td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->jarak_tapak_1?></td>
                            <td><?=$row->jarak_tapak_2?></td>
                            <td><?=$row->pic?></td>
                            <td><?=$row->transport_01?></td>
                            <td><?=$row->transportasi_umum?></td>
                            <td><?=$row->jarak_ke_terminal?></td>
                            <td><?=$row->no_tgl_penetapan?></td>
                            <td><?=$row->nama_terminal?></td>
                            <td><?=$row->transport_02?></td>
                            <td>&nbsp;</td>
                        </tr>
                    </tbody>
                </table>                
            </div>
            <?}?>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
