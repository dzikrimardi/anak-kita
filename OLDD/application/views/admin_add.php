<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create
        <small>Admin</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Admin</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">User Info</h3>
            </div>
            <div class="form-horizontal box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" placeholder="First Name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" placeholder="Last Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputEmail3" placeholder="Username">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">E-mail</label>

                    <div class="col-sm-10">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                        <input type="email" class="form-control" placeholder="Email">
                      </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputEmail3" placeholder="Password">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Retype&nbsp;Password</label>

                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="inputEmail3" placeholder="Retype Password">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Groups</label>
                    <div class="col-sm-10">
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="groups[]" value="1"> Surveyor Kelompok 1
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="groups[]" value="1"> Surveyor Kelompok 2
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="groups[]" value="1"> Surveyor Kelompok 3
                            </label>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-default">Cancel</button>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
