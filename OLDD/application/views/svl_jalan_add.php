<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
//$id         = $this->input->get('lokasi');

$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data    = $this->input->get('lokasi');
$back_url   = base_url()."svl_jalan/";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey Lapangan</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Jalan</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">&nbsp;</h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST">
            <div class="form-horizontal box-body">
                <!--edit-->
                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                        Jalan
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">User & Group</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->email?>" ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>           



                        <div class="col-sm-6 row">
                            <select name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$data_db->group_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->name?> ( <?=$row->description?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--//edit-->

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kode Asset Internal</label>

                    <div class="col-sm-3">
                        <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Asset Internal" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>
                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Jalan di Depan Tapak 
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Jalan di Depan Tapak </label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_ada_jalan" type="radio" value="1" name="r3" class="flat-red" <?=(!empty($data_db->ada_jalan)?$data_db->ada_jalan:'' == '1')?'checked':''?> > Ya 
                            &nbsp;&nbsp;&nbsp;
                            <input name="inp_ada_jalan" type="radio" value="2" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->ada_jalan)?$data_db->ada_jalan:'') == '2')?'checked':''?> > Tidak 
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Lebar jalan <br/>depan aset/properti</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_leb_jal" type="text" class="form-control" placeholder=" 0.0 Meter"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lebar_jalan)?$data_db->lebar_jalan:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Meter</small> | Dari Jalan Utama</label>                        
                        </div>

                    </div>
                </div>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kelas Jalan</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_kel_jal" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_jalan)?$data_db->kelas_jalan:'') == '1')?'checked':''?> > Arteri Primer
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kel_jal" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_jalan)?$data_db->kelas_jalan:'') == '2')?'checked':''?> > Arteri Sekunder 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kel_jal" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_jalan)?$data_db->kelas_jalan:'') == '3')?'checked':''?> > Kolektor
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kel_jal" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_jalan)?$data_db->kelas_jalan:'') == '4')?'checked':''?> > Lokal/Lingkungan 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Jumlah Lajur</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_jum_laj" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jumlah_lajur)?$data_db->jumlah_lajur:'') == '1')?'checked':''?> > 1 (satu lajur)
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_jum_laj" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jumlah_lajur)?$data_db->jumlah_lajur:'') == '2')?'checked':''?> > 2 (dua lajur)
                            &nbsp;&nbsp;&nbsp;

                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kondisi Lalu lintas</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_kon_lal" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_lalin)?$data_db->kondisi_lalin:'') == '1')?'checked':''?> > Tinggi
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kon_lal" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_lalin)?$data_db->kondisi_lalin:'') == '2')?'checked':''?> > Cukup Tinggi
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kon_lal" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_lalin)?$data_db->kondisi_lalin:'') == '3')?'checked':''?> > Rendah
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kon_lal" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi_lalin)?$data_db->kondisi_lalin:'') == '4')?'checked':''?> > Cukup Rendah
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Permukaan Jalan</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_perm_jal" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:'') == '1')?'checked':''?> > Aspal penetrasi
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perm_jal" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:'') == '2')?'checked':''?> > Aspal hotmix
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perm_jal" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:'') == '3')?'checked':''?> > Beton
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perm_jal" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:'') == '4')?'checked':''?> > Paving
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perm_jal" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:'') == '5')?'checked':''?> > Perkerasan sirtu
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_perm_jal" value="6" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->permukaan_jalan)?$data_db->permukaan_jalan:'') == '6')?'checked':''?> > Perkerasan tanah
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kondisi</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_kond" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi)?$data_db->kondisi:'') == '1')?'checked':''?> > Baik
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kond" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi)?$data_db->kondisi:'') == '2')?'checked':''?> > Sedang
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_kond" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kondisi)?$data_db->kondisi:'') == '3')?'checked':''?> > Rusak
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Drainase</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_drain" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->drainase)?$data_db->drainase:'') == '1')?'checked':''?> > Terbuka
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_drain" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->drainase)?$data_db->drainase:'') == '2')?'checked':''?> > Tertutup
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_drain" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->drainase)?$data_db->drainase:'') == '3')?'checked':''?> > Didalam Tanah
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_drain" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->drainase)?$data_db->drainase:'') == '4')?'checked':''?> > Didalam Tanah
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Penerangan</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_pener" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->penerangan)?$data_db->penerangan:'') == '1')?'checked':''?> > Ada
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_pener" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->penerangan)?$data_db->penerangan:'') == '2')?'checked':''?> > Tidak Ada
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Peruntukan Lokasi
                        <small>
                            <br/>
                             (opsi bisa diedit setelah cek tata kota/tidak wajib diisi di lokasi objek)
                        </small>
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Peruntukan</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_peruntukan" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '1')?'checked':''?> > Perumahan
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_peruntukan" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '2')?'checked':''?> > Komersial
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_peruntukan" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '3')?'checked':''?> > Industri
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_peruntukan" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '4')?'checked':''?> > Pertanian
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_peruntukan" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '5')?'checked':''?> > Campuran
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <div class="pull-left">                                    
                                    <input name="inp_peruntukan" value="6" type="radio" name="r3" class=" pull-right flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->peruntukan)?$data_db->peruntukan:'') == '6')?'checked':''?> > 
                                </div>
                                <div class="pull-left">
                                    <div class="pull-left">
                                        &nbsp;
                                        Lainnya
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="pull-left">
                                        <input name="inp_peruntukan_dll" type="text" class="form-control pull-left" placeholder="Peruntukan Lainnya" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->peruntukan_lainnya)?$data_db->peruntukan_lainnya:''?>">
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">KDB</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3">
                            <input name="inp_kdb" type="text" class="form-control" placeholder=".....%" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kdb)?$data_db->kdb:''?>"> 
                        </div>

                        <div class="col-sm-9">
                            <label for="" class="control-label label-title">%</label>                        
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">KLB</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3">
                            <input name="inp_klb" type="text" class="form-control" placeholder="" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->klb)?$data_db->klb:''?>">
                        </div>

                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"></label>                        
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">GSB</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3">
                            <input name="inp_gsb" type="text" class="form-control" placeholder="0.0 Meter" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->gsb)?$data_db->gsb:''?>">
                        </div>

                        <div class="col-sm-9">
                            <label for="" class="control-label label-title">Meter</label>                        
                        </div>
                    </div>
                </div>




                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tinggi maksimum </label>

                    <div class="col-sm-10">
                        <div class="col-sm-3">
                            <input name="inp_tinggi_maks" type="text" class="form-control" id="inputEmail3" placeholder="Tinggi Maks" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tinggi_maks)?$data_db->tinggi_maks:''?>">
                        </div>

                        <div class="col-sm-9">
                            <label for="" class="control-label label-title">lapis/lantai bangunan yang diijinkan</label>                        
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sesuai Peraturan</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3">
                            <input name="inp_sesuai_per" type="text" class="form-control" placeholder="" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sesuai_peraturan)?$data_db->sesuai_peraturan:''?>">
                        </div>

                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"></label>                        
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sesuai Eksisting</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="inp_sesuai_eks" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sesuai_eksisting)?$data_db->sesuai_eksisting:'') == '1')?'checked':''?> > Ada
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_sesuai_eks" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->sesuai_eksisting)?$data_db->sesuai_eksisting:'') == '2')?'checked':''?> > Tidak Ada
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>
                </div>

                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Bangunan Petunjuk di Sekitar Properti 
                    </h2>
                </center>
                <br/>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label  label-title">Petunjuk</label>

                    <div class="col-sm-10">
                        <div class="col-sm-6">
                            <input name="inp_petun_1" type="text" class="form-control" id="" placeholder="Petunjuk 1" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk1)?$data_db->petunjuk1:''?>">
                        </div>
                        <div class="col-sm-6">
                            <input name="inp_petun_2" type="text" class="form-control" id="" placeholder="Petunjuk 2" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk2)?$data_db->petunjuk2:''?>">
                        </div>

                        <div class="col-sm-6">
                            <input name="inp_petun_3" type="text" class="form-control" id="" placeholder="Petunjuk 3" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk3)?$data_db->petunjuk3:''?>">
                        </div>

                        <div class="col-sm-6">
                            <input name="inp_petun_4" type="text" class="form-control" id="" placeholder="Petunjuk 4" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk4)?$data_db->petunjuk4:''?>">
                        </div>

                        <div class="col-sm-6">
                            <input name="inp_petun_5" type="text" class="form-control" id="" placeholder="Petunjuk 5" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk5)?$data_db->petunjuk5:''?>">
                        </div>

                        <div class="col-sm-6">
                            <input name="inp_petun_6" type="text" class="form-control" id="" placeholder="Petunjuk 6" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->petunjuk6)?$data_db->petunjuk6:''?>">
                        </div>

                    </div>
                </div>
                <br/>
                <br/>
                <hr/>
                <center>
                    <h2>
                        Lain-lain
                    </h2>
                </center>
                <br/>

                <hr/>
                    <center>
                        <h4 style="<?=($this->session->userdata('administrator') != '1')?'opacity:0.5"':'';?>">
                            (Administrator)
                        </h4>
                    </center>
                </br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="1" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> 
                                > Ya &nbsp;&nbsp;&nbsp;
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="0" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> 
                                > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input 
                                type="hidden" 
                                name="inp_approval" 
                                value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>




                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
