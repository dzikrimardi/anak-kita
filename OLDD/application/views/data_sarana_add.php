<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(4);
$edit       = '';
$delete     = $this->uri->segment(3) == 'delete'?'true':'';
if($this->uri->segment(3) == 'add' || $this->uri->segment(3) == 'edit' ){
    $edit   = 'true';
}

//$back_url   = base_url()."data/jalan/";
?>


<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add Sarana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Sarana</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=base_url()?>data/sarana/add" method="POST">
            <div class="form-horizontal box-body">
                <?if($id){?>
                <?//print_r($data_db);?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data</label>

                    <div class="col-sm-3">
                        <label class="col-sm-2 control-label row"><?=$id?></label>
                        <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id?>" >
                    </div>
                </div>
                <?}?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Group Id</label>

                    <div class="col-sm-10">
                        <input name="inp_group_id" type="text" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">User Id</label>

                    <div class="col-sm-10">
                        <input name="inp_user_id" type="text" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kode Tapak</label>

                    <div class="col-sm-10">
                        <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Tapak">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nama Pasar</label>

                    <div class="col-sm-10">
                        <input name="inp_nama_pas" type="text" class="form-control" placeholder="Nama Pasar">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jarak</label>

                    <div class="col-sm-10">
                        <input name="inp_jarak" type="text" class="form-control" placeholder="Jarak">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sarana Umum</label>

                    <div class="col-sm-10">
                        <input name="inp_sarana_um" type="text" class="form-control" placeholder="Sarana Umum">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sarana Umum Desc</label>

                    <div class="col-sm-10">
                        <input name="inp_sarana_um_desc" type="text" class="form-control" id="inputEmail3" placeholder="Sarana Umum Desc">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sarana Tersambung</label>

                    <div class="col-sm-10">
                        <input name="inp_sarana_ter" type="text" class="form-control" id="inputEmail3" placeholder="Sarana Tersambung">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sarana Tersambung Desc</label>

                    <div class="col-sm-10">
                        <input name="inp_sarana_ter_desc" type="text" class="form-control" id="inputEmail3" placeholder="Sarana Tersambung Desc">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Bentuk Tapak</label>

                    <div class="col-sm-10">
                        <input name="inp_bentuk_tap" type="text" class="form-control" id="inputEmail3" placeholder="Bentuk Tapak">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Bentuk Tapak d</label>

                    <div class="col-sm-10">
                        <input name="inp_bentuk_tap_d" type="text" class="form-control" id="inputEmail3" placeholder="Bentuk Tapak d">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Topografi</label>

                    <div class="col-sm-10">
                        <input name="inp_top" type="text" class="form-control" id="inputEmail3" placeholder="Topografi">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Topografi d</label>

                    <div class="col-sm-10">
                        <input name="inp_top_d" type="text" class="form-control" id="inputEmail3" placeholder="Topografi d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Tanah</label>

                    <div class="col-sm-10">
                        <input name="inp_jenis_tan" type="text" class="form-control" id="inputEmail3" placeholder="Jenis Tanah">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Jenis Tanah d</label>

                    <div class="col-sm-10">
                        <input name="inp_jenis_tan_d" type="text" class="form-control" id="inputEmail3" placeholder="Jenis Tanah d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Elevasi</label>

                    <div class="col-sm-10">
                        <input name="inp_elevasi" type="text" class="form-control" id="inputEmail3" placeholder="Elevasi">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Elevasi Meter</label>

                    <div class="col-sm-10">
                        <input name="inp_elevasi_m" type="text" class="form-control" id="inputEmail3" placeholder="Elevasi Meter">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Batas Barat</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_bar" type="text" class="form-control" id="inputEmail3" placeholder="Batas Barat">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Batas Timur</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_tim" type="text" class="form-control" id="inputEmail3" placeholder="Batas Timur">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Batas Utara</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_ut" type="text" class="form-control" id="inputEmail3" placeholder="Batas Utara">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Batas Selatan</label>

                    <div class="col-sm-10">
                        <input name="inp_bat_sel" type="text" class="form-control" id="inputEmail3" placeholder="Batas Selatan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">View Terbaik</label>

                    <div class="col-sm-10">
                        <input name="inp_view_ter" type="text" class="form-control" id="inputEmail3" placeholder="View Terbaik">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">View Keterangan</label>

                    <div class="col-sm-10">
                        <input name="inp_view_ket" type="text" class="form-control" id="inputEmail3" placeholder="View Terbaik">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approv" type="text" class="form-control" id="inputEmail3" placeholder="Approval">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Respond</label>

                    <div class="col-sm-10">
                        <input name="inp_resp" type="text" class="form-control" id="inputEmail3" placeholder="Respond">
                    </div>
                </div>
               
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/sarana/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/sarana" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/sarana/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
