<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper_">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Bangunan</li>
      </ol>
    </section>

    <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Bangunan</h3>
              <a href="<?=base_url()?>data/bangunan/add" class="btn btn-small btn-info pull-right">
                    Input &nbsp; 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Bangunan</th>
                            <th>Id Lokasi</th>
                            <th>Jenis Bangunan</th>
                            <th>Tipe Bangunan</th>
                            <th>Nama Bangunan</th>
                            <th>Nomor Asset</th>
                            <th>Tahun Bangun</th>
                            <th>Luas Bangunan 01</th>
                            <th>Luas Bangunan 02</th>
                            <th>Jumlah Lantai</th>
                            <th>Jumlah Lantai d</th>
                            <th>Pondasi</th>
                            <th>Pondasi d</th>
                            <th>Rangka</th>
                            <th>Rangka d1</th>
                            <th>Rangka d2</th>
                            <th>Dinding</th>
                            <th>Dinding d</th>
                            <th>Atap</th>
                            <th>Atap d</th>
                            <th>Rangka Atap</th>
                            <th>Rangka Atap d</th>
                            <th>Langit Langit</th>
                            <th>Langit Langit d</th>
                            <th>Lantai</th>
                            <th>Lantai d</th>
                            <th>Partisi</th>
                            <th>Partisi d</th>
                            <th>Pintu</th>
                            <th>Pintu d</th>
                            <th>Rangka Pintu</th>
                            <th>Rangka Pintu d</th>
                            <th>Jendela</th>
                            <th>Rangka Jendela</th>
                            <th>Rangka Jendela d</th>
                            <th>Listrik</th>
                            <th>Listrik watt</th>
                            <th>Air</th>
                            <th>Air dalam</th>
                            <th>Plumbing</th>
                            <th>Telepon</th>
                            <th>Telepon Jenis</th>
                            <th>Ac Unit</th>
                            <th>Ac Jenis</th>
                            <th>Ac Jenis d</th>
                            <th>Ac Pk</th>
                            <th>Pemadam</th>
                            <th>Penangkal Petir</th>
                            <th>Penangkal Petir d</th>
                            <th>Lift Jumlah</th>
                            <th>Lift Jenis</th>
                            <th>Lift Kap org</th>
                            <th>Lift Kap brg</th>
                            <th>Lift Merk Orang</th>
                            <th>Lift Merk Barang</th>
                            <th>Elevator Unit</th>
                            <th>Elevator Lebar</th>
                            <th>Elevator Panjang</th>
                            <th>Elevator Kap Hari</th>
                            <th>Kondisi Bangunan</th>
                            <th>Status Pengelolaan</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Asset</th>
                            <th>Status d</th>
                            <th>Panjang Bangunan</th>
                            <th>Lebar Bangunan</th>
                            <th>imb no</th>
                            <th>imb tanggal</th>
                            <th>imb keluar</th>
                            <th>Status Perolehan</th>
                            <th>Status Perolehan d</th>
                            <th>Nilai Perolehan</th>
                            <th>Status Penguasaan</th>
                            <th>Catatan</th>
                            <th>Sketsa Bangunan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_bangunan?></td>
                          <td><?=$row->id_lokasi?></td>
                          <td><?=$row->jenis_bangunan?></td>
                          <td><?=$row->tipe_bangunan?></td>
                          <td><?=$row->nama_bangunan?></td>
                          <td><?=$row->nomor_asset?></td>
                          <td><?=$row->tahun_bangun?></td>
                          <td><?=$row->luas_bangunan_01?></td>
                          <td><?=$row->luas_bangunan_02?></td>
                          <td><?=$row->jml_lantai?></td>
                          <td><?=$row->jml_lantai_d?></td>
                          <td><?=$row->pondasi?></td>
                          <td><?=$row->pondasi_d?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->rangka_d1?></td>
                          <td><?=$row->rangka_d2?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->dinding_d?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->atap_d?></td>
                          <td><?=$row->rangka_atap?></td>
                          <td><?=$row->rangka_atap_d?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->langit_langit_d?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->lantai_d?></td>
                          <td><?=$row->partisi?></td>
                          <td><?=$row->partisi_d?></td>
                          <td><?=$row->pintu?></td>
                          <td><?=$row->pintu_d?></td>
                          <td><?=$row->rangka_pintu?></td>
                          <td><?=$row->rangka_pintu_d?></td>
                          <td><?=$row->jendela?></td>
                          <td><?=$row->rangka_jendela?></td>
                          <td><?=$row->rangka_jendela_d?></td>
                          <td><?=$row->listrik?></td>
                          <td><?=$row->listrik_watt?></td>
                          <td><?=$row->air?></td>
                          <td><?=$row->air_dalam?></td>
                          <td><?=$row->plumbing?></td>
                          <td><?=$row->telepon?></td>
                          <td><?=$row->telepon_jenis?></td>
                          <td><?=$row->ac_unit?></td>
                          <td><?=$row->ac_jenis?></td>
                          <td><?=$row->ac_jenis_d?></td>
                          <td><?=$row->ac_pk?></td>
                          <td><?=$row->pemadam?></td>
                          <td><?=$row->penangkal_petir?></td>
                          <td><?=$row->penangkal_petir_d?></td>
                          <td><?=$row->lift_jml?></td>
                          <td><?=$row->lift_jenis?></td>
                          <td><?=$row->lift_kap_org?></td>
                          <td><?=$row->lift_kap_brg?></td>
                          <td><?=$row->lift_merek_orang?></td>
                          <td><?=$row->lift_merek_barang?></td>
                          <td><?=$row->elevator_unit?></td>
                          <td><?=$row->elevator_lebar?></td>
                          <td><?=$row->elevator_panjang?></td>
                          <td><?=$row->elevator_kap_hari?></td>
                          <td><?=$row->kondisi_bangunan?></td>
                          <td><?=$row->status_pengelolaan?></td>
                          <td><?=$row->pengguna?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><?=$row->status_asset?></td>
                          <td><?=$row->status_d?></td>
                          <td><?=$row->panjang_bangunan?></td>
                          <td><?=$row->lebar_bangunan?></td>
                          <td><?=$row->imb_no?></td>
                          <td><?=$row->imb_tgl?></td>
                          <td><?=$row->imb_keluar?></td>
                          <td><?=$row->status_perolehan?></td>
                          <td><?=$row->status_perolehan_d?></td>
                          <td><?=$row->nilai_perolehan?></td>
                          <td><?=$row->status_penguasaan?></td>
                          <td><?=$row->catatan?></td>
                          <td><?=$row->sketsa_bangunan?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                           <th>No</th>
                            <th>Id Bangunan</th>
                            <th>Id Lokasi</th>
                            <th>Jenis Bangunan</th>
                            <th>Tipe Bangunan</th>
                            <th>Nama Bangunan</th>
                            <th>Nomor Asset</th>
                            <th>Tahun Bangun</th>
                            <th>Luas Bangunan 01</th>
                            <th>Luas Bangunan 02</th>
                            <th>Jumlah Lantai</th>
                            <th>Jumlah Lantai d</th>
                            <th>Pondasi</th>
                            <th>Pondasi d</th>
                            <th>Rangka</th>
                            <th>Rangka d1</th>
                            <th>Rangka d2</th>
                            <th>Dinding</th>
                            <th>Dinding d</th>
                            <th>Atap</th>
                            <th>Atap d</th>
                            <th>Rangka Atap</th>
                            <th>Rangka Atap d</th>
                            <th>Langit Langit</th>
                            <th>Langit Langit d</th>
                            <th>Lantai</th>
                            <th>Lantai d</th>
                            <th>Partisi</th>
                            <th>Partisi d</th>
                            <th>Pintu</th>
                            <th>Pintu d</th>
                            <th>Rangka Pintu</th>
                            <th>Rangka Pintu d</th>
                            <th>Jendela</th>
                            <th>Rangka Jendela</th>
                            <th>Rangka Jendela d</th>
                            <th>Listrik</th>
                            <th>Listrik watt</th>
                            <th>Air</th>
                            <th>Air dalam</th>
                            <th>Plumbing</th>
                            <th>Telepon</th>
                            <th>Telepon Jenis</th>
                            <th>Ac Unit</th>
                            <th>Ac Jenis</th>
                            <th>Ac Jenis d</th>
                            <th>Ac Pk</th>
                            <th>Pemadam</th>
                            <th>Penangkal Petir</th>
                            <th>Penangkal Petir d</th>
                            <th>Lift Jumlah</th>
                            <th>Lift Jenis</th>
                            <th>Lift Kap org</th>
                            <th>Lift Kap brg</th>
                            <th>Lift Merk Orang</th>
                            <th>Lift Merk Barang</th>
                            <th>Elevator Unit</th>
                            <th>Elevator Lebar</th>
                            <th>Elevator Panjang</th>
                            <th>Elevator Kap Hari</th>
                            <th>Kondisi Bangunan</th>
                            <th>Status Pengelolaan</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Asset</th>
                            <th>Status d</th>
                            <th>Panjang Bangunan</th>
                            <th>Lebar Bangunan</th>
                            <th>imb no</th>
                            <th>imb tanggal</th>
                            <th>imb keluar</th>
                            <th>Status Perolehan</th>
                            <th>Status Perolehan d</th>
                            <th>Nilai Perolehan</th>
                            <th>Status Penguasaan</th>
                            <th>Catatan</th>
                            <th>Sketsa Bangunan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Bangunan</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                      <tr>
                            <th>No</th>
                            <th>Id Bangunan</th>
                            <th>Id Lokasi</th>
                            <th>Jenis Bangunan</th>
                            <th>Tipe Bangunan</th>
                            <th>Nama Bangunan</th>
                            <th>Nomor Asset</th>
                            <th>Tahun Bangun</th>
                            <th>Luas Bangunan 01</th>
                            <th>Luas Bangunan 02</th>
                            <th>Jumlah Lantai</th>
                            <th>Jumlah Lantai d</th>
                            <th>Pondasi</th>
                            <th>Pondasi d</th>
                            <th>Rangka</th>
                            <th>Rangka d1</th>
                            <th>Rangka d2</th>
                            <th>Dinding</th>
                            <th>Dinding d</th>
                            <th>Atap</th>
                            <th>Atap d</th>
                            <th>Rangka Atap</th>
                            <th>Rangka Atap d</th>
                            <th>Langit Langit</th>
                            <th>Langit Langit d</th>
                            <th>Lantai</th>
                            <th>Lantai d</th>
                            <th>Partisi</th>
                            <th>Partisi d</th>
                            <th>Pintu</th>
                            <th>Pintu d</th>
                            <th>Rangka Pintu</th>
                            <th>Rangka Pintu d</th>
                            <th>Jendela</th>
                            <th>Rangka Jendela</th>
                            <th>Rangka Jendela d</th>
                            <th>Listrik</th>
                            <th>Listrik watt</th>
                            <th>Air</th>
                            <th>Air dalam</th>
                            <th>Plumbing</th>
                            <th>Telepon</th>
                            <th>Telepon Jenis</th>
                            <th>Ac Unit</th>
                            <th>Ac Jenis</th>
                            <th>Ac Jenis d</th>
                            <th>Ac Pk</th>
                            <th>Pemadam</th>
                            <th>Penangkal Petir</th>
                            <th>Penangkal Petir d</th>
                            <th>Lift Jumlah</th>
                            <th>Lift Jenis</th>
                            <th>Lift Kap org</th>
                            <th>Lift Kap brg</th>
                            <th>Lift Merk Orang</th>
                            <th>Lift Merk Barang</th>
                            <th>Elevator Unit</th>
                            <th>Elevator Lebar</th>
                            <th>Elevator Panjang</th>
                            <th>Elevator Kap Hari</th>
                            <th>Kondisi Bangunan</th>
                            <th>Status Pengelolaan</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Asset</th>
                            <th>Status d</th>
                            <th>Panjang Bangunan</th>
                            <th>Lebar Bangunan</th>
                            <th>imb no</th>
                            <th>imb tanggal</th>
                            <th>imb keluar</th>
                            <th>Status Perolehan</th>
                            <th>Status Perolehan d</th>
                            <th>Nilai Perolehan</th>
                            <th>Status Penguasaan</th>
                            <th>Catatan</th>
                            <th>Sketsa Bangunan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_bangunan?></td>
                          <td><?=$row->id_lokasi?></td>
                          <td><?=$row->jenis_bangunan?></td>
                          <td><?=$row->tipe_bangunan?></td>
                          <td><?=$row->nama_bangunan?></td>
                          <td><?=$row->nomor_asset?></td>
                          <td><?=$row->tahun_bangun?></td>
                          <td><?=$row->luas_bangunan_01?></td>
                          <td><?=$row->luas_bangunan_02?></td>
                          <td><?=$row->jml_lantai?></td>
                          <td><?=$row->jml_lantai_d?></td>
                          <td><?=$row->pondasi?></td>
                          <td><?=$row->pondasi_d?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->rangka_d1?></td>
                          <td><?=$row->rangka_d2?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->dinding_d?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->atap_d?></td>
                          <td><?=$row->rangka_atap?></td>
                          <td><?=$row->rangka_atap_d?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->langit_langit_d?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->lantai_d?></td>
                          <td><?=$row->partisi?></td>
                          <td><?=$row->partisi_d?></td>
                          <td><?=$row->pintu?></td>
                          <td><?=$row->pintu_d?></td>
                          <td><?=$row->rangka_pintu?></td>
                          <td><?=$row->rangka_pintu_d?></td>
                          <td><?=$row->jendela?></td>
                          <td><?=$row->rangka_jendela?></td>
                          <td><?=$row->rangka_jendela_d?></td>
                          <td><?=$row->listrik?></td>
                          <td><?=$row->listrik_watt?></td>
                          <td><?=$row->air?></td>
                          <td><?=$row->air_dalam?></td>
                          <td><?=$row->plumbing?></td>
                          <td><?=$row->telepon?></td>
                          <td><?=$row->telepon_jenis?></td>
                          <td><?=$row->ac_unit?></td>
                          <td><?=$row->ac_jenis?></td>
                          <td><?=$row->ac_jenis_d?></td>
                          <td><?=$row->ac_pk?></td>
                          <td><?=$row->pemadam?></td>
                          <td><?=$row->penangkal_petir?></td>
                          <td><?=$row->penangkal_petir_d?></td>
                          <td><?=$row->lift_jml?></td>
                          <td><?=$row->lift_jenis?></td>
                          <td><?=$row->lift_kap_org?></td>
                          <td><?=$row->lift_kap_brg?></td>
                          <td><?=$row->lift_merek_orang?></td>
                          <td><?=$row->lift_merek_barang?></td>
                          <td><?=$row->elevator_unit?></td>
                          <td><?=$row->elevator_lebar?></td>
                          <td><?=$row->elevator_panjang?></td>
                          <td><?=$row->elevator_kap_hari?></td>
                          <td><?=$row->kondisi_bangunan?></td>
                          <td><?=$row->status_pengelolaan?></td>
                          <td><?=$row->pengguna?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><?=$row->status_asset?></td>
                          <td><?=$row->status_d?></td>
                          <td><?=$row->panjang_bangunan?></td>
                          <td><?=$row->lebar_bangunan?></td>
                          <td><?=$row->imb_no?></td>
                          <td><?=$row->imb_tgl?></td>
                          <td><?=$row->imb_keluar?></td>
                          <td><?=$row->status_perolehan?></td>
                          <td><?=$row->status_perolehan_d?></td>
                          <td><?=$row->nilai_perolehan?></td>
                          <td><?=$row->status_penguasaan?></td>
                          <td><?=$row->catatan?></td>
                          <td><?=$row->sketsa_bangunan?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                           <th>No</th>
                            <th>Id Bangunan</th>
                            <th>Id Lokasi</th>
                            <th>Jenis Bangunan</th>
                            <th>Tipe Bangunan</th>
                            <th>Nama Bangunan</th>
                            <th>Nomor Asset</th>
                            <th>Tahun Bangun</th>
                            <th>Luas Bangunan 01</th>
                            <th>Luas Bangunan 02</th>
                            <th>Jumlah Lantai</th>
                            <th>Jumlah Lantai d</th>
                            <th>Pondasi</th>
                            <th>Pondasi d</th>
                            <th>Rangka</th>
                            <th>Rangka d1</th>
                            <th>Rangka d2</th>
                            <th>Dinding</th>
                            <th>Dinding d</th>
                            <th>Atap</th>
                            <th>Atap d</th>
                            <th>Rangka Atap</th>
                            <th>Rangka Atap d</th>
                            <th>Langit Langit</th>
                            <th>Langit Langit d</th>
                            <th>Lantai</th>
                            <th>Lantai d</th>
                            <th>Partisi</th>
                            <th>Partisi d</th>
                            <th>Pintu</th>
                            <th>Pintu d</th>
                            <th>Rangka Pintu</th>
                            <th>Rangka Pintu d</th>
                            <th>Jendela</th>
                            <th>Rangka Jendela</th>
                            <th>Rangka Jendela d</th>
                            <th>Listrik</th>
                            <th>Listrik watt</th>
                            <th>Air</th>
                            <th>Air dalam</th>
                            <th>Plumbing</th>
                            <th>Telepon</th>
                            <th>Telepon Jenis</th>
                            <th>Ac Unit</th>
                            <th>Ac Jenis</th>
                            <th>Ac Jenis d</th>
                            <th>Ac Pk</th>
                            <th>Pemadam</th>
                            <th>Penangkal Petir</th>
                            <th>Penangkal Petir d</th>
                            <th>Lift Jumlah</th>
                            <th>Lift Jenis</th>
                            <th>Lift Kap org</th>
                            <th>Lift Kap brg</th>
                            <th>Lift Merk Orang</th>
                            <th>Lift Merk Barang</th>
                            <th>Elevator Unit</th>
                            <th>Elevator Lebar</th>
                            <th>Elevator Panjang</th>
                            <th>Elevator Kap Hari</th>
                            <th>Kondisi Bangunan</th>
                            <th>Status Pengelolaan</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Asset</th>
                            <th>Status d</th>
                            <th>Panjang Bangunan</th>
                            <th>Lebar Bangunan</th>
                            <th>imb no</th>
                            <th>imb tanggal</th>
                            <th>imb keluar</th>
                            <th>Status Perolehan</th>
                            <th>Status Perolehan d</th>
                            <th>Nilai Perolehan</th>
                            <th>Status Penguasaan</th>
                            <th>Catatan</th>
                            <th>Sketsa Bangunan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
