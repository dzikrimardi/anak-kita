<header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>L</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Anakita System</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
                  <span class="hidden-xs">Webmaster</span>
                </a>
                <ul class="dropdown-menu">
                    <!--<li class="user-body">
                    <div class="row">
                      <div class="col-xs-4 text-center">
                        <a href="#">Followers</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Sales</a>
                      </div>
                      <div class="col-xs-4 text-center">
                        <a href="#">Friends</a>
                      </div>
                    </div>
                    </li>-->
                    <!-- Menu Footer-->
                    <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a href="<?=base_url()?>logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                    </li>
                </ul>
            </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
        </div>

    </nav>
</header>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="height: 65px">
        <div class="pull-left info" style="left: 5px">
          <p>Webmaster </p>
          <a href="<?=base_url()?>panel/account"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <!--<form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?=$this->uri->segment(1)=='home'?'active':''?>">
          <a href="<?=base_url()?>home">
            <i class="fa fa-home"></i> <span>Home</span>
            <span class="pull-right-container">
              <!--<i class="fa fa-angle-left pull-right"></i>-->
            </span>
          </a>
          <!--<ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>-->
        </li>
        <?if($this->session->userdata('administrator') == '1'){?>
         <li class="<?=$this->uri->segment(1)=='users'?'active':''?> treeview">
          <a href="#">
            <i class="fa fa-users"></i>
            <span>Users</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='users' && $this->uri->segment(2)==''?'active':''?>" >
                <a href="<?=base_url()?>users"><i class="fa fa-circle-o"></i> List</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='add'?'active':''?>" >
                <a href="<?=base_url()?>users/add"><i class="fa fa-circle-o"></i> Create</a>
            </li>
<!--            <li class="<?=$this->uri->segment(2)=='groups'?'active':''?>" >
                <a href="<?=base_url()?>users/groups"><i class="fa fa-circle-o"></i> User Groups</a>
            </li>
			-->
          </ul>
        </li>
          <?}?>
<!--        <li>
          <a href="pages/widgets.html">
            <i class="ion ion-image"></i> <span>Pemberian Penugasan</span>
            <!--<span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>-->
          </a>
        </li>
 -->        <li class="<?=$this->uri->segment(1)=='data' || substr($this->uri->segment(1),0,3)=='svl'?'active':''?> treeview">
          <a href="#">
            <i class="ion ion-edit"></i>
            <span>Data Anak Asuh</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
            <ul class="treeview-menu">
                <li>
                 </li>
                <li class="<?=$this->uri->segment(1)=='svl_lokasi'?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
		                   <a href="<?=base_url()?>svl_lokasi"><i class="fa fa-files-o"></i>&nbsp;&nbsp;&nbsp; Data Pribadi</a>
					<? } //else { ?>
                </li>
<!--                <li class="<?=$this->uri->segment(1)=='svl_jalan'?'active':''?>" >
                      <a href="<?=base_url()?>svl_jalan">
                      <i class="fa fa-file-o" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp; Jalan di Depan Tapak</a>
                </li>
                <li class="<?=$this->uri->segment(1)=='svl_sarana'?'active':''?>" >
                      <a href="<?=base_url()?>svl_sarana">
                      <i class="fa fa-file-o"></i>&nbsp;&nbsp;&nbsp; Sarana Umum</a>
                </li>
                <li>
                    <font>
                        &nbsp;&nbsp;&nbsp;-----------------------------------
                    </font>
                </li>
                <li>
                    <font color="gray">
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FORM TANAH
                      <br/>
                    </font>
                </li>
 -->
                <li class="<?=$this->uri->segment(1)=='svl_legal'?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
                      <a href="<?=base_url()?>svl_bangunan">
                      <i class="fa fa-files-o"></i>&nbsp;&nbsp;&nbsp; Riwayat Vaksinasi</a>
					<? } //else { ?>
                </li>
<!--                <li class="<?=$this->uri->segment(1)=='svl_aset'?'active':''?>" >
                      <a href="<?=base_url()?>svl_aset">
                      <i class="fa fa-files-o"></i>&nbsp;&nbsp;&nbsp; Dokumen</a>
                </li>
                <li>
                    <font>
                        &nbsp;&nbsp;&nbsp;-----------------------------------
                    </font>
                </li>

                 <li class="<?=$this->uri->segment(1)=='svl_hbu'?'active':''?>" >
                      <a href="<?=base_url()?>svl_hbu">
                      <i class="fa fa-files-o"></i>&nbsp;&nbsp;&nbsp; HBU</a>
                </li>
 -->
                <li class="<?=$this->uri->segment(1)=='svl_hbu'?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
                      <a href="<?=base_url()?>svl_hbu">
                      <i class="fa fa-files-o"></i>&nbsp;&nbsp;&nbsp; Riwayat Kesehatan</a>
					<? } //else { ?>
                </li>
<!--                <li class="<?=$this->uri->segment(1)=='svl_bangunan'?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
                      <a href="<?=base_url()?>svl_bangunan">
                      <i class="fa fa-files-o"></i>&nbsp;&nbsp;&nbsp; Dokumen Penunjang</a>
					<? } //else { ?>
                </li>
 -->
            </ul>
        </li>
        <!--
        <li class="treeview <?=substr($this->uri->segment(1),0,3)=='svs'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i>
            <span>Status Survey</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='svs_status'?'active':''?>" >
                <a href="<?=base_url()?>svs_status"><i class="fa fa-circle-o"></i> Status</a>
            </li>
            <li class="<?=$this->uri->segment(1)=='svs_rekap'?'active':''?>" >
                <a href="<?=base_url()?>svs_rekap"><i class="fa fa-circle-o"></i> Rekap</a>
            </li>
          </ul>
        </li>
        -->
<!--        <li class="treeview <?=$this->uri->segment(1)=='survey_admin'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i>
            <span>Survey-Admin</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='survey_admin' && $this->uri->segment(2)=='download'?'active':''?>" >
                <a href="<?=base_url()?>survey_admin/download"><i class="fa fa-circle-o"></i> Download Hasil Survey</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='upload'?'active':''?>" >
                <a href="<?=base_url()?>survey_admin/upload"><i class="fa fa-circle-o"></i> Upload Hasil Pengolahan</a>
            </li>
          </ul>
        </li>
 -->

        <li class="treeview <?=$this->uri->segment(1)=='data_survey'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i> <span>Data Visual</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='data_survey' && $this->uri->segment(2)=='foto'?'active':''?>" >
                <a href="<?=base_url()?>data_survey/foto"><i class="fa fa-circle-o"></i> Daftar Foto/Gambar</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='kategori'?'active':''?>" >
                <a href="<?=base_url()?>data_survey/kategori"><i class="fa fa-circle-o"></i> Kategori</a>
            </li>
          </ul>
        </li>
        <li class="treeview <?=$this->uri->segment(1)=='laporan' || substr($this->uri->segment(1),0,3)=='lpr'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(2)=='per_lokasi'?'active':''?>" >
                <a href="<?=base_url()?>lpr_vaksinasi"><i class="fa fa-circle-o text-red"></i>Daftar Riwayat Vaksinasi</a>
            </li>
            <li class="<?=$this->uri->segment(1)=='lpr_inventaris'?'active':''?>" >
                <a href="<?=base_url()?>lpr_kesehatan"><i class="fa fa-circle-o text-yellow"></i>Daftar Riwayat Kesehatan</a>
            </li>
            <li class="<?=$this->uri->segment(1)=='laporan'?'active':''?'active':''?>" >
                <a href="<?=base_url()?>lpr_lengkap"><i class="fa fa-circle-o text-aqua"></i>Daftar Riwayat Lengkap</a>
            </li>
<!--            <li class="<?=$this->uri->segment(1)=='lpr_apprasial'?'active':''?>" >
                <a href="<?=base_url()?>lpr_apprasial"><i class="fa fa-circle-o text-green"></i>Data Foto</a>
            </li>  -->
          </ul>
        </li>
<!--        <li class="treeview <?=$this->uri->segment(1)=='visual'?'active':''?>">
          <a href="#">
            <i class="ion ion-edit"></i> <span>Visualisasi Titik</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(2)=='produk'?'active':''?>" >
                <a href="<?=base_url()?>visual/produk"><i class="ion ion-edit"></i>Daftar Produk</a>
            </li>
            <li class="<?=$this->uri->segment(2)=='kategori'?'active':''?>" >
                <a href="<?=base_url()?>visual/kategori"><i class="ion ion-edit"></i>Kategori</a>
            </li>
          </ul>
        </li>
 -->        <li class="<?=$this->uri->segment(1)=='admin'?'active':''?> treeview">
          <a href="#">
            <i class="fa fa-cog"></i> <span>Admin Panel</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(1)=='admin' && $this->uri->segment(2)==''?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
		                <a href="<?=base_url()?>admin"><i class="ion ion-edit"></i>Admin Users</a>
					<? } //else { ?>
            </li>
            <li class="<?=$this->uri->segment(2)=='add'?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
        		        <a href="<?=base_url()?>admin/add"><i class="ion ion-edit"></i>Create Admin User</a>
					<? } //else { ?>
            </li>
            <li class="<?=$this->uri->segment(2)=='groups'?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
		                <a href="<?=base_url()?>admin/groups"><i class="ion ion-edit"></i>Create Admin Groups</a>
					<? } //else { ?>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Utilities</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?=$this->uri->segment(2)==''?'active':''?>" >
			 		<? if($this->session->userdata('user') != 'guest'){ ?>
		                <a href="<?=base_url() . "svl_pembanding/edit/" . $this->session->userdata('id_yayasan') . "?lokasi=" . $this->session->userdata('id_yayasan')?> "><i class="ion ion-edit"></i>Setup System</a>
					<? } //else { ?>
            </li>
            <li class="<?=$this->uri->segment(2)==''?'active':''?>" >
			 		<? if(!$this->session->userdata('user') == 'guest'){ ?>
		                <a href="<?=base_url()?>utilities/database"><i class="ion ion-edit"></i>Database Version</a>
					<? } //else { ?>
            </li>
          </ul>
        </li>
<!--        <li class="treeview">
            <a href="<?=base_url()?>logout">
                <i class="fa fa-sign-out"></i> <span>Sign Out</span>
            </a>
            <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                <li class="treeview">
                    <a href="#"><i class="fa fa-circle-o"></i> Level One
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                    <li class="treeview">
                        <a href="#"><i class="fa fa-circle-o"></i> Level Two
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
 -->        <!--<li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
<!--        <li class="header">USEFUL LINKS</li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Frontend Website</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>API Site</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-green"></i> <span>Github Repo</span></a></li>
 -->      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  
