
<!-- jQuery 3 -->
<script src="<?=base_url()?>vendors/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?=base_url()?>vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?=base_url()?>vendors/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?=base_url()?>js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="<?=base_url()?>vendors/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?=base_url()?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?=base_url()?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?=base_url()?>vendors/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<?if(false){?>
<!-- ChartJS -->
<script src="<?=base_url()?>vendors/Chart.js/Chart.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?=base_url()?>js/demo.js"></script>
<?}?>

<script type="text/javascript">
	function printArea(divName) {
	     var printContents = document.getElementById(divName).innerHTML;
	     var originalContents = document.body.innerHTML;

	     document.body.innerHTML = printContents;

	     window.print();

	     document.body.innerHTML = originalContents;
	}
</script>