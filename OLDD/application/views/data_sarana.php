<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper_">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Lapangan
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Sarana</li>
      </ol>
    </section>

    <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Sarana Umum</h3>
              <a href="<?=base_url()?>data/sarana/add" class="btn btn-small btn-info pull-right">
                    Input &nbsp; 
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->nama_pasar?></td>
                          <td><?=$row->jarak?></td>
                          <td><?=$row->sarana_umum?></td>
                          <td><?=$row->sarana_umum_desc?></td>
                          <td><?=$row->sarana_tersambung?></td>
                          <td><?=$row->sarana_tersam_desc?></td>
                          <td><?=$row->bentuk_tapak?></td>
                          <td><?=$row->bentuk_tapak_d?></td>
                          <td><?=$row->topografi?></td>
                          <td><?=$row->topografi_d?></td>
                          <td><?=$row->jenis_tanah?></td>
                          <td><?=$row->jenis_tanah_d?></td>
                          <td><?=$row->elevasi?></td>
                          <td><?=$row->elevasi_meter?></td>
                          <td><?=$row->batas_barat?></td>
                          <td><?=$row->batas_timur?></td>
                          <td><?=$row->batas_utara?></td>
                          <td><?=$row->batas_selatan?></td>
                          <td><?=$row->view_terbaik?></td>
                          <td><?=$row->view_keterangan?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                    <?
                      }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Sarana Umum</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->nama_pasar?></td>
                          <td><?=$row->jarak?></td>
                          <td><?=$row->sarana_umum?></td>
                          <td><?=$row->sarana_umum_desc?></td>
                          <td><?=$row->sarana_tersambung?></td>
                          <td><?=$row->sarana_tersam_desc?></td>
                          <td><?=$row->bentuk_tapak?></td>
                          <td><?=$row->bentuk_tapak_d?></td>
                          <td><?=$row->topografi?></td>
                          <td><?=$row->topografi_d?></td>
                          <td><?=$row->jenis_tanah?></td>
                          <td><?=$row->jenis_tanah_d?></td>
                          <td><?=$row->elevasi?></td>
                          <td><?=$row->elevasi_meter?></td>
                          <td><?=$row->batas_barat?></td>
                          <td><?=$row->batas_timur?></td>
                          <td><?=$row->batas_utara?></td>
                          <td><?=$row->batas_selatan?></td>
                          <td><?=$row->view_terbaik?></td>
                          <td><?=$row->view_keterangan?></td>
                          <td><?=$row->approval?></td>
                          <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                    <?
                      }
                    }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Nama Pasar</th>
                            <th>Jarak</th>
                            <th>Sarana Umum</th>
                            <th>Sarana Umum Desc</th>
                            <th>Sarana Tersambung</th>
                            <th>Sarana Tersambung Desc</th>
                            <th>Bentuk Tapak</th>
                            <th>Bentuk Tapak d</th>
                            <th>Topografi</th>
                            <th>Topografi d</th>
                            <th>Jenis Tanah</th>
                            <th>Jenis Tanah d</th>
                            <th>Elevasi</th>
                            <th>Elevasi Meter</th>
                            <th>Batas Barat</th>
                            <th>Batas Timur</th>
                            <th>Batas Utara</th>
                            <th>Batas Selatan</th>
                            <th>View Terbaik</th>
                            <th>View Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
