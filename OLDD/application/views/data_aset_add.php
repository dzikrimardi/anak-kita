<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add Asset</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Asset</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=base_url()?>data/aset/add" method="POST">
            <div class="form-horizontal box-body">
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data</label>

                    <div class="col-sm-10">
                        <input name="inp_id_data" type="text" class="form-control" placeholder="Id Data">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">User Id</label>

                    <div class="col-sm-10">
                        <input name="inp_user_id" type="text" class="form-control" placeholder="User Id">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Kelola</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_kel" type="text" class="form-control" placeholder="Status Kelola">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Kelola d</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_kel_d" type="text" class="form-control" placeholder="Status Kelola d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pengguna</label>

                    <div class="col-sm-10">
                        <input name="inp_peng" type="text" class="form-control" placeholder="Pengguna">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pengguna Bidang</label>

                    <div class="col-sm-10">
                        <input name="inp_peng_bid" type="text" class="form-control" placeholder="Pengguna Bidang">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pengguna Bagian</label>

                    <div class="col-sm-10">
                        <input name="inp_peng_bag" type="text" class="form-control" placeholder="Pengguna Bagian">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Manfaat</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_m" type="text" class="form-control" id="inputEmail3" placeholder="Status Manfaat">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Manfaat m</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_man_m" type="text" class="form-control" id="inputEmail3" placeholder="Status Manfaat m">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Status Legal</label>

                    <div class="col-sm-10">
                        <input name="inp_stat_leg" type="text" class="form-control" id="inputEmail3" placeholder="Status Legal">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Catatan un Clear</label>

                    <div class="col-sm-10">
                        <input name="inp_cat_un_clear" type="text" class="form-control" id="inputEmail3" placeholder="Catatan un Clear">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Catatan un Clean</label>

                    <div class="col-sm-10">
                        <input name="inp_cat_un_clean" type="text" class="form-control" id="inputEmail3" placeholder="Catatan un Clean">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sengketa</label>

                    <div class="col-sm-10">
                        <input name="inp_seng" type="text" class="form-control" id="inputEmail3" placeholder="Sengketa">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Sengketa d</label>

                    <div class="col-sm-10">
                        <input name="inp_seng_d" type="text" class="form-control" id="inputEmail3" placeholder="Sengketa d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Mediasi</label>

                    <div class="col-sm-10">
                        <input name="inp_med" type="text" class="form-control" id="inputEmail3" placeholder="Mediasi">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Mediasi d</label>

                    <div class="col-sm-10">
                        <input name="inp_med_d" type="text" class="form-control" id="inputEmail3" placeholder="Mediasi d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Gugatan</label>

                    <div class="col-sm-10">
                        <input name="inp_gug" type="text" class="form-control" id="inputEmail3" placeholder="Gugatan">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Gugatan d</label>

                    <div class="col-sm-10">
                        <input name="inp_gug_d" type="text" class="form-control" id="inputEmail3" placeholder="Gugatan d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pemblokiran</label>

                    <div class="col-sm-10">
                        <input name="inp_pem" type="text" class="form-control" id="inputEmail3" placeholder="Pemblokiran">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pemblokiran d</label>

                    <div class="col-sm-10">
                        <input name="inp_pem_d" type="text" class="form-control" id="inputEmail3" placeholder="Pemblokiran d">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approv" type="text" class="form-control" id="inputEmail3" placeholder="Approval">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Respond</label>

                    <div class="col-sm-10">
                        <input name="inp_resp" type="text" class="form-control" id="inputEmail3" placeholder="Respond">
                    </div>
                </div>
               
                <div class="box-footer">
                    <a href="<?=base_url()?>data/aset" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/aset" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/aset" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/aset/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=base_url()?>data/aset" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=base_url()?>data/aset/add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
