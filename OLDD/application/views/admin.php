<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper_">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Admin - Panel
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Admin</li>
      </ol>
    </section>

    <!-- Main content -->
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Admin User</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Address</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Salt</th>
                            <th>Email</th>
                            <th>Activation Code</th>
                            <th>Last Login</th>
                            <th>Active</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                           <th>No</th>
                            <th>Id Address</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Salt</th>
                            <th>Email</th>
                            <th>Activation Code</th>
                            <th>Last Login</th>
                            <th>Active</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Admin User</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Id Address</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Salt</th>
                            <th>Email</th>
                            <th>Activation Code</th>
                            <th>Last Login</th>
                            <th>Active</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                           <th>No</th>
                            <th>Id Address</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>Salt</th>
                            <th>Email</th>
                            <th>Activation Code</th>
                            <th>Last Login</th>
                            <th>Active</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
