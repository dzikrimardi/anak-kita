<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_hbu/";
$exp_url = base_url()."svl_exp_hbu";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        <?=$this->session->userdata('nama_yayasan')?>
           <small>&nbsp;</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Anak Asuh</a></li>

 <!--           <?if($this->input->get('lokasi')){?>

            <li>
                <a href="<?//=$back_url?>">
                    Personnel
                </a>
            </li>
            <li class="active">Riwayat Kesehatan</li>

            <?}else{?>

            <li class="active">Personnel</li>

            <?}?>
			-->
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
					<? $this->session->set_userdata('nama_anak_yatim', $data_db_r->nama_bumn); ?>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tempat/Tanggal Lahir</b> <a class="pull-right"><?=$data_db_r->latitude?>, <?=$data_db_r->tgl_survey?></a>
                 <li class="list-group-item text-muted">
                  <b>Nama Ayah Kandung</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
              </ul>
                <center>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right" style="margin-left: 15px">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?if($this->input->get('lokasi')){?>
                        Data Riwayat Kesehatan
                    <?}else{?>
                        Data Riwayat Kesehatan                
                    <?}?>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                           <th>No</th>
                            <th width="150px">
                                <center>
                                   Kode Data        
                                </center>
                            </th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Alamat</th>
                            <th>Nama Ayah</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <i class="fa fa-files-o" aria-hidden="true"></i> : 
                                    <a href="<?=$back_url?>?lokasi=<?=$row->id_data?>">
                                     <?=$row->id_data?>
                                    </a>     
                                </center>                           
                            </td>
                             <td><?=$row->nama_bumn?></td>
							<td><? if ($row->banjir == 1) {
                            			echo "Laki-laki";
			                          }else{
            			                echo "Perempuan";
                        			  }?></td>
                             <td><?=$row->latitude?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td><?=$row->alamat?></td>
                            <td><?=$row->nama_asset?></td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                </table>
                <?}else{?>

                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Gejala/Penyakit</th>
                            <th>Catatan Dokter</th>
                            <th>Obat Diresepkan</th>
                            <th>Menerima Obat (Y/T)</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_data_hbu?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data_hbu?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_data_hbu?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                          <td><?=$row->penggunaan_lahan_d?></td>
                          <td><?=$row->pemanfaatan_lahan_d?></td>
                          <td><?=$row->kesesuaian_lahan_d?></td>
                          <td><? if ($row->optimalisasi_asset == 1) { 
                              echo "Ya";
                            }else if ($row->optimalisasi_asset == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                         <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                </table>
                <?}?>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List HBU</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Optimatisasi Asset</th>
                            <th>Optimatisasi Asset d</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Rekomendasi</th>
                            <th>Nilai Investasi</th>
                            <th>IRR</th>
                            <th>NPV</th>
                            <th>Laporan Lengkap</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><? if ($row->optimalisasi_asset == 1) { 
                              echo "True";
                            }else if ($row->optimalisasi_asset == 2) { 
                              echo "False";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->optimalisasi_asset_d?></td>
                          <td><? if ($row->jenis_properti == 1) { 
                              echo "Hotel";
                            }else if ($row->jenis_properti == 2) { 
                              echo "Apartemen";
                            }else if ($row->jenis_properti == 3) { 
                              echo "Gedung Perkantoran";
                            }else if ($row->jenis_properti == 4) { 
                              echo "Ruko";
                            }else if ($row->jenis_properti == 5) { 
                              echo "Mall";
                            }else if ($row->jenis_properti == 6) { 
                              echo "Perumahan";
                            }else if ($row->jenis_properti == 7) { 
                              echo "Kawasan Wisata";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->nama?></td>
                          <td><?=$row->luas_tanah?> M</td>
                          <td><?=$row->luas_bangunan?> M</td>
                          <td><?=$row->jarak_tapak?></td>
                          <td><?=$row->jumlah_kamar?></td>
                          <td><? if ($row->kelas_hotel == 1) { 
                              echo "Bintang Melati";
                            }else if ($row->kelas_hotel == 2) { 
                              echo "Bintang 2";
                            }else if ($row->kelas_hotel == 3) { 
                              echo "Bintang 3";
                            }else if ($row->kelas_hotel == 4) { 
                              echo "Bintang 4";
                            }else if ($row->kelas_hotel == 5) { 
                              echo "Bintang 5";
                            }?></td>
                          <td><? if ($row->grade_apartemen == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_apartemen == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_perkantoran == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_perkantoran == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_ruko == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_ruko == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_mall == 1) { 
                              echo "Grade A";
                            }else if ($row->grade_mall == 2) { 
                              echo "Grade B";
                            }else{ 
                              echo "Grade C";
                            }?></td>
                          <td><? if ($row->grade_perumahan == 1) { 
                              echo "Mewah";
                            }else if ($row->grade_perumahan == 2) { 
                              echo "Menengah";
                            }else{ 
                              echo "Sederhana";
                            }?></td>
                          <td><? if ($row->kawasan_wisata == 1) { 
                              echo "Mewah";
                            }else if ($row->kawasan_wisata == 2) { 
                              echo "Menengah";
                            }else{ 
                              echo "Sederhana";
                            }?></td>
                          <td><?=$row->rekomendasi?></td>
                          <td><?=$row->nilai_investasi?></td>
                          <td><?=$row->irr?></td>
                          <td><?=$row->npv?></td>
                          <td><?=$row->laporan_lengkap?></td> 
                          <td><?=$row->keterangan?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Optimatisasi Asset</th>
                            <th>Optimatisasi Asset d</th>
                            <th>Jenis Properti</th>
                            <th>Nama</th>
                            <th>Luas Tanah</th>
                            <th>Luas Bangunan</th>
                            <th>Jarak Tapak</th>
                            <th>Jumlah Kamar</th>
                            <th>Kelas Hotel</th>
                            <th>Grade Apartemen</th>
                            <th>Grade Perkantoran</th>
                            <th>Grade Ruko</th>
                            <th>Grade Mall</th>
                            <th>Grade Perumahan</th>
                            <th>Kawasan Wisata</th>
                            <th>Rekomendasi</th>
                            <th>Nilai Investasi</th>
                            <th>IRR</th>
                            <th>NPV</th>
                            <th>Laporan Lengkap</th>
                            <th>Keterangan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>     
                <?}?>            
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
                <li class="list-group-item text-muted">
