<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
//print_r($data_db);
$groups = explode(',', $data_db->id_list_groups);

$group = array();
foreach ($groups as $data) {
    //print_r($data);
    $group[$data] = 'true';
}
//print_r($groups);
//print_r($group);
$editable = !empty($editable)?$editable:''; 
?>

<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit
        <small>User</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> User</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">User Info</h3>
            </div>
            <form action="<?=base_url()?>users/action" method="POST">
            <div class="form-horizontal box-body">

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>

                    <div class="col-sm-3">
                        <?if($editable){?>
                        <input name="inp_user" type="text" class="form-control" id="inputEmail3" placeholder="Username" value="<?=!empty($data_db->username)?$data_db->username:''?>">
                        <?}else{?>
                        <label class="control-label">
                        <?=!empty($data_db->username)?$data_db->username:''?>
                        </label>
                        <?}?>
                    </div>
                </div>
                <br/>
                <?if($editable){?>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-3">
                        <input name="inp_pass" type="password" class="form-control" id="inputEmail3" placeholder="Password" value="<?=!empty($data_db->password)?$data_db->password:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Retype&nbsp;Password</label>

                    <div class="col-sm-3">
                        <input name="inp_pass_c" type="password" class="form-control" id="inputEmail3" placeholder="Retype Password" value="<?=!empty($data_db->password)?$data_db->password:''?>">
                    </div>
                </div>
                <?}?>

                <hr/>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
                    <div class="col-sm-10">
                        <?if($editable){?>
                        <input name="inp_first" type="text" class="form-control" id="inputEmail3" placeholder="First Name" value="<?=!empty($data_db->first_name)?$data_db->first_name:''?>">
                        <?}else{?>
                        <label class="control-label">
                        <?=!empty($data_db->first_name)?$data_db->first_name:''?>
                        </label>
                        <?}?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>

                    <div class="col-sm-10">
                        <?if($editable){?>
                        <input name="inp_last" type="text" class="form-control" id="inputEmail3" placeholder="First Name" value="<?=!empty($data_db->last_name)?$data_db->last_name:''?>">
                        <?}else{?>
                        <label class="control-label">
                        <?=!empty($data_db->last_name)?$data_db->last_name:''?>
                        </label>
                        <?}?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">E-mail</label>

                    <div class="col-sm-10">
                        <?if($editable){?>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            <input name="inp_email" type="email" class="form-control" placeholder="Email" value="<?=!empty($data_db->email)?$data_db->email:''?>">
                        </div>
                        <?}else{?>
                        <label class="control-label">
                        <?=!empty($data_db->email)?$data_db->email:''?>
                        </label>
                        <?}?>
                    </div>
                </div>

                <hr/>

<!--                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Groups</label>
                    <div class="col-sm-10">
                        <?if($editable){?>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="inp_groups[]" value="1" <?=!empty($group[1])?'checked="checked"':''?> > Surveyor Kelompok 1
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="inp_groups[]" value="2" <?=!empty($group[2])?'checked="checked"':''?> > Surveyor Kelompok 2
                            </label>
                        </div>
                        <div class="checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="inp_groups[]" value="3" <?=!empty($group[3])?'checked="checked"':''?> > Surveyor Kelompok 3
                            </label>
                        </div>
                        <?}else{?>
                        <label class="control-label">
                        <?=!empty($data_db->list_groups)?$data_db->list_groups:''?>
                        </label>
                        <?}?>
                    </div>
                </div> -->
                <div class="box-footer">
                    <a href="<?=base_url()?>users" class="btn btn-default pull-right">back</a>
                    <?if($editable){?>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <input type="hidden" name="inp_id" value="<?=$this->uri->segment(3)?>">
                    <?}?>
                </div>
            </form>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
