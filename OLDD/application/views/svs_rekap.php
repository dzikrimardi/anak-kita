<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$back_url = base_url()."svs_status/";
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Survey - Rekap
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Survey</a></li>
        <li class="active">Rekap</li>
      </ol>
    </section>

    <!-- Main content -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

          <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <?if($this->input->get('group_id')){?>
                    <a href="<?=base_url()?>svs_rekap">
                        
                        <i class="fa fa-sign-out fa-rotate-180"></i>
                        Kembali 
                    </a>
                    |
                    <?}?>

                    Goal Completion
                </h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <?if(!$this->input->get('group_id')){?>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">
                <table class="table no-margin">
                  <thead>
                  <tr>
                    <th>ID</th>
                    <th>Desc</th>
                    <th>Status</th>
                    <th>&nbsp;</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?if($ds_goal->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($ds_goal->result() as $row) { 
                    $i++;
                    $status_unit    = 0;
                    if(intval($row->group_done) == 0 or intval($row->group_all) == 0){
                        $percen         = 0;
                    }else{
                        $percen         = (intval($row->group_done) *100)/intval($row->group_all);
                    }
                        //$percen         = (3 *100)/intval($row->group_all);
                    ?>

                    <tr>
                        <td>
                            <a>
                                <?=$row->name?>
                            </a>
                        </td>
                        <td>
                                <?=$row->description?>
                        </td>
                        <td>
                            <a href="<?=base_url()?>svs_rekap/?group_id=<?=$row->id?>" class="btn btn-default">
                                Detail
                            </a>
                        </td>
                        <td>
                            <div class="progress-group">
                                <span class="progress-text">Progress</span>
                                <span class="progress-number"><b><?=$row->group_done?></b>/<?=$row->group_all?></span>

                                <div class="progress sm">
                                  <div class="progress-bar progress-bar-aqua" style="width: <?=round($percen)?>%"></div>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <?}?>
                    <?}?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->

            <?}else{?>

            <!-- /.box-header -->
            <div class="box-body">
                <div class="box-body table-responsive">
                    <table id="data_table" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>&nbsp;</th>
                                <th>Id Data</th>
                                <th>Status</th>
                                <th>Kode Asset Internal</th>
                                <th>Nama Unit</th>
                                <th>Nama Asset</th>
                                <th>Tgl Survey</th>
                                <th>Approval</th>
                                <th>Respond</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        $status_unit = $row->status;
                        ?>
                            <tr>
                                <td><?=$i?></td>
                                <td>
                                    <!--
                                    <center>
                                        <a href="<?//=$back_url?>edit/<?=$row->id_data?>">
                                            <i class="fa fa-edit" alt="edit"></i>
                                        </a> 
                                    </center>                           
                                    -->
                                </td>
                                <td><?=$row->id_data?></td>
                                <td>
                                    <?if($status_unit == '1'){?>
                                        <span class="label label-danger">Belum lengkap</span>
                                    <?}else if($status_unit == '2'){?>
                                        <span class="label label-warning">Lengkap, Belum diverifikasi</span>
                                    <?}else if($status_unit == '3'){?>
                                        <span class="label label-success">Lengkap, Sudah diverifikasi</span>
                                    <?}else{?>
                                        <span class="label label-info">ditugaskan ke surveyor</span>
                                    <?}?>
                                </td>
                                <td><?=$row->kode_tapak?></td>
                                <td><?=$row->nama_bumn?></td>
                                <td><?=$row->nama_asset?></td>
                                <td><?=$row->tgl_survey?></td>
                                <td><?=$row->approval==1?'yes':'-'?></td>
                                <td><?=$row->respond?></td>
                            </tr>
                        <?}?>
                        <?}?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>&nbsp;</th>
                                <th>Id Data</th>
                                <th>Status</th>
                                <th>Kode Asset Internal</th>
                                <th>Nama Unit</th>
                                <th>Nama Asset</th>
                                <th>Tgl Survey</th>
                                <th>Approval</th>
                                <th>Respond</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                  <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <?}?>

            <!--
            <div class="box-footer clearfix">
              <a href="javascript:void(0)" class="btn btn-sm btn-info btn-flat pull-left">Place New Order</a>
              <a href="javascript:void(0)" class="btn btn-sm btn-default btn-flat pull-right">View All Orders</a>
            </div>
        -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
