<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_bangunan.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Bangunan</th>
                            <th>Sketsa</th>
                            <th>Id Lokasi</th>
                            <th>Jenis Bangunan</th>
                            <!--<th>Jenis Bangunan Lainnya</th>-->
                            <th>Tipe Bangunan</th>
                            <th>Nama Bangunan</th>
                            <th>Nomor Asset</th>
                            <th>Tahun Bangun</th>
                            <th>Luas Bangunan 01</th>
                            <th>Luas Bangunan 02</th>
                            <th>Jumlah Lantai</th>
                            <th>Jumlah Lantai d</th>
                            <th>Pondasi</th>
                            <th>Pondasi d</th>
                            <th>Rangka</th>
                            <th>Rangka d1</th>
                            <th>Rangka d2</th>
                            <th>Dinding</th>
                            <th>Dinding d</th>
                            <th>Atap</th>
                            <th>Atap d</th>
                            <th>Rangka Atap</th>
                            <th>Rangka Atap d</th>
                            <th>Langit Langit</th>
                            <th>Langit Langit d</th>
                            <th>Lantai</th>
                            <th>Lantai d</th>
                            <th>Partisi</th>
                            <th>Partisi d</th>
                            <th>Pintu</th>
                            <th>Pintu d</th>
                            <th>Rangka Pintu</th>
                            <th>Rangka Pintu d</th>
                            <th>Jendela</th>
                            <th>Rangka Jendela</th>
                            <th>Rangka Jendela d</th>
                            <th>Listrik</th>
                            <th>Listrik watt</th>
                            <th>Air</th>
                            <th>Air dalam</th>
                            <th>Plumbing</th>
                            <th>Telepon</th>
                            <th>Telepon Jenis</th>
                            <th>Ac Unit</th>
                            <th>Ac Jenis</th>
                            <th>Ac Jenis d</th>
                            <th>Ac Pk</th>
                            <th>Pemadam</th>
                            <th>Penangkal Petir</th>
                            <th>Penangkal Petir d</th>
                            <th>Lift Jumlah</th>
                            <th>Lift Jenis</th>
                            <th>Lift Kap org</th>
                            <th>Lift Kap brg</th>
                            <th>Lift Merk Orang</th>
                            <th>Lift Merk Barang</th>
                            <th>Elevator Unit</th>
                            <th>Elevator Lebar</th>
                            <th>Elevator Panjang</th>
                            <th>Elevator Kap Hari</th>
                            <th>Kondisi Bangunan</th>
                            <th>Status Pengelolaan</th>
                            <th>Pengguna</th>
                            <th>Pengguna Bidang</th>
                            <th>Pengguna Bagian</th>
                            <th>Status Asset</th>
                            <th>Status d</th>
                            <th>Luas Bangunan</th>
                            <th>Panjang Bangunan</th>
                            <th>Lebar Bangunan</th>
                            <th>imb no</th>
                            <th>imb tanggal</th>
                            <th>imb keluar</th>
                            <th>Status Perolehan</th>
                            <th>Status Perolehan d</th>
                            <th>Nilai Perolehan</th>
                            <th>Status Penguasaan</th>
                            <th>Catatan</th>
                            <th>Sketsa Bangunan</th>
                            <th>Nilai Wajar</th>
                            <th>Bukti Perhitungan</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_bangunan?></td>
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi){?>          
                                    <a data-toggle="modal" data-target="#img_u_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_u_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>


                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>

                            </td>

                          </td>
                          <td><?=$row->id_lokasi?></td>
                          <td><? if ($row->jenis_bangunan == 1) { 
                              echo "Rumah Tinggal";
                            }else if ($row->jenis_bangunan == 2) { 
                              echo "Gedung";
                            }else if ($row->jenis_bangunan == 3) { 
                              echo "Gudang";
                            }else if ($row->jenis_bangunan == 4) { 
                              echo "Lainnya";
                            }else{ 
                              echo "-";
                            }?></td>
                          <!--<td><?//=$row->jenis_bangunan_d?></td>-->
                          <td><? if ($row->tipe_bangunan == 1) { 
                              echo "Sederhana";
                            }else if ($row->tipe_bangunan == 2) { 
                              echo "Menengah";
                            }else if ($row->tipe_bangunan == 3) { 
                              echo "Menengah - Mewah";
                            }else if ($row->tipe_bangunan == 4) { 
                              echo "Mewah";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->nama_bangunan?></td>
                          <td><?=$row->nomor_asset?></td>
                          <td><?=$row->tahun_bangun?></td>
                          <td><?=$row->luas_bangunan_01?></td>
                          <td><?=$row->luas_bangunan_02?></td>
                          <td><? if ($row->jml_lantai == 1) { 
                              echo "1 (Satu)";
                            }else if ($row->jml_lantai == 2) { 
                              echo "2 (dua)";
                            }else if ($row->jml_lantai == 3) { 
                              echo "Lainnya";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->jml_lantai_d?></td>
                          <td><? if ($row->pondasi == 1) { 
                              echo "Batu Kali";
                            }else if ($row->pondasi == 2) { 
                              echo "Beton";
                            }else if ($row->pondasi == 3) { 
                              echo "Tiang Pancang";
                            }else if ($row->pondasi == 4) { 
                              echo "Lainnya";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pondasi_d?></td>
                          <td><? if ($row->rangka == 1) { 
                              echo "Beton";
                            }else if ($row->rangka == 2) { 
                              echo "Baja";
                            }else if ($row->rangka == 3) { 
                              echo "Kayu";
                            }else if ($row->rangka == 4) { 
                              echo "Lainnya";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka_d1?></td>
                          <td><?=$row->rangka_d2?></td>
                          <td><? if ($row->dinding == 1) { 
                              echo "Bata Pls / No Pls";
                            }else if ($row->dinding == 2) { 
                              echo "Plywood";
                            }else if ($row->dinding == 3) { 
                              echo "Papan";
                            }else if ($row->dinding == 4) { 
                              echo "Seng Gel.";
                            }else if ($row->dinding == 5) { 
                              echo "Bilik";
                            }else if ($row->dinding == 6) { 
                              echo "Alumunium Gel.";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->dinding_d?></td>
                          <td><? if ($row->atap == 1) { 
                              echo "Genteng Beton";
                            }else if ($row->atap == 2) { 
                              echo "Genteng Keramik";
                            }else if ($row->atap == 3) { 
                              echo "Spandek";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->atap_d?></td>
                          <td><? if ($row->rangka_atap == 1) { 
                              echo "Kayu";
                            }else if ($row->rangka_atap == 2) { 
                              echo "Baju";
                            }else if ($row->rangka_atap == 3) { 
                              echo "Beton";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka_atap_d?></td>
                          <td><? if ($row->langit_langit == 1) { 
                              echo "Akustik";
                            }else if ($row->langit_langit == 2) { 
                              echo "Plywood";
                            }else if ($row->langit_langit == 3) { 
                              echo "Gypsum / Asbes";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->langit_langit_d?></td>
                          <td><? if ($row->lantai == 1) { 
                              echo "Granit";
                            }else if ($row->lantai == 2) { 
                              echo "Keramik";
                            }else if ($row->lantai == 3) { 
                              echo "Marmer / Ubin PC";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->lantai_d?></td>
                          <td><? if ($row->partisi == 1) { 
                              echo "Bata Pls / No Pls";
                            }else if ($row->partisi == 2) { 
                              echo "Plywood";
                            }else if ($row->partisi == 3) { 
                              echo "Seng Gel.";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->partisi_d?></td>
                          <td><? if ($row->pintu == 1) { 
                              echo "Kaca";
                            }else if ($row->pintu == 2) { 
                              echo "Panel Kayu";
                            }else if ($row->pintu == 3) { 
                              echo "Plywood";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pintu_d?></td>
                          <td><? if ($row->rangka_pintu == 1) { 
                              echo "Alumunium";
                            }else if ($row->rangka_pintu == 2) { 
                              echo "Kayu";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka_pintu_d?></td>
                          <td><? if ($row->jendela == 1) { 
                              echo "Kaca";
                            }else if ($row->jendela == 2) { 
                              echo "Kaca Nako";
                            }else if ($row->jendela == 3) { 
                              echo "Papan";
                            }else if ($row->jendela == 4) { 
                              echo "Teralis Besi";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka_jendela?></td>
                          <td><?=$row->rangka_jendela_d?></td>
                          <td><? if ($row->listrik == 1) { 
                              echo "PLN";
                            }else if ($row->listrik == 2) { 
                              echo "Genset";
                            }else{ 
                              echo "Tidak Ada";
                            }?></td>
                          <td><?=$row->listrik_watt?></td>
                          <td><? if ($row->air == 1) { 
                              echo "PAM";
                            }else if ($row->air == 2) { 
                              echo "sumur / Sumur Bor";
                            }else{ 
                              echo "Tidak Ada";
                            }?></td>
                          <td><?=$row->air_dalam?></td>
                          <td><? if ($row->plumbing == 1) { 
                              echo "Ya";
                            }else if ($row->plumbing == 2) { 
                              echo "Tidak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->telepon == 1) { 
                              echo "PABX";
                            }else if ($row->telepon == 2) { 
                              echo "Key Telephone";
                            }else{ 
                              echo "Tidak Ada";
                            }?></td>
                          <td><?=$row->telepon_jenis?></td>
                          <td><? if ($row->ac_unit == 1) { 
                              echo "Split";
                            }else if ($row->ac_unit == 2) { 
                              echo "Window";
                            }else{ 
                              echo "Tidak Ada";
                            }?></td>
                          <td><? if ($row->ac_jenis == 1) { 
                              echo "Central";
                            }else if ($row->ac_jenis == 2) { 
                              echo "Dust AC";
                            }else if ($row->ac_jenis == 3) { 
                              echo "Cassete";
                            }else if ($row->ac_jenis == 4) { 
                              echo "Ceilling";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->ac_jenis_d?></td>
                          <td><?=$row->ac_pk?></td>
                          <td><? if ($row->pemadam == 1) { 
                              echo "Hydrant";
                            }else if ($row->pemadam == 2) { 
                              echo "Sprinkler";
                            }else if ($row->pemadam == 3) { 
                              echo "Alarm & Smoke Detector";
                            }else if ($row->pemadam == 4) { 
                              echo "Ceilling";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->penangkal_petir == 1) { 
                              echo "Konvensional";
                            }else if ($row->penangkal_petir == 2) { 
                              echo "Radion Active";
                            }else{ 
                              echo "Tidak Ada";
                            }?></td>
                          <td><?=$row->penangkal_petir_d?></td>
                          <td><?=$row->lift_jml?></td>
                          <td><? if ($row->lift_jenis == 1) { 
                              echo "Penumpang";
                            }else if ($row->lift_jenis == 2) { 
                              echo "Barang";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->lift_kap_org?></td>
                          <td><?=$row->lift_kap_brg?></td>
                          <td><?=$row->lift_merek_orang?></td>
                          <td><?=$row->lift_merek_barang?></td>
                          <td><?=$row->elevator_unit?></td>
                          <td><?=$row->elevator_lebar?></td>
                          <td><?=$row->elevator_panjang?></td>
                          <td><?=$row->elevator_kap_hari?></td>
                          <td><? if ($row->kondisi_bangunan == 1) { 
                              echo "Sangat Baik";
                            }else if ($row->kondisi_bangunan == 2) { 
                              echo "Baik";
                            }else if ($row->kondisi_bangunan == 3) { 
                              echo "Cukup Baik";
                            }else if ($row->kondisi_bangunan == 4) { 
                              echo "Kurang Baik";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->status_pengelolaan == 1) { 
                              echo "Dipakai Sendiri";
                            }else if ($row->status_pengelolaan == 2) { 
                              echo "Disewakan";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->pengguna?></td>
                          <td><?=$row->pengguna_bidang?></td>
                          <td><?=$row->pengguna_bagian?></td>
                          <td><? if ($row->status_asset == 1) { 
                              echo "Digunakan";
                            }else if ($row->status_asset == 2) { 
                              echo "Kosong";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_d?></td>
                          <td><?=$row->luas_bangunan?></td>
                          <td><?=$row->panjang_bangunan?></td>
                          <td><?=$row->lebar_bangunan?></td>
                          <td><?=$row->imb_no?></td>
                          <td><?=$row->imb_tgl?></td>
                          <td><?=$row->imb_keluar?></td>
                          <td><? if ($row->status_perolehan == 1) { 
                              echo "Pembelian";
                            }else if ($row->status_perolehan == 2) { 
                              echo "Hibah";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->status_perolehan_d?></td>
                          <td><?=$row->nilai_perolehan?></td>
                          <td><? if ($row->status_penguasaan == 1) { 
                              echo "Clean & Clear";
                            }else if ($row->status_penguasaan == 2) { 
                              echo "Clear & UnClean";
                            }else if ($row->status_penguasaan == 3) { 
                              echo "UnClean & Clear";
                            }else if ($row->status_penguasaan == 4) { 
                              echo "UnClean & UnClear";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->catatan?></td>
                          <td><?=$row->sketsa_bangunan?></td>
                          <td><?=$row->nilai_wajar?></td>
                          <td><?=$row->bukti_perhitungan?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
