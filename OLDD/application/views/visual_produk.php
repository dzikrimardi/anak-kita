<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Visualisasi - Produk
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Visualisasi</a></li>
        <li class="active">Laporan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Produk</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Tapak</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama BUMN</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport&nbsp;1</th>
                            <th>Transport&nbsp;2</th>
                            <th>Alternatif&nbsp;1</th>
                            <th>Alternatif&nbsp;2</th>
                            <th>Alternatif&nbsp;3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->longitude?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->nomor_asset?></td>
                            <td><?=$row->banjir==1?'Banjir':'Tidak Banjir'?></td>
                            <td><?=$row->jarak_tapak_1?></td>
                            <td><?=$row->jarak_tapak_2?></td>
                            <td><?=$row->kemudahan?></td>
                            <td><?=$row->transportasi_umum?></td>
                            <td><?=$row->jarak_ke_terminal?></td>
                            <td><?=$row->nama_terminal?></td>
                            <td><?=$row->transport_01?></td>
                            <td><?=$row->transport_02?></td>
                            <td><?=$row->alternatif1?></td>
                            <td><?=$row->alternatif2?></td>
                            <td><?=$row->alternatif3?></td>
                            <td><?=$row->approval==1?'yes':'-'?></td>
                            <td><?=$row->respond?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Tapak</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama BUMN</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport&nbsp;1</th>
                            <th>Transport&nbsp;2</th>
                            <th>Alternatif&nbsp;1</th>
                            <th>Alternatif&nbsp;2</th>
                            <th>Alternatif&nbsp;3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;sadas</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>Data Lokasi</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Tapak</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama BUMN</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport 01</th>
                            <th>Transport 02</th>
                            <th>Alternatif 2</th>
                            <th>Alternatif 3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Actions</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                        <tr>
                          <td>1</td>
                          <td>Surveyor&nbsp;Kelompok&nbsp;1</td>
                          <td>tsbudiyanto</td>
                          <td>tsbudijanto@gmail.com</td>
                          <td>Trisetyo</td>
                          <td>Budiyanto</td>
                          <td>Actiive</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                          <td>Edit</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Tapak</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama BUMN</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport 01</th>
                            <th>Transport 02</th>
                            <th>Alternatif 2</th>
                            <th>Alternatif 3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Actions</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
