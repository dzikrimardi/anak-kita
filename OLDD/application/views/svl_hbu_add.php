<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
//$id         = $this->input->get('lokasi');
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data     = $this->input->get('lokasi');
$back_url   = base_url()."svl_hbu/";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Riwayat Kesehatan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i>Data Riwayat</a></li>
        <li>></i> Kesehatan</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST" enctype="multipart/form-data">
            <div class="form-horizontal box-body">

                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                            Riwayat Kesehatan
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Yayasan</label>

                    <div class="col-sm-10">
 

                        <div class="col-sm-6 row">
                            <select name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
 <!--                               <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?> -->
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$this->session->userdata('group_id'):'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->description?></option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--//edit-->

                <!--edit-->
                <?
                //$id = $this->input->get('lokasi');
                ?>

<!--
                <?if(!$id){?>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Id Data</label>

                    <div class="col-sm-10">
                        <label for="" class="control-label">
                        <font style="opacity: 0.25">
                            (auto)
                        </font>
                        </label>
                    </div>
                </div>
                <?}?>
-->

				<div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Anak Asuh</label>
					 <div class="col-sm-5">
                            <input name="inp_01" type="text" class="form-control" placeholder="Lainnya"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=$this->session->userdata('nama_anak_yatim');?>" >
                     </div>
                </div>



                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Gejala/Penyakit</label>

					
					<div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_penggu_lah_d"':'view_kode_tapak';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_penggu_lah_d"':'view_kode_tapak';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->penggunaan_lahan_d)?$data_db->penggunaan_lahan_d:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_penggu_lah_d" 
                            name="inp_penggu_lah_d" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->penggunaan_lahan_d)?$data_db->penggunaan_lahan_d:''?></textarea>
                        <?}?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Catatan Dokter</label>


					<div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_peman_lah_d"':'view_kode_tapak';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_peman_lah_d"':'view_kode_tapak';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->pemanfaatan_lahan_d)?$data_db->pemanfaatan_lahan_d:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_peman_lah_d" 
                            name="inp_peman_lah_d" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->pemanfaatan_lahan_d)?$data_db->pemanfaatan_lahan_d:''?></textarea>
                        <?}?>

                    </div>


                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Obat Diresepkan</label>

					<div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_keses_lah_d"':'view_kode_tapak';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_keses_lah_d"':'view_kode_tapak';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->kesesuaian_lahan_d)?$data_db->kesesuaian_lahan_d:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_keses_lah_d" 
                            name="inp_keses_lah_d" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->kesesuaian_lahan_d)?$data_db->kesesuaian_lahan_d:''?></textarea>
                        <?}?>

                    </div>


                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Menerima Obat (Y/T)</label>

                    <div class="col-sm-10">
                            <div class="pull-left">
                                <input name="inp_optimalisasi" value="1" type="radio" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->optimalisasi_asset)?$data_db->optimalisasi_asset:'') == '1')?'checked':''?> >  Ada
                                &nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="pull-left">
                                <input name="inp_optimalisasi" value="2" type="radio" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->optimalisasi_asset)?$data_db->optimalisasi_asset:'') == '2')?'checked':''?> >  Tidak Ada
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="hidden">
                                <div class="pull-left">
                                    <div class="pull-left">
                                        <input name="inp_optimalisasi_d" type="text" class="form-control pull-left" placeholder="Detail Optimalisasi Asset" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->optimalisasi_asset_d)?$data_db->optimalisasi_asset_d:''?>">
                                    </div>
                                </div>
                            </div>
                            &nbsp;&nbsp;&nbsp;
                        <!--<label for="" class="control-label label-title">
                        <small>
                            Ketersediaan Fasilitas/ Sarana umum dilingkungan aset
                        </small>
                        </label>-->
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Jenis Properti</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_jen_pro" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '1')?'checked':''?> > Hotel
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jen_pro" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '2')?'checked':''?> > Apartemen
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jen_pro" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '3')?'checked':''?> > Gedung Perkantoran
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jen_pro" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '4')?'checked':''?> > Ruko
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jen_pro" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '5')?'checked':''?> > Mall
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jen_pro" value="6" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '6')?'checked':''?> > Perumahan
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_jen_pro" value="7" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->jenis_properti)?$data_db->jenis_properti:'') == '7')?'checked':''?> > Kawasan Wisata
                                &nbsp;&nbsp;&nbsp;
                            </div>
                        </label>
                    </div>
                </div>







                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Nama</label>

                    <div class="col-sm-6">
                        <input name="inp_nama" type="text" class="form-control" placeholder="Nama" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama)?$data_db->nama:''?>">
                    </div>
                </div>


                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Luas Tanah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_luas_tan" type="text" class="form-control" id="" placeholder="Luas (m2)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->luas_tanah)?$data_db->luas_tanah:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>M<sup>2</sup></small></label>                        
                        </div>

                    </div>
                </div>


                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Luas Bangunan</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_luas_bang" type="text" class="form-control" placeholder="Luas Bangunan" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->luas_bangunan)?$data_db->luas_bangunan:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>M<sup>2</sup></small></label>                        
                        </div>

                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Jarak Tapak</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_jarak_tap" type="text" class="form-control" id="" placeholder="Jarak Tapak" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak)?$data_db->jarak_tapak:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>Dari Lokasi</small></label>                        
                        </div>

                    </div>
                </div>


                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Jumlah Kamar</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_jum_kam" type="text" class="form-control" id="inputEmail3" placeholder="Jumlah Kamar" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jumlah_kamar)?$data_db->jumlah_kamar:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small>(untuk Hotel/ Apartemen) </small></label>                        
                        </div>

                    </div>
                </div>




                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Kelas Hotel</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_kel_hot" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_hotel)?$data_db->kelas_hotel:'') == '1')?'checked':''?> > Bintang&nbsp;Melati &star; &star; &star; &star; &star;
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kel_hot" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_hotel)?$data_db->kelas_hotel:'') == '1')?'checked':''?> > Bintang&nbsp;(1) &starf; &star; &star; &star; &star;
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kel_hot" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_hotel)?$data_db->kelas_hotel:'') == '2')?'checked':''?> > Bintang&nbsp;(2) &starf; &starf; &star; &star; &star;
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kel_hot" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_hotel)?$data_db->kelas_hotel:'') == '3')?'checked':''?> > Bintang&nbsp;(3) &starf; &starf; &starf; &star; &star;
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kel_hot" value="4" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_hotel)?$data_db->kelas_hotel:'') == '4')?'checked':''?> > Bintang&nbsp;(4) &starf; &starf; &starf; &starf; &star;
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kel_hot" value="5" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kelas_hotel)?$data_db->kelas_hotel:'') == '5')?'checked':''?> > Bintang&nbsp;(5) &starf; &starf; &starf; &starf; &starf;
                                &nbsp;&nbsp;&nbsp;
                            </div>
                        </label>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Grade Apartemen</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_grad_aprt" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_apartemen)?$data_db->grade_apartemen:'') == '1')?'checked':''?> > Grade A
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_aprt" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_apartemen)?$data_db->grade_apartemen:'') == '2')?'checked':''?> > Grade B
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_aprt" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_apartemen)?$data_db->grade_apartemen:'') == '3')?'checked':''?> > Grade C
                                &nbsp;&nbsp;&nbsp;
                            </div>

                        </label>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Grade Perkantoran</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_grad_perk" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_perkantoran)?$data_db->grade_perkantoran:'') == '1')?'checked':''?> > Grade A
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_perk" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_perkantoran)?$data_db->grade_perkantoran:'') == '2')?'checked':''?> > Grade B
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_perk" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_perkantoran)?$data_db->grade_perkantoran:'') == '3')?'checked':''?> > Grade C
                                &nbsp;&nbsp;&nbsp;
                            </div>

                        </label>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Grade Ruko</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_grad_ruk" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_ruko)?$data_db->grade_ruko:'') == '1')?'checked':''?> > Grade A
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_ruk" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_ruko)?$data_db->grade_ruko:'') == '2')?'checked':''?> > Grade B
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_ruk" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_ruko)?$data_db->grade_ruko:'') == '3')?'checked':''?> > Grade C
                                &nbsp;&nbsp;&nbsp;
                            </div>

                        </label>
                    </div>
                </div>


                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Grade Mall</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_grad_mall" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_mall)?$data_db->grade_mall:'') == '1')?'checked':''?> > Grade A
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_mall" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_mall)?$data_db->grade_mall:'') == '2')?'checked':''?> > Grade B
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_mall" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_mall)?$data_db->grade_mall:'') == '3')?'checked':''?> > Grade C
                                &nbsp;&nbsp;&nbsp;
                            </div>

                        </label>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Grade Perumahan</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_grad_per" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_perumahan)?$data_db->grade_perumahan:'') == '1')?'checked':''?> > Mewah
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_per" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_perumahan)?$data_db->grade_perumahan:'') == '2')?'checked':''?> > Menengah
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_grad_per" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->grade_perumahan)?$data_db->grade_perumahan:'') == '3')?'checked':''?> > Sederhana
                                &nbsp;&nbsp;&nbsp;
                            </div>

                        </label>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Kawasan Wisata</label>

                    <div class="col-sm-10">
                        <label>
                            <div class="pull-left">
                                <input name="inp_kaw_wis" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kawasan_wisata)?$data_db->kawasan_wisata:'') == '1')?'checked':''?> > Mewah
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kaw_wis" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kawasan_wisata)?$data_db->grade_perumahan:'') == '2')?'checked':''?> > Menengah
                                &nbsp;&nbsp;&nbsp;
                            </div>

                            <div class="pull-left">
                                <input name="inp_kaw_wis" value="3" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->kawasan_wisata)?$data_db->kawasan_wisata:'') == '3')?'checked':''?> > Sederhana
                                &nbsp;&nbsp;&nbsp;
                            </div>

                        </label>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Rekomendasi</label>

                    <div class="col-sm-10">
                        <textarea name="inp_rekomendasi" class="form-control" placeholder="rekomendasi apa untuk tapak tersebut: apakah hotel, restoran, kantor, dsb" <?=empty($edit)?'disabled="disabled"':'';?>><?=!empty($data_db->rekomendasi)?$data_db->rekomendasi:''?></textarea>
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Nilai Investasi</label>

                    <div class="col-sm-3">
                        <input name="inp_nilai_investasi" type="text" class="form-control" id="" placeholder="Nilai Investasi dari rekomendasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nilai_investasi)?$data_db->nilai_investasi:''?>">
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">IRR</label>

                    <div class="col-sm-3">
                        <input name="inp_irr" type="text" id="" class="form-control" placeholder="Aspek IRR dalam penilaian asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->irr)?$data_db->irr:''?>">
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">NPV</label>

                    <div class="col-sm-3">
                        <input name="inp_npv" type="text" class="form-control" id="" placeholder="Aspek NPV dalam penilaian asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->npv)?$data_db->npv:''?>">
                    </div>
                </div>

                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Laporan Lengkap</label>

                    <div class="col-sm-10">
                        <input name="inp_laporan_lengkap" type="file" id="" placeholder="Laporan Lengkap perhitungan investasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->laporan_lengkap)?$data_db->laporan_lengkap:''?>">
                        <?=!empty($data_db->laporan_lengkap)?'<br/>Download : <a href="'.base_url().'img_files/file/hbu/'.$data_db->laporan_lengkap.'">'.$data_db->laporan_lengkap.'</a>':''?>
                    </div>
                </div>

                <div class="hidden">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Keterangan</label>

                    <div class="col-sm-10">
                        <textarea name="inp_ket" class="form-control" <?=empty($edit)?'disabled="disabled"':'';?>><?=!empty($data_db->keterangan)?$data_db->keterangan:''?></textarea>
                    </div>
                </div>


                <div class="hidden">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="1" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> 
                                > Ya &nbsp;&nbsp;&nbsp;
                            <input 
                                name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" 
                                type="radio" 
                                name="r3" 
                                class="flat-red" 
                                value="0" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> 
                                > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input 
                                type="hidden" 
                                name="inp_approval" 
                                value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="hidden">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>

                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
