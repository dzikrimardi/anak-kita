<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_lokasi.xls");
 
// Tambahkan table
?>
<!-- Content Wrapper. Contains page content -->

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Asset Internal</th>
                            <th>Lokasi (Latitude)</th>
                            <th>Alamat</th>
                            <th>Tgl Survey</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport&nbsp;1</th>
                            <th>Transport&nbsp;2</th>
                            <th>Alternatif&nbsp;1</th>
                            <th>Alternatif&nbsp;2</th>
                            <th>Alternatif&nbsp;3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->latitude?>, <?=$row->longitude?></td>
                            <td><?=$row->alamat?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->nomor_asset?></td>
                            <td><?=$row->banjir==1?'Banjir':'Tidak Banjir'?></td>
                            <td><?=$row->jarak_tapak_1?> M</td>
                            <td><?=$row->jarak_tapak_2?> M</td>
                            <td><? if ($row->kemudahan == 0) { 
                              echo "Sangat Mudah";
                            }else if ($row->kemudahan == 1) { 
                              echo "Mudah";
                            }else if ($row->kemudahan == 2) { 
                              echo "Agak Sulit";
                            }else if ($row->kemudahan == 3) { 
                              echo "Sulit";
                            }else{ 
                              echo "Sangat Sulit";
                            }?></td>
                            <td><?=$row->transportasi_umum?></td>
                            <td><?=$row->jarak_ke_terminal?> M</td>
                            <td><?=$row->nama_terminal?></td>
                            <td><?=$row->transport_01?></td>
                            <td><?=$row->transport_02?></td>
                            <td><?=$row->alternatif1?></td>
                            <td><?=$row->alternatif2?></td>
                            <td><?=$row->alternatif3?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                            <td><? if ($row->status == 1) { 
                              echo "Tapak Sudah ditugaskan ke Surveyor";
                            }else if ($row->status == 2) { 
                              echo "Surveyor sudah mengisi data dan lengkap siap direview";
                            }else if ($row->status == 3) { 
                              echo "Data sudah diverifikasi dan dinyatakan benar";
                            }else if ($row->status == 4) { 
                              echo "Data sudah diverifikasi tapi masih harus dikoreksi oleh surveyor";
                            }else{ 
                              echo "-";
                            }?></td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
           
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
