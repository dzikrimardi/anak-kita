<?php
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment;Filename=hasil_survey.xls");

echo "<html>";
echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=Windows-1252\">";
echo "<body>";?>
                <table border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Asset Internal</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport 01</th>
                            <th>Transport 02</th>
                            <th>Alternatif 2</th>
                            <th>Alternatif 3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                           <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->latitude?>, <?=$row->longitude?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->nomor_asset?></td>
                            <td><?=$row->banjir==1?'Banjir':'Tidak Banjir'?></td>
                            <td><?=$row->jarak_tapak_1?> M/Km</td>
                            <td><?=$row->jarak_tapak_2?> M/Km</td>
                            <td><? if ($row->kemudahan == 0) { 
                              echo "Sangat Mudah";
                            }else if ($row->kemudahan == 1) { 
                              echo "Mudah";
                            }else if ($row->kemudahan == 2) { 
                              echo "Agak Sulit";
                            }else if ($row->kemudahan == 3) { 
                              echo "Sulit";
                            }else{ 
                              echo "Sangat Sulit";
                            }?></td>
                            <td><?=$row->transportasi_umum?></td>
                            <td><?=$row->jarak_ke_terminal?> M</td>
                            <td><?=$row->nama_terminal?></td>
                            <td><?=$row->transport_01?></td>
                            <td><?=$row->transport_02?></td>
                            <td><?=$row->alternatif1?></td>
                            <td><?=$row->alternatif2?></td>
                            <td><?=$row->alternatif3?></td>
                            <td><?=$row->approval==1?'yes':'-'?></td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Kode Asset Internal</th>
                            <th>Longitude</th>
                            <th>Tgl Survey</th>
                            <th>Nama Unit</th>
                            <th>Nama Asset</th>
                            <th>Nomor Asset</th>
                            <th>Banjir</th>
                            <th>Jarak Tapak 1</th>
                            <th>Jarak Tapak 2</th>
                            <th>Kemudahan</th>
                            <th>Transportasi Umum</th>
                            <th>Jarak ke Terminal</th>
                            <th>Nama Terminal</th>
                            <th>Transport 01</th>
                            <th>Transport 02</th>
                            <th>Alternatif 2</th>
                            <th>Alternatif 3</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>                 

<?
echo "</body>";
echo "</html>";
?>             