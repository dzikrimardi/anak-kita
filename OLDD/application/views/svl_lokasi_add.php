<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<!-- //20171228 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}

$back_url   = base_url()."svl_lokasi/";

$file_id    = time();
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah/Edit
        <small>Data Pribadi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Anak Asuh</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Data Pribadi</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">&nbsp;</h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action" method="POST">
            <div class="form-horizontal box-body">
                <center>
                    <h2>
                        Data Pribadi 
                    </h2>
                </center>
                <br/>
                <!--edit-->
                <?if($id){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                       <h5>
                            <small>
                                Kode Data : <?=$id?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/><br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>
				
                <!--//edit-->
                <div class="form-group" visible="false">
                    <label for="" class="col-sm-2 control-label label-title">Yayasan</label>

                    <div class="col-sm-6">
                        <select id="group_id" name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                            <?if($this->session->userdata('administrator') == '1'){?>
                            <option value="" >- Pilih Grup -</option>
                            <?}?>
                            <?
                            $ms_db = $db_ms_groups;
                            if($ms_db->num_rows()){
                            ?>
                            <?foreach ($ms_db->result() as $row) { ?>
                            <option <?= (!empty($data_db->group_id)?$data_db->group_id:'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->description?></option>
                            <?}?>
                            <?}?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Anak</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_nama_bumn" type="text" class="form-control" placeholder="Nama Personnel"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_bumn)?$data_db->nama_bumn:''?>">
                        </div>
 <!--                       <div class="col-sm-8 row">
                            <div class="col-sm-3 row">
                                <label for="" class="control-label label-title">Kode Asset Internal</label>
                            </div>                    
                            <div class="col-sm-9 row">
                                <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Asset Internal"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                            </div>
                        </div>
 -->                    
 				</div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2  control-label label-title">Jenis Kelamin</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                              <label>
                            <input name="inp_banjir" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->banjir)?$data_db->banjir:'') == '1')?'checked':''?> > Laki-laki 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_banjir" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->banjir)?$data_db->banjir:'') == '2')?'checked':''?> > Perempuan 
                            &nbsp;&nbsp;&nbsp;

                        </label>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Tempat Lahir</label>

                    <div class="col-sm-10 row">
                        <div class="col-sm-5"">
                            <input name="inp_latitude" id="latitude" type="text" class="form-control" placeholder="Tempat Lahir"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->latitude)?$data_db->latitude:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tanggal Lahir</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_tgl_survey" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tgl_survey)?$data_db->tgl_survey:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Alamat</label>

                    <div class="col-sm-10">
                        <input name="inp_alamat" type="text" class="form-control" id="" placeholder="Alamat"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alamat)?$data_db->alamat:''?>">
                        <small class="label-title"></small>
                    </div>
                </div>

 <!--               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">PIC</label>

                    <div class="col-sm-6">
                        <select name="inp_pic" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                            <?if($this->session->userdata('administrator') == '1'){?>
                            <option value="" >- Pilih User -</option>
                            <?}?>
                            <?
                            $ms_db = $db_ms_users;
                            if($ms_db->num_rows()){
                            ?>
                            <?foreach ($ms_db->result() as $row) { ?>
                            <option 
                                <?= (!empty($data_db->pic)?$data_db->pic:'') == $row->email ?'selected=""selected':''?> 
                                <?= (!empty($data_db->pic)?$data_db->pic:'') == $row->id ?'selected=""selected':''?> 
                                value="<?=$row->email?>" >
                                    <?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )
                                </option>
                            <?}?>
                            <?}?>
                        </select>
                    </div>
                </div>
 -->

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Ayah</label>

                    <div class="col-sm-6">
                        <input name="inp_nama_asset" type="text" class="form-control" placeholder="Nama Ayah" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_asset)?$data_db->nama_asset:''?>">
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">NIK Ayah</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_longitude" type="text" class="form-control" placeholder="NIK Ayah"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->longitude)?$data_db->longitude:''?>">
                        </div>
 				</div>
 				</div>
				
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Alamat Ayah</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_kode_tapak"':'view_kode_tapak';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_kode_tapak"':'view_kode_tapak';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_kode_tapak" 
                            name="inp_kode_tapak" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?></textarea>
                        <?}?>

                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">No HP Ayah</label>

                    <div class="col-sm-6">
                        <input name="inp_jarak_tapak_1" type="text" class="form-control" placeholder="No HP Ayah" <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak_1)?$data_db->jarak_tapak_1:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Ibu</label>

                    <div class="col-sm-10">
                        <div class="col-sm-3 row">
                            <input name="inp_jarak_tapak_2" type="text" class="form-control" placeholder="Nama Ibu"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak_2)?$data_db->jarak_tapak_2:''?>">
                        </div>
                        <div class="col-sm-9">
                            <label for="" class="control-label label-title"><small></small></label>                        
                        </div>

                    </div>
                </div>

				<div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">NIK Ibu</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_pic" type="text" class="form-control" placeholder="NIK Ibu"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pic)?$data_db->pic:''?>">
                        </div>
 				</div>
 				</div>

                <!--<div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Kode&nbsp;Aset<br/>(Sesuai Catatan)</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_nomor_asset" type="text" class="form-control" id="" placeholder="nomor_asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_asset)?$data_db->nomor_asset:''?>">
                        </div>
                        <div class="col-sm-8 row">
                            <div class="col-sm-6 row">
                                <label for="" class="control-label label-title">
                                    Nomor, Tanggal Penetapan Aset
                                </label>
                            </div>                    
                            <div class="col-sm-6 row">
                                <input name="inp_no_tgl_penetapan" type="text" class="form-control" id="" placeholder="Nomor, Tanggal Penetapan Aset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_tgl_penetapan)?$data_db->no_tgl_penetapan:''?>">
                            </div>
                        </div>
                    </div>
                </div>-->

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Alamat Ibu</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_transport_01"':'view_kode_tapak';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_transport_01"':'view_kode_tapak';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->transport_01)?$data_db->transport_01:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_transport_01" 
                            name="inp_transport_01" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->transport_01)?$data_db->transport_01:''?></textarea>
                        <?}?>

                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">No. Telepon Ibu</label>

                    <div class="col-sm-6">
                        <input name="inp_transportasi_umum" type="text" class="form-control" id="" placeholder="No. Telepon Ibu"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transportasi_umum)?$data_db->transportasi_umum:''?>">
                        <small class="label-title"></small>
                    </div>
                </div>

               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Wali</label>

                    <div class="col-sm-10">

                        <div class="col-sm-4 row">
                                <input name="inp_jarak_ke_terminal" type="text" class="form-control" id="" placeholder="Nama Wali"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_ke_terminal)?$data_db->jarak_ke_terminal:''?>">
                        </div>
                    </div>
                </div>

				<div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">NIK Wali</label>

                    <div class="col-sm-10">
                        <div class="col-sm-4 row">
                            <input name="inp_no_tgl_penetapan" type="text" class="form-control" placeholder="NIK Wali"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_tgl_penetapan)?$data_db->no_tgl_penetapan:''?>">
                        </div>
 				</div>
 				</div>

               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Alamat Wali</label>

                    <div class="col-sm-10">
                            <input name="inp_nama_terminal" type="text" class="form-control" id="" placeholder="Alamat Wali"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_terminal)?$data_db->nama_terminal:''?>">

                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">No. Telepon Wali</label>

                    <div class="col-sm-10">
                         <div class="col-sm-4 row">
                       <input name="inp_transport_02" type="text" class="form-control" id="" placeholder="No. Telepon Wali"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transport_02)?$data_db->transport_02:''?>">
                        <small class="label-title"></small>
                         </div>
                   </div>
                </div>


                 <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Berat badan</label>

                    <div class="col-sm-10">
                         <div class="col-sm-4 row">
                        <input name="inp_alternatif1" type="text" class="form-control" id="" placeholder="(dalam kg)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif1)?$data_db->alternatif1:''?>">
                         </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tinggi badan</label>

                    <div class="col-sm-10">
                         <div class="col-sm-4 row">
                        <input name="inp_alternatif2" type="text" class="form-control" id="" placeholder="(dalam meter)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif2)?$data_db->alternatif2:''?>">
	                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Indeks massa tubuh</label>

                    <div class="col-sm-10">
                         <div class="col-sm-4 row">
                        <input name="inp_alternatif3" type="text" class="form-control" id="" placeholder="(dalam kg/m2)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif3)?$data_db->alternatif3:''?>">
	                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Suhu badan</label>

                    <div class="col-sm-10">
                         <div class="col-sm-4 row">
                        <input name="inp_sketsa_lokasi" type="text" class="form-control" id="" placeholder="(dalam derajat Celcius)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
	                    </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tekanan darah</label>

                    <div class="col-sm-10">
                         <div class="col-sm-4 row">
                        <input name="inp_approval" type="text" class="form-control" id="" placeholder="(dalam mm/Hg)"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->approval)?$data_db->approval:''?>">
                    </div>
                    </div>
                </div>
				
               <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Catatan/Saran</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            name="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea 
                            id="inp_respond" 
                            name="inp_respond" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>

                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- //20171228 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })

    //20171228
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })

</script>
<?if($this->uri->segment(2) == 'add' ){?>
<script type="text/javascript">
/*
      var x = document.getElementById("view_lat");
      //x.innerHTML = "TEST Latitude: " ;
      function getLocation() {
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
          } else {
              x.innerHTML = '<font color="red">Geolocation is not supported by this browser.</font>';
          }
      }
      function showPosition(position) {
          //x.innerHTML = "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude;
          $( "#latitude" ).val(position.coords.latitude);
          $( "#longitude" ).val(position.coords.longitude);
      }
    //$( document ).ready(function() {
        $( "#get_lat" ).click(getLocation());                
    //});  
    */
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })


</script>
<?}?>
<script type="text/javascript">
    $("#group_id").change(function() {
        //alert("test");
        in_f_upload.document.getElementById("group_id").value = $("#group_id").val();
    });    
</script>
<script type="text/javascript">
    $("#get_loc").click(function() {
        //var message = document.getElementById("message").value;
        //var lengthLong = document.getElementById("length").checked;

        /* 
            Call the 'makeToast' method in the Java code. 
            'app' is specified in MainActivity.java when 
            adding the JavaScript interface. 
         */
        //window.top.app.makeToast(message, lengthLong);    
        app.get_location();    
        var split_loc   = app.get_location();
        var loc_arr     = split_loc.split(",");
        alert(app.get_location());
        //alert(loc_arr[0]);
        //alert(loc_arr[1]);
      $( "#latitude" ).val(loc_arr[0]);
      $( "#longitude" ).val(loc_arr[1]);

    });

</script>


<?php $this->load->view('inc/home_footer_body'); ?>
