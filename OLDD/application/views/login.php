<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/login_header_head'); ?>
<?    $this->load->view('inc/login_header_css'); ?>
<?    $this->load->view('inc/login_header_meta_title'); ?>
<?    $this->load->view('inc/login_header_body'); ?>


<div class="login-box">
  <div class="login-logo">
    <a href="<?=base_url();?>">Database <b>Anakita</b></a>
  </div>
  <!-- /.login-logo -->
    <center>
        <strong>
            <font style="font-size: x-medium">
              INFORMASI RIWAYAT HIDUP ANAK YATIM PIATU 
            </font>
        </strong>
    </center>
    <br/>
  <div class="login-box-body">
    <center>
      <img src="img/PJB_LOGO.jpg" width="200px">
    </center>
    <p class="login-box-msg">
    </p>

    <form action="<?=base_url();?>home" method="post">
      <div class="form-group has-feedback">
        <input name="user" type="text" class="form-control" placeholder="UserID">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input name="pass" type="password" class="form-control" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
          <br/><br/>
	    	<center>
          <button type="submit" name="login" class="btn btn-primary btn-lg pull-center" style="display: block; margin: 0 auto;">Login</button>
		    </center>
 
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<?php $this->load->view('inc/home_footer_js'); ?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=base_url()?>js/pages/dashboard2.js"></script>
<script type="text/javascript">
    <?if($this->uri->segment(1) == 'logout'){?>
        app.makeToast('log', true);
        app.log_out();    
        app.makeToast('out', true);
    <?}?>
</script>
