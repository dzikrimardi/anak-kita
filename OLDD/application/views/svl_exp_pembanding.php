<?php
// Fungsi header dengan mengirimkan raw data excel
header("Content-type: application/vnd-ms-excel");
 
// Mendefinisikan nama file ekspor "hasil-export.xls"
header("Content-Disposition: attachment; filename=survey_pembanding.xls");
 
// Tambahkan table
?>

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-12">
          <div class="box">

            <!-- /.box-header -->


                <table id="data_table" class="table table-bordered table-hover" border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Id Data Banding</th>
                            <th>Sketsa</th>
                            <th>Tampak Depan</th>
                            <th>Tampak Jalan Depan</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data_banding?></td>
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi){?>          
                                    <a data-toggle="modal" data-target="#img_u_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_u_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>
                            </td>   
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi_d){?>          
                                    <a data-toggle="modal" data-target="#img_d_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_d?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_d_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_d?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>
                            </td>     
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi_j){?>          
                                    <a data-toggle="modal" data-target="#img_j_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_j?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_j_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_j?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>
                            </td>                                                                                
                          <td><?=$row->latitude?></td>
                          <td><?=$row->longitude?></td>
                          <td><?=$row->foto_data?></td>
                          <td><?=$row->harga_penawaran?></td>
                          <td><?=$row->harga_transaksi?></td>
                          <td><?=$row->tanggal?></td>
                          <td><?=$row->sumber_data?></td>
                          <td><?=$row->no_telepon?></td>
                          <td><? if ($row->peruntukan == 1) { 
                              echo "Perumahan";
                            }else if ($row->peruntukan == 2) { 
                              echo "Komersial";
                            }else if ($row->peruntukan == 3) { 
                              echo "Industri";
                            }else if ($row->peruntukan == 4) { 
                              echo "Pertanian";
                            }else if ($row->peruntukan == 5) { 
                              echo "Campuran";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->lebar_jalan_01?> M</td>
                          <td><? if ($row->perkerasan == 1) { 
                              echo "Aspal Penetrasi";
                            }else if ($row->perkerasan == 2) { 
                              echo "Aspal Hotmix";
                            }else if ($row->perkerasan == 3) { 
                              echo "Beton";
                            }else if ($row->perkerasan == 4) { 
                              echo "Paving";
                            }else if ($row->perkerasan == 5) { 
                              echo "Perkerasan Sirtu";
                            }else if ($row->perkerasan == 6) { 
                              echo "Perkerasan Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->kondisi_jalan == 1) { 
                              echo "Baik";
                            }else if ($row->kondisi_jalan == 2) { 
                              echo "Sedang";
                            }else if ($row->kondisi_jalan == 3) { 
                              echo "Rusak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->angkutan_umum == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><?=$row->luas_tanah?> M</td>
                          <td><?=$row->sertifikat?></td>
                          <td><?=$row->bentuk?></td>
                          <td><?=$row->elevasi?> M</td>
                          <td><?=$row->lebar_jalan_02?> M</td>
                          <td><? if ($row->sudut == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><? if ($row->tusuk_sate == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><?=$row->harga_m2?></td>
                          <td><?=$row->luas_m2?> M</td>
                          <td><?=$row->dibangun_tahun?></td>
                          <td><? if ($row->penggunaan == 0) { 
                              echo "Kantor";
                            }else if ($row->penggunaan == 1) { 
                              echo "Ruko";
                            }else if ($row->penggunaan == 2) { 
                              echo "Rumah Tinggal";
                            }else if ($row->penggunaan == 3) { 
                              echo "Gudang";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->jumlah_lantai?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->kondisi?></td>
                          <td><?=$row->harga_m2_bangunan?> M</td>
                          <td><?=$row->sketsa?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                </table>
            </div>         
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
