<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_legal/";
$exp_url = base_url()."svl_exp_legal";
?>
<style type="text/css">
    .table-in td{
        padding: 2px
    }
    .table-in th{
        padding: 5px;
        color: gray;
    }
</style>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Legal
        <small>&nbsp;</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Laporan</a></li>

            <li class="active">Legal</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Unit</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Aset</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tanggal Survey</b> <a class="pull-right"><?=$data_db_r->tgl_survey?></a>
                </li>
              </ul>
                <center>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right" style="margin-left: 15px">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    Laporan
                </h3>
            </div>


            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>
                                <center>
                                    Detail        
                                </center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <i class="fa fa-files-o" aria-hidden="true"></i> : 
                                <a href="<?=$back_url?>?lokasi=<?=$row->id_data?>">
                                 <?=$row->id_data?>
                                </a>     
                                <table class="table-in">
                                    <tr>
                                        <th>
                                            Nama&nbsp;Unit
                                        </th>
                                        <td>
                                            &nbsp;<?=$row->nama_bumn?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <th>Tapak</th>
                                        <td>&nbsp;<?=$row->kode_tapak?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <th>Tanah</th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>Kode&nbsp;Sertifikat</th>
                                                    <th>Luas</th>
                                                    <th>Status&nbsp;Tanah</th>
                                                    <th>No&nbsp;Surat</th>
                                                    <th>Nama&nbsp;Pemegang</th>
                                                    <th><b>Clear?</b></th>
                                                    <th><b>Clean?</b></th>
                                                    <th><b>Rekomendasi</b></th>
                                                </tr>
                                                <?
                                                $lokasi = $row->id_data;
                                                $sql = "SELECT * FROM `survey04_status` WHERE id_data = '$lokasi' ";
                                                $one_data_db  = $this->db->query($sql);
                                                $i_one = 0;
                                                foreach ($one_data_db->result() as $one_row) { 
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?=$one_row->kode_sertifikat?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->luas_tapak?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->status_tanah?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->nomor_surat?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->nama_pemegang?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->nilai_wajar?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->clear?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->clean?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->rekomendasi?>
                                                    </td>
                                                </tr>
                                                <?
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>
                                    <!--<tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <th>Bangunan</th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>No&nbsp;Asset</th>
                                                    <th>Jenis</th>
                                                    <th>Type</th>
                                                    <th>Nama&nbsp;Bangunan</th>
                                                    <th>Luas&nbsp;Bangunan</th>
                                                    <th>Tahun&nbsp;Bangunan</th>
                                                    <th><b>Clear?</b></th>
                                                    <th><b>Clean?</b></th>
                                                    <th><b>Rekomendasi</b></th>
                                                </tr>
                                                <?
                                                $lokasi = $row->id_data;                                                
                                                $sql = "SELECT * FROM `survey09_bangunan` WHERE id_data = '$lokasi' ";
                                                //$sql = "SELECT * FROM `survey04_status` WHERE id_data = '$lokasi' ";
                                                $two_data_db  = $this->db->query($sql);
                                                $i_one = 0;
                                                foreach ($two_data_db->result() as $two_row) { 
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?=$two_row->nomor_asset?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->jenis_bangunan == 1) { 
                                                          echo "Rumah Tinggal";
                                                        }else if ($two_row->jenis_bangunan == 2) { 
                                                          echo "Gedung";
                                                        }else if ($two_row->jenis_bangunan == 3) { 
                                                          echo "Gudang";
                                                        }else{ 
                                                          echo "Lainnya";
                                                        }?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->tipe_bangunan == 1) { 
                                                          echo "Sederhana";
                                                        }else if ($two_row->tipe_bangunan == 2) { 
                                                          echo "Menengah";
                                                        }else if ($two_row->tipe_bangunan == 3) { 
                                                          echo "Menengah - Mewah";
                                                        }else{ 
                                                          echo "Mewah";
                                                        }?>
                                                    </td>
                                                    <td>
                                                        <?=$two_row->nama_bangunan?>
                                                    </td>
                                                    <td>
                                                        <?=$two_row->luas_bangunan?>
                                                    </td>
                                                    <td>
                                                        <?=$two_row->tahun_bangun?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->nilai_wajar?>
                                                    </td>
                                                </tr>
                                                <?
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>-->
                                </table>                              
                            </td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                <?}else{?>

                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data Status</th>
                            <th>Kode Asset Internal Detail</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Nilai Wajar</th>
                            <th>Bukti Perhitungan</th>
                            <th>Clear</th>
                            <th>Clean</th>
                            <th>Rekomendasi</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                           <td><?=$i?></td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_data_status?>?lokasi=<?=$row->id_data?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data_status?>?lokasi=<?=$row->id_data?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_data_status?>?lokasi=<?=$row->id_data?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                          <td><?=$row->id_data_status?></td>
                          <td><?=$row->id_asset_internal?></td>
                          <td><?=$row->luas_tapak?></td>
                          <td><?=$row->status_tanah?></td>
                          <td><?=$row->status_tanah_ajb_d?></td>
                          <td><?=$row->status_tanah_desc1?></td>
                          <td><?=$row->status_tanah_desc2?></td>
                          <td><?=$row->nomor_surat?></td>
                          <td><?=$row->kode_sertifikat?></td>
                          <td><?=$row->nama_pemegang?></td>
                          <td><?=$row->tgl_terbit?></td>
                          <td><?=$row->tgl_berakhir?></td>
                          <td><?=$row->no_tgl_sk?></td>
                          <td><?=$row->no_peta_pendaftaran?></td>
                          <td><?=$row->no_gambar_situasi?></td>
                          <td><?=$row->tgl_surat_ukur?></td>
                          <td><?=$row->keadaan_tanah?></td>
                          <td><?=$row->perubahan_sertifikat?></td>
                          <td><?=$row->jenis_perubahan?></td>
                          <td><?=$row->pembebasan_hak?></td>
                          <td><?=$row->jenis_bukti?></td>
                          <td><?=$row->nomor_bukti?></td>
                          <td><?=$row->tanggal_bukti?></td>
                          <td><?=$row->nilai_wajar?></td>
                          <td><a href="<?=base_url()?>img_files/file/aset/<?=$row->bukti_perhitungan?>"><?=$row->bukti_perhitungan?></a></td>
                          <td><?=$row->clear?></td>
                          <td><?=$row->clean?></td>
                          <td><?=$row->rekomendasi?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data Status</th>
                            <th>Kode Asset Internal Detail</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Nilai Wajar</th>
                            <th>Bukti Perhitungan</th>
                            <th>Clear</th>
                            <th>Clean</th>
                            <th>Rekomendasi</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                <?}?>
            </div>
            <!-- /.box-body -->
            <?if(!$this->session->userdata('r2d2')){?>
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Data Legal</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                         <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Nilai Wajar</th>
                            <th>Bukti Perhitungan</th>
                            <th>Clear</th>
                            <th>Clean</th>
                            <th>Rekomendasi</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                      <?
                      $i = 0;
                      foreach ($data_db->result() as $row) { 
                      $i++;
                      ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->luas_tapak?></td>
                          <td><?=$row->status_tanah?></td>
                          <td><?=$row->status_tanah_ajb_d?></td>
                          <td><?=$row->status_tanah_desc1?></td>
                          <td><?=$row->status_tanah_desc2?></td>
                          <td><?=$row->nomor_surat?></td>
                          <td><?=$row->kode_sertifikat?></td>
                          <td><?=$row->nama_pemegang?></td>
                          <td><?=$row->tgl_terbit?></td>
                          <td><?=$row->tgl_berakhir?></td>
                          <td><?=$row->no_tgl_sk?></td>
                          <td><?=$row->no_peta_pendaftaran?></td>
                          <td><?=$row->no_gambar_situasi?></td>
                          <td><?=$row->tgl_surat_ukur?></td>
                          <td><?=$row->keadaan_tanah?></td>
                          <td><?=$row->perubahan_sertifikat?></td>
                          <td><?=$row->jenis_perubahan?></td>
                          <td><?=$row->pembebasan_hak?></td>
                          <td><?=$row->jenis_bukti?></td>
                          <td><?=$row->nomor_bukti?></td>
                          <td><?=$row->tanggal_bukti?></td>
                          <td><?=$row->nilai_wajar?></td>
                          <td><?=$row->bukti_perhitungan?></td>
                          <td><?=$row->clear?></td>
                          <td><?=$row->clean?></td>
                          <td><?=$row->rekomendasi?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Luas Tapak</th>
                            <th>Status Tanah</th>
                            <th>Status Tanah ajb d</th>
                            <th>Status Tanah desc1</th>
                            <th>Status Tanah desc2</th>
                            <th>Nomor Surat</th>
                            <th>Kode Sertifikat</th>
                            <th>Nama Pemegang</th>
                            <th>Tanggal Terbit</th>
                            <th>Tanggal Berakhir</th>
                            <th>No Tanggal SK</th>
                            <th>No Peta Pendaftaran</th>
                            <th>No Gambar Situasi</th>
                            <th>Tanggal Surat Ukur</th>
                            <th>Keadaan Tanah</th>
                            <th>Perubahan Sertifikat</th>
                            <th>Jenis Perubahan</th>
                            <th>Pembebasan Hak</th>
                            <th>Jenis Bukti</th>
                            <th>Nomor Bukti</th>
                            <th>Tanggal Bukti</th>
                            <th>Nilai Wajar</th>
                            <th>Bukti Perhitungan</th>
                            <th>Clear</th>
                            <th>Clean</th>
                            <th>Rekomendasi</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>     
                <?}?>             
            </div>
            <?}?>             
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
