<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>
<?
$id         = $this->uri->segment(3);
//$id         = $this->input->get('lokasi');
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data    = $this->input->get('lokasi');
$back_url   = base_url()."svl_bangunan/";

$file_id    = time();
?>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah/Edit
        <small>Riwayat Vaksinasi</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Vaksinasi</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>

            <form action="<?=$back_url?>action?lokasi=<?=$id_data?>" method="POST" enctype="multipart/form-data" id="regist">
            <div class="form-horizontal box-body">
                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                        Vaksinasi
                        </small>
                        <h5>
                            <small>
                                Kode Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>

 <!--               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Id Data Bangunan</label>

                    <div class="col-sm-10">
                        <label for="" class="control-label">
                        <font style="opacity: 0.25">
                            (auto)
                        </font>
                        </label>
                    </div>
                </div>
-->
 
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Yayasan</label>

                    <div class="col-sm-10">
<!--                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->email ?'selected=""selected':''?> 
                                        value="<?=$row->email?>" 
                                        ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>           
-->
                        <div class="col-sm-6 row">
                            <select id="group_id" name="inp_group_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
 <!--                               <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih Grup -</option>
                                <?}?>  -->
                                <?
                                $ms_db = $db_ms_groups;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option <?= (!empty($data_db->group_id)?$this->session->userdata('group_id'):'') == $row->id ?'selected=""selected':''?> value="<?=$row->id?>" ><?=$row->description?></option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>
                    </div>
                </div>
				<div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Nama Anak Asuh</label>
					 <div class="col-sm-5">
                            <input name="inp_01" type="text" class="form-control" placeholder="Lainnya"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=$this->session->userdata('nama_anak_yatim');?>" >
                     </div>
					
					
<!--                    <div class="col-sm-10">
                        <div class="col-sm-6 row">
                            <select name="inp_user_id" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                                <?if($this->session->userdata('administrator') == '1'){?>
                                <option value="" >- Pilih User -</option>
                                <?}?>
                                <?
                                $ms_db = $db_ms_users;
                                if($ms_db->num_rows()){
                                ?>
                                <?foreach ($ms_db->result() as $row) { ?>
                                <option 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->id ?'selected=""selected':''?> 
                                        <?= (!empty($data_db->user_id)?$data_db->user_id:'') == $row->email ?'selected=""selected':''?> 
                                        value="<?=$row->email?>" 
                                        ><?=$row->first_name?> <?=$row->last_name?> ( <?=$row->email?> )</option>
                                <?}?>
                                <?}?>
                            </select>
                        </div>           
                    </div> -->
					
                </div>
				
				
                <!--//edit-->

<!--                <div class="box-header with-border">
                <center>
                    <h3>
                        Data Bangunan 
                    </h3>
                </center>
            </div>
			-->
        </br>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Pemberi</label>
                    <div class="col-sm-5">
                            <input name="inp_pond_d" type="text" class="form-control" placeholder="Lainnya"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pondasi_d)?$data_db->pondasi_d:''?>" >
                        </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Wajib/Tambahan</label>

                    <div class="col-sm-3">
                        <label>
                            <input name="inp_lift_jen" value="1" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->lift_jenis)?$data_db->lift_jenis:'') == '1')?'checked':''?> > Wajib 
                            &nbsp;&nbsp;&nbsp;

                            <input name="inp_lift_jen" value="2" type="radio" name="r3" class="flat-red" <?=empty($edit)?'disabled="disabled"':'';?> <?=((!empty($data_db->lift_jenis)?$data_db->lift_jenis:'') == '2')?'checked':''?> > Tambahan 
                            &nbsp;&nbsp;&nbsp;
                        </label>
                    </div>

                </div>


                 <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Imunisasi Wajib</label>

                    <div class="col-sm-5">
                        <select name="inp_jen_bang" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                            <option value="" >- Pilih Imunisasi Wajib -</option>
                            <option value="1" <?=((!empty($data_db->jenis_bangunan)?$data_db->jenis_bangunan:'') == '1')?'selected':''?> >Hepatitis B</option>
                            <option value="2" <?=((!empty($data_db->jenis_bangunan)?$data_db->jenis_bangunan:'') == '2')?'selected':''?> >Polio</option>
                            <option value="3" <?=((!empty($data_db->jenis_bangunan)?$data_db->jenis_bangunan:'') == '3')?'selected':''?> >BCG</option>
                            <option value="4" <?=((!empty($data_db->jenis_bangunan)?$data_db->jenis_bangunan:'') == '4')?'selected':''?> >Campak</option>
							<option value="5" <?=((!empty($data_db->jenis_bangunan)?$data_db->jenis_bangunan:'') == '4')?'selected':''?> >Pentavalen (DPT-HB-HiB)</option>
                        </select>
                    </div>
                    <!--<?if ((!empty($data_db->jenis_bangunan)?$data_db->jenis_bangunan:'') == '4' or true) {?>
                        <div class="col-sm-3 row">
                            <input name="inp_jen_bang_d" type="text" class="form-control" placeholder="Jenis Bangunan..."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jenis_bangunan_d)?$data_db->jenis_bangunan_d:''?>" >
                        </div>
                    <?}else{?>
                        <div class="col-sm-3 row">
                            <input name="inp_jen_bang_d" disabled="disabled" type="text" class="form-control" placeholder="Jenis Bangunan..."  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jenis_bangunan_d)?$data_db->jenis_bangunan_d:''?>" >
                        </div>
                    <?}?>-->
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Imunisasi Tambahan</label>

                    <div class="col-sm-5">
                        <select name="inp_tip_bang" <?=empty($edit)?'disabled="disabled"':'';?> class="form-control">
                            <option value="" >- Pilih Imunisasi Tambahan -</option>
                            <option value="1" <?=((!empty($data_db->tipe_bangunan)?$data_db->tipe_bangunan:'') == '1')?'selected':''?> >Pneumokokus Conjugate Vaccine (PCV)</option>
                            <option value="2" <?=((!empty($data_db->tipe_bangunan)?$data_db->tipe_bangunan:'') == '2')?'selected':''?> >Varisela</option>
                            <option value="3" <?=((!empty($data_db->tipe_bangunan)?$data_db->tipe_bangunan:'') == '3')?'selected':''?> >Influenza</option>
                            <option value="4" <?=((!empty($data_db->tipe_bangunan)?$data_db->tipe_bangunan:'') == '4')?'selected':''?> >Hepatitis A</option>
							<option value="5" <?=((!empty($data_db->tipe_bangunan)?$data_db->tipe_bangunan:'') == '4')?'selected':''?> >HPV (human papiloma virus)</option>
                        </select>
                    </div>
                </div>

               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-1</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_nama_pem" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_bangunan)?$data_db->nama_bangunan:''?>">
                        </div>
                    </div>
                </div>


               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-2</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_nom_ass" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_asset)?$data_db->nomor_asset:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-3</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_jml_lan_d" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jml_lantai_d)?$data_db->jml_lantai_d:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-4</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_rang_d_1" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->rangka_d1)?$data_db->rangka_d1:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-5</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_rang_d_2" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->rangka_d2)?$data_db->rangka_d2:''?>">
                        </div>
                    </div>
                </div>
               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-6</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_rang_jen_d" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->rangka_jendela_d)?$data_db->rangka_jendela_d:''?>">
                        </div>
                    </div>
                </div>
               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-7</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_ac_jen_d" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->ac_jenis_d)?$data_db->ac_jenis_d:''?>">
                        </div>
                    </div>
                </div>
               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tgl Pemberian ke-8</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_lift_merk_orang" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lift_merek_orang)?$data_db->lift_merek_orang:''?>">
                        </div>
                    </div>
                </div>
				
                <hr/>

                <div class="form-group">
                    <label for="inp_cat" class="col-sm-2 control-label label-title">Catatan</label>

                    <div class="col-sm-10">
                        <textarea 
                            id="<?=($this->session->userdata('administrator') == '0')?'inp_cat"':'view_cat';?>" 
                            name="<?=($this->session->userdata('administrator') == '0')?'inp_cat"':'view_cat';?>" 
                            class="form-control" 
                            <?=empty($edit)||($this->session->userdata('administrator') != '0')?'disabled="disabled"':'';?>
                        ><?=!empty($data_db->catatan)?$data_db->catatan:''?></textarea>
                        <? if(($this->session->userdata('administrator') != '0')){?>
                        <textarea 
                            id="inp_cat" 
                            name="inp_cat" 
                            style="visibility:hidden" 
                        ><?=!empty($data_db->catatan)?$data_db->catatan:''?></textarea>
                        <? }?>

                    </div>
                </div>
		<!--
                    <center>
                        <h4 style="<?=($this->session->userdata('administrator') != '1')?'opacity:0.5"':'';?>">
                            (Administrator)
                        </h4>
                    </center>
                </br>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Approval</label>

                    <div class="col-sm-10">
                        <label>
                            <input name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" type="radio" name="r3" value="1" class="flat-red" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=(!empty($data_db->approval)?$data_db->approval:'' == '1')?'checked':''?> > Ya 
                            &nbsp;&nbsp;&nbsp;
                            <input name="<?=($this->session->userdata('administrator') == '1')?'inp_approval"':'view_approval';?>" type="radio" name="r3" value="0" class="flat-red" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?> <?=empty($data_db->approval)?'checked':''?> > Tidak 
                            <?if(($this->session->userdata('administrator') != '1')){?>
                            <input type="hidden" name="inp_approval" value="<?=!empty($data_db->approval)?$data_db->approval:'';?>">
                            <?}?>
                        </label>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Respond</label>

                    <div class="col-sm-10">
                        <textarea id="<?=($this->session->userdata('administrator') == '1')?'inp_respond"':'view_respond';?>" name="inp_respond" class="form-control" <?=empty($edit)||($this->session->userdata('administrator') != '1')?'disabled="disabled"':'';?>><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?if(($this->session->userdata('administrator') != '1')){?>
                        <textarea id="inp_respond" style="visibility:hidden" ><?=!empty($data_db->respond)?$data_db->respond:''?></textarea>
                        <?}?>

                    </div>
                </div>
				-->
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>

          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    $("#group_id").change(function() {
        //alert("test");
        in_f_upload.document.getElementById("group_id").value = $("#group_id").val();
    });    
</script>

<!-- page script -->
<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    $(function () {
        function m2(){
            var m2  = 0;
            var p   = $('#p').val();
            var l   = $('#l').val();

            if(p && l){
                m2 = p*l;
                $('#m2').html(m2 + " m<sup>2</sup>");
            }
        }
        $( "#p" ).change(function() {
          //alert( $( "#p" ).val());
          m2();
        });
        $( "#l" ).change(function() {
          //alert( $( "#l" ).val());
          m2();
        });
        //$('#l').change(alert("tes"));
        //$('#l').change(m2());
        //$('#p').change(m2());
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#regist").validate({
            rules:{
                kode_asset:"required"
                },
            messages:{
                kode_asset:"Kode Asset Tidak Boleh"
                /*nama_wali:"Masukkan nama wali mahasiswa",
                tgl_lahir_wali:"Pilih tanggal lahir wali mahasiswa",
                bln_lahir_wali:"Pilih bulan lahir wali mahasiswa",
                thn_lahir_wali:"Pilih tahun lahir wali mahasiswa",
                pendidikan_wali:"Pilih pendidikan wali mahasiswa",
                pekerjaan_wali:"Pilih pekerjaan wali mahasiswa",
                penghasilan_wali:"Pilih penghasilan wali mahasiswa"*/
            },
            errorClass: "help-inline",
            errorElement: "span",
            highlight:function(element, errorClass, validClass) {
                $(element).parents('.form-group').addClass('has-error');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.form-group').removeClass('has-error');
                $(element).parents('.form-group').addClass('has-success');
            }
        });
    });
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
