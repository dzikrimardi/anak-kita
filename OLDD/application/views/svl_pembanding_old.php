<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_pembanding/";
$exp_url = base_url()."svl_exp_pembanding";
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Survey - Pembanding
            <small>&nbsp;</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Survey</a></li>

            <?if($this->input->get('lokasi')){?>

            <li>
                <a href="<?=$back_url?>">
                    Lokasi
                </a>
            </li>
            <li class="active">Pembanding</li>

            <?}else{?>

            <li class="active">Lokasi</li>

            <?}?>
        </ol>    
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Unit</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Aset</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tanggal Survey</b> <a class="pull-right"><?=$data_db_r->tgl_survey?></a>
                </li>
              </ul>
                <center>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right" style="margin-left: 15px">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?if($this->input->get('lokasi')){?>
                        Pembanding
                    <?}else{?>
                        Lokasi                
                    <?}?>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <?if(!$this->input->get('lokasi')){?>
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th width="150px">
                                <center>
                                    Detail        
                                </center>
                            </th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <center>
                                    <i class="fa fa-files-o" aria-hidden="true"></i>&nbsp;:&nbsp;<a href="<?=$back_url?>?lokasi=<?=$row->id_data?>"><?=$row->id_data?></a>     
                                </center>                           
                            </td>
                            <td><?=$row->kode_tapak?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td><?=$row->nama_asset?></td>
                            <td><?=$row->tgl_survey?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>Kode Asset Internal</th>
                            <th>Nama Unit</th>
                            <th>Nama Aset</th>
                            <th>Tanggal Survey</th>
                            <th>Approval</th>
                            <th>Respond</th>
                        </tr>
                    </tfoot>
                </table>
                <?}else{?>

                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data Banding</th>
                            <th>Sketsa</th>
                            <th>Tampak Depan</th>
                            <th>Tampak Jalan Depan</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        //$img_file_exists    = base_url().'img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_d;
                        //$file_exists        = (strpos($img_file_exists, "404") === false);
                        //file_exists('img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_d)
                        //$img_file_d         = $img_file_exists;
                        $img_file           = base_url().'img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi;
                        if(!(file_exists('img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi))){
                            $img_file       = base_url().'img_files/group_/'.$row->sketsa_lokasi;
                        }

                        $img_file_d         = base_url().'img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_d;
                        if(!(file_exists('img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_d))){
                            $img_file_d     = base_url().'img_files/group_/'.$row->sketsa_lokasi_d;
                        }

                        $img_file_j         = base_url().'img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_j;
                        if(!(file_exists('img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_j))){
                            $img_file_j     = base_url().'img_files/group_/'.$row->sketsa_lokasi_j;
                        }

                        ?>
                        <tr>
                          <td>
                            <?=$i?>
                            <?
                            if($this->input->get('bug')){
                                //print_r($row);
                                var_dump(file_exists('img_files/group_'.$row->group_id.'/'.$row->sketsa_lokasi_d));
                            }?>  
                          </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>delete/<?=$row->id_data_banding?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <font color="red">
                                        <i class="fa fa-times" alt="edit"></i>
                                        </font>
                                    </a>                                     
                                </center>
                            </td>
                            <td>
                                <center>
                                    <a href="<?=$back_url?>edit/<?=$row->id_data_banding?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-edit" alt="edit"></i>
                                    </a> 
                                    <span>&nbsp;&nbsp;&nbsp;</span> 
                                    <a href="<?=$back_url?>view/<?=$row->id_data_banding?>?lokasi=<?=$this->input->get('lokasi')?>">
                                        <i class="fa fa-eye" alt="view"></i>
                                    </a>     
                                </center>                           
                            </td>
                          <td><?=$row->id_data_banding?></td>
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi){?>          
                                    <a data-toggle="modal" data-target="#img_u_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=$img_file?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_u_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=$img_file?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>
                            </td>   
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi_d){?>          
                                    <a data-toggle="modal" data-target="#img_d_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=$img_file_d?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_d_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=$img_file_d?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>
                            </td>     
                            <td>
                                <center>
                                    <?if($row->sketsa_lokasi_j){?>          
                                    <a data-toggle="modal" data-target="#img_j_<?=$i?>" href="#view">
                                        <img width="50px" src="<?=$img_file_j?>">
                                    </a>                      

                                    <!-- Modal -->
                                    <div id="img_j_<?=$i?>" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Sketsa Lokasi</h4>
                                          </div>
                                          <div class="modal-body">
                                                <p>
                                                    <center>
                                                        <img style="max-width: 550px" src="<?=$img_file_j?>">
                                                    </center>
                                                </p>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                          </div>
                                        </div>

                                      </div>
                                    </div>
                                    <?}else{?>
                                    -
                                    <?}?>                                
                                </center>
                            </td>                                                                                
                          <td><?=$row->latitude?></td>
                          <td><?=$row->longitude?></td>
                          <td><?=$row->foto_data?></td>
                          <td><?=$row->harga_penawaran?></td>
                          <td><?=$row->harga_transaksi?></td>
                          <td><?=$row->tanggal?></td>
                          <td><?=$row->sumber_data?></td>
                          <td><?=$row->no_telepon?></td>
                          <td><? if ($row->peruntukan == 1) { 
                              echo "Lainnya";
                            }else if ($row->peruntukan == 2) { 
                              echo "Komersial";
                            }else if ($row->peruntukan == 3) { 
                              echo "Industri";
                            }else if ($row->peruntukan == 4) { 
                              echo "Pertanian";
                            }else if ($row->peruntukan == 5) { 
                              echo "Campuran";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->lebar_jalan_01?> M</td>
                          <td><? if ($row->perkerasan == 1) { 
                              echo "Aspal Penetrasi";
                            }else if ($row->perkerasan == 2) { 
                              echo "Aspal Hotmix";
                            }else if ($row->perkerasan == 3) { 
                              echo "Beton";
                            }else if ($row->perkerasan == 4) { 
                              echo "Paving";
                            }else if ($row->perkerasan == 5) { 
                              echo "Perkerasan Sirtu";
                            }else if ($row->perkerasan == 6) { 
                              echo "Perkerasan Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->kondisi_jalan == 1) { 
                              echo "Baik";
                            }else if ($row->kondisi_jalan == 2) { 
                              echo "Sedang";
                            }else if ($row->kondisi_jalan == 3) { 
                              echo "Rusak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->angkutan_umum == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><?=$row->luas_tanah?> M</td>
                          <td><?=$row->sertifikat?></td>
                          <td><?=$row->bentuk?></td>
                          <td><?=$row->elevasi?> M</td>
                          <td><?=$row->lebar_jalan_02?> M</td>
                          <td><? if ($row->sudut == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><? if ($row->tusuk_sate == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><?=$row->harga_m2?></td>
                          <td><?=$row->luas_m2?> M</td>
                          <td><?=$row->dibangun_tahun?></td>
                          <td><? if ($row->penggunaan == 0) { 
                              echo "Kantor";
                            }else if ($row->penggunaan == 1) { 
                              echo "Ruko";
                            }else if ($row->penggunaan == 2) { 
                              echo "Rumah Tinggal";
                            }else if ($row->penggunaan == 3) { 
                              echo "Gudang";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->jumlah_lantai?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->kondisi?></td>
                          <td><?=$row->harga_m2_bangunan?> M</td>
                          <td><?=$row->sketsa?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Id Data Banding</th>
                            <th>Sketsa</th>
                            <th>Tampak Depan</th>
                            <th>Tampak Jalan Depan</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
                <?}?>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
                    <h1>List Pembanding</h1>
                </center>
                <table border="1" style="width: 100%">
                    <thead>
                       <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?if($data_db->num_rows()){?>
                        <?
                        $i = 0;
                        foreach ($data_db->result() as $row) { 
                        $i++;
                        ?>
                        <tr>
                          <td><?=$i?></td>
                          <td><?=$row->id_data?></td>
                          <td><?=$row->latitude?></td>
                          <td><?=$row->longitude?></td>
                          <td><?=$row->foto_data?></td>
                          <td><?=$row->harga_penawaran?></td>
                          <td><?=$row->harga_transaksi?></td>
                          <td><?=$row->tanggal?></td>
                          <td><?=$row->sumber_data?></td>
                          <td><?=$row->no_telepon?></td>
                          <td><? if ($row->peruntukan == 1) { 
                              echo "Perumahan";
                            }else if ($row->peruntukan == 2) { 
                              echo "Komersial";
                            }else if ($row->peruntukan == 3) { 
                              echo "Industri";
                            }else if ($row->peruntukan == 4) { 
                              echo "Pertanian";
                            }else if ($row->peruntukan == 5) { 
                              echo "Campuran";
                            }else{ 
                              echo "Lainnya";
                            }?></td>
                          <td><?=$row->lebar_jalan_01?> M</td>
                          <td><? if ($row->perkerasan == 1) { 
                              echo "Aspal Penetrasi";
                            }else if ($row->perkerasan == 2) { 
                              echo "Aspal Hotmix";
                            }else if ($row->perkerasan == 3) { 
                              echo "Beton";
                            }else if ($row->perkerasan == 4) { 
                              echo "Paving";
                            }else if ($row->perkerasan == 5) { 
                              echo "Perkerasan Sirtu";
                            }else if ($row->perkerasan == 6) { 
                              echo "Perkerasan Tanah";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->kondisi_jalan == 1) { 
                              echo "Baik";
                            }else if ($row->kondisi_jalan == 2) { 
                              echo "Sedang";
                            }else if ($row->kondisi_jalan == 3) { 
                              echo "Rusak";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><? if ($row->angkutan_umum == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><?=$row->luas_tanah?> M</td>
                          <td><?=$row->sertifikat?></td>
                          <td><?=$row->bentuk?></td>
                          <td><?=$row->elevasi?> M</td>
                          <td><?=$row->lebar_jalan_02?> M</td>
                          <td><? if ($row->sudut == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><? if ($row->tusuk_sate == 1) { 
                              echo "Ada";
                            }else{ 
                              echo "Tidak";
                            }?></td>
                          <td><?=$row->harga_m2?></td>
                          <td><?=$row->luas_m2?> M</td>
                          <td><?=$row->dibangun_tahun?></td>
                          <td><? if ($row->penggunaan == 0) { 
                              echo "Kantor";
                            }else if ($row->penggunaan == 1) { 
                              echo "Ruko";
                            }else if ($row->penggunaan == 2) { 
                              echo "Rumah Tinggal";
                            }else if ($row->penggunaan == 3) { 
                              echo "Gudang";
                            }else{ 
                              echo "-";
                            }?></td>
                          <td><?=$row->rangka?></td>
                          <td><?=$row->jumlah_lantai?></td>
                          <td><?=$row->lantai?></td>
                          <td><?=$row->dinding?></td>
                          <td><?=$row->atap?></td>
                          <td><?=$row->langit_langit?></td>
                          <td><?=$row->kondisi?></td>
                          <td><?=$row->harga_m2_bangunan?> M</td>
                          <td><?=$row->sketsa?></td>
                            <td>
                                <center>
                                    <?=$row->approval==1?'<font color="green"><i class="fa fa-check" aria-hidden="true"></i></font>':'-'?></td>
                                </center>
                            </td>
                            <td><?=$row->respond?></td>
                          <td></td>
                        </tr>
                        <?
                          }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Id Data</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Foto Data</th>
                            <th>Harga Penawaran</th>
                            <th>Harga Transaksi</th>
                            <th>Tanggal</th>
                            <th>Sumber Data</th>
                            <th>No Telepon</th>
                            <th>Peruntukan</th>
                            <th>Lebar Jalan 01</th>
                            <th>Perkerasan</th>
                            <th>Kondisi Jalan</th>
                            <th>Angkutan Umum</th>
                            <th>Luas Tanah</th>
                            <th>Sertifikat</th>
                            <th>Bentuk</th>
                            <th>Elevasi</th>
                            <th>Lebar Jalan 02</th>
                            <th>Sudut</th>
                            <th>Tusuk Sate</th>
                            <th>Harga M2</th>
                            <th>Luas M2</th>
                            <th>Di Bangun Tahun</th>
                            <th>Penggunaan</th>
                            <th>Rangka</th>
                            <th>Jumlah Lantai</th>
                            <th>Lantai</th>
                            <th>Dinding</th>
                            <th>Atap</th>
                            <th>Langit Langit</th>
                            <th>Kondisi</th>
                            <th>Harga M2 Bangunan</th>
                            <th>Sketsa</th>
                            <th>Approval</th>
                            <th>Respond</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>     
                <?}?>                   
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
