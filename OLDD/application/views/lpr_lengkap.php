<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>


<?
$back_url = base_url()."svl_legal/";
$exp_url = base_url()."svl_exp_legal";
?>
<style type="text/css">
    .table-in td{
        padding: 2px
    }
    .table-in th{
        padding: 5px;
        color: gray;
    }
</style>

<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="<?=!$this->session->userdata('r2d2')?'content-wrapper':''?>">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Laporan Riwayat Lengkap
        <small>&nbsp;</small>
      </h1>
        <ol class="breadcrumb">
            <li><a href="<?=$back_url?>"><i class="fa fa-pencil-square-o"></i>Laporan</a></li>

            <li class="active">Riwayat Lengkap</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        <?if($this->input->get('lokasi')){?>

        <div class="box box-primary">
            <div class="box-body box-profile">
              <h3 class="profile-username text-center">ID DATA : <?=$data_db_r->id_data?></h3>

              <p class="text-muted text-center">&nbsp;</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item text-muted">
                  <b>Nama Anak</b> <a class="pull-right"><?=$data_db_r->nama_bumn?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Nama Aset</b> <a class="pull-right"><?=$data_db_r->nama_asset?></a>
                </li>
                <li class="list-group-item text-muted">
                  <b>Tanggal Survey</b> <a class="pull-right"><?=$data_db_r->tgl_survey?></a>
                </li>
              </ul>
                <center>
                    <a href="<?=$back_url?>add?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-primary pull-right" style="margin-left: 15px">
                        Input &nbsp; 
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </a>
                    <a href="<?=$exp_url?>?lokasi=<?=$this->input->get('lokasi')?>" class="btn btn-small btn-success pull-right">
                        Export to Excel &nbsp; 
                    </a>
                    <i class="fa fa-files-o fa-2x" aria-hidden="true"></i>
                    <a href="<?=$back_url?>" class="btn btn-default pull-left"><b>Back </b></a>
                </center>
            </div>
            <!-- /.box-body -->
          </div>

        <?}?>

      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
                <h3 class="box-title">
                    <?=$this->session->userdata('nama_yayasan')?>
                </h3>
            </div>


            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;
                                
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <table class="table-in">
                                    <tr>
                                        <th>
                                            Nama&nbsp;Anak:
                                        </th>
                                        <td colspan="3">
                                            &nbsp;<?=$row->nama_bumn?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <th>Alamat:</th>
                                        <td colspan="3">&nbsp;<?=$row->alamat?></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <th colspan="3">Riwayat Vaksinasi</th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Type Imunisasi</th>
                                                    <th>Jenis Wajib</th>
                                                    <th>Jenis Tambahan</th>
                                                    <th>Pemberian ke-1</th>
                                                    <th>Pemberian ke-2</th>
                                                    <th>Pemberian ke-3</th>
                                                    <th>Pemberian ke-4</th>
                                                    <th>Pemberian ke-5</th>
                                                    <th>Pemberian ke-6</th>
                                                    <th>Pemberian ke-7</th>
                                                    <th>Pemberian ke-8</th>
                                                </tr>
                                                <?
                                                $lokasi = $row->id_data;                                                
                                                $sql = "SELECT * FROM `survey09_bangunan` WHERE id_data = '$lokasi' ";
                                                //$sql = "SELECT * FROM `survey04_status` WHERE id_data = '$lokasi' ";
                                                $two_data_db  = $this->db->query($sql);
                                                $i_one = 1;
                                                foreach ($two_data_db->result() as $two_row) { 
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?=$i_one?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->lift_jenis == 1) { 
                                                          echo "Wajib";
                                                        }else if ($two_row->lift_jenis == 2) { 
                                                          echo "Tambahan";
                                                        }else{ 
                                                          echo "Lainnya";
                                                        }?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->jenis_bangunan == 1) { 
															  echo "Hepatitis B";
															}else if ($two_row->jenis_bangunan == 2) { 
															  echo "Polio";
															}else if ($two_row->jenis_bangunan == 3) { 
															  echo "BCG";
															}else if ($two_row->jenis_bangunan == 4) { 
															  echo "Campak (DPT-HB-HiB)";
															}else{ 
															  echo "-";
															}?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->tipe_bangunan == 1) { 
														  echo "Pneumokokus Conjugate Vaccine (PCV)";
														}else if ($two_row->tipe_bangunan == 2) { 
														  echo "Varisela";
														}else if ($two_row->tipe_bangunan == 3) { 
														  echo "Influenza";
														}else if ($two_row->tipe_bangunan == 4) { 
														  echo "Hepatitis A";
														}else if ($two_row->tipe_bangunan == 5) { 
														  echo "HPV (human papiloma virus)";
														}else{ 
														  echo "-";
														}?>
													</td>
                          <td><? echo substr($two_row->nama_bangunan,8,2) . "-" . substr($two_row->nama_bangunan,5,2) . "-" . substr($two_row->nama_bangunan,0,4) ; ?></td>
                          <td><? echo substr($two_row->nomor_asset,8,2) . "-" . substr($two_row->nomor_asset,5,2) . "-" . substr($two_row->nomor_asset,0,4) ; ?></td>
                          <td><? echo substr($two_row->jml_lantai_d,8,2) . "-" . substr($two_row->jml_lantai_d,5,2) . "-" . substr($two_row->jml_lantai_d,0,4) ; ?></td>
                          <td><? echo substr($two_row->rangka_d1,8,2) . "-" . substr($two_row->rangka_d1,5,2) . "-" . substr($two_row->rangka_d1,0,4) ; ?></td>
                          <td><? echo substr($two_row->rangka_d2,8,2) . "-" . substr($two_row->rangka_d2,5,2) . "-" . substr($two_row->rangka_d2,0,4) ; ?></td>
                          <td><? echo substr($two_row->rangka_jendela_d,8,2) . "-" . substr($two_row->rangka_jendela_d,5,2) . "-" . substr($two_row->rangka_jendela_d,0,4) ; ?></td>
                          <td><? echo substr($two_row->ac_jenis_d,8,2) . "-" . substr($two_row->ac_jenis_d,5,2) . "-" . substr($two_row->ac_jenis_d,0,4) ; ?></td>
                          <td><? echo substr($two_row->lift_merek_orang,8,2) . "-" . substr($two_row->lift_merek_orang,5,2) . "-" . substr($two_row->lift_merek_orang,0,4) ; ?></td>
                                                </tr>
                                                <?
												$i_one = $i_one+1;
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>
									
									                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <th colspan="3">Riwayat Kesehatan</th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Gejala/Penyakit</th>
                                                    <th>Catatan Dokter</th>
                                                    <th>Obat Diresepkan</th>
                                                    <th>Terima Obat?</th>
                                                </tr>
                                                <?
                                                $lokasi = $row->id_data;
                                                $sql = "SELECT * FROM `survey07_hbu` WHERE id_data = '$lokasi' ";
                                                $one_data_db  = $this->db->query($sql);
                                                $i_one = 1;
                                                foreach ($one_data_db->result() as $one_row) { 
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?=$i_one?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->penggunaan_lahan_d?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->pemanfaatan_lahan_d?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->kesesuaian_lahan_d?>
                                                    </td>
													  <td><? if ($one_row->optimalisasi_asset == 1) { 
														  echo "Ya";
														}else if ($one_row->optimalisasi_asset == 2) { 
														  echo "Tidak";
														}else{ 
														  echo "-";
														}?></td>
													</tr>
                                                <?
												$i_one = $i_one+1;
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>

                                </table>                              
                            </td>
                        </tr>
                    <?}?>                        
                    <?}?>                        
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <?if(!$this->session->userdata('r2d2')){?>
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <?//if($this->input->get('lokasi')){?>
            <div id="print-area" style="display: none">
                <center>
					<h2><?=$this->session->userdata('nama_yayasan')?></h2>
                    <h4>Laporan Riwayat Lengkap</h4>
                </center>
                <table style="width: 100%" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>&nbsp;
                                
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    //print_r($row);
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td>
                                <table style="width: 100%" >
                                    <tr>
                                        <th>
                                            Nama&nbsp;Anak:
                                        </th>
                                        <td colspan="3">
                                            <?=$row->nama_bumn?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <th>Alamat:</th>
                                        <td colspan="3">&nbsp;<?=$row->alamat?></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <th colspan="3">Riwayat Vaksinasi</th>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Type Imunisasi</th>
                                                    <th>Jenis Wajib</th>
                                                    <th>Jenis Tambahan</th>
                                                    <th>Pemberian ke-1</th>
                                                    <th>Pemberian ke-2</th>
                                                    <th>Pemberian ke-3</th>
                                                    <th>Pemberian ke-4</th>
                                                    <th>Pemberian ke-5</th>
                                                    <th>Pemberian ke-6</th>
                                                    <th>Pemberian ke-7</th>
                                                    <th>Pemberian ke-8</th>
                                                </tr>
                                                <?
                                                $lokasi = $row->id_data;                                                
                                                $sql = "SELECT * FROM `survey09_bangunan` WHERE id_data = '$lokasi' ";
                                                //$sql = "SELECT * FROM `survey04_status` WHERE id_data = '$lokasi' ";
                                                $two_data_db  = $this->db->query($sql);
                                                $i_one = 1;
                                                foreach ($two_data_db->result() as $two_row) { 
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?=$i_one?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->lift_jenis == 1) { 
                                                          echo "Wajib";
                                                        }else if ($two_row->lift_jenis == 2) { 
                                                          echo "Tambahan";
                                                        }else{ 
                                                          echo "Lainnya";
                                                        }?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->jenis_bangunan == 1) { 
															  echo "Hepatitis B";
															}else if ($two_row->jenis_bangunan == 2) { 
															  echo "Polio";
															}else if ($two_row->jenis_bangunan == 3) { 
															  echo "BCG";
															}else if ($two_row->jenis_bangunan == 4) { 
															  echo "Campak (DPT-HB-HiB)";
															}else{ 
															  echo "-";
															}?>
                                                    </td>
                                                    <td>
                                                        <? if ($two_row->tipe_bangunan == 1) { 
														  echo "Pneumokokus Conjugate Vaccine (PCV)";
														}else if ($two_row->tipe_bangunan == 2) { 
														  echo "Varisela";
														}else if ($two_row->tipe_bangunan == 3) { 
														  echo "Influenza";
														}else if ($two_row->tipe_bangunan == 4) { 
														  echo "Hepatitis A";
														}else if ($two_row->tipe_bangunan == 5) { 
														  echo "HPV (human papiloma virus)";
														}else{ 
														  echo "-";
														}?>
													</td>
                          <td><? echo substr($two_row->nama_bangunan,8,2) . "-" . substr($two_row->nama_bangunan,5,2) . "-" . substr($two_row->nama_bangunan,0,4) ; ?></td>
                          <td><? echo substr($two_row->nomor_asset,8,2) . "-" . substr($two_row->nomor_asset,5,2) . "-" . substr($two_row->nomor_asset,0,4) ; ?></td>
                          <td><? echo substr($two_row->jml_lantai_d,8,2) . "-" . substr($two_row->jml_lantai_d,5,2) . "-" . substr($two_row->jml_lantai_d,0,4) ; ?></td>
                          <td><? echo substr($two_row->rangka_d1,8,2) . "-" . substr($two_row->rangka_d1,5,2) . "-" . substr($two_row->rangka_d1,0,4) ; ?></td>
                          <td><? echo substr($two_row->rangka_d2,8,2) . "-" . substr($two_row->rangka_d2,5,2) . "-" . substr($two_row->rangka_d2,0,4) ; ?></td>
                          <td><? echo substr($two_row->rangka_jendela_d,8,2) . "-" . substr($two_row->rangka_jendela_d,5,2) . "-" . substr($two_row->rangka_jendela_d,0,4) ; ?></td>
                          <td><? echo substr($two_row->ac_jenis_d,8,2) . "-" . substr($two_row->ac_jenis_d,5,2) . "-" . substr($two_row->ac_jenis_d,0,4) ; ?></td>
                          <td><? echo substr($two_row->lift_merek_orang,8,2) . "-" . substr($two_row->lift_merek_orang,5,2) . "-" . substr($two_row->lift_merek_orang,0,4) ; ?></td>
                                                </tr>
                                                <?
												$i_one = $i_one+1;
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>
									
									                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <th colspan="3">Riwayat Kesehatan</th>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Gejala/Penyakit</th>
                                                    <th>Catatan Dokter</th>
                                                    <th>Obat Diresepkan</th>
                                                    <th>Terima Obat?</th>
                                                </tr>
                                                <?
                                                $lokasi = $row->id_data;
                                                $sql = "SELECT * FROM `survey07_hbu` WHERE id_data = '$lokasi' ";
                                                $one_data_db  = $this->db->query($sql);
                                                $i_one = 1;
                                                foreach ($one_data_db->result() as $one_row) { 
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?=$i_one?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->penggunaan_lahan_d?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->pemanfaatan_lahan_d?>
                                                    </td>
                                                    <td>
                                                        <?=$one_row->kesesuaian_lahan_d?>
                                                    </td>
													  <td><? if ($one_row->optimalisasi_asset == 1) { 
														  echo "Ya";
														}else if ($one_row->optimalisasi_asset == 2) { 
														  echo "Tidak";
														}else{ 
														  echo "-";
														}?></td>
													</tr>
                                                <?
												$i_one = $i_one+1;
                                                }
                                                ?>
                                            </table>
                                        </td>
                                    </tr>
                                </table>                              
                            </td>
                        </tr>
                    <?}?>                        
                    <?}?>          
                </table>     
                <?//}?>             
            </div>
            <?}?>             
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
