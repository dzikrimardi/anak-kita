<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>




<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Dokumen
        <small>&nbsp;</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-users"></i> Dokumen</a></li>
        <li class="active">Foto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List Foto/Dokumen</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
                <table id="data_table" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Penduduk</th>
                            <th>Nama </th>
                            <th>Akte Lahir</th>
                            <th>Kartu Keluarga</th>
                            <th>Kartu BPJS</th>
                            <th>KTP</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->id_data?></td>
                            <td><?=$row->nama_bumn?></td>
                            <td>
                                <? if($row->sketsa_lokasi){?>          
                                <a data-toggle="modal" data-target="#img_<?=$i?>" href="#view">
                                    <img width="320px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi?>">
                                </a>                      

                                <div id="img_<?=$i?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Sketsa Lokasi</h4>
                                      </div>
                                      <div class="modal-body">
                                            <p>
                                                <center>
                                                    <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi?>">
                                                </center>
                                            </p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>


                                <? }else{ ?>
                                - Tidak Ada Gambar -
                                <? } ?>
                            </td>
                            <td>
                                <?if($row->sketsa_lokasi_u){?>          
                                <a data-toggle="modal" data-target="#img_u_<?=$i?>" href="#view">
                                    <img width="320px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_u?>">
                                </a>                      

                                <!-- Modal -->
                                <div id="img_u_<?=$i?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Sketsa Lokasi</h4>
                                      </div>
                                      <div class="modal-body">
                                            <p>
                                                <center>
                                                    <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_u?>">
                                                </center>
                                            </p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>


                                <?}else{?>
                                - Tidak Ada Gambar -
                                <?}?>
                            </td>
                            <td>
                                <?if($row->sketsa_lokasi_s){?>          
                                <a data-toggle="modal" data-target="#img_s_<?=$i?>" href="#view">
                                    <img width="320px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_s?>">
                                </a>                      

                                <!-- Modal -->
                                <div id="img_s_<?=$i?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Sketsa Lokasi</h4>
                                      </div>
                                      <div class="modal-body">
                                            <p>
                                                <center>
                                                    <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_s?>">
                                                </center>
                                            </p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>


                                <?}else{?>
                                - Tidak Ada Gambar -
                                <?}?>
                            </td>
                            <td>
                                <?if($row->sketsa_lokasi_t){?>          
                                <a data-toggle="modal" data-target="#img_t_<?=$i?>" href="#view">
                                    <img width="320px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_t?>">
                                </a>                      

                                <!-- Modal -->
                                <div id="img_t_<?=$i?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Sketsa Lokasi</h4>
                                      </div>
                                      <div class="modal-body">
                                            <p>
                                                <center>
                                                    <img style="max-width: 550px" src="<?=base_url()?>img_files/group_<?=$row->group_id?>/<?=$row->sketsa_lokasi_t?>">
                                                </center>
                                            </p>
                                      </div>
                                      <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>

                                  </div>
                                </div>


                                <?}else{?>
                                - Tidak Ada Gambar -
                                <?}?>
                            </td>

                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>ID Penduduk</th>
                            <th>Nama </th>
                            <th>Akte Lahir</th>
                            <th>Kartu Keluarga</th>
                            <th>Kartu BPJS</th>
                            <th>KTP</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
            <hr>
            <center>
                <button type="button" class="btn btn-info btn-app" onclick="printArea('print-area')">
                    <i class="fa fa-print"></i>
                </button>
            </center>
            <div id="print-area" style="display: none">
                <center>
                    <h1>Data Lokasi</h1>
                </center>
                <table border="1" style="width: 100%">
                                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>File</th>
                            <th>&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?if($data_db->num_rows()){?>
                    <?
                    $i = 0;
                    foreach ($data_db->result() as $row) { 
                    $i++;
                    ?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$row->judul?></td>
                            <td><?=$row->image_url?></td>
                            <td>&nbsp;</td>
                        </tr>
                    <?}?>
                    <?}?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>File</th>
                            <th>Type</th>
                        </tr>
                    </tfoot>
                </table>                
            </div>
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->
<script>
  $(function () {
    $('#data_table').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
