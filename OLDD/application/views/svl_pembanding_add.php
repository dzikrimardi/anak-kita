<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="<?=base_url()?>vendors/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?=base_url()?>plugins/iCheck/all.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
//$id         = $this->input->get('lokasi');
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}
$id_data    = $this->input->get('lokasi');
// khusus untuk setup saja
//$id_data = $this->uri->segment(3);
//echo $id_data;
//exit;

$back_url   = base_url()."svl_pembanding/";

$file_id    = time();

$img_file   = "";
$img_file_d = "";
$img_file_j = "";

if (!empty($this->session->userdata('group_id'))) {
		$group_id = $this->session->userdata('group_id');
	} else {
	$group_id = $data_db->group_id;
}
echo $this->session->userdata('group_id');
//exit;
if(!empty($data_db->sketsa_lokasi)){
    $img_file           = base_url().'img_files/group_'.$group_id.'/'.$data_db->sketsa_lokasi;
    if(!(file_exists('img_files/group_'.$group_id.'/'.$data_db->sketsa_lokasi))){
        $img_file       = base_url().'img_files/group_/'.$data_db->sketsa_lokasi;
    }
	//echo $img_file;
	//exit;
}

if(!empty($data_db->sketsa_lokasi_d)){
    $img_file_d         = base_url().'img_files/group_'.$group_id.'/'.$data_db->sketsa_lokasi_d;
    if(!(file_exists('img_files/group_'.$group_id.'/'.$data_db->sketsa_lokasi_d))){
        $img_file_d     = base_url().'img_files/group_/'.$data_db->sketsa_lokasi_d;
    }
}

if(!empty($data_db->sketsa_lokasi_j)){
    $img_file_j         = base_url().'img_files/group_'.$group_id.'/'.$data_db->sketsa_lokasi_j;
    if(!(file_exists('img_files/group_'.$group_id.'/'.$data_db->sketsa_lokasi_j))){
        $img_file_j     = base_url().'img_files/group_/'.$data_db->sketsa_lokasi_j;
    }
}
?>


<div class="<?=!$this->session->userdata('r2d2')?'wrapper':''?>">

    <?php if(!$this->session->userdata('r2d2')){$this->load->view('inc/home_menu');} ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Setup
        <small>Yayasan</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Application</a></li>
        <li class="">Setup</li>
        <li class="active">Yayasan</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>
            </div>
            <?if(empty($exec_query)){?>
            <?if(!empty($delete)){?>
            <center>
                <h1>
                    <font color="red">
                        Data Ini Akan Dihapus? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>                        
                </h1>
            </center>
            <?}?>
            <form action="<?=$back_url?>action?lokasi=<?=$id?>" method="POST">
            <div class="form-horizontal box-body">

                <?if($id_data){?>
                <?//print_r($data_db);?>
                <center>
                    <h1>
                        <small>
                        Yayasan
                        </small>
                        <h5>
                            <small>
                                ID Data : <?=$id_data?>
                                <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id_data?>" >                                
                                <br/>
                                ID : <?=$id?$id:'auto'?>
                                <input type="hidden" name="inp_id" type="text" class="form-control" value="<?=$id?>" >                                
                                <br/>
                                <br/>
                            </small>
                        </h5>
                    </h1>
                </center>
                <?}?>


                <?if(empty($id)){?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Id Data Banding</label>

                    <div class="col-sm-10">
                        <label for="inputEmail3" class="control-label">
                        <font style="opacity: 0.25">
                            (auto)
                        </font>
                        </label>
                    </div>
                </div>
                <?}?>

                <div class="box-header with-border">
                    <center>
                        <h3>
                            Info Detail
                        </h3>
                    </center>
                </div>
                </br>
                <!--

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Foto Data</label>

                    <div class="col-sm-10">
                        <input name="inp_sketsa_lokasi" type="file" id="" placeholder="sketsa_lokasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>

                -->
                <div class="form-group" style="visibility:collapse">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Group</label>

                    <div class="col-sm-6">
                        <input name="inp_group_id" type="text" class="form-control" id="inp_group_id" placeholder="GroupID"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->group_id)?$data_db->group_id:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Nama</label>

                    <div class="col-sm-6">
                        <input name="inp_rangka" type="text" class="form-control" id="inputEmail3" placeholder="Nama Yayasan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->rangka)?$data_db->rangka:''?>">
                    </div>
                </div>
               <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tanggal Berdiri</label>

                    <div class="col-sm-3">

                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input name="inp_tgl" type="text" class="form-control datepicker" placeholder="yyyy-mm-dd"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tanggal)?$data_db->tanggal:''?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Alamat</label>

                    <div class="col-sm-6">
                        <input name="inp_dind" type="text" class="form-control" id="inputEmail3" placeholder="Alamat"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->dinding)?$data_db->dinding:''?>">
                    </div>
                </div>
				<div class="form-group">
                    <label for="inp_no_telp" class="col-sm-2 control-label">No Telepon/Fax</label>

                    <div class="col-sm-6">
                        <input name="inp_no_telp" type="text" class="form-control" id="inp_no_telp" placeholder="No Telepon/Fax"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_telepon)?$data_db->no_telepon:''?>">
                    </div>
                </div>
				
				
				<div class="box-header with-border">
                    <center>
                        <h3>
                            Pengurus
                        </h3>
                    </center>
                </div>
                </br>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label label-title">Ketua</label>

                    <div class="col-sm-6">
                        <input name="inp_lantai" type="text" class="form-control" id="inputEmail3" placeholder="Nama Ketua"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->lantai)?$data_db->lantai:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Sekretaris</label>

                    <div class="col-sm-6">
                        <input name="inp_atap" type="text" class="form-control" placeholder="Nama Sekretaris"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->atap)?$data_db->atap:''?>">
                    </div>
                </div>



                <div class="form-group">
                    <label for="inp_langit_langit" class="col-sm-2 control-label">Bendahara</label>

                    <div class="col-sm-6">
                        <input name="inp_langit_langit" type="text" class="form-control" id="inp_langit_langit" placeholder="Nama Bendahara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->langit_langit)?$data_db->langit_langit:''?>">
                    </div>
                </div>



                 <div class="box-header with-border">
                    <center>
                        <h3>
                            Dokumen Legal
                        </h3>
                    </center>
                </div>
                </br>
            
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Surat Ijin Operasional</label>

                    <div class="col-sm-10">
                        <span class="pull-left ">
                            <?php
                                $file_url =base_url().'img_files/group_'.(!empty($data_db->group_id)?$data_db->group_id:'').'/'.(!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:'pbd_'.$file_id.'_sketsa.jpg');
								//echo 
                                $file_url = $img_file;
                                //echo "file_url:".$file_url;exit;
                                //if($edit){
                                    $url          = $file_url;
									$response     = "";
									$file_exists  = 0;
									if($url <>"") {
                                    	$response     = get_headers($url, 1);
	                                    $file_exists  = (strpos($response[0], "404") === false);
									}
                                    if($file_exists){
                            ?>
                                <img src="<?=$file_url?>" style="width:320px">
                            <?php 
                                    }
                            
                                //}
                            ?>
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi" name="inp_sketsa_lokasi" type="hidden" placeholder="sketsa_lokasi Utara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:'pbd_'.$file_id.'_sketsa.jpg'?>">
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi_new" name="inp_sketsa_lokasi_new" type="hidden" value="<?='pbd_'.$file_id.'_sketsa.jpg'?>">
                        </span>
                        <span class="pull-left ">
                            <iframe id="in_f_upload" name="in_f_upload" src="<?=base_url()?>svl_lokasi/upload?file_id=<?='pbd_'.$file_id.'_sketsa.jpg'?>" frameborder="0" border="1" src="" scrolling="yes" scrollbar="yes" style="height: 320px;width: 360px" > </iframe> 
                        </span>
                    
                    </div>
                </div>

               <div class="box-header with-border">
               <center>
                    <h2>
                        Foto Lokasi
                    </h2>
                </center>
               </div>
                <br/>
                
            
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tampak Depan</label>

                    <div class="col-sm-10">
                        <span class="pull-left ">
                            <?php 
                                $file_url =base_url().'img_files/group_'.(!empty($data_db->group_id)?$data_db->group_id:'').'/'.(!empty($data_db->sketsa_lokasi_d)?$data_db->sketsa_lokasi_d:'pbd_'.$file_id.'_dpn.jpg');
                                $file_url = $img_file_d;
                                //echo "file_url:".$file_url;
                                //if($edit){
//                                    $url          = $file_url;
//                                    $response     = get_headers($url, 1);
//                                    $file_exists  = (strpos($response[0], "404") === false);
                                    $url          = $file_url;
									$response     = "";
									$file_exists  = 0;
									if($url <>"") {
                                    	$response     = get_headers($url, 1);
	                                    $file_exists  = (strpos($response[0], "404") === false);
									}

                                    if($file_exists){
                            ?>
                                <img src="<?=$file_url?>" style="width:320px">
                            <?php 
                                    }
                            
                                //}
                            ?>
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi_dpn" name="inp_sketsa_lokasi_dpn" type="hidden" placeholder="sketsa_lokasi Utara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi_d)?$data_db->sketsa_lokasi_d:'pbd_'.$file_id.'_dpn.jpg'?>">
                            <input id="inp_sketsa_lokasi_dpn_new" name="inp_sketsa_lokasi_dpn_new" type="hidden" value="<?='pbd_'.$file_id.'_dpn.jpg'?>">
                            <iframe id="in_f_upload" name="in_f_upload" src="<?=base_url()?>svl_lokasi/upload?file_id=<?='pbd_'.$file_id.'_dpn.jpg'?>" frameborder="0" border="1" src="" scrolling="yes" scrollbar="yes" style="height: 320px;width: 360px" > </iframe> 
                        </span>
                    
                    </div>
                </div>


                
            
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label label-title">Tampak Samping</label>

                    <div class="col-sm-10">
                        <span class="pull-left ">
                            <?php 
                                $file_url =base_url().'img_files/group_'.(!empty($data_db->group_id)?$data_db->group_id:'').'/'.(!empty($data_db->sketsa_lokasi_j)?$data_db->sketsa_lokasi_j:'pbd_'.$file_id.'_jln_dpn.jpg');
                                $file_url = $img_file_j;
                                //echo "file_url:".$file_url;
                                //if($edit){
//                                    $url          = $file_url;
//                                    $response     = get_headers($url, 1);
//                                    $file_exists  = (strpos($response[0], "404") === false);
                                    $url          = $file_url;
									$response     = "";
									$file_exists  = 0;
									if($url <>"") {
                                    	$response     = get_headers($url, 1);
	                                    $file_exists  = (strpos($response[0], "404") === false);
									}

                                    if($file_exists){
                            ?>
                                <img src="<?=$file_url?>" style="width:320px">
                            <?php 
                                    }
                            
                                //}
                            ?>
                        </span>
                        <span class="pull-left ">
                            <input id="inp_sketsa_lokasi_jln_dpn" name="inp_sketsa_lokasi_jln_dpn" type="hidden" placeholder="sketsa_lokasi Utara"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi_j)?$data_db->sketsa_lokasi_j:'pbd_'.$file_id.'_jln_dpn.jpg'?>">
                            <input id="inp_sketsa_lokasi_jln_dpn_new" name="inp_sketsa_lokasi_jln_dpn_new" type="hidden" value="<?='pbd_'.$file_id.'_jln_dpn.jpg'?>">
                            <iframe id="in_f_upload" name="in_f_upload" src="<?=base_url()?>svl_lokasi/upload?file_id=<?='pbd_'.$file_id.'_jln_dpn.jpg'?>" frameborder="0" border="1" src="" scrolling="yes" scrollbar="yes" style="height: 320px;width: 360px" > </iframe> 
                        </span>
                    
                    </div>
                </div>




                



 

                <div class="box-footer">
                    <a href="<?=$back_url?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <? if($id){?>
                    <? if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <? } ?>
                    <? if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <? }?>
                    <? }else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <? }?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>

          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>edit/<?=$id_data?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>edit/<?=$id_data?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>edit/<?=$id_data?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>edit/<?=$id_data?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add?lokasi=<?=$id_data?>" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>edit/<?=$id_data?>?lokasi=<?=$id_data?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>

<!-- //20171230 -->
<!-- bootstrap datepicker -->
<script src="<?=base_url()?>vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?=base_url()?>plugins/iCheck/icheck.min.js"></script>

<!-- page script -->
<script type="text/javascript">
    $("#group_id").change(function() {
        //alert("test");
        in_f_upload.document.getElementById("group_id").value = $("#group_id").val();
    });    
</script>

<script>
    //20171230
    //Date picker
    $('.datepicker').datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

</script>
<?php $this->load->view('inc/home_footer_body'); ?>
