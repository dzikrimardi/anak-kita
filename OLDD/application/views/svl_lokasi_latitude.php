<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('inc/home_header_head'); ?>

<?php $this->load->view('inc/home_header_css'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?=base_url()?>vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">

<?php $this->load->view('inc/home_header_meta_title'); ?>
<?php $this->load->view('inc/home_header_body'); ?>

<?
$id         = $this->uri->segment(3);
$edit       = '';
$delete     = $this->uri->segment(2) == 'delete'?'true':'';

if($this->uri->segment(2) == 'add' || $this->uri->segment(2) == 'edit' ){
    $edit   = 'true';
}

$back_url   = base_url()."svl_lokasi/";
?>


<div class="wrapper">

    <?php $this->load->view('inc/home_menu'); ?>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tambah
        <small>Survey</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-user"></i> Survey Lapangan</a></li>
        <li><a href="#"><i class="fa fa-user"></i> Lokasi</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- Input addon -->
          <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Lokasi</h3>
            </div>
            <?if(empty($exec_query)){?>
            <form action="<?=$back_url?>action" method="POST">
            <div class="form-horizontal box-body">
                <!--//edit-->
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Latitude</label>

                    <div class="col-sm-10">
                        <div id="view_lat">
                            
                        </div>
                        <input id="inp_view_lat" name="inp_view_lat" type="text" class="form-control" placeholder="Latitude" value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>

                <!--edit-->
                <?if($id){?>
                <?//print_r($data_db);?>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Id Data</label>

                    <div class="col-sm-3">
                        <label class="col-sm-2 control-label row"><?=$id?></label>
                        <input type="hidden" name="inp_id_data" type="text" class="form-control" value="<?=$id?>" >
                    </div>
                </div>
                <?}?>
                <!--//edit-->
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Kode Tapak</label>

                    <div class="col-sm-10">
                        <input name="inp_kode_tapak" type="text" class="form-control" placeholder="Kode Tapak"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kode_tapak)?$data_db->kode_tapak:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">group_id</label>

                    <div class="col-sm-10">
                        <input name="inp_group_id" type="text" class="form-control" placeholder="group_id"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->group_id)?$data_db->group_id:''?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Longitude</label>

                    <div class="col-sm-10">
                        <input name="inp_longitude" type="text" class="form-control" placeholder="Longitude"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->longitude)?$data_db->longitude:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Latitude</label>

                    <div class="col-sm-10">
                        <input name="inp_latitude" type="text" class="form-control" placeholder="Latitude"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->latitude)?$data_db->latitude:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Tanggal Survey</label>

                    <div class="col-sm-10">
                        <input name="inp_tgl_survey" type="text" class="form-control" placeholder="Tanggal Survey"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->tgl_survey)?$data_db->tgl_survey:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Pic</label>

                    <div class="col-sm-10">
                        <input name="inp_pic" type="text" class="form-control" id="inputEmail3" placeholder="Pic"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->pic)?$data_db->pic:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">nama_bumn</label>

                    <div class="col-sm-10">
                        <input name="inp_nama_bumn" type="text" class="form-control" id="inputEmail3" placeholder="nama_bumn"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_bumn)?$data_db->nama_bumn:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">no_tgl_penetapan</label>

                    <div class="col-sm-10">
                        <input name="inp_no_tgl_penetapan" type="text" class="form-control" id="inputEmail3" placeholder="no_tgl_penetapan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->no_tgl_penetapan)?$data_db->no_tgl_penetapan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">nama_asset</label>

                    <div class="col-sm-10">
                        <input name="inp_nama_asset" type="text" class="form-control" id="inputEmail3" placeholder="nama_asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_asset)?$data_db->nama_asset:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">nomor_asset</label>

                    <div class="col-sm-10">
                        <input name="inp_nomor_asset" type="text" class="form-control" id="inputEmail3" placeholder="nomor_asset"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nomor_asset)?$data_db->nomor_asset:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">banjir</label>

                    <div class="col-sm-10">
                        <input name="inp_banjir" type="text" class="form-control" id="inputEmail3" placeholder="banjir"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->banjir)?$data_db->banjir:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">jarak_tapak_1</label>

                    <div class="col-sm-10">
                        <input name="inp_jarak_tapak_1" type="text" class="form-control" id="inputEmail3" placeholder="jarak_tapak_1"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak_1)?$data_db->jarak_tapak_1:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">jarak_tapak_2</label>

                    <div class="col-sm-10">
                        <input name="inp_jarak_tapak_2" type="text" class="form-control" id="inputEmail3" placeholder="jarak_tapak_2"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_tapak_2)?$data_db->jarak_tapak_2:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">kemudahan</label>

                    <div class="col-sm-10">
                        <input name="inp_kemudahan" type="text" class="form-control" id="inputEmail3" placeholder="kemudahan"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->kemudahan)?$data_db->kemudahan:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">transportasi_umum</label>

                    <div class="col-sm-10">
                        <input name="inp_transportasi_umum" type="text" class="form-control" id="inputEmail3" placeholder="transportasi_umum"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transportasi_umum)?$data_db->transportasi_umum:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">jarak_ke_terminal</label>

                    <div class="col-sm-10">
                        <input name="inp_jarak_ke_terminal" type="text" class="form-control" id="inputEmail3" placeholder="jarak_ke_terminal"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->jarak_ke_terminal)?$data_db->jarak_ke_terminal:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">nama_terminal</label>

                    <div class="col-sm-10">
                        <input name="inp_nama_terminal" type="text" class="form-control" id="inputEmail3" placeholder="nama_terminal"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->nama_terminal)?$data_db->nama_terminal:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">transport_01</label>

                    <div class="col-sm-10">
                        <input name="inp_transport_01" type="text" class="form-control" id="inputEmail3" placeholder="transport_01"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transport_01)?$data_db->transport_01:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">banjir</label>

                    <div class="col-sm-10">
                        <input name="inp_transport_02" type="text" class="form-control" id="inputEmail3" placeholder="transport_02"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->transport_02)?$data_db->transport_02:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">banjir</label>

                    <div class="col-sm-10">
                        <input name="inp_" type="text" class="form-control" id="inputEmail3" placeholder="banjir"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->banjir)?$data_db->banjir:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">alternatif1</label>

                    <div class="col-sm-10">
                        <input name="inp_alternatif1" type="text" class="form-control" id="inputEmail3" placeholder="alternatif1"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif1)?$data_db->alternatif1:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">alternatif2</label>

                    <div class="col-sm-10">
                        <input name="inp_alternatif2" type="text" class="form-control" id="inputEmail3" placeholder="alternatif2"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif2)?$data_db->alternatif2:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">alternatif3</label>

                    <div class="col-sm-10">
                        <input name="inp_alternatif3" type="text" class="form-control" id="inputEmail3" placeholder="alternatif3"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->alternatif3)?$data_db->alternatif3:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">sketsa_lokasi</label>

                    <div class="col-sm-10">
                        <input name="inp_sketsa_lokasi" type="text" class="form-control" id="inputEmail3" placeholder="sketsa_lokasi"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->sketsa_lokasi)?$data_db->sketsa_lokasi:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">approval</label>

                    <div class="col-sm-10">
                        <input name="inp_approval" type="text" class="form-control" id="inputEmail3" placeholder="approval"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->approval)?$data_db->approval:''?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">respond</label>

                    <div class="col-sm-10">
                        <input name="inp_respond" type="text" class="form-control" id="inputEmail3" placeholder="respond"  <?=empty($edit)?'disabled="disabled"':'';?> value="<?=!empty($data_db->respond)?$data_db->respond:''?>">
                    </div>
                </div>


                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">back</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                <!--edit-->
                    <?if($id){?>
                    <?if(!empty($edit)){?>
                    <button type="submit" class="btn btn-warning btn-lg pull-right">Update</button>
                    <?}?>
                    <?if(!empty($delete)){?>
                    <input type="hidden" name="delete" value="true">
                    <button type="submit" class="btn btn-danger btn-lg pull-right">HAPUS</button>
                    <font color="red" class="pull-right">
                        Apakah Anda Yakin Ingin Menghapus Data Ini? &nbsp;&nbsp;&nbsp;&nbsp;
                    </font>
                    <?}?>
                    <?}else{?>
                    <button type="submit" class="btn btn-info btn-lg pull-right">Save</button>
                    <?}?>
                </div>
            <!-- /.box-body -->
            </div>
            </form>
          <!-- /.box -->
          <?}else if($exec_query == 'ok'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="green">
                            Simpan Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'update'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="orange">
                            Ubah Berhasil!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'delete'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Data Telah Dihapus!
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                </div>                
            </div>
          <?}else if($exec_query == 'error_pass_c'){?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Gagal, Password tidak sama
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>
          <?}else{?>
            <div class="form-horizontal box-body">
                <center>
                    <h1>
                        <font color="red">
                            Error
                        </font>
                    </h1>
                </center>
                <div class="box-footer">
                    <a href="<?=$back_url?>" class="btn btn-default pull-right">Kembali</a>
                    <span class=" pull-right">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                    <a href="<?=$back_url?>add" class="btn btn-info pull-right">Input Ulang</a>
                </div>                
            </div>          
            <?}?>
        </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

</div>

<?php $this->load->view('inc/home_footer_js'); ?>
<!-- DataTables -->
<script src="<?=base_url()?>vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- page script -->

<script>
  $(function () {
    $('#data_users').DataTable()
    /*
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
    */
  })
    <?if($this->input->get('r2d2')){?>
        var x = document.getElementById("r2d2-page").contentWindow.document.getElementById("view_lat");
        //var x = document.getElementById("view_lat");
    <?}else{?>
        var x = document.getElementById("view_lat");
    <?}?>
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }
    function showPosition(position) {
        x.innerHTML = "Latitude: " + position.coords.latitude +
        "<br>Longitude: " + position.coords.longitude;
    }
    $( document ).ready(function() {
        //alert('cek lokasi');
        getLocation();
    });
</script>
<?php $this->load->view('inc/home_footer_body'); ?>
