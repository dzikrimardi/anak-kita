<?php



defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_pembanding extends CI_Controller {



	public function index(){
		$sql_where 	= "";
		$sql_and 	= "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}
		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$lokasi' $sql_and";

	        $data_db 			= $this->db->query($sql);

	        $data['data_db_r'] 	= $data_db->row(); 
			$sql 				= "	SELECT *,(
											SELECT sketsa_lokasi 
											from survey13_ft_pembanding 
											where survey13_ft_pembanding.id_data_banding = survey08_pembanding.id_data_banding 
											order by 'id_foto_pembanding' desc limit 1
										) as sketsa_lokasi,(
											SELECT tampak_depan 
											from survey13_ft_pembanding 
											where survey13_ft_pembanding.id_data_banding = survey08_pembanding.id_data_banding 
											order by 'id_foto_pembanding' desc limit 1
										) as sketsa_lokasi_d,(
											SELECT tampak_jalan_depan 
											from survey13_ft_pembanding 
											where survey13_ft_pembanding.id_data_banding = survey08_pembanding.id_data_banding 
											order by 'id_foto_pembanding' desc limit 1
										) as sketsa_lokasi_j 
									FROM 
										`survey08_pembanding` 
									WHERE 
										id_data = '$lokasi' $sql_and";
		}else{
			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}
		//echo $sql ;exit;

        $data_db 			= $this->db->query($sql);

        $data['data_db'] 	= $data_db; 
		$this->load->view('svl_pembanding',$data);

	}



	function db_master(){
		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}
		$sql 					= "SELECT * FROM `groups` $sql_where_g";

        $data_db 				= $this->db->query($sql);

        $data['db_ms_groups'] 	= $data_db; 
		$sql 					= "SELECT * FROM `users` $sql_where_u";

        $data_db 				= $this->db->query($sql);

        $data['db_ms_users'] 	= $data_db; 
		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}
		$id 				= $this->uri->segment(3); 
		$sql 				= "	SELECT *,(
											SELECT sketsa_lokasi 
											from survey13_ft_pembanding 
											where survey13_ft_pembanding.id_data_banding = survey08_pembanding.id_data_banding 
											order by 'id_foto_pembanding' desc limit 1
										) as sketsa_lokasi,(
											SELECT tampak_depan 
											from survey13_ft_pembanding 
											where survey13_ft_pembanding.id_data_banding = survey08_pembanding.id_data_banding 
											order by 'id_foto_pembanding' desc limit 1
										) as sketsa_lokasi_d,(
											SELECT tampak_jalan_depan 
											from survey13_ft_pembanding 
											where survey13_ft_pembanding.id_data_banding = survey08_pembanding.id_data_banding 
											order by 'id_foto_pembanding' desc limit 1
										) as sketsa_lokasi_j
								FROM 
									`survey08_pembanding` 
								WHERE 
								id_data_banding = '$id' $sql_and";

        $data_db 			= $this->db->query($sql)->row();

        $data['data_db'] 	= $data_db; 
		return $data;

	}



	function add(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Tambah Pembanding';
		$this->load->view('svl_pembanding_add',$data);

	}



	function view(){
		$data = $this->db_master();
		$data['title'] = 'Survey - View Pembanding';
		$this->load->view('svl_pembanding_add',$data);

	}



	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Setup Yayasan';
		$this->load->view('svl_pembanding_add',$data);

	}



	function delete(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Hapus Pembanding';
		$this->load->view('svl_pembanding_add',$data);

	}



	function action(){
		$data['title'] = 'Survey - Pembanding';
		if($this->input->post('inp_id')){
			$id_child 	= $this->input->post('inp_id');
		}else{
			$id_child 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}
		$id 			= $id_child; 
		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey08_pembanding` WHERE id_data_banding = '$id'";

	        if($this->db->query($sql)){

	        	$data['exec_query'] = 'delete';

	        }else{

	        	$data['exec_query'] = 'failed';



	        }
		}
		if($this->input->post() and !$this->input->post('delete')){
		
		       $sql	 = "SELECT * FROM v_maks_group";

        		$db_group  			= $this->db->query($sql)->row();
        		$maks_group_id		= $db_group->max_group;
            	//$id         = $db_user->id;
			
				//$inp_id_data 		= $this->input->post('inp_id_data'); 
				$inp_id_data_banding= $id;  
				$inp_id_data		= $id;  
				$inp_user_id 	 	= $this->input->post('inp_user_id'); 
				$inp_group_id0 	 	= $this->input->post('inp_group_id');
				//$inp_group_id0 	 	= $this->session->userdata('group_id');
				//echo "$inp_group_id0 = " . $inp_group_id0 . ", $maks_group_id = " . $maks_group_id;
				//exit;
				// kalau data baru berarti $inp_group_id0 = $maks_group_id
				if($inp_group_id0 < $maks_group_id)
				{
					$inp_group_id =$inp_group_id0;
				} else
				{
					$inp_group_id		= $maks_group_id; 
				}
				$inp_latitude 		= $this->input->post('inp_latitude'); 
				$inp_longitude 		= $this->input->post('inp_longitude'); 
				$inp_foto_data 		= $this->input->post('inp_foto_data'); 
				$inp_harga_pen 		= $this->input->post('inp_harga_pen'); 
				$inp_harga_trans 	= $this->input->post('inp_harga_trans'); 
				$inp_tgl			= $this->input->post('inp_tgl'); 
				$inp_sum_data 		= $this->input->post('inp_sum_data'); 
				$inp_no_telp 		= $this->input->post('inp_no_telp'); 
				$inp_perunt			= $this->input->post('inp_perunt'); 
				$inp_lebar_jln_01 	= $this->input->post('inp_lebar_jln_01'); 
				$inp_perker  		= $this->input->post('inp_perker'); 
				$inp_kond_jal 		= $this->input->post('inp_kond_jal'); 
				$inp_ang_um 		= $this->input->post('inp_ang_um'); 
				$inp_luas_tan 	    = $this->input->post('inp_luas_tan'); 
				$inp_sert 			= $this->input->post('inp_sert'); 
				$inp_bent 			= $this->input->post('inp_bent');
				$inp_elv 			= $this->input->post('inp_elv'); 
				$inp_elv 			= str_replace(",",".",$inp_elv);
				$inp_lebar_jln_02 	= $this->input->post('inp_lebar_jln_02');
				$inp_transport_01 	= $this->input->post('inp_transport_01'); 
				$inp_sudut 			= $this->input->post('inp_sudut');
				$inp_harg_m2 		= $this->input->post('inp_harg_m2'); 
				$inp_luas_m2 		= $this->input->post('inp_luas_m2');
				$inp_dbangun_thn 	= $this->input->post('inp_dbangun_thn'); 
				$inp_penggu 		= $this->input->post('inp_penggu');
				$inp_rangka 		= $this->input->post('inp_rangka'); 
				$inp_juml_lantai 	= $this->input->post('inp_juml_lantai');
				$inp_lantai 		= $this->input->post('inp_lantai'); 
				$inp_dind 			= $this->input->post('inp_dind');
				$inp_atap 			= $this->input->post('inp_atap'); 
				$inp_langit_langit 	= $this->input->post('inp_langit_langit');
				$inp_kondisi 		= $this->input->post('inp_kondisi'); 
				$inp_harga_m2_bang 	= $this->input->post('inp_harga_m2_bang');
				$inp_sketsa 		= $this->input->post('inp_sketsa'); 
				$inp_approval 		= $this->input->post('inp_approval');
				$inp_respond 		= $this->input->post('inp_respond'); 


				$sql	= "	REPLACE INTO 
								survey08_pembanding (
												`id_data_banding`, 
												`id_data`, 
												`user_id`, 
												`group_id`, 
												`latitude`, 
												`longitude`, 
												`foto_data`, 
												`harga_penawaran`, 
												`harga_transaksi`, 
												`tanggal`, 
												`sumber_data`, 
												`no_telepon`, 
												`peruntukan`, 
												`lebar_jalan_01`, 
												`perkerasan`, 
												`kondisi_jalan`, 
												`angkutan_umum`, 
												`luas_tanah`, 
												`sertifikat`, 
												`bentuk`, 
												`elevasi`, 
												`lebar_jalan_02`, 
												`sudut`, 
												`tusuk_sate`, 
												`harga_m2`, 
												`luas_m2`, 
												`dibangun_tahun`, 
												`penggunaan`, 
												`rangka`, 
												`jumlah_lantai`, 
												`lantai`, 
												`dinding`, 
												`atap`, 
												`langit_langit`, 
												`kondisi`, 
												`harga_m2_bangunan`, 
												`sketsa`, 
												`approval`, 
												`respond`
								) VALUES (
												'$inp_id_data_banding', 
												'$inp_id_data', 
												'$inp_user_id', 
												'$inp_group_id', 
												'$inp_latitude', 
												'$inp_longitude', 
												'$inp_foto_data', 
												'$inp_harga_pen', 
												'$inp_harga_trans', 
												'$inp_tgl', 
												'$inp_sum_data', 
												'$inp_no_telp', 
												'$inp_perunt', 
												'$inp_lebar_jln_01', 
												'$inp_perker', 
												'$inp_kond_jal', 
												'$inp_ang_um', 
												'$inp_luas_tan', 
												'$inp_sert', 
												'$inp_bent',
												'$inp_elv',
												'$inp_lebar_jln_02',
												'$inp_transport_01',
												'$inp_sudut',
												'$inp_harg_m2',
												'$inp_luas_m2',
												'$inp_dbangun_thn',
												'$inp_penggu',
												'$inp_rangka',
												'$inp_juml_lantai',
												'$inp_lantai',
												'$inp_dind',
												'$inp_atap',
												'$inp_langit_langit',
												'$inp_kondisi',
												'$inp_harga_m2_bang',
												'$inp_sketsa',
												'$inp_approval',
												'$inp_respond'
									);
					";


					/*
					$sql	= "
								INSERT INTO 
									`survey08_pembanding` (
										`id_data_banding`, 
										`id_data`, 
										`id_asset_internal`, 
										`user_id`, 
										`group_id`, 
										`latitude`, 
										`longitude`, 
										`jarak_obyek`, 
										`foto_data`, 
										`harga_penawaran`, 
										`harga_transaksi`, 
										`tanggal`, 
										`sumber_data`, 
										`no_telepon`, 
										`peruntukan`, 
										`lebar_jalan_01`, 
										`perkerasan`, 
										`kondisi_jalan`, 
										`angkutan_umum`, 
										`luas_tanah`, 
										`sertifikat`, 
										`bentuk`, 
										`elevasi`, 
										`lebar_jalan_02`, 
										`sudut`, 
										`tusuk_sate`, 
										`harga_m2`, 
										`luas_m2`, 
										`dibangun_tahun`, 
										`penggunaan`, 
										`rangka`, 
										`jumlah_lantai`, 
										`lantai`, 
										`dinding`, 
										`atap`, 
										`langit_langit`, 
										`kondisi`, 
										`harga_m2_bangunan`, 
										`sketsa`, 
										`approval`, 
										`respond`
									) VALUES (
										`id_data_banding`, 
										`id_data`, 
										`id_asset_internal`, 
										`user_id`, 
										`group_id`, 
										`latitude`, 
										`longitude`, 
										`jarak_obyek`, 
										`foto_data`, 
										`harga_penawaran`, 
										`harga_transaksi`, 
										`tanggal`, 
										`sumber_data`, 
										`no_telepon`, 
										`peruntukan`, 
										`lebar_jalan_01`, 
										`perkerasan`, 
										`kondisi_jalan`, 
										`angkutan_umum`, 
										`luas_tanah`, 
										`sertifikat`, 
										`bentuk`, 
										`elevasi`, 
										`lebar_jalan_02`, 
										`sudut`, 
										`tusuk_sate`, 
										`harga_m2`, 
										`luas_m2`, 
										`dibangun_tahun`, 
										`penggunaan`, 
										`rangka`, 
										`jumlah_lantai`, 
										`lantai`, 
										`dinding`, 
										`atap`, 
										`langit_langit`, 
										`kondisi`, 
										`harga_m2_bangunan`, 
										`sketsa`, 
										`approval`, 
										`respond`
										);	
								";	
						*/			
			        if($this->db->query($sql)){
						if($id){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }







                    $file_url 			= base_url().'img_files/group_'.$inp_group_id.'/'.$this->input->post('inp_sketsa_lokasi_new');

					if(!(file_exists($file_url))){
							$file_url       = base_url().'img_files/group_/'.$this->input->post('inp_sketsa_lokasi_new');
						}


//					echo $file_url;
//					exit;
                    $url          		= $file_url;

                    $response     		= get_headers($url, 1);

                    $file_exists  		= (strpos($response[0], "404") === false);
					$sketsa_lokasi		= $file_exists?$this->input->post('inp_sketsa_lokasi_new'):$this->input->post('inp_sketsa_lokasi');






                    $file_url 			= base_url().'img_files/group_'.$inp_group_id.'/'.$this->input->post('inp_sketsa_lokasi_dpn_new');
					if(!(file_exists($file_url))){
							$file_url       = base_url().'img_files/group_/'.$this->input->post('inp_sketsa_lokasi_dpn_new');
						}

                    $url          		= $file_url;

                    $response     		= get_headers($url, 1);

                    $file_exists  		= (strpos($response[0], "404") === false);
					$sketsa_lokasi_d	= $file_exists?$this->input->post('inp_sketsa_lokasi_dpn_new'):$this->input->post('inp_sketsa_lokasi_dpn');







                    $file_url 			= base_url().'img_files/group_'.$inp_group_id.'/'.$this->input->post('inp_sketsa_lokasi_jln_dpn_new');
					if(!(file_exists($file_url))){
							$file_url       = base_url().'img_files/group_/'.$this->input->post('inp_sketsa_lokasi_jln_dpn_new');
						}

                    $url          		= $file_url;

                    $response     		= get_headers($url, 1);

                    $file_exists  		= (strpos($response[0], "404") === false);
					$sketsa_lokasi_j	= $file_exists?$this->input->post('inp_sketsa_lokasi_jln_dpn_new'):$this->input->post('inp_sketsa_lokasi_jln_dpn');
		        	$sql_ft	= "	REPLACE INTO 
								survey13_ft_pembanding (
										`id_foto_pembanding`,
										`group_id`,
										`id_data_banding`, 
										`tampak_depan`,
										`tampak_jalan_depan`,
										`sketsa_lokasi`
								) VALUES (
										IFNULL((SELECT MAX(tb13.id_foto_pembanding) AS id_foto_pembanding  FROM survey13_ft_pembanding AS tb13 where tb13.id_data_banding = '$inp_id_data_banding'),null),
										'$inp_group_id', 
										'$inp_id_data_banding', 
										'$sketsa_lokasi_d',
										'$sketsa_lokasi_j',
										'$sketsa_lokasi'
								);
					";

					//echo $sql_ft;
					//exit;
					$this->db->query($sql_ft);




			        }else{
			        	$data['exec_query'] = 'failed';
			        }
			}
		$this->load->view('svl_pembanding_add',$data);



	}

}

