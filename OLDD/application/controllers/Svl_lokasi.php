<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_lokasi extends CI_Controller {

	public function index(){

		$data['title'] = 'Data - Anak Asuh';

		$sql_where = "";
 		//if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		//}

		$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where ";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 
		//echo $sql;
		//exit;

 		$this->load->view('svl_lokasi',$data);
	}

	function db_master(){

		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}

		$sql 					= "SELECT `groups`.`id`,
										`survey08_pembanding`.`group_id`, 
										`survey08_pembanding`.`rangka` AS `description` 
										FROM `survey08_pembanding` 
										INNER JOIN `groups` ON `survey08_pembanding`.`group_id` = `groups`.`id`
										$sql_where_g ";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db; 

		//echo $sql;
		//exit;

		$sql 					= "SELECT * FROM `users` $sql_where_u ";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db; 

		return $data;
	}

	function upload(){
		$data = $this->db_master();

		$data['title'] = 'Survey - Upload';

		$this->load->view('svl_lokasi_upload',$data);
	}

	function upload_frame(){
		$data = $this->db_master();

		$data['title'] = 'Survey - Upload';

		$this->load->view('svl_lokasi_upload_frame',$data);
	}

	function add(){
		$data = $this->db_master();

		$data['title'] = 'Survey - Tambah Lokasi';

		$this->load->view('svl_lokasi_add',$data);
	}

	function view(){
		$data = $this->db_master();
		$data['title'] = 'Survey - View Lokasi';

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$sql 				= "SELECT * FROM `survey01_lokasi` $sql_and ";


		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_lokasi_add',$data);
	}

	function lat(){
		$data['title'] = 'Survey - Latitude Lokasi';

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id'";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_lokasi_latitude',$data);
	}

	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Edit Lokasi';

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')" ;
		}

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT *, 
								(select batas_utara from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_u,
								(select batas_selatan from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_s,
								(select batas_timur from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_t,
								(select batas_barat from survey11_ft_batas_tapak where survey11_ft_batas_tapak.id_data = survey01_lokasi.id_data order by 'id_foto_batas_tapak' desc limit 1) as sketsa_lokasi_b 
		 FROM `survey01_lokasi` WHERE id_data = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_lokasi_add',$data);
	}

	function delete(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Hapus Lokasi';

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')" ;
		}

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_lokasi_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Lokasi';

		if($this->input->post('inp_id_data')){
			$id 	= $this->input->post('inp_id_data');
		}else{
			$id 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey01_lokasi` WHERE id_data = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){

			$id_data 			= $id; 
			$group_id 			= $this->input->post('inp_group_id'); 
			$kode_tapak 		= $this->input->post('inp_kode_tapak'); 
			$longitude 			= $this->input->post('inp_longitude'); 
			$latitude 			= $this->input->post('inp_latitude'); 
			$alamat 			= $this->input->post('inp_alamat'); 
			$tgl_survey 		= $this->input->post('inp_tgl_survey'); 
			$pic 				= $this->input->post('inp_pic'); 
			$nama_bumn 			= $this->input->post('inp_nama_bumn'); 
			$no_tgl_penetapan 	= $this->input->post('inp_no_tgl_penetapan'); 
			$nama_asset 		= $this->input->post('inp_nama_asset'); 
			$nomor_asset 		= $this->input->post('inp_nomor_asset'); 
			$banjir				= $this->input->post('inp_banjir'); 
			$jarak_tapak_1 		= $this->input->post('inp_jarak_tapak_1'); 
			$jarak_tapak_2 		= $this->input->post('inp_jarak_tapak_2'); 
			$kemudahan 			= $this->input->post('inp_kemudahan'); 
			$transportasi_umum 	= $this->input->post('inp_transportasi_umum'); 
			$jarak_ke_terminal 	= $this->input->post('inp_jarak_ke_terminal'); 
			$nama_terminal 		= $this->input->post('inp_nama_terminal'); 
			$transport_01 		= $this->input->post('inp_transport_01'); 
			$transport_02 		= $this->input->post('inp_transport_02'); 
			$alternatif1 		= $this->input->post('inp_alternatif1'); 
			$alternatif2 		= $this->input->post('inp_alternatif2'); 
			$alternatif3 		= $this->input->post('inp_alternatif3'); 
			$sketsa_lokasi 		= $this->input->post('inp_sketsa_lokasi'); 
			$approval 			= $this->input->post('inp_approval'); 
			$respond 			= $this->input->post('inp_respond');
			$status 			= $this->input->post('inp_status');


            $file_url 			= base_url().'img_files/group_'.$group_id.'/'.$this->input->post('inp_sketsa_lokasi_new');
            $url          		= $file_url;
            $response     		= get_headers($url, 1);
            $file_exists  		= (strpos($response[0], "404") === false);

			//$sketsa_lokasi		= $file_exists?$this->input->post('inp_sketsa_lokasi_new'):$this->input->post('inp_sketsa_lokasi');
			if($this->input->post('inp_id_data')){
				$id_data 			= $this->input->post('inp_id_data'); 
				$sql	= 	"	UPDATE `survey01_lokasi` 
								SET 
									`group_id` 			= '$group_id', 
									`kode_tapak` 		= '$kode_tapak', 
									`longitude` 		= '$longitude', 
									`latitude` 			= '$latitude', 
									`alamat` 			= '$alamat', 
									`tgl_survey` 		= '$tgl_survey', 
									`pic` 				= '$pic', 
									`nama_bumn` 		= '$nama_bumn', 
									`no_tgl_penetapan` 	= '$no_tgl_penetapan', 
									`nama_asset` 		= '$nama_asset', 
									`nomor_asset` 		= '$nomor_asset', 
									`banjir` 			= '$banjir', 
									`jarak_tapak_1` 	= '$jarak_tapak_1', 
									`jarak_tapak_2` 	= '$jarak_tapak_2', 
									`kemudahan` 		= '$kemudahan', 
									`transportasi_umum` = '$transportasi_umum', 
									`jarak_ke_terminal` = '$jarak_ke_terminal', 
									`nama_terminal` 	= '$nama_terminal', 
									`transport_01` 		= '$transport_01', 
									`transport_02` 		= '$transport_02', 
									`alternatif1` 		= '$alternatif1', 
									`alternatif2` 		= '$alternatif2', 
									`alternatif3` 		= '$alternatif3', 
									`sketsa_lokasi` 	= '$sketsa_lokasi', 
									`approval` 			= '$approval', 
									`respond` 			= '$respond',
									`status` 			= '$status'
								WHERE 
									 `id_data` = '$id_data'
							";	
			}else{
				//echo $id_data . " REPLACE ";
				//exit;

				$sql	= "	REPLACE INTO 
								survey01_lokasi (
												`id_data`, 
												`group_id`, 
												`kode_tapak`, 
												`longitude`, 
												`latitude`, 
												`alamat`,
												`tgl_survey`, 
												`pic`, 
												`nama_bumn`, 
												`no_tgl_penetapan`, 
												`nama_asset`, 
												`nomor_asset`, 
												`banjir`, 
												`jarak_tapak_1`, 
												`jarak_tapak_2`, 
												`kemudahan`, 
												`transportasi_umum`, 
												`jarak_ke_terminal`, 
												`nama_terminal`, 
												`transport_01`, 
												`transport_02`, 
												`alternatif1`, 
												`alternatif2`, 
												`alternatif3`, 
												`sketsa_lokasi`, 
												`approval`, 
												`respond`,
												`status`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$kode_tapak', 
												'$longitude', 
												'$latitude', 
												'$alamat',
												'$tgl_survey', 
												'$pic', 
												'$nama_bumn', 
												'$no_tgl_penetapan', 
												'$nama_asset', 
												'$nomor_asset', 
												'$banjir', 
												'$jarak_tapak_1', 
												'$jarak_tapak_2', 
												'$kemudahan', 
												'$transportasi_umum', 
												'$jarak_ke_terminal', 
												'$nama_terminal', 
												'$transport_01', 
												'$transport_02', 
												'$alternatif1', 
												'$alternatif2', 
												'$alternatif3', 
												'$sketsa_lokasi', 
												'$approval', 
												'$respond',
												'$status'
									);
					";

			}
			
		        if($this->db->query($sql)){
					


					if($this->input->post('inp_id_data')){
			        	$data['exec_query'] = 'update';
			        }else{
			        	$data['exec_query'] = 'ok';
			        }

                    $file_url 			= base_url().'img_files/group_'.$group_id.'/'.$this->input->post('inp_sketsa_lokasi_new_u');
                    $url          		= $file_url;
                    $response     		= get_headers($url, 1);
                    $file_exists  		= (strpos($response[0], "404") === false);

					$sketsa_lokasi_u 	= $file_exists?$this->input->post('inp_sketsa_lokasi_new_u'):$this->input->post('inp_sketsa_lokasi_u'); 



                    $file_url 			= base_url().'img_files/group_'.$group_id.'/'.$this->input->post('inp_sketsa_lokasi_new_s');
                    $url          		= $file_url;
                    $response     		= get_headers($url, 1);
                    $file_exists  		= (strpos($response[0], "404") === false);

					$sketsa_lokasi_s	= $file_exists?$this->input->post('inp_sketsa_lokasi_new_s'):$this->input->post('inp_sketsa_lokasi_s'); 



                    $file_url 			= base_url().'img_files/group_'.$group_id.'/'.$this->input->post('inp_sketsa_lokasi_new_t');
                    $url          		= $file_url;
                    $response     		= get_headers($url, 1);
                    $file_exists  		= (strpos($response[0], "404") === false);

					$sketsa_lokasi_t	= $file_exists?$this->input->post('inp_sketsa_lokasi_new_t'):$this->input->post('inp_sketsa_lokasi_t'); 



                    $file_url 			= base_url().'img_files/group_'.$group_id.'/'.$this->input->post('inp_sketsa_lokasi_new_b');
                    $url          		= $file_url;
                    $response     		= get_headers($url, 1);
                    $file_exists  		= (strpos($response[0], "404") === false);

					$sketsa_lokasi_b	= $file_exists?$this->input->post('inp_sketsa_lokasi_new_b'):$this->input->post('inp_sketsa_lokasi_b'); 

		        	$sql_batas	= "	REPLACE INTO 
							survey11_ft_batas_tapak (
											`id_foto_batas_tapak`,
											`id_data`, 
											`batas_utara`, 
											`batas_selatan`, 
											`batas_timur`,  
											`batas_barat`
							) VALUES (
											IFNULL((SELECT tb11.id_foto_batas_tapak FROM survey11_ft_batas_tapak AS tb11 where tb11.id_data = '$id_data'),null),
											'$id_data', 
											'$sketsa_lokasi_u', 
											'$sketsa_lokasi_s', 
											'$sketsa_lokasi_t', 
											'$sketsa_lokasi_b'
								);
					";
					$this->db->query($sql_batas);


		        	$sql_batas	= "	REPLACE INTO 
									survey12_ft_sketsa_tapak (
											`id_foto_sketsa_tapak`,
											`group_id`,
											`id_data`, 
											`lokasi_sketsa`
									) VALUES (
											IFNULL((SELECT tb12.id_foto_sketsa_tapak FROM survey12_ft_sketsa_tapak AS tb12 where tb12.id_data = '$id_data'),null),
											'$group_id', 
											'$id_data', 
											'$sketsa_lokasi'
									);
					";
					$this->db->query($sql_batas);
		        }else{
		        	$data['exec_query'] = 'failed';
		        }

		}

		$this->load->view('svl_lokasi_add',$data);

	}
}
