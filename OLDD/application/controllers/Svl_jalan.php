<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Svl_jalan extends CI_Controller {

	public function index(){
		$data['title'] = 'Survey - Jalan';

		$sql_where = "";
		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= "	SELECT *,
											(	SELECT count(*) 
												FROM `survey02_jalan` 
												WHERE 
													survey02_jalan.id_data = survey01_lokasi.id_data
											) AS c_data_r
									FROM `survey01_lokasi` 
									WHERE 
										id_data = '$lokasi' 
										$sql_and
									";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row(); 

			$sql 				= "SELECT * FROM `survey02_jalan` WHERE id_data = '$lokasi' $sql_and";
		}else{
			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}
		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_jalan',$data);
	}

	function db_master(){
		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}

		$sql 					= "SELECT * FROM `groups` $sql_where_g";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db; 

		$sql 					= "SELECT * FROM `users` $sql_where_u";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db; 

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey02_jalan` WHERE id_data = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		return $data;
	}

	function add(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Tambah Jalan';
		
		$this->load->view('svl_jalan_add',$data);
	}

	function view(){
		$data = $this->db_master();
		$data['title'] = 'Survey - View Jalan';

		$this->load->view('svl_jalan_add',$data);
	}

	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Edit Jalan';

		$this->load->view('svl_jalan_add',$data);
	}

	function delete(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Hapus Jalan';

		$this->load->view('svl_jalan_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Jalan';

		if($this->input->post('inp_id_data')){
			$id 	= $this->input->post('inp_id_data');
		}else{
			$id 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey02_jalan` WHERE id_data = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){


				$id_data 			= $id; 
				$group_id 			= $this->input->post('inp_group_id'); 
				$user_id 			= $this->input->post('inp_user_id'); 
				$ada_jalan 			= $this->input->post('inp_ada_jalan'); 
				$kode_tapak			= $this->input->post('inp_kode_tapak'); 
				$peruntukan 		= $this->input->post('inp_peruntukan'); 
				$peruntukan_lainnya = $this->input->post('inp_peruntukan_dll'); 
				$kdb 				= $this->input->post('inp_kdb'); 
				$klb 				= $this->input->post('inp_klb'); 
				$gsb 			 	= $this->input->post('inp_gsb'); 
				$tinggi_maks 		= $this->input->post('inp_tinggi_maks'); 
				$sesuai_per 		= $this->input->post('inp_sesuai_per'); 
				$sesuai_eks 		= $this->input->post('inp_sesuai_eks'); 
				$leb_jal 			= $this->input->post('inp_leb_jal'); 
				$kel_jal 			= $this->input->post('inp_kel_jal'); 
				$jum_laj 			= $this->input->post('inp_jum_laj'); 
				$kon_lal 		 	= $this->input->post('inp_kon_lal'); 
				$perm_jal 		 	= $this->input->post('inp_perm_jal'); 
				$kond 		 		= $this->input->post('inp_kond'); 
				$drain 		 		= $this->input->post('inp_drain'); 
				$pener 		 		= $this->input->post('inp_pener'); 
				$petun_1 	 		= $this->input->post('inp_petun_1'); 
				$petun_2 	 		= $this->input->post('inp_petun_2'); 
				$petun_3 	 		= $this->input->post('inp_petun_3'); 
				$petun_4 	 		= $this->input->post('inp_petun_4'); 
				$petun_5 			= $this->input->post('inp_petun_5'); 
				$petun_6 			= $this->input->post('inp_petun_6');
				$approval 			= $this->input->post('inp_approval');
				$respond 			= $this->input->post('inp_respond');


				$sql	= "	REPLACE INTO 
								survey02_jalan (
												`id_data`, 
												`group_id`, 
												`user_id`, 
												`ada_jalan`, 
												`kode_tapak`, 
												`peruntukan`, 
												`peruntukan_lainnya`, 
												`kdb`, 
												`klb`, 
												`gsb`, 
												`tinggi_maks`, 
												`sesuai_peraturan`, 
												`sesuai_eksisting`, 
												`lebar_jalan`, 
												`kelas_jalan`, 
												`jumlah_lajur`, 
												`kondisi_lalin`, 
												`permukaan_jalan`, 
												`kondisi`, 
												`drainase`, 
												`penerangan`, 
												`petunjuk1`, 
												`petunjuk2`, 
												`petunjuk3`, 
												`petunjuk4`,
												`petunjuk5`,
												`petunjuk6`,
												`approval`, 
												`respond`
								) VALUES (
												'$id_data', 
												'$group_id', 
												'$user_id', 
												'$ada_jalan', 
												'$kode_tapak', 
												'$peruntukan', 
												'$peruntukan_lainnya', 
												'$kdb', 
												'$klb', 
												'$gsb', 
												'$tinggi_maks', 
												'$sesuai_per', 
												'$sesuai_eks', 
												'$leb_jal', 
												'$kel_jal', 
												'$jum_laj', 
												'$kon_lal', 
												'$perm_jal', 
												'$kond', 
												'$drain', 
												'$pener', 
												'$petun_1', 
												'$petun_2', 
												'$petun_3', 
												'$petun_4', 
												'$petun_5', 
												'$petun_6', 
												'$approval', 
												'$respond'
									);
					";
		        if($this->db->query($sql)){
					if($this->input->post('inp_id_data')){
			        	$data['exec_query'] = 'update';
			        }else{
			        	$data['exec_query'] = 'ok';
			        }
		        }else{
		        	$data['exec_query'] = 'failed';
		        }

		}

		$this->load->view('svl_jalan_add',$data);

	}
}
