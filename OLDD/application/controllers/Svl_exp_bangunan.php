<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_exp_bangunan extends CI_Controller {

	public function index(){

		$data['title'] = 'Riwayat Imunisasi';


		$sql_where 	= "";
		$sql_and 	= "";
		if(!$this->session->userdata('administrator') == '1'){

			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')";
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')";
		}



		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$lokasi' $sql_and";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row();
			$sql 				= "	SELECT *,(
											SELECT sketsa_bangunan 
											from `survey14_ft_sketsa bangunan` 
											where `survey14_ft_sketsa bangunan`.id_bangunan = survey09_bangunan.id_bangunan 
											order by 'id_sketsa_bangunan' desc limit 1
										) as sketsa_lokasi
								 	FROM 
								 		`survey09_bangunan` 
								 	WHERE id_data = '$lokasi' $sql_and";
		}else{

			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}

		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db;


		$this->load->view('svl_exp_bangunan',$data);
	}

}
