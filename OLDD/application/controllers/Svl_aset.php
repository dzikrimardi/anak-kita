<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Svl_aset extends CI_Controller {

	public function index(){

		$sql_where 	= "";
		$sql_and 	= "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$lokasi 		= $this->input->get('lokasi');
		if($lokasi){
			$sql 				= "SELECT * FROM `survey01_lokasi` WHERE id_data = '$lokasi' $sql_and";
	        $data_db 			= $this->db->query($sql);
	        $data['data_db_r'] 	= $data_db->row(); 

			$sql 				= "SELECT * FROM `survey06_kelola` WHERE id_data = '$lokasi' $sql_and";
		}else{
			$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where";
		}
		//echo $sql ;exit;
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

		$this->load->view('svl_aset',$data);
	}

	function db_master(){
		$sql_where_g = "";
		$sql_where_u = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where_g 		= "WHERE id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
			$sql_where_u 		= "WHERE id = '$user_id'" ;
		}

		$sql 					= "SELECT * FROM `groups` $sql_where_g";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_groups'] 	= $data_db; 

		$sql 					= "SELECT * FROM `users` $sql_where_u";
        $data_db 				= $this->db->query($sql);
        $data['db_ms_users'] 	= $data_db; 

		$sql_and = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_and 		= "AND group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$id 				= $this->uri->segment(3); 
		$sql 				= "SELECT * FROM `survey06_kelola` WHERE id_kelola = '$id' $sql_and";
        $data_db 			= $this->db->query($sql)->row();
        $data['data_db'] 	= $data_db; 

		return $data;
	}

	function add(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Tambah Asset';
		$this->load->view('svl_aset_add',$data);

	}

	function view(){
		$data = $this->db_master();
		$data['title'] = 'Survey - View Asset';

		$this->load->view('svl_aset_add',$data);
	}

	function edit(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Edit Asset';

		$this->load->view('svl_aset_add',$data);
	}

	function delete(){
		$data = $this->db_master();
		$data['title'] = 'Survey - Hapus Asset';

		$this->load->view('svl_aset_add',$data);
	}

	function action(){

		$data['title'] = 'Survey - Asset';

		if($this->input->post('inp_id')){
			$id_child 	= $this->input->post('inp_id');
		}else{
			$id_child 	= date('y').date('m').date('d').date('H').date('i').date('s').'_000'.rand(0,9); 
		}
		$id 			= $id_child; 

		$data['exec_query'] = '';
		if($this->input->post('delete')){
			$sql = "DELETE FROM `survey06_kelola` WHERE id_kelola = '$id'";
	        if($this->db->query($sql)){
	        	$data['exec_query'] = 'delete';
	        }else{
	        	$data['exec_query'] = 'failed';

	        }
		}

		if($this->input->post() and !$this->input->post('delete')){

				$id_data 			= $this->input->post('inp_id_data');  
				$id_kelola 			= $id;  
				$user_id 			= $this->input->post('inp_user_id'); 
				$group_id 			= $this->input->post('inp_group_id'); 
				$stat_kel 	 		= $this->input->post('inp_stat_kel'); 
				$stat_kel_d 		= $this->input->post('inp_stat_kel_d'); 
				$peng 			  	= $this->input->post('inp_peng'); 
				$peng_bid 			= $this->input->post('inp_peng_bid'); 
				$peng_bag 			= $this->input->post('inp_peng_bag'); 
				$stat_m 			= $this->input->post('inp_stat_m'); 
				$stat_m_man_m		= $this->input->post('inp_stat_man_m'); 
				$stat_leg 			= $this->input->post('inp_stat_leg'); 
				$inp_cat_un_clear 	= $this->input->post('inp_cat_un_clear'); 
				$inp_cat_un_clean	= $this->input->post('inp_cat_un_clean'); 
				$inp_seng 	 		= $this->input->post('inp_seng'); 
				$inp_seng_d  		= $this->input->post('inp_seng_d'); 
				$inp_med 			= $this->input->post('inp_med'); 
				$inp_med_d 			= $this->input->post('inp_med_d'); 
				$inp_gug 	    	= $this->input->post('inp_gug'); 
				$inp_gug_d 	 	 	= $this->input->post('inp_gug_d'); 
				$inp_pem 		 	= $this->input->post('inp_pem'); 
				$inp_pem_d 	 	 	= $this->input->post('inp_pem_d'); 
				$approval 			= $this->input->post('inp_approval'); 
				$respond 			= $this->input->post('inp_respond');

				$sql	= "	REPLACE INTO 
								survey06_kelola (
												`id_kelola`, 
												`id_data`, 
												`user_id`, 
												`group_id`, 
												`status_kelola`, 
												`status_kelola_d`, 
												`pengguna`, 
												`pengguna_bidang`, 
												`pengguna_bagian`, 
												`status_manfaat`, 
												`status_manfaat_m`, 
												`status_legal`, 
												`catatan_unclear`, 
												`catatan_unclean`, 
												`sengketa`, 
												`sengketa_d`, 
												`mediasi`, 
												`mediasi_d`, 
												`gugatan`, 
												`gugatan_d`, 
												`pemblokiran`, 
												`pemblokiran_d`, 
												`approval`, 
												`respond`
								) VALUES (
												'$id_kelola', 
												'$id_data', 
												'$user_id', 
												'$group_id', 
												'$stat_kel', 
												'$stat_kel_d', 
												'$peng', 
												'$peng_bid', 
												'$peng_bag', 
												'$stat_m', 
												'$stat_m_man_m', 
												'$stat_leg', 
												'$inp_cat_un_clear', 
												'$inp_cat_un_clean', 
												'$inp_seng', 
												'$inp_seng_d', 
												'$inp_med', 
												'$inp_med_d', 
												'$inp_gug', 
												'$inp_gug_d', 
												'$inp_pem', 
												'$inp_pem_d', 
												'$approval', 
												'$respond'
									);
					";

			        if($this->db->query($sql)){
						if($this->input->post('inp_id')){
				        	$data['exec_query'] = 'update';
				        }else{
				        	$data['exec_query'] = 'ok';
				        }
			        }else{
			        	$data['exec_query'] = 'failed';
			        }

			}

		$this->load->view('svl_aset_add',$data);

	}
}
