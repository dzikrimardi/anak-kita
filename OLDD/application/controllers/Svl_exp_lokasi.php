<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Svl_exp_lokasi extends CI_Controller {

	public function index(){

		$data['title'] = 'Survey - Lokasi';

		$sql_where = "";
		if(!$this->session->userdata('administrator') == '1'){
			$user_id 		= $this->session->userdata('user_id');
			$sql_where 		= "WHERE group_id IN (SELECT users_groups.group_id FROM users_groups WHERE users_groups.user_id = '$user_id')"; 
		}

		$sql 				= "SELECT * FROM `survey01_lokasi` $sql_where ";
        $data_db 			= $this->db->query($sql);
        $data['data_db'] 	= $data_db; 

        //echo $sql;
		$this->load->view('svl_exp_lokasi',$data);
	}

}
